#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2014 
#au titre des revenus perçus en 2013. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************

 #
                                                                        #######
  ####   #    #    ##    #####      #     #####  #####   ######         #    #
 #    #  #    #   #  #   #    #     #       #    #    #  #                  #
 #       ######  #    #  #    #     #       #    #    #  #####             #
 #       #    #  ######  #####      #       #    #####   #                #
 #    #  #    #  #    #  #          #       #    #   #   #                #
  ####   #    #  #    #  #          #       #    #    #  ###### #######   #
 #
 #
 #
 #
 #
 #
 #
 #                           REVENU IMPOSABLE
 #
 #                           ET       CHARGES
 #
 #
 #
 #
regle 700:
application : bareme , iliad , batch  ;
pour z=1,2:
RB5z = max( 0, RB0z + TETONEQUO1) ;
RB55 = max( 0, RB05 + TETONEQUOM1) ;
regle 701:
application : bareme , iliad , batch  ;
RB01 = (RI1 * (1 - positif((VARIPTEFP * positif(ART1731BIS) + IPTEFP * (1 - ART1731BIS)) 
			   + (VARIPTEFN * positif(ART1731BIS) + IPTEFN * (1 - ART1731BIS)) +INDTEFF))) 
	+ ((VARIPTEFP * positif(ART1731BIS) + (IPTEFP- (PVIMPOS + COD3WI)*positif(IPTEFP+IPTEFN) * null(1-FLAG_EXIT)+ max(0,PVSURSI-IPTEFN)*positif(IPTEFN+IPTEFP) * null(2 -FLAG_EXIT)) * (1 - ART1731BIS)) + RFROBOR * V_INDTEO * present(IPTEFP))
	+TEFFREVTOT*(1-positif((VARIPTEFP * positif(ART1731BIS) + (IPTEFP- (PVIMPOS + COD3WI)*positif(IPTEFP+IPTEFN) * null(1-FLAG_EXIT)+ max(0,PVSURSI-IPTEFN)*positif(IPTEFN+IPTEFP) * null(2 -FLAG_EXIT))* (1 - ART1731BIS))))*INDTEFF;
RB02 = (RI2 * (1 - positif((VARIPTEFP * positif(ART1731BIS) + IPTEFP * (1 - ART1731BIS)) 
			+ (VARIPTEFN * positif(ART1731BIS) + IPTEFN * (1 - ART1731BIS))+ INDTEFF))) 
	+ positif((VARIPTEFP * positif(ART1731BIS) + IPTEFP * (1 - ART1731BIS)))*((VARIPTEFP * positif(ART1731BIS) 
                                                   + (IPTEFP- (PVIMPOS + COD3WI)*positif(IPTEFP+IPTEFN) * null(1-FLAG_EXIT)+ max(0,PVSURSI-IPTEFN)*positif(IPTEFN+IPTEFP) * null(2 -FLAG_EXIT)) * (1 - ART1731BIS)) +RFROBOR * V_INDTEO * present(IPTEFP)+ DPAE) 
	+ TEFFREVTOT*(1-positif((VARIPTEFP * positif(ART1731BIS) + (IPTEFP- (PVIMPOS + COD3WI)*positif(IPTEFP+IPTEFN) * null(1-FLAG_EXIT)+ max(0,PVSURSI-IPTEFN)*positif(IPTEFN+IPTEFP) * null(2 -FLAG_EXIT)) * (1 - ART1731BIS))))*INDTEFF;
RB04 = IND_TDR;
regle 70105:
application : iliad , bareme , batch ;
RB05 = (VARRMOND * positif(ART1731BIS) + RMOND * (1 - ART1731BIS));
RB06 = RE168+TAX1649;
regle 7011:
application : iliad , batch  ;
TONEQUO1 = somme(x=1..4:RPQx) + somme(x=V,C,1..4: RPQFx) + somme(x=V,C,1..4: RPQPALIMx) + somme(x=1,2 : RPQRFx)
	 + RPQRCMDC + RPQRCMFU  + RPQRCMCH  + RPQRCMTR  + RPQRCMTS + RPQRCMGO +somme(x=V,C,1..4 : RPQPRRx) + RPQRVO  + RPQRVO5  + RPQRVO6  + RPQRVO7
	 + somme(x=V,C,1..4 : RPQTSx) + somme(x=V,C,1..4 : RPQTSREMPx);
TONEQUOM1 = somme(x=1..4:RPQxM) + somme(x=V,C,1..4: RPQFxM) + somme(x=V,C,1..4: RPQPALIMxM) + somme(x=1,2 : RPQRFxM)
	 + RPQRCMDCM + RPQRCMFUM  + RPQRCMCHM  + RPQRCMTRM  + RPQRCMTSM + RPQRCMGOM +somme(x=V,C,1..4 : RPQPRRxM) + RPQRVOM  + RPQRVO5M  + RPQRVO6M  + RPQRVO7M
	 + somme(x=V,C,1..4 : RPQTSxM) + somme(x=V,C,1..4 : RPQTSREMPxM);
TONEQUO1T = somme(x=1..4:TRPQx) + somme(x=V,C,1..4: TRPQFx) + somme(x=V,C,1..4: TRPQPALIMx) + somme(x=1,2 : TRPQRFx)
	 + TRPQRCMDC + TRPQRCMFU  + TRPQRCMCH  + TRPQRCMTR  + TRPQRCMTS + TRPQRCMGO +somme(x=V,C,1..4 : TRPQPRRx) + TRPQRVO  + TRPQRVO5  + TRPQRVO6  + TRPQRVO7
	 + somme(x=V,C,1..4 : TRPQTSx) + somme(x=V,C,1..4 : TRPQTSREMPx);
TONEQUOM1T = somme(x=1..4:TRPQxM) + somme(x=V,C,1..4: TRPQFxM) + somme(x=V,C,1..4: TRPQPALIMxM) + somme(x=1,2 : TRPQRFxM)
	 + TRPQRCMDCM + TRPQRCMFUM  + TRPQRCMCHM  + TRPQRCMTRM  + TRPQRCMTSM + TRPQRCMGOM +somme(x=V,C,1..4 : TRPQPRRxM) + TRPQRVOM  + TRPQRVO5M  + TRPQRVO6M  + TRPQRVO7M
	 + somme(x=V,C,1..4 : TRPQTSxM) + somme(x=V,C,1..4 : TRPQTSREMPxM);
TETONEQUO1 = TONEQUO1 * (1-positif(INDTEFF)) + TONEQUO1T * positif(INDTEFF);
TETONEQUOM1 = TONEQUOM1 * (1-positif(INDTEFF)) + TONEQUOM1T * positif(INDTEFF);
regle 70111  :
application : iliad , batch  ;
REV1 = GLN1 * (1-INDTEFF) + TGLN1 * INDTEFF;
REV2 = GLN2 * (1-INDTEFF) + TGLN2 * INDTEFF;   
REV3 = GLN3 * (1-INDTEFF) + TGLN3 * INDTEFF;
REV4V = GLN4DAJV;
REV4C = GLN4DBJC;
REV4VC = GLN4DAJV + GLN4DBJC;
REV4 = 4BAQTOTNET+GLN4V + GLN4C;
REV4TVC = GLN4V + GLN4C;
REVF = somme(i=V,C,1..4: PENFi);
REVALIM = somme (i=V,C,1..4: PENALIMi) * (1-INDTEFF) + somme (i=V,C,1..4: TPENALIMi) * INDTEFF;
REVALIMQHT = somme (i=V,C,1..4: PENALIMi);
REVTS = somme (i=V,C,1..4:TSNN2TSi) * (1-INDTEFF) + somme (i=V,C,1..4:TTSNN2TSi) * INDTEFF;
REVTSQHT = somme (i=V,C,1..4:TSNN2TSi);
REVTSREMP = somme (i=V,C,1..4:TSNN2REMPi)*(1-INDTEFF)+ somme (i=V,C,1..4:TTSNN2REMPi)*INDTEFF;
REVTSREMPQHT = somme (i=V,C,1..4:TSNN2REMPi);
REVPRR = somme (i=V,C,1..4:PRR2i)*(1-INDTEFF)+somme (i=V,C,1..4:TPRR2i)*INDTEFF;
REVPRRQHT = somme (i=V,C,1..4:PRR2i);
REVRVO = T2RV;
REVRCM = 2RCM + 3RCM + 4RCM + 5RCM + 6RCM + 7RCM;
REVRF = 2REVF+3REVF;
REVQTOT = somme(i=1..4 : REVi) + REVALIM + REVF+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF;
REVQTOTQHT = somme(i=1..3 : GLNi) + REV4 + REVALIMQHT + REVF+REVTSQHT+REVTSREMPQHT+REVPRRQHT+REVRVO+REVRCM+REVRF;
regle 70112  :
application : iliad , batch  ;
GL1 = positif(REV2+REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * GLN1 / TOTALQUOHT) +
     (1 - positif(REV2+REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * TONEQUOHT;
GL2 = positif(REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * GLN2 / TOTALQUOHT) +
     (1 - positif(REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GL1);
GL3 = positif(REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * GLN3 / TOTALQUOHT) +
     (1 - positif(REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT - GL1 - GL2);
GL4V = arr(TONEQUOHT * REV4V / TOTALQUOHT);
GL4C = arr(TONEQUOHT * REV4C / TOTALQUOHT);
GL4VC = arr(TONEQUOHT * REV4VC / TOTALQUOHT);
GL4TVC = arr(TONEQUOHT * REV4TVC / TOTALQUOHT);
GL4 = positif(REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * REV4 / TOTALQUOHT) +
     (1 - positif(REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT - GL1 - GL2 - GL3);
GLTOT = somme(i=1..4:GLi);
GLFV = positif(PENFC+PENF1+PENF2+PENF3+PENF4+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PENFV / TOTALQUOHT) +
      (1 - positif(PENFC+PENF1+PENF2+PENF3+PENF4+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT);
GLFC = positif(PENF1+PENF2+PENF3+PENF4+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PENFC / TOTALQUOHT) +
      (1 - positif(PENF1+PENF2+PENF3+PENF4+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFV);
GLF1 = positif(PENF2+PENF3+PENF4+REV1+REV2+REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PENF1 / TOTALQUOHT) +
      (1 - positif(PENF2+PENF3+PENF4+REV1+REV2+REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFV-GLFC);
GLF2 = positif(PENF2+PENF3+PENF4+REV1+REV2+REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PENF2 / TOTALQUOHT) +
      (1 - positif(PENF2+PENF3+PENF4+REV1+REV2+REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFV-GLFC-GLF1);
GLF3 = positif(PENF4+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PENF3 / TOTALQUOHT) +
      (1 - positif(PENF4+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFV-GLFC-GLF1-GLF2);
GLF4 = positif(REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PENF4 / TOTALQUOHT) +
      (1 - positif(REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFV-GLFC-GLF1-GLF2-GLF3);
GLFTOT = somme(i=V,C,1..4:GLFi);
GLPALIMV = positif(PENALIMC+PENALIM1+PENALIM2+PENALIM3+PENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PENALIMV / TOTALQUOHT) +
      (1 - positif(PENALIMC+PENALIM1+PENALIM2+PENALIM3+PENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT);
GLPALIMC = positif(PENALIM1+PENALIM2+PENALIM3+PENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PENALIMC / TOTALQUOHT) +
      (1 - positif(PENALIM1+PENALIM2+PENALIM3+PENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMV);
GLPALIM1 = positif(PENALIM2+PENALIM3+PENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PENALIM1 / TOTALQUOHT) +
      (1 - positif(PENALIM2+PENALIM3+PENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMV-GLPALIMC);
GLPALIM2 = positif(PENALIM3+PENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PENALIM2 / TOTALQUOHT) +
      (1 - positif(PENALIM3+PENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMV-GLPALIMC-GLPALIM1);
GLPALIM3 = positif(PENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PENALIM3 / TOTALQUOHT) +
      (1 - positif(PENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMV-GLPALIMC-GLPALIM1-GLPALIM2);
GLPALIM4 = positif(REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PENALIM4 / TOTALQUOHT) +
      (1 - positif(REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMV-GLPALIMC-GLPALIM1-GLPALIM2-GLPALIM3);
GLPALIMTOT = somme(i=V,C,1..4:GLPALIMi);
GLTSV = positif(TSNN2TSC+TSNN2TS1+TSNN2TS2+TSNN2TS3+TSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * TSNN2TSV / TOTALQUOHT) +
      (1 - positif(TSNN2TSC+TSNN2TS1+TSNN2TS2+TSNN2TS3+TSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT);
GLTSC = positif(TSNN2TS1+TSNN2TS2+TSNN2TS3+TSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * TSNN2TSC / TOTALQUOHT) +
      (1 - positif(TSNN2TS1+TSNN2TS2+TSNN2TS3+TSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSV);
GLTS1 = positif(TSNN2TS2+TSNN2TS3+TSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * TSNN2TS1 / TOTALQUOHT) +
      (1 - positif(TSNN2TS2+TSNN2TS3+TSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSV-GLTSC);
GLTS2 = positif(TSNN2TS3+TSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * TSNN2TS2 / TOTALQUOHT) +
      (1 - positif(TSNN2TS3+TSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSV-GLTSC-GLTS1);
GLTS3 = positif(TSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * TSNN2TS3 / TOTALQUOHT) +
      (1 - positif(TSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSV-GLTSC-GLTS1-GLTS2);
GLTS4 = positif(REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * TSNN2TS4 / TOTALQUOHT) +
      (1 - positif(REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSV-GLTSC-GLTS1-GLTS2-GLTS3);
GLTSTOT = somme(i=V,C,1..4:GLTSi);
GLTSREMPV = positif(TSNN2REMPC+TSNN2REMP1+TSNN2REMP2+TSNN2REMP3+TSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * TSNN2REMPV / TOTALQUOHT) +
      (1 - positif(TSNN2REMPC+TSNN2REMP1+TSNN2REMP2+TSNN2REMP3+TSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLTSTOT-GLPALIMTOT);
GLTSREMPC = positif(TSNN2REMP1+TSNN2REMP2+TSNN2REMP3+TSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * TSNN2REMPC / TOTALQUOHT) +
      (1 - positif(TSNN2REMP1+TSNN2REMP2+TSNN2REMP3+TSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPV);
GLTSREMP1 = positif(TSNN2REMP2+TSNN2REMP3+TSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * TSNN2REMP1 / TOTALQUOHT) +
      (1 - positif(TSNN2REMP2+TSNN2REMP3+TSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPV-GLTSREMPC);
GLTSREMP2 = positif(TSNN2REMP3+TSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * TSNN2REMP2 / TOTALQUOHT) +
      (1 - positif(TSNN2REMP3+TSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPV-GLTSREMPC-GLTSREMP1);
GLTSREMP3 = positif(TSNN2REMP4+TSNN2REMPC+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * TSNN2REMP3 / TOTALQUOHT) +
      (1 - positif(TSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPV-GLTSREMPC-GLTSREMP1-GLTSREMP2);
GLTSREMP4 = positif(REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * TSNN2REMP4 / TOTALQUOHT) +
      (1 - positif(REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPV-GLTSREMPC-GLTSREMP1-GLTSREMP2-GLTSREMP3);
GLTSREMPTOT = somme(i=V,C,1..4:GLTSREMPi);
GLPRRV = positif(PRR2C+PRR21+PRR22+PRR23+PRR24+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PRR2V / TOTALQUOHT) +
      (1 - positif(PRR2C+PRR21+PRR22+PRR23+PRR24+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT);
GLPRRC = positif(PRR21+PRR22+PRR23+PRR24+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PRR2C / TOTALQUOHT) +
      (1 - positif(PRR21+PRR22+PRR23+PRR24+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRV);
GLPRR1 = positif(PRR22+PRR23+PRR24+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PRR21 / TOTALQUOHT) +
      (1 - positif(PRR22+PRR23+PRR24+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRV-GLPRRC);
GLPRR2 = positif(PRR23+PRR24+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PRR22 / TOTALQUOHT) +
      (1 - positif(PRR23+PRR24+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRV-GLPRRC-GLPRR1);
GLPRR3 = positif(PRR24+REVRVO+REVRCM+REVRF)*arr(TONEQUOHT * PRR23 / TOTALQUOHT) +
      (1 - positif(PRR24+REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRV-GLPRRC-GLPRR1-GLPRR2);
GLPRR4 = positif(REVRVO+REVRCM+REVRF)*arr(TONEQUO * PRR24 / TOTALQUO) +
      (1 - positif(REVRVO+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRV-GLPRRC-GLPRR1-GLPRR2-GLPRR3);
GLPRRTOT = somme(i=V,C,1..4:GLPRRi);
GLRVO = positif(2RV2 + 2RV3 + 2RV4 +REVRCM+REVRF)*arr(TONEQUOHT * 2RV1 / TOTALQUOHT) +
      (1 - positif(2RV2 + 2RV3 + 2RV4+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRTOT);
GLRVO5 = positif(2RV3 + 2RV4+REVRCM+REVRF)*arr(TONEQUOHT * 2RV2 / TOTALQUOHT) +
      (1 - positif(2RV3 + 2RV4+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRTOT-GLRVO);
GLRVO6 = positif(2RV4+REVRCM+REVRF)*arr(TONEQUOHT * 2RV3 / TOTALQUOHT) +
      (1 - positif(2RV4+REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLPRRTOT-GLTSREMPTOT-GLRVO-GLRVO5);
GLRVO7 = positif(REVRCM+REVRF)*arr(TONEQUOHT * 2RV4 / TOTALQUOHT) +
      (1 - positif(REVRCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRTOT-GLRVO-GLRVO5-GLRVO6);
GLRVOTOT = GLRVO + GLRVO5 + GLRVO6 + GLRVO7;
GLRCMDC  = positif(3RCM+4RCM+5RCM+6RCM+7RCM+REVRF)*arr(TONEQUOHT * 2RCM / TOTALQUOHT) +
      (1 - positif(3RCM+4RCM+5RCM+6RCM+7RCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRTOT-GLRVOTOT);
GLRCMFU  = positif(4RCM+5RCM+6RCM+7RCM+REVRF)*arr(TONEQUOHT * 3RCM / TOTALQUOHT) +
      (1 - positif(4RCM+5RCM+6RCM+7RCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRTOT-GLRVOTOT - GLRCMDC);
GLRCMCH  = positif(5RCM+6RCM+7RCM+REVRF)*arr(TONEQUOHT * 4RCM / TOTALQUOHT) +
      (1 - positif(5RCM+6RCM+7RCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRTOT-GLRVOTOT - GLRCMDC - GLRCMFU);
GLRCMTS  = positif(6RCM+7RCM+REVRF)*arr(TONEQUOHT * 5RCM / TOTALQUOHT) +
      (1 - positif(6RCM+7RCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRTOT-GLRVOTOT - GLRCMDC - GLRCMFU - GLRCMCH);
GLRCMGO = positif(7RCM+REVRF)*arr(TONEQUOHT * 6RCM / TOTALQUOHT) +
      (1 - positif(7RCM+REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRTOT-GLRVOTOT - GLRCMDC - GLRCMFU - GLRCMCH - GLRCMTS);
GLRCMTR  = positif(REVRF)*arr(TONEQUOHT * 7RCM / TOTALQUOHT) +
      (1 - positif(REVRF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRTOT-GLRVOTOT - GLRCMDC - GLRCMFU - GLRCMCH - GLRCMGO - GLRCMTS);
GLRCMTOT = GLRCMDC + GLRCMFU + GLRCMCH + GLRCMGO + GLRCMTR + GLRCMTS;
GLRF1  = positif(3REVF)*arr(TONEQUOHT * 2REVF / TOTALQUOHT) +
      (1 - positif(3REVF)) * (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRTOT-GLRVOTOT-GLRCMTOT);
GLRF2  = (TONEQUOHT-GLTOT-GLFTOT-GLPALIMTOT-GLTSTOT-GLTSREMPTOT-GLPRRTOT-GLRVOTOT-GLRCMTOT-GLRF1);
GLRFTOT = GLRF1 + GLRF2;
regle 701121  :
application : iliad , batch  ;
TGL1 = positif(REV2+REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TGLN1 / TOTALQUO) +
     (1 - positif(REV2+REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * TONEQUO;
TGL2 = positif(REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TGLN2 / TOTALQUO) +
     (1 - positif(REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGL1);
TGL3 = positif(REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TGLN3 / TOTALQUO) +
     (1 - positif(REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO - TGL1 - TGL2);
TGL4V = arr(TONEQUO * REV4V / TOTALQUO);
TGL4C = arr(TONEQUO * REV4C / TOTALQUO);
TGL4VC = arr(TONEQUO * REV4VC / TOTALQUO);
TGL4TVC = arr(TONEQUO * REV4TVC / TOTALQUO);
TGL4 = positif(REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * REV4 / TOTALQUO) +
     (1 - positif(REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO - TGL1 - TGL2 - TGL3);
TGLTOT = somme(i=1..4:TGLi);
TGLFV = positif(PENFC+PENF1+PENF2+PENF3+PENF4+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * PENFV / TOTALQUO) +
      (1 - positif(PENFC+PENF1+PENF2+PENF3+PENF4+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT);
TGLFC = positif(PENF1+PENF2+PENF3+PENF4+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * PENFC / TOTALQUO) +
      (1 - positif(PENF1+PENF2+PENF3+PENF4+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFV);
TGLF1 = positif(PENF2+PENF3+PENF4+REV1+REV2+REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * PENF1 / TOTALQUO) +
      (1 - positif(PENF2+PENF3+PENF4+REV1+REV2+REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFV-TGLFC);
TGLF2 = positif(PENF2+PENF3+PENF4+REV1+REV2+REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * PENF2 / TOTALQUO) +
      (1 - positif(PENF2+PENF3+PENF4+REV1+REV2+REV3+REV4+REVF+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFV-TGLFC-TGLF1);
TGLF3 = positif(PENF4+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * PENF3 / TOTALQUO) +
      (1 - positif(PENF4+REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFV-TGLFC-TGLF1-TGLF2);
TGLF4 = positif(REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * PENF4 / TOTALQUO) +
      (1 - positif(REVALIM+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFV-TGLFC-TGLF1-TGLF2-TGLF3);
TGLFTOT = somme(i=V,C,1..4:TGLFi);
TGLPALIMV = positif(TPENALIMC+TPENALIM1+TPENALIM2+TPENALIM3+TPENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TPENALIMV / TOTALQUO) +
      (1 - positif(TPENALIMC+TPENALIM1+TPENALIM2+TPENALIM3+TPENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT);
TGLPALIMC = positif(TPENALIM1+TPENALIM2+TPENALIM3+TPENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TPENALIMC / TOTALQUO) +
      (1 - positif(TPENALIM1+TPENALIM2+TPENALIM3+TPENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMV);
TGLPALIM1 = positif(TPENALIM2+TPENALIM3+TPENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TPENALIM1 / TOTALQUO) +
      (1 - positif(TPENALIM2+TPENALIM3+TPENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMV-TGLPALIMC);
TGLPALIM2 = positif(TPENALIM3+TPENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TPENALIM2 / TOTALQUO) +
      (1 - positif(TPENALIM3+TPENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMV-TGLPALIMC-TGLPALIM1);
TGLPALIM3 = positif(TPENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TPENALIM3 / TOTALQUO) +
      (1 - positif(TPENALIM4+REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMV-TGLPALIMC-TGLPALIM1-TGLPALIM2);
TGLPALIM4 = positif(REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TPENALIM4 / TOTALQUO) +
      (1 - positif(REVTS+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMV-TGLPALIMC-TGLPALIM1-TGLPALIM2-TGLPALIM3);
TGLPALIMTOT = somme(i=V,C,1..4:TGLPALIMi);
TGLTSV = positif(TTSNN2TSC+TTSNN2TS1+TTSNN2TS2+TTSNN2TS3+TTSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TTSNN2TSV / TOTALQUO) +
      (1 - positif(TTSNN2TSC+TTSNN2TS1+TTSNN2TS2+TTSNN2TS3+TTSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT);
TGLTSC = positif(TTSNN2TS1+TTSNN2TS2+TTSNN2TS3+TTSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TTSNN2TSC / TOTALQUO) +
      (1 - positif(TTSNN2TS1+TTSNN2TS2+TTSNN2TS3+TTSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSV);
TGLTS1 = positif(TTSNN2TS2+TTSNN2TS3+TTSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TTSNN2TS1 / TOTALQUO) +
      (1 - positif(TTSNN2TS2+TTSNN2TS3+TTSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-GLTOT-GLFTOT-GLPALIMTOT-TGLTSV-TGLTSC);
TGLTS2 = positif(TTSNN2TS3+TTSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TTSNN2TS2 / TOTALQUO) +
      (1 - positif(TTSNN2TS3+TTSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSV-TGLTSC-TGLTS1);
TGLTS3 = positif(TTSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TTSNN2TS3 / TOTALQUO) +
      (1 - positif(TTSNN2TS4+REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSV-TGLTSC-TGLTS1-TGLTS2);
TGLTS4 = positif(REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TTSNN2TS4 / TOTALQUO) +
      (1 - positif(REVTSREMP+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSV-TGLTSC-TGLTS1-TGLTS2-TGLTS3);
TGLTSTOT = somme(i=V,C,1..4:TGLTSi);
TGLTSREMPV = positif(TTSNN2REMPC+TTSNN2REMP1+TTSNN2REMP2+TTSNN2REMP3+TTSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TTSNN2REMPV / TOTALQUO) +
      (1 - positif(TTSNN2REMPC+TTSNN2REMP1+TTSNN2REMP2+TTSNN2REMP3+TTSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLTSTOT-GLPALIMTOT);
TGLTSREMPC = positif(TTSNN2REMP1+TTSNN2REMP2+TTSNN2REMP3+TTSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TTSNN2REMPC / TOTALQUO) +
      (1 - positif(TTSNN2REMP1+TTSNN2REMP2+TTSNN2REMP3+TTSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPV);
TGLTSREMP1 = positif(TTSNN2REMP2+TTSNN2REMP3+TTSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TTSNN2REMP1 / TOTALQUO) +
      (1 - positif(TTSNN2REMP2+TTSNN2REMP3+TTSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPV-TGLTSREMPC);
TGLTSREMP2 = positif(TTSNN2REMP3+TTSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TTSNN2REMP2 / TOTALQUO) +
      (1 - positif(TTSNN2REMP3+TTSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPV-TGLTSREMPC-TGLTSREMP1);
TGLTSREMP3 = positif(TTSNN2REMP4+TTSNN2REMPC+REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TTSNN2REMP3 / TOTALQUO) +
      (1 - positif(TTSNN2REMP4+REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPV-TGLTSREMPC-TGLTSREMP1-TGLTSREMP2);
TGLTSREMP4 = positif(REVPRR+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TTSNN2REMP4 / TOTALQUO) +
      (1 - positif(REVPRR+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPV-TGLTSREMPC-TGLTSREMP1-TGLTSREMP2-TGLTSREMP3);
TGLTSREMPTOT = somme(i=V,C,1..4:TGLTSREMPi);
TGLPRRV = positif(TPRR2C+TPRR21+TPRR22+TPRR23+TPRR24+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TPRR2V / TOTALQUO) +
      (1 - positif(TPRR2C+TPRR21+TPRR22+TPRR23+TPRR24+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT);
TGLPRRC = positif(TPRR21+TPRR22+TPRR23+TPRR24+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TPRR2C / TOTALQUO) +
      (1 - positif(TPRR21+TPRR22+TPRR23+TPRR24+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT-TGLPRRV);
TGLPRR1 = positif(TPRR22+TPRR23+TPRR24+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TPRR21 / TOTALQUO) +
      (1 - positif(TPRR22+TPRR23+TPRR24+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT-TGLPRRV-TGLPRRC);
TGLPRR2 = positif(TPRR23+TPRR24+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TPRR22 / TOTALQUO) +
      (1 - positif(TPRR23+TPRR24+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT-TGLPRRV-TGLPRRC-TGLPRR1);
TGLPRR3 = positif(TPRR24+REVRVO+REVRCM+REVRF)*arr(TONEQUO * TPRR23 / TOTALQUO) +
      (1 - positif(TPRR24+REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT-TGLPRRV-TGLPRRC-TGLPRR1-TGLPRR2);
TGLPRR4 = positif(REVRVO+REVRCM+REVRF)*arr(TONEQUO * TPRR24 / TOTALQUO) +
      (1 - positif(REVRVO+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-GLTSREMPTOT-GLPRRV-GLPRRC-GLPRR1-GLPRR2-GLPRR3);
TGLPRRTOT = somme(i=V,C,1..4:TGLPRRi);
TGLRVO = positif(2RV2 + 2RV3 + 2RV4 +REVRCM+REVRF)*arr(TONEQUO * 2RV1 / TOTALQUO) +
      (1 - positif(2RV2 + 2RV3 + 2RV4+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT-TGLPRRTOT);
TGLRVO5 = positif(2RV3 + 2RV4+REVRCM+REVRF)*arr(TONEQUO * 2RV2 / TOTALQUO) +
      (1 - positif(2RV3 + 2RV4+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT-TGLPRRTOT-TGLRVO);
TGLRVO6 = positif(2RV4+REVRCM+REVRF)*arr(TONEQUO * 2RV3 / TOTALQUO) +
      (1 - positif(2RV4+REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLPRRTOT-TGLTSREMPTOT-TGLRVO-TGLRVO5);
TGLRVO7 = positif(REVRCM+REVRF)*arr(TONEQUO * 2RV4 / TOTALQUO) +
      (1 - positif(REVRCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT-TGLPRRTOT-TGLRVO-TGLRVO5-TGLRVO6);
TGLRVOTOT = TGLRVO + TGLRVO5 + TGLRVO6 + TGLRVO7;
TGLRCMDC  = positif(3RCM+4RCM+5RCM+6RCM+7RCM+REVRF)*arr(TONEQUO * 2RCM / TOTALQUO) +
      (1 - positif(3RCM+4RCM+5RCM+6RCM+7RCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT-TGLPRRTOT-TGLRVOTOT);
TGLRCMFU  = positif(4RCM+5RCM+6RCM+7RCM+REVRF)*arr(TONEQUO * 3RCM / TOTALQUO) +
      (1 - positif(4RCM+5RCM+6RCM+7RCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT-TGLPRRTOT-TGLRVOTOT - TGLRCMDC);
TGLRCMCH  = positif(5RCM+6RCM+7RCM+REVRF)*arr(TONEQUO * 4RCM / TOTALQUO) +
      (1 - positif(5RCM+6RCM+7RCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT-TGLPRRTOT-TGLRVOTOT - TGLRCMDC - TGLRCMFU);
TGLRCMTS  = positif(6RCM+7RCM+REVRF)*arr(TONEQUO * 5RCM / TOTALQUO) +
      (1 - positif(6RCM+7RCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT-TGLPRRTOT-TGLRVOTOT - TGLRCMDC - TGLRCMFU - TGLRCMCH);
TGLRCMGO = positif(7RCM+REVRF)*arr(TONEQUO * 6RCM / TOTALQUO) +
      (1 - positif(7RCM+REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT-TGLPRRTOT-TGLRVOTOT - TGLRCMDC - TGLRCMFU - TGLRCMCH - TGLRCMTS);
TGLRCMTR  = positif(REVRF)*arr(TONEQUO * 7RCM / TOTALQUO) +
      (1 - positif(REVRF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT-TGLPRRTOT-TGLRVOTOT - TGLRCMDC - TGLRCMFU - TGLRCMCH - TGLRCMGO - TGLRCMTS);
TGLRCMTOT = TGLRCMDC + TGLRCMFU + TGLRCMCH + TGLRCMGO + TGLRCMTR + TGLRCMTS;
TGLRF1  = positif(3REVF)*arr(TONEQUO * 2REVF / TOTALQUO) +
      (1 - positif(3REVF)) * (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT-TGLPRRTOT-TGLRVOTOT-TGLRCMTOT);
TGLRF2  = (TONEQUO-TGLTOT-TGLFTOT-TGLPALIMTOT-TGLTSTOT-TGLTSREMPTOT-TGLPRRTOT-TGLRVOTOT-TGLRCMTOT-TGLRF1);
TGLRFTOT = TGLRF1 + TGLRF2;
TEGL1 = GL1 * (1-positif(INDTEFF)) + TGL1 * positif(INDTEFF);
TEGL2 = GL2 * (1-positif(INDTEFF)) + TGL2 * positif(INDTEFF);
TEGL3 = GL3 * (1-positif(INDTEFF)) + TGL3 * positif(INDTEFF);
TEGL4V = GL4V * (1-positif(INDTEFF)) + TGL4V * positif(INDTEFF);
TEGL4C = GL4C * (1-positif(INDTEFF)) + TGL4C * positif(INDTEFF);
TEGL4VC = GL4VC * (1-positif(INDTEFF)) + TGL4VC * positif(INDTEFF);
TEGL4TVC = GL4TVC * (1-positif(INDTEFF)) + TGL4TVC * positif(INDTEFF);
TEGL4 = GL4 * (1-positif(INDTEFF)) + TGL4 * positif(INDTEFF);
TEGLFV = GLFV * (1-positif(INDTEFF)) + TGLFV * positif(INDTEFF);
TEGLFC = GLFC * (1-positif(INDTEFF)) + TGLFC * positif(INDTEFF);
TEGLF1 = GLF1 * (1-positif(INDTEFF)) + TGLF1 * positif(INDTEFF);
TEGLF2 = GLF2 * (1-positif(INDTEFF)) + TGLF2 * positif(INDTEFF);
TEGLF3 = GLF3 * (1-positif(INDTEFF)) + TGLF3 * positif(INDTEFF);
TEGLF4 = GLF4 * (1-positif(INDTEFF)) + TGLF4 * positif(INDTEFF);
TEGLPALIMV = GLPALIMV * (1-positif(INDTEFF)) + TGLPALIMV * positif(INDTEFF);
TEGLPALIMC = GLPALIMC * (1-positif(INDTEFF)) + TGLPALIMC * positif(INDTEFF);
TEGLPALIM1 = GLPALIM1 * (1-positif(INDTEFF)) + TGLPALIM1 * positif(INDTEFF);
TEGLPALIM2 = GLPALIM2 * (1-positif(INDTEFF)) + TGLPALIM2 * positif(INDTEFF);
TEGLPALIM3 = GLPALIM3 * (1-positif(INDTEFF)) + TGLPALIM3 * positif(INDTEFF);
TEGLPALIM4 = GLPALIM4 * (1-positif(INDTEFF)) + TGLPALIM4 * positif(INDTEFF);
TEGLTSV = GLTSV * (1-positif(INDTEFF)) + TGLTSV * positif(INDTEFF);
TEGLTSC = GLTSC * (1-positif(INDTEFF)) + TGLTSC * positif(INDTEFF);
TEGLTS1 = GLTS1 * (1-positif(INDTEFF)) + TGLTS1 * positif(INDTEFF);
TEGLTS2 = GLTS2 * (1-positif(INDTEFF)) + TGLTS2 * positif(INDTEFF);
TEGLTS3 = GLTS3 * (1-positif(INDTEFF)) + TGLTS3 * positif(INDTEFF);
TEGLTS4 = GLTS4 * (1-positif(INDTEFF)) + TGLTS4 * positif(INDTEFF);
TEGLTSREMPV = GLTSREMPV * (1-positif(INDTEFF)) + TGLTSREMPV * positif(INDTEFF);
TEGLTSREMPC = GLTSREMPC * (1-positif(INDTEFF)) + TGLTSREMPC * positif(INDTEFF);
TEGLTSREMP1 = GLTSREMP1 * (1-positif(INDTEFF)) + TGLTSREMP1 * positif(INDTEFF);
TEGLTSREMP2 = GLTSREMP2 * (1-positif(INDTEFF)) + TGLTSREMP2 * positif(INDTEFF);
TEGLTSREMP3 = GLTSREMP3 * (1-positif(INDTEFF)) + TGLTSREMP3 * positif(INDTEFF);
TEGLTSREMP4 = GLTSREMP4 * (1-positif(INDTEFF)) + TGLTSREMP4 * positif(INDTEFF);
TEGLPRRV = GLPRRV * (1-positif(INDTEFF)) + TGLPRRV * positif(INDTEFF);
TEGLPRRC = GLPRRC * (1-positif(INDTEFF)) + TGLPRRC * positif(INDTEFF);
TEGLPRR1 = GLPRR1 * (1-positif(INDTEFF)) + TGLPRR1 * positif(INDTEFF);
TEGLPRR2 = GLPRR2 * (1-positif(INDTEFF)) + TGLPRR2 * positif(INDTEFF);
TEGLPRR3 = GLPRR3 * (1-positif(INDTEFF)) + TGLPRR3 * positif(INDTEFF);
TEGLPRR4 = GLPRR4 * (1-positif(INDTEFF)) + TGLPRR4 * positif(INDTEFF);
TEGLRVO = GLRVO * (1-positif(INDTEFF)) + TGLRVO * positif(INDTEFF);
TEGLRVO5 = GLRVO5 * (1-positif(INDTEFF)) + TGLRVO5 * positif(INDTEFF);
TEGLRVO6 = GLRVO6 * (1-positif(INDTEFF)) + TGLRVO6 * positif(INDTEFF);
TEGLRVO7 = GLRVO7 * (1-positif(INDTEFF)) + TGLRVO7 * positif(INDTEFF);
TEGLRCMDC = GLRCMDC * (1-positif(INDTEFF)) + TGLRCMDC * positif(INDTEFF);
TEGLRCMFU = GLRCMFU * (1-positif(INDTEFF)) + TGLRCMFU * positif(INDTEFF);
TEGLRCMCH = GLRCMCH * (1-positif(INDTEFF)) + TGLRCMCH * positif(INDTEFF);
TEGLRCMGO = GLRCMGO * (1-positif(INDTEFF)) + TGLRCMGO * positif(INDTEFF);
TEGLRCMTR = GLRCMTR * (1-positif(INDTEFF)) + TGLRCMTR * positif(INDTEFF);
TEGLRCMTS = GLRCMTS * (1-positif(INDTEFF)) + TGLRCMTS * positif(INDTEFF);
TEGLRF1 = 