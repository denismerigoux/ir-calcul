#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2018]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des 
#Finances Publiques pour permettre le calcul de l'imp�t sur le revenu 2018 
#au titre des revenus per�us en 2017. La pr�sente version a permis la 
#g�n�ration du moteur de calcul des cha�nes de taxation des r�les d'imp�t 
#sur le revenu de ce mill�sime.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
regle 831000:
application : iliad ;


RRFI = positif(RFON + DRCF + RFMIC - MICFR -max(0,RFDANT - DEFRFNONI)) * (RFON + DRCF + max(0,RFMIC - MICFR -max(0,RFDANT  - DEFRFNONI)))
     + (1-positif(RFON + DRCF + RFMIC - MICFR -max(0,RFDANT - DEFRFNONI))) * (positif(RFDANT-DEFRFNONI) * DEFRFNONI+DRCF+RFON) ;
RRFIPS = RRFI ; 

regle 831010:
application : iliad ;


MICFR=arr(RFMIC*TX_MICFON/100);

regle 831020:
application : iliad ;


RMF=RFMIC - MICFR;


RMFN = max(0,RMF - RFDANT * (1-positif(DEFRFNONI))
                   - min(RFDANT, max(0,RFDANT-DEFRFNONI)) * positif(DEFRFNONI));

PASRFASS=positif(RMF-RFDANT)*(RMFN-(COD4BK - arr(COD4BK * TX_MICFON/100) - arr(COD4BK * RFDANT/RFMIC))*positif(RMFN));

regle 831030:
application : iliad ;


RFCD = RFORDI + FONCI + REAMOR ;

regle 831040:
application : iliad ;


RFCE = max(0,RFCD- RFDORD* (1-positif(DEFRFNONI))
                   - min(RFDORD, max(0,RFDORD+RFDHIS+RFDANT-DEFRFNONI)) * positif(DEFRFNONI));
RFCEAPS = max(0,RFORDI- RFDORD* (1-positif(DEFRFNONI))
                   - min(RFDORD, max(0,RFDORD+RFDHIS+RFDANT-DEFRFNONI)) * positif(DEFRFNONI));
RFCEPS = max(0,RFCD-RFDORD* (1-positif(DEFRFNONI))
                   - min(RFDORD, max(0,RFDORD+RFDHIS+RFDANT-DEFRFNONI)) * positif(DEFRFNONI));

DFCE = min(0,RFCD- RFDORD* (1-positif(DEFRFNONI))
                   - min(RFDORD, max(0,RFDORD+RFDHIS+RFDANT-DEFRFNONI)) * positif(DEFRFNONI));

RFCF = max(0,RFCE-RFDHIS);
RFCFPS = (RFCEPS-RFDHIS);
RFCFAPS = max(0,RFCEAPS-RFDHIS);

DRCF  = min(0,RFCE-RFDHIS);

RFCG = max(0,RFCF- RFDANT* (1-positif(DEFRFNONI))
                   - min(RFDANT, max(0,RFDANT-DEFRFNONI)) * positif(DEFRFNONI));
DFCG = min(0,RFCF- RFDANT* (1-positif(DEFRFNONI))
                   - min(RFDANT, max(0,RFDANT-DEFRFNONI)) * positif(DEFRFNONI));

regle 831050:
application : iliad ;

RFON = arr(RFCG*RFORDI/RFCD);

2REVF = min( arr ((RFCG)*(FONCI)/RFCD) , RFCG-RFON) ;
3REVF = min( arr (RFCG*(REAMOR)/RFCD) , RFCG-RFON-2REVF) ;
RFQ = FONCI + REAMOR ;
regle 831055:
application : iliad ;

RFDANT4BA = max(0,RFORDI - RFON);
RFDANTRBA = max(0,FONCI - 2REVF);
RFDANTSBA = max(0,REAMOR - 3REVF);

PASRF = present(RFORDI) * ((RFON  - (COD4BL - arr(COD4BL * RFDANT4BA/RFORDI))*(1-positif(-DFCG)) ) * positif_ou_nul(RFON - (COD4BL - arr(COD4BL * RFDANT4BA/RFORDI))))
    +  (1-present(RFORDI))  * present(COD4BL) * 0;

regle 831060:
application : iliad ;

 
DEF4BB = min(RFDORD,RFORDI + RFMIC * 0.70 + FONCI + REAMOR) ;
DEFRF4BB = min(RFDORD,max(DEF4BB1731,max(DEF4BB_P,DEF4BBP2))) * positif(SOMMERF_2) * (1-PREM8_11) ;

regle 831070:
application : iliad ;
 
 
DEF4BD = min(RFDANT,RFORDI + RFMIC * 0.70 + FONCI + REAMOR-RFDORD - RFDHIS) ;
DEFRF4BD = min(RFDANT,max(DEF4BD1731,max(DEF4BD_P,DEF4BDP2)))* positif(SOMMERF_2) * (1-PREM8_11) ;

regle 831080:
application : iliad ;

DEF4BC = max(0, RFORDI + RFMIC * 0.70 + FONCI + REAMOR - RFDORD) ;
DEFRF4BC = max(0,RFDHIS-max(DEF4BC1731,max(DEF4BC_P,DEF4BCP2))) * positif(DFANTIMPU)*(1-positif(PREM8_11))
          + RFDHIS * positif(DFANTIMPU)*positif(PREM8_11);
regle 834210:
application : iliad ;

RFREVENU = (RFORDI + RFMIC * 0.70 + FONCI + REAMOR);
regle 834215:
application : iliad ;
DEFRFNONIBIS =  min(RFDORD,RFORDI + RFMIC * 0.70 + FONCI + REAMOR) + max(0,min(RFDANT,RFORDI + RFMIC * 0.70 + FONCI + REAMOR-RFDORD - RFDHIS));
regle 831090:
application : iliad ;
DEFRFNONI1 =  RFDORD + RFDANT - DEFRFNONIBIS;
DEFRFNONI2 = positif(SOMMERF_2) * null(PREM8_11) *
                  (max(0,RFDORD + RFDANT - max(DEFRFNONI1731,max(DEFRFNONI_P,DEFRFNONIP2))
                                         - max(0,RFREVENU - RFREVENUP3)))
            + PREM8_11 * positif(RFORDI + RFMIC * 0.70 + FONCI + REAMOR) * DEFRFNONIBIS
            + 0;
DEFRFNONI = positif(null(PREM8_11) * positif(DEFRFNONI2-DEFRFNONI1)) * DEFRFNONI2 + 0
            + PREM8_11 * positif(DEFRFNONI2) * (DEFRFNONI2+DEFRFNONI1) + 0;

 
