#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2018]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des 
#Finances Publiques pour permettre le calcul de l'imp�t sur le revenu 2018 
#au titre des revenus per�us en 2017. La pr�sente version a permis la 
#g�n�ration du moteur de calcul des cha�nes de taxation des r�les d'imp�t 
#sur le revenu de ce mill�sime.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
verif 2011:
application : iliad  ;
si
   APPLI_OCEANS = 0
   et
   IND_10V = 0
   et
   RDSYVO > 0

alors erreur I00101 ;
verif 2012:
application : iliad  ;
si
   APPLI_OCEANS = 0
   et
   IND_10C = 0
   et
   RDSYCJ > 0

alors erreur I00102 ;
verif 2013:
application : iliad ;
si
(
   APPLI_OCEANS = 0
   et
  (
    IND_101 = 0
   ou
    IND_102 = 0
   ou
    IND_103 = 0
   ou
    IND_104 = 0
  ) et RDSYPP > 0
)
alors erreur I00103 ;
verif 208:
application :  iliad ;

si
   APPLI_COLBERT + APPLI_BATCH + APPLI_ILIAD = 1
   et
   CHRFAC > 0
   et
   CHNFAC + 0 = 0
   et
   positif(NATIMP) = 1
   et
   V_CNR = 0

alors erreur I008 ;
verif 209:
application :  iliad ;

si
   APPLI_COLBERT + APPLI_BATCH + APPLI_ILIAD = 1
   et
   RDCOM > 0
   et
   NBACT + 0  = 0
   et
   positif(NATIMP) = 1

alors erreur I009 ;
verif 210:
application : iliad ;


si
   APPLI_OCEANS = 0
   et
  (
        V_0AV  = 1
        et
       (
       (
        positif(XETRANC) + positif(EXOCETC) + positif(FRNC)
        + positif(PENINC) + positif(CODRBZ)
        + positif(TSHALLOC) + positif(CODDBJ) + positif(CODEBJ) + positif(ALLOC)
        + positif(SALEXTC)  + positif(COD1BE) + positif(COD1BH)
        + positif(PRBC) + positif(2PRBC) + positif(PEBFC)
        + positif(CARTSC) + positif(REMPLAC) + positif(CARPEC) + positif(PENSALC)
        + positif(BA1AC)
        + somme (i=H,C:
                        somme(j= A,N: somme(k=R,D: positif(BjikEC))) +
                        somme(j=N: positif(BIiDjC)) + positif(BIiNOC)
          )
        + positif(BICREC) + positif(BI2AC) + positif(BICDEC)
        + positif(TSASSUC)
        + positif(GSALC) + positif(PCAPTAXC)
        + positif(COD1OX) + positif(COD1BF) + positif(COD1BL) + positif(COD1BM)
 + positif(COD1HA) + positif(COD1HB) + positif(CODRBF) + positif(CODRBG) + positif(CODRBL) 
 + positif(CODRBM) +  positif( BAFORESTC ) 
 + positif( BAFPVC ) + positif( BAF1AC )
 + positif( BAEXC ) + positif( BACREC ) + positif( 4BACREC ) + positif( BA1AC )
 + positif(BACDEC)
 + positif( BAHEXC ) + positif( BAHREC ) + positif( 4BAHREC )
 + positif( BAHDEC ) + positif( BAPERPC ) + positif( BANOCGAC )
 + positif( AUTOBICVC ) + positif( AUTOBICPC ) + positif( MIBEXC ) + positif( MIBVENC )
 + positif( MIBPRESC ) + positif( MIBPVC ) + positif( MIB1AC ) + positif( MIBDEC )
 + positif( BICEXC ) + positif( BICNOC ) 
 + positif( BI1AC ) + positif(BICDNC )
 + positif( BIHEXC ) + positif( BIHNOC ) 
 + positif( BIHDNC ) 
 + positif( MIBMEUC ) + positif( MIBGITEC ) + positif( MIBNPEXC ) + positif( MIBNPVENC )
 + positif( MIBNPPRESC ) + positif( MIBNPPVC ) + positif( MIBNP1AC ) + positif( MIBNPDEC )
 + positif( BICNPEXC ) + positif( BICREC ) + positif( LOCNPCGAC )
 + positif( BI2AC ) + positif( LOCDEFNPCGAC)
 + positif( BICNPHEXC ) + positif( BICHREC ) + positif( LOCNPC )
 + positif( BICHDEC)
 + positif(LOCDEFNPC)
 + positif( AUTOBNCC ) + positif( BNCPROEXC ) + positif( BNCPROC )
 + positif( BNCPROPVC ) + positif( BNCPRO1AC ) + positif( BNCPRODEC )
 + positif( BNCEXC ) + positif( BNCREC ) + positif( BN1AC )
 + positif( BNCDEC )
 + positif( BNHEXC ) + positif( BNHREC ) + positif( BNHDEC )
 + positif ( BNCCRC ) + positif ( CESSASSC ) + positif( XHONOAAC ) + positif( XHONOC )
 + positif( BNCNPC ) + positif( BNCNPPVC ) + positif( BNCNP1AC ) + positif( BNCNPDEC )
 + positif( BNCNPREXAAC ) + positif( BNCAABC ) + positif( BNCNPREXC ) + positif( ANOVEP )
 + positif( INVENTC ) + positif( PVINCE ) + positif( BNCAADC)
 + positif( DNOCEPC ) + positif( BNCCRFC )
 + positif( RCSC ) + positif( BANOCGAC ) + positif( PVSOCC )
 + positif( PERPC ) + positif( PERP_COTC ) + positif( PLAF_PERPC )
 + positif ( PERPPLAFCC ) + positif ( PERPPLAFNUC1 ) + positif ( PERPPLAFNUC2 ) + positif ( PERPPLAFNUC3 )
 + positif ( RDSYCJ )

 + positif(COD5EB) + positif(COD5EF) + positif(COD5EG) + positif(COD5EK) + positif(COD5EM) + positif(COD5EN)
 + positif(COD5FY) + positif(COD5FZ) + positif(COD5RZ) + positif(COD5VP) + positif(COD5VR) + positif(COD5VS)
 + positif(COD5VT) + positif(COD5VU) + positif(COD5YB) + positif(COD5YP) + positif(COD5YR) + positif(COD5YS)
 + positif(COD5YX) + positif(COD5YY) + positif(COD5YZ) + positif(COD5YK) + positif(COD5YN) + positif(COD5YO)
 + positif(COD5BD) + positif(COD5AI) + positif(COD5YA) + positif(COD5YC) + positif(COD5YD) + positif(COD5YE)
 + positif(COD5YF) + positif(COD5OW) +  positif(COD8WM)
     )
        > 0
     )
   )
alors erreur I010 ;
verif 211:
application : iliad  ;


si
   APPLI_OCEANS = 0
   et
   (V_0AM + V_0AO + 0 = 1) et V_0AS = 1 et V_0AP+0 = 0 et V_0AF+0 = 0
   et
   ANNEEREV - V_0DA < 74
   et
   ANNEEREV - V_0DB < 74

alors erreur I011 ;
verif 212:
application :  iliad ;


si
   APPLI_OCEANS = 0
   et
   (V_0AM + V_0AO + 0 = 0 )
   et
   V_0AZ + 0 = 0
   et
   V_0AP + 0 = 0
   et
   V_0AW = 1
   et
   ANNEEREV - V_0DA < 74
  
alors erreur I012 ;
verif 214:
application :  iliad ;


si 
   APPLI_OCEANS + APPLI_COLBERT = 0 
   et
    (
       V_BT0CF >0
          et V_0CH >0
              et positif(V_0CF+0) != 1
                   et V_BT0CF + 0 = somme(i=0..5:positif(V_BT0Fi+0))
                     et V_BT0CH + 0 = somme(i=0..5:positif(V_BT0Hi+0))
                       et V_0CF + 0 = somme(i=0..5:positif(V_0Fi+0))
                         et V_0CH + 0 = somme(i=0..5:positif(V_0Hi+0))
                           et ((     V_0CH < V_BT0CF   )
                                ou
                               (     V_0CH = V_BT0CF
                                  et somme(i=0..5:V_0Hi+0) != somme(i=0..5:V_BT0Fi+0)         )
                                ou
                               (     V_0CH = V_BT0CF
                                  et somme(i=0..5:V_0Hi+0) = somme(i=0..5:V_BT0Fi+0)
                                  et somme(i=0..5: (1/V_0Hi)) != somme(i=0..5: (1/V_BT0Fi))   )
                               ou
                               (     V_0CH > V_BT0CF
        et somme(i=0..5:positif(somme(j=0..5:null(V_0Hj - V_BT0Fi)))*V_BT0Fi) != somme(i=0..5:V_BT0Fi)
                               )
                               ou
                               (     V_0CH > V_BT0CF
        et somme(i=0..5:positif(somme(j=0..5:null(V_0Hj - V_BT0Fi)))*V_BT0Fi) = somme(i=0..5:V_BT0Fi)
        et somme(i=0..5:positif(somme(j=0..5:null(V_0Hi - V_BT0Fj)))*V_0Hi) < somme(i=0..5:V_BT0Fi)
                               )
                              )
    )
   et
   V_IND_TRAIT = 4

alors erreur I014 ;
verif 215:
application :  iliad ;


si
   V_IND_TRAIT = 4
   et
   V_CNR + 0 != 1
   et
   (
    DEFRCM + 0 > V_BTDFRCM1 + PLAF_PRECONS * (1 - positif(V_BTDFRCM1))
    ou
    DEFRCM2 + 0 > V_BTDFRCM2 + PLAF_PRECONS * (1 - positif(V_BTDFRCM2))
    ou
    DEFRCM3 + 0 > V_BTDFRCM3 + PLAF_PRECONS * (1 - positif(V_BTDFRCM3))
    ou
    DEFRCM4 + 0 > V_BTDFRCM4 + PLAF_PRECONS * (1 - positif(V_BTDFRCM4))
    ou
    DEFRCM5 + 0 > V_BTDFRCM5 + PLAF_PRECONS * (1 - positif(V_BTDFRCM5))
    ou
    DEFRCM6 + 0 > V_BTDFRCM6 + PLAF_PRECONS * (1 - positif(V_BTDFRCM6)))

alors erreur I015 ;
verif 216:
application :  iliad ;

si
   V_IND_TRAIT > 0
   et
   V_CNR + 0 != 1
   et
   positif(PVSURSI + PVIMPOS + CODRWA + CODRWB + COD3TA + COD3TB + 0) = 1

alors erreur I016 ;
verif 217:
application : iliad ;

si
   V_IND_TRAIT = 5
   et
   null(5 - LIGI017) = 1

alors erreur I017;
