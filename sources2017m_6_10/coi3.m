#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2018]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des 
#Finances Publiques pour permettre le calcul de l'imp�t sur le revenu 2018 
#au titre des revenus per�us en 2017. La pr�sente version a permis la 
#g�n�ration du moteur de calcul des cha�nes de taxation des r�les d'imp�t 
#sur le revenu de ce mill�sime.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
verif 309:
application : iliad ;


si
   APPLI_OCEANS = 0
   et
   present (V_BTCO2044P) = 1
   et
   present (CO2044P)   = 0
   et
   V_IND_TRAIT = 4

alors erreur IM09 ;
verif 3111:
application : iliad ;


si
   APPLI_ILIAD=1
   et
   V_0CF+0 != somme (i = 0..7: positif(V_0Fi+0))

alors erreur IM1101 ;
verif 3113:
application : iliad ;


si
   APPLI_ILIAD=1
   et
   V_0CH != somme (i = 0..5: positif(V_0Hi+0))

alors erreur IM1102 ;
verif 3116:
application : iliad ;


si
   APPLI_ILIAD=1
   et
   V_0DJ != somme (i = 0..5: positif(V_0Ji+0))

alors erreur IM1103 ;
verif 314:
application : iliad ;


si
   APPLI_OCEANS = 0
   et
   positif(null(V_NOTRAIT - 23) + null(V_NOTRAIT - 33) + null(V_NOTRAIT - 43) + null(V_NOTRAIT - 53) + null(V_NOTRAIT - 63)) = 0
   et
   IREST >= LIM_RESTIT

alors erreur IM14 ;
verif 3161:
application : iliad ;


si
   APPLI_OCEANS = 0
   et
   V_ZDC+0 = 0
   et
   V_BTMUL+0 = 0
   et
   V_0AX+0 = 0 et V_0AY+0 = 0 et V_0AZ+0 = 0 et V_0AO+0 = 0
   et
   V_BTRNI > LIM_BTRNI
   et
   RNI > V_BTRNI * 9
   et
   RNI < 100000
   et
   V_IND_TRAIT = 4

alors erreur IM1601 ;
verif 3162:
application : iliad ;

si
   APPLI_OCEANS = 0
   et
   V_ZDC+0 = 0
   et
   V_BTMUL+0 = 0
   et
   V_0AX+0 = 0 et V_0AY+0 = 0 et V_0AZ+0 = 0 et V_0AO+0 = 0
   et
   V_BTRNI > LIM_BTRNI
   et
   RNI > V_BTRNI * 5
   et
   RNI >= 100000
   et
   V_IND_TRAIT = 4

alors erreur IM1602 ;
verif 317:
application : iliad ;


si
   APPLI_OCEANS = 0
   et
   V_BTIMP > LIM_BTIMP
   et
   IINET >= V_BTIMP * 2
   et
   V_ZDC+0 = 0
   et
   V_IND_TRAIT = 4

alors erreur IM17 ;
verif 318:
application : iliad ;


si
   APPLI_OCEANS = 0
   et
   V_BTIMP > LIM_BTIMP
   et
   IINET <= V_BTIMP / 2
   et
   V_ZDC+0 = 0
   et
   V_IND_TRAIT = 4

alors erreur IM18 ;
verif 319:
application : iliad ;


si
   APPLI_OCEANS = 0
   et
   (V_IND_TRAIT = 4
    et V_BT0CF + 0 = somme(i=0..5:positif(V_BT0Fi+0))
    et V_BT0CH + 0 = somme(i=0..5:positif(V_BT0Hi+0))
    et V_0CF + 0 = somme(i=0..5:positif(V_0Fi+0))
    et V_0CH + 0 = somme(i=0..5:positif(V_0Hi+0))
    et
     (
       V_BT0CH + V_BT0CF + 0 > V_0CH + V_0CF
       ou
       (V_BT0CF = 1 et V_0CF =1 et V_0CH + 0 = 0 et pour un i dans 0,1: V_0Fi = ANNEEREV )
       ou
       (V_BT0CF = 1 et V_0CH =1 et V_0CF + 0 = 0 et pour un i dans 0,1: V_0Hi = ANNEEREV )
       ou
       (V_BT0CH = 1 et V_0CH =1 et V_0CF + 0 = 0 et pour un i dans 0,1: V_0Hi = ANNEEREV )
       ou
       (V_BT0CH = 1 et V_0CF =1 et V_0CH + 0 = 0 et pour un i dans 0,1: V_0Fi = ANNEEREV )
     )
   )

alors erreur IM19 ;
verif 320:
application : iliad ;


si
   APPLI_OCEANS = 0
   et
   V_NOTRAIT + 0 != 14
   et
   V_BTANC + 0 = 1
   et
   ((V_BTNI1+0 )non dans (50,92))
   et
  ((V_BTNATIMP+0) non dans (1,71))
   et
   IINET > LIM_BTIMP * 2
   et
   V_ZDC + 0 = 0
   et 
   V_IND_TRAIT = 4

alors erreur IM20 ;
verif 340:
application : iliad ;


si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT > 0
   et
   positif(ANNUL2042) = 1

alors erreur IM40 ;
verif 342:
application : iliad ;


si
                    (FLAGDERNIE+0 = 1) et  ((DEFRI = 1)  et (PREM8_11=1))

alors erreur IM42 ;
verif 3421:
application : iliad ;


si
                    (FLAGDERNIE+0 = 1) et  ((DEFRI = 1)  et (PREM8_11=0) et (VARR10+0=0) et (ANO1731=0))

alors erreur IM42 ;
verif 343:
application : iliad ;


si
                      ((DEFRI = 0)  et (DEFRIMAJ = 1))

alors erreur IM43 ;
verif 3441:
application : iliad ;

si
  V_IND_TRAIT = 5
  et
  INDCODIFI = 0
  et
  positif(COD8OT + 0) = 0
  et
  positif(IRNET) = 0
  et
  positif_ou_nul((25000 * NBPT) - REVKIRE) = 1

alors erreur IM4401 ;
verif 3442:
application : iliad ;

si
  V_IND_TRAIT = 5
  et
  INDCODIFI = 0
  et
  positif(COD8OT + 0) = 1
  et
  positif(IRNET) = 0
  et
  positif_ou_nul((25000 * NBPT) - REVKIRE) = 1

alors erreur IM4402 ;
