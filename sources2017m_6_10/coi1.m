#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2018]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des 
#Finances Publiques pour permettre le calcul de l'imp�t sur le revenu 2018 
#au titre des revenus per�us en 2017. La pr�sente version a permis la 
#g�n�ration du moteur de calcul des cha�nes de taxation des r�les d'imp�t 
#sur le revenu de ce mill�sime.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
verif 2:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   1 - V_CNR > 0
   et
   positif(RNOUV) = 1
   et
   positif(RDSNO) = 1
   et
   positif(COD7EN + CINE1 + CINE2) = 1

alors erreur DD02 ;
verif 3:
application : iliad  ;

si
   APPLI_OCEANS = 0
   et
   1 - V_CNR > 0
   et
   RCMFR > LIM_CONTROLE
   et
   RCMFR > 0.30 * (RCMABD + RCMHAD + REVACT + DISQUO + RCMHAB + INTERE + RCMTNC + REVPEA + REGPRIV + RESTUC + COD2TT + 0)

alors erreur DD03 ;
verif 4:
application : iliad  ;

si
   APPLI_OCEANS = 0
   et
   V_CNR + 0 != 1
   et
   ((RCMABD + RCMHAD + RCMHAB + REVACT + DISQUO + INTERE + RCMTNC + REVPEA + COD2FA + 0 > 0
     et
     RCMAVFT > ((1/3) * (RCMABD + RCMHAD + RCMHAB + REVACT + DISQUO + INTERE + RCMTNC + REVPEA + COD2FA)) +  PLAF_AF)
    ou
    (DIREPARGNE > ((PPLIB + RCMLIB + RCMHAD + RCMHAB + DISQUO + INTERE + COD2FA + BPVRCM) * (538/1000)) + PLAF_AF
     et
     PPLIB + RCMLIB + RCMHAD + RCMHAB + DISQUO + INTERE + COD2FA + BPVRCM + 0 > 0))

alors erreur DD04 ;
verif 5:
application :  iliad ;

si
   APPLI_COLBERT + APPLI_OCEANS = 0
   et
   V_ZDC + 0 = 0
   et
   V_BTMUL = 0
   et
   V_0AX+0 = 0 et V_0AY+0 = 0 et V_0AZ+0= 0
   et
   V_BTRNI > LIM_BTRNI10
   et
   RNI < V_BTRNI/5
   et
   V_BTANC + 0 = 1
   et
   ((V_BTNI1 + 0) non dans (50,92))
   et
   V_IND_TRAIT = 4

alors erreur DD05 ;
verif 8:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   pour un i dans V,C,P:
   (
    (MIBVENi + MIBNPVENi + MIBGITEi + LOCGITi > LIM_MIBVEN)
    ou
    (MIBPRESi + MIBNPPRESi + MIBMEUi > LIM_MIBPRES)
    ou
    (MIBVENi + MIBNPVENi + MIBGITEi + LOCGITi + MIBPRESi + MIBNPPRESi + MIBMEUi <= LIM_MIBVEN
     et
     MIBPRESi + MIBNPPRESi + MIBMEUi > LIM_MIBPRES)
    ou
    (MIBVENi + MIBNPVENi + MIBGITEi + LOCGITi + MIBPRESi + MIBNPPRESi + MIBMEUi > LIM_MIBVEN)
    ou
    (BNCPROi + BNCNPi > LIM_SPEBNC)
  )

alors erreur DD08 ;
verif 9:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_CNR + 0 != 1
   et
   positif(PRETUDANT + 0) = 1
   et
   positif(V_BTPRETUD + 0) = 1
   et
   V_IND_TRAIT = 4

alors erreur DD09 ;
verif 11: 
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   RFMIC > 0
   et
   RFDANT> 0

alors erreur DD11 ;
verif 15:
application : iliad  ;

si (APPLI_OCEANS=0) et (
          (
                ( RDPRESREPORT +0  > V_BTPRESCOMP  +  LIM_REPORT )
           ou
                ( PRESCOMP2000 + PRESCOMPJUGE  +0 > LIM_REPORT  et
                   V_BTPRESCOMP  + 0> 0 )
           ou
                ( RDPRESREPORT +0  > LIM_REPORT et V_BTPRESCOMP+0 = 0 )
          )
          et
          (
              1 - V_CNR > 0
          )
          et
          (
              RPRESCOMP > 0
          )
         et
          ((APPLI_ILIAD = 1 et V_NOTRAIT+0 < 16)
             ou APPLI_COLBERT = 1
             ou ((V_BTNI1+0) non dans (50,92) et APPLI_BATCH = 1))
                       )
alors erreur DD15 ;
verif 16:
application :  iliad ;

si
   APPLI_BATCH + APPLI_ILIAD + APPLI_OCEANS = 1
   et
   1 - V_CNR > 0
   et
   CHRFAC > 0
   et
   V_0CR > 0
   et
   RFACC != 0

alors erreur DD16 ;
verif 18:
application :  iliad ;


si
   APPLI_COLBERT + APPLI_OCEANS = 0
   et
   DAR > LIM_CONTROLE
   et
   V_BTRNI > 0
   et
   ((V_BTNI1+0) non dans (50,92))
   et
   V_IND_TRAIT = 4

alors erreur DD18 ;
verif 20:
application :  iliad ;


si
   APPLI_COLBERT + APPLI_OCEANS = 0
   et
   V_BTANC = 1
   et
   DAGRI1 + DAGRI2 + DAGRI3 + DAGRI4 + DAGRI5 + DAGRI6 > LIM_CONTROLE + V_BTDBA
   et
   V_IND_TRAIT = 4

alors erreur DD20 ;
verif 21:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   1 - V_CNR > 0
   et
   (CREAIDE + 0) > (LIM_AIDOMI3 * (1 - positif(PREMAIDE)) + LIM_PREMAIDE2 * positif(PREMAIDE))
   et
   INAIDE = 1
   et
   (positif(V_0AP+0)=0
    et positif(V_0AF+0)=0
    et positif(V_0CG+0)=0
    et positif(V_0CI+0)=0
    et positif(V_0CR+0)=0
   )

alors erreur DD21 ;
verif 22:
application :  iliad ;


si
   APPLI_COLBERT + APPLI_OCEANS = 0
   et
   (V_BTCSGDED * (1-present(DCSG)) + DCSG) > V_BTCSGDED +  LIM_CONTROLE
   et
   1 - V_CNR > 0
   et
   RDCSG > 0
   et
   ((APPLI_ILIAD = 1 et V_NOTRAIT+0 < 16)
    ou
    ((V_BTNI1+0) non dans (50,92) et APPLI_BATCH = 1))

alors erreur DD22 ;
verif 26:
application :  iliad ;

si
   APPLI_COLBERT + APPLI_BATCH + APPLI_ILIAD = 1
   et
   RFORDI + FONCI + REAMOR + RFDORD + RFDHIS + RFDANT > LIM_BTREVFONC
   et
   V_BTANC = 1
   et
   V_BTIRF = 0
   et
   V_IND_TRAIT = 4

alors erreur DD26 ;
verif 27:
application :  iliad ;


si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT = 4
   et
   (1 - V_CNR) > 0
   et
   (REPSNO3 > LIM_CONTROLE + V_BTPME4
    ou
    REPSNO2 > LIM_CONTROLE + V_BTPME3
    ou
    REPSNO1 > LIM_CONTROLE + V_BTPME2
    ou
    REPSNON > LIM_CONTROLE + V_BTPME1
    ou
    COD7CQ > LIM_CONTROLE + V_BTITENT4
    ou
    COD7CR > LIM_CONTROLE + V_BTITENT3
    ou
    COD7CV > LIM_CONTROLE + V_BTITENT2
    ou
    COD7CX > LIM_CONTROLE + V_BTITENT1)
   et
   positif(NATIMP + 0) = 1

alors erreur DD27 ;
verif 28:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   CREPROSP > 0
   et
   positif(V_BTCREPROSP + 0) = 1
   et
   V_IND_TRAIT = 4

alors erreur DD28 ;
verif 29:
application :  iliad ;


si
   APPLI_OCEANS = 0
   et
   V_CNR + 0 = 0
   et
   positif(NATIMP) = 1
   et
   ((REPDON03 > LIM_CONTROLE + V_BTDONS5)
    ou
    (REPDON04 > LIM_CONTROLE + V_BTDONS4)
    ou
    (REPDON05 > LIM_CONTROLE + V_BTDONS3)
    ou
    (REPDON06 > LIM_CONTROLE + V_BTDONS2)
    ou
    (REPDON07 > LIM_CONTROLE + V_BTDONS1))
   et
   V_IND_TRAIT = 4

alors erreur DD29 ;
verif 30:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_REGCO + 0 = 1
   et
   positif(PRODOM + PROGUY + 0) = 0
   et
  (positif(COD7BH + 0) = 1
   ou
   positif(COD7BK + 0) = 1
   ou
   positif(COD7BL + 0) = 1)

alors erreur DD30 ;
verif 34:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et 
   V_CNR + 0 != 1
   et
   positif(FIPCORSE+0) = 1
   et
   positif(FFIP + FCPI) = 1
                         
alors erreur DD34 ;
verif 37:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_CNR + 0 != 1
   et
   positif(V_BTRFRN2 + 0) = 1
   et
   (pour un i dans V,C,P:
    (AUTOBICVi > LIM_MIBVEN)
    ou
    (AUTOBICPi > LIM_MIBPRES)
    ou
    (AUTOBICVi + AUTOBICPi > LIM_MIBVEN)
    ou
    (AUTOBNCi > LIM_SPEBNC))

alors erreur DD37 ;
verif 381:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_CNR + 0 != 1
   et
   V_BTRFRN2 + 0 > arr(LIM_BARN2 * V_BTNBP2)
   et
   pour un i dans V,C,P: positif(AUTOBICVi + AUTOBICPi + AUTOBNCi) = 1

alors erreur DD3801 ;
verif 382:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_CNR + 0 != 1
   et
   positif(V_BTRFRN2 + 0) = 0
   et
   1 - positif_ou_nul(RFRN2) = 1
   et
   pour un i dans V,C,P: positif(AUTOBICVi + AUTOBICPi + AUTOBNCi) = 1

alors erreur DD3802 ;
verif 39:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT = 4
   et
   V_CNR + 0 != 1
   et
   REPGROREP1 + REPGROREP2 + REPGROREP11 + REPGROREP12 + REPGROREP13 + REPGROREP14 + COD6HP + COD6HQ  > LIM_CONTROLE + V_BTNUREPAR

alors erreur DD39 ;
verif 40:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT = 4
   et
   V_CNR + 0 != 1
   et
   CELRREDLE + CELRREDLM + CELRREDLN + CELRREDLG + CELRREDLK + CELRREDLQ  > LIM_CONTROLE + V_BTRRCEL4

alors erreur DD40 ;
verif 41:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT = 4
   et
   V_CNR + 0 != 1
   et
   LOCMEUBIX + LOCMEUBIY + COD7PA + COD7PF + COD7PK + COD7PP  > LIM_CONTROLE + V_BTRILMNP5

alors erreur DD41 ;
verif 48:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT = 4
   et
   V_CNR + 0 != 1
   et
   CELRREDLD + CELRREDLS + CELRREDLT + CELRREDLH + CELRREDLL + CELRREDLR  > LIM_CONTROLE + V_BTRRCEL3

alors erreur DD48 ;
verif 49:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT = 4
   et
   V_CNR + 0 != 1
   et
   LOCMEUBIH + LOCMEUBJC + COD7PB + COD7PG + COD7PL + COD7PQ  > LIM_CONTROLE + V_BTRILMNP4

alors erreur DD49 ;
verif 50:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT = 4
   et
   V_CNR + 0 != 1
   et
   PATNAT2 + PATNAT3 + PATNAT4 > LIM_CONTROLE + V_BTPATNAT

alors erreur DD50 ;
verif 52:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT = 4
   et
   V_CNR + 0 != 1
   et
    LNPRODEF10 + LNPRODEF9 + LNPRODEF8 + LNPRODEF7 + LNPRODEF6 + LNPRODEF5
    + LNPRODEF4 + LNPRODEF3 + LNPRODEF2 + LNPRODEF1 > LIM_CONTROLE + V_BTDEFNPLOC

alors erreur DD52 ;
verif 53:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT = 4
   et
   V_CNR + 0 != 1
   et
   DEFBIC6 + DEFBIC5 + DEFBIC4 + DEFBIC3 + DEFBIC2 + DEFBIC1 > LIM_CONTROLE + V_BTBICDF

alors erreur DD53 ;
verif 57:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT = 4
   et
   V_CNR + 0 != 1
   et
   CELRREDLF + CELRREDLZ + CELRREDLX + CELRREDLI + CELRREDLO + CELRREDLU  > LIM_CONTROLE + V_BTRRCEL2

alors erreur DD57 ;
verif 58:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT = 4
   et
   V_CNR + 0 != 1
   et
   LOCMEUBIZ + LOCMEUBJI + COD7PC + COD7PH + COD7PM + COD7PR  > LIM_CONTROLE + V_BTRILMNP3

alors erreur DD58 ;
verif 61:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT > 0
   et
   positif(CVNSALAV + 0) = 1
   et
   positif(BPV18V + BPCOPTV + BPV40V + BPCOSAV + 0) = 0

alors erreur DD61 ;
verif 64:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT = 4
   et
   V_CNR + 0 != 1
   et
   CELRREDMG + CELRREDMH + CELRREDLJ + CELRREDLP +  CELRREDLV  > LIM_CONTROLE + V_BTRRCEL1

alors erreur DD64 ;
verif 65:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT = 4
   et
   V_CNR + 0 != 1
   et
   LOCMEUBJS + COD7PD + COD7PI + COD7PN + COD7PS  > LIM_CONTROLE + V_BTRILMNP2

alors erreur DD65 ;
verif 66:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT = 4
   et
   V_CNR + 0 != 1
   et
   COD7PE + COD7PJ + COD7PO + COD7PT  > LIM_CONTROLE + V_BTRILMNP1

alors erreur DD66 ;
verif 67:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_IND_TRAIT = 4
   et
   V_CNR + 0 != 1
   et
   COD7CY + COD7DY + COD7EY + COD7FY  > LIM_CONTROLE + V_BTPLAFPME1

alors erreur DD67 ;
