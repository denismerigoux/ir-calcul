#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2015 
#au titre des revenus perçus en 2014. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
regle 99991000:
application : iliad , batch ;

pour i = V,C,P:
TMIB_TVENi = MIBVENi + AUTOBICVi + MIBNPVENi + MIBGITEi+LOCGITi;

pour i = V,C,P:
TMIB_TPRESi = MIBPRESi + AUTOBICPi + MIBNPPRESi + MIBMEUi;

pour i = V,C,P:
TMIB_TTi = TMIB_TVENi + TMIB_TPRESi;
regle 99991004:
application : iliad , batch ;


pour i = V,C,P:
TMIB_AVi = min ( TMIB_TVENi,
                         (max(MIN_MBIC,
                              arr( (TMIB_TVENi)*TX_MIBVEN/100))
                         )
              );
pour i = V,C,P:
TMIB_VENTAi = min ( (MIBVENi + MIBNPVENi),
                         (max(MIN_MBIC,
                              arr( (MIBVENi + MIBNPVENi)*TX_MIBVEN/100))
                         )
              );
pour i = V,C,P:
TMIB_AUTOAVi= TMIB_AVi - TMIB_VENTAi; 


pour i = V,C,P:
TMIB_APi = min ( TMIB_TPRESi,
                         (max(MIN_MBIC,
                              arr( (TMIB_TPRESi)*TX_MIBPRES/100))
                         )
               );
pour i = V,C,P:
TMIB_PRESAi = min ( (MIBPRESi + MIBNPPRESi),
                         (max(MIN_MBIC,
                              arr( (MIBPRESi + MIBNPPRESi)*TX_MIBPRES/100))
                         )
               );
pour i = V,C,P:
TMIB_AUTOAPi= TMIB_APi - TMIB_PRESAi; 
pour i = V,C,P:
TPMIB_AVi = min ( (MIBVENi + AUTOBICVi),
                         (max(MIN_MBIC,
                              arr( (MIBVENi+ AUTOBICVi)*TX_MIBVEN/100))
                         )
              );
pour i = V,C,P:
TPMIB_APi = min ( (MIBPRESi+ AUTOBICPi),
                         (max(MIN_MBIC,
                              arr( (MIBPRESi+ AUTOBICPi)*TX_MIBPRES/100))
                         )
               );


regle 99991005:
application : iliad , batch ;

pour i = V,C,P:
TMIB_ABVi = max(0,arr(TMIB_AVi * (MIBVENi + AUTOBICVi)/ (TMIB_TVENi)));
pour i = V,C,P:
TMIB_ABNPVi = max(0,arr(TMIB_AVi * MIBNPVENi / TMIB_TVENi))* positif(present(MIBGITEi)+present(LOCGITi))
	      + (TMIB_AVi - TMIB_ABVi) * (1 - positif(present(MIBGITEi)+present(LOCGITi)));
pour i = V,C,P:
TMIB_ABNPVLi = (TMIB_AVi - TMIB_ABVi - TMIB_ABNPVi) *  positif(present(MIBGITEi)+present(LOCGITi));

pour i = V,C,P:
TMIB_ABPi = max(0,arr(TMIB_APi * (MIBPRESi + AUTOBICPi)/ (TMIB_TPRESi)));
pour i = V,C,P:
TMIB_ABNPPi = max(0,arr(TMIB_APi * MIBNPPRESi / (TMIB_TPRESi))) * present(MIBMEUi)
	      + (TMIB_APi - TMIB_ABPi) * (1 - present(MIBMEUi));
pour i = V,C,P:
TMIB_ABNPPLi = (TMIB_APi - TMIB_ABPi - TMIB_ABNPPi) *  present(MIBMEUi);


regle 99991006:
application : iliad , batch ;
pour i = V,C,P:
TPMIB_NETVi = MIBVENi + AUTOBICVi - TPMIB_AVi;
pour i = V,C,P:
TPMIB_NETPi = MIBPRESi + AUTOBICPi - TPMIB_APi;

pour i = V,C,P:
TMIB_NETVi = MIBVENi + AUTOBICVi - TMIB_ABVi;
TMIBNETVF = somme(i=V,C,P:TMIB_NETVi) ;
pour i = V,C,P:
TMIB_NETNPVi = MIBNPVENi - TMIB_ABNPVi;
TMIBNETNPVF = somme(i=V,C,P:TMIB_NETNPVi);

pour i = V,C,P:
TMIB_NETPi = MIBPRESi + AUTOBICPi - TMIB_ABPi;
TMIBNETPF = somme(i=V,C,P:TMIB_NETPi) ;
pour i = V,C,P:
TMIB_NETNPPi = MIBNPPRESi - TMIB_ABNPPi;
TMIBNETNPPF = somme(i=V,C,P:TMIB_NETNPPi);

TBICPABV =   arr((TMIB_ABVV * AUTOBICVV/(MIBVENV+AUTOBICVV))
          + (TMIB_ABPV * AUTOBICPV/(MIBPRESV+AUTOBICPV)));

TBICPROVC = max(0,arr((TMIB_ABVC * AUTOBICVC/(MIBVENC+AUTOBICVC)) + (TMIB_ABPC * AUTOBICPC/(MIBPRESC+AUTOBICPC))));

TBICPABC =  min(TBICPROVC,arr((TMIB_ABVC * AUTOBICVC/(MIBVENC+AUTOBICVC))
          + (TMIB_ABPC * AUTOBICPC/(MIBPRESC+AUTOBICPC))));

TBICPROVP = max(0,arr((TMIB_ABVP * AUTOBICVP/(MIBVENP+AUTOBICVP)) + (TMIB_ABPP * AUTOBICPP/(MIBPRESP+AUTOBICPP))));

TBICPABP =  min(TBICPROVP,arr((TMIB_ABVP * AUTOBICVP/(MIBVENP+AUTOBICVP))
          + (TMIB_ABPP* AUTOBICPP/(MIBPRESP+AUTOBICPP))));

TBICNPABV = arr((TMIB_ABNPVV /(MIBNPVENV))
          + (TMIB_ABNPPV /(MIBNPPRESV)));
TBICNPPROVC = max(0,arr((TMIB_ABNPVC /(MIBNPVENC)) + (TMIB_ABNPPC /(MIBNPPRESC))));
TBICNPABC = min(TBICNPPROVC,arr((TMIB_ABNPVC /(MIBNPVENC))
          + (TMIB_ABNPPC /(MIBNPPRESC))));
TBICNPPROVP = max(0,arr((TMIB_ABNPVP /(MIBNPVENP)) + (TMIB_ABNPPP /(MIBNPPRESP))));
TBICNPABP = min(TBICNPPROVP,arr((TMIB_ABNPVP /(MIBNPVENP))
          + (TMIB_ABNPPP /(MIBNPPRESP))));
ABICPDECV = AUTOBICVV + AUTOBICPV;
ABICPDECC = AUTOBICVC + AUTOBICPC;
ABICPDECP = AUTOBICVP + AUTOBICPP;
ABICPNETV =  AUTOBICVV + AUTOBICPV - max(0,TMIB_AUTOAVV-TMIB_ABNPVLV) -max(0,TMIB_AUTOAPV-TMIB_ABNPPLV);
ABICPNETC =  AUTOBICVC + AUTOBICPC - max(0,TMIB_AUTOAVC-TMIB_ABNPVLC) -max(0,TMIB_AUTOAPC-TMIB_ABNPPLC);
ABICPNETP =  AUTOBICVP + AUTOBICPP - max(0,TMIB_AUTOAVP-TMIB_ABNPVLP) -max(0,TMIB_AUTOAPP-TMIB_ABNPPLP);

AUTOBICPNET = ABICPNETV + ABICPNETC + ABICPNETP;
regle 99991009:                                                                    
application : iliad , batch  ;                          
pour i = V,C,P:                                                                 
TSPETOTi = BNCPROi + AUTOBNCi + BNCNPi ;
regle 99991010:
application : iliad , batch  ;                          
pour i = V,C,P:                                                                 
TSPEBASABi=TSPETOTi;
pour i = V,C,P:                                                                 
TSPEABi = arr((max(MIN_SPEBNC,(TSPEBASABi * SPETXAB/100))) * 
                       positif_ou_nul(TSPETOTi - MIN_SPEBNC)) +
          arr((min(MIN_SPEBNC,TSPEBASABi )) * 
                       positif(MIN_SPEBNC - TSPETOTi)); 
regle 99991011:
application : iliad , batch  ;                          
pour i = V,C,P:                                                                 
TSPEABPi = arr((TSPEABi * (BNCPROi + AUTOBNCi))/TSPETOTi);                                  
pour i = V,C,P:                                                                 
TBNCPABi = arr(TSPEABPi * AUTOBNCi/(BNCPROi+AUTOBNCi)); 
pour i = V,C,P:                                                                 
TBNCTOTABi = arr(TSPEABi * (AUTOBNCi)/(TSPETOTi)); 

pour i = V,C,P:                                                                 
TSPEABNPi = TSPEABi - TSPEABPi;                                  
pour i = V,C,P:                                                                 
TBNCNPABi = (TBNCTOTABi - TBNCPABi) ;

pour i = V,C,P:                                                                 
ABNCPDECi =  AUTOBNCi; 
pour i = V,C,P:                                                                 
ABNCPNETi =  AUTOBNCi - TBNCPABi; 
pour i = V,C,P:                                                                 
HONODECi = XHONOi + XHONOAAi;
pour i = V,C,P:                                                                 
HONONETi = arr(XHONOi * MAJREV) + XHONOAAi ;
AUTOBNCPNET = ABNCPNETV + ABNCPNETC + ABNCPNETP;
HONONET = HONONETV + HONONETC + HONONETP;
regle 99991012:
application : iliad , batch  ;                          
pour i = V,C,P:                                                                 
TSPENETPi = max (0,(BNCPROi  + AUTOBNCi - TSPEABPi));
pour i = V,C,P:                                                                 
TSPENETNPi = max (0,(BNCNPi - TSPEABNPi));
pour i = V,C,P:                                                                 
TSPENETi = TSPENETPi + TSPENETNPi;
TSPENET = somme(i=V,C,P:(TSPENETi));
regle 99991020:
application : iliad , batch  ;                          
pour i = V,C,P:                                                                 
TXSPEAAi = BNCREi + XHONOAAi - (BNCDEi * (1 - positif(ART1731BIS) ));
regle 99991022:
application : iliad , batch  ;                          
pour i = V,C,P:                                                                 
TXSPEHi = max(0,arr((BNHREi + XHONOi - (BNHDEi * (1 - positif(ART1731BIS) )))*MAJREV))
	 + min(0,(BNHREi + XHONOi - (BNHDEi * (1 - positif(ART1731BIS) ))));
regle 99991024:
application : iliad , batch  ;                          
pour i = V,C,P:                                                                 
TXSPENETi = TXSPEAAi + TXSPEHi;
regle 99991026:
application : iliad , batch  ;                          
TXSPENET = somme(i=V,C,P:(TXSPENETi));
 #
 #                         REVENUS CATEGORIELS NETS
 #                 TS
 #
regle 99992000:
application : iliad , batch  ;
TTSBNV = TSHALLOV + ALLOV+max(0,SALEXTV - CO[DGFIP][2017]AD);
TTSBNC = TSHALLOC + ALLOC+max(0,SALEXTC - CO[DGFIP][2017]BD);
TTSBN1 = TSHALLO1 + ALLO1+max(0,SALEXT1 - CO[DGFIP][2017]CD);
TTSBN2 = TSHALLO2 + ALLO2+max(0,SALEXT2 - CO[DGFIP][2017]DD);
TTSBN3 = TSHALLO3 + ALLO3+max(0,SALEXT3 - CO[DGFIP][2017]ED);
TTSBN4 = TSHALLO4 + ALLO4+max(0,SALEXT4 - CO[DGFIP][2017]FD);
TTSHALLOP=TSHALLO1+TSHALLO2+TSHALLO3+TSHALLO4;
TALLOP=ALLO1+ALLO2+ALLO3+ALLO4;
TTSBNP=TTSHALLOP+TALLOP;

pour i=V,C:
T2TSNi = CARTSi + REMPLAi;
pour i=1,2,3,4:
T2TSNi = CARTSPi + REMPLAPi;
TEXTSV = TTSBNV + BPCOSAV + GLDGRATV + T2TSNV;
TEXTSC = TTSBNC + BPCOSAC + GLDGRATC + T2TSNC;
TGATASAV = BPCOSAV + GLDGRATV ;
TGATASAC = BPCOSAC + GLDGRATC ;

pour i=1..4:
TEXTSi = TTSBNi + T2TSNi;
TTSBV = TEXTSV + somme(x=1..3:GLDxV)+CODDAJ+CODEAJ ;
TTSBC = TEXTSC + somme(x=1..3:GLDxC)+CODDBJ+CODEBJ ;
pour i=1,2,3,4:
TTSBi = TEXTSi;
TTSBP = somme(i=1..4:TTSBi);
pour i=V,C,1..4:
TPRBi = PRBRi + PALIi + PENINi;

T2PRBV = CARPEV + PENSALV + CODRAZ;
T2PRBC = CARPEC + PENSALC + CODRBZ;
T2PRB1 = CARPE[DGFIP][2017] + PENSAL[DGFIP][2017] + CODRCZ;
T2PRB2 = CARPE[DGFIP][2017] + PENSAL[DGFIP][2017] + CODRDZ;
T2PRB3 = CARPEP3 + PENSALP3 + CODREZ;
T2PRB4 = CARPEP4 + PENSALP4 + CODRFZ;
TEXPRV = TPRBV + CO[DGFIP][2017]AH + T2PRBV + PEBFV;
TEXPRC = TPRBC + CO[DGFIP][2017]BH + T2PRBC + PEBFC;
TEXPR1 = TPRB1 + CO[DGFIP][2017]CH + T2PRB1 + PEB[DGFIP][2017];
TEXPR2 = TPRB2 + CO[DGFIP][2017]DH + T2PRB2 + PEB[DGFIP][2017];
TEXPR3 = TPRB3 + CO[DGFIP][2017]EH + T2PRB3 + PEBF3;
TEXPR4 = TPRB4 + CO[DGFIP][2017]FH + T2PRB4 + PEBF4;
pour i = V,C,1..4:
TEXSPBi = TEXTSi + TEXPRi ;
regle 99992100:
application : iliad , batch  ;
pour i = V,C,1..4:
TTPS10i = arr (TTSBi * TX_DEDFORFTS /100);
pour i = V,C,1..4:
TDFNi =  min( PLAF_DEDFORFTS , TTPS10i );
regle 99992200:
application : iliad , batch  ;
pour i = V,C,1..4:
TDEDMINi = positif(DETSi)* MIN_DEMEMPLOI + (1- positif(DETSi))* MIN_DEDSFORFTS;
pour i = V,C,1..4:
T10MINSi= max( min(TTSBi,TDEDMINi) , TDFNi );
pour i = V,C,1..4:
TIND_10MIN_0i = positif(TDEDMINi - TDFNi ) * positif (TTSBi );
pour i = V,C,1..4 :
TIND_MINi = 1 - positif( TIND_10MIN_0i );
regle 99992300:
application : iliad , batch  ;
T10MINSP = T10MINS1 + T10MINS2 + T10MINS3 + T10MINS4;
TFRDPROVV = TTSBNV + TPRV + PALIV - TAPRV;
TFRDPROVC = TTSBNC + TPRC + PALIC - TAPRC;
TFRDPROV1 = TTSBN1 + PRBR1 + PAL[DGFIP][2017] - TAPR1;
TFRDPROV2 = TTSBN2 + PRBR2 + PAL[DGFIP][2017] - TAPR2;
TFRDPROV3 = TTSBN3 + PRBR3 + PALI3 - TAPR3;
TFRDPROV4 = TTSBN4 + PRBR4 + PALI4 - TAPR4;
TFRDPROVP = TFRDPROV1 +TFRDPROV2 +TFRDPROV3 +TFRDPROV4;
TFRDP = (1-positif(PREM8_11)) * (FRNP+CO[DGFIP][2017]CE+CO[DGFIP][2017]DE+CO[DGFIP][2017]EE+CO[DGFIP][2017]FE) * positif(FRNP+CO[DGFIP][2017]CE+CO[DGFIP][2017]DE+CO[DGFIP][2017]EE+CO[DGFIP][2017]FE - T10MINSP)
      + null(4-V_IND_TRAIT) * positif(PREM8_11) * min(FRNP+CO[DGFIP][2017]CE+CO[DGFIP][2017]DE+CO[DGFIP][2017]EE+CO[DGFIP][2017]FE,TFRDPROVP)
      + null(5-V_IND_TRAIT) * positif(PREM8_11) * min(FRNP+CO[DGFIP][2017]CE+CO[DGFIP][2017]DE+CO[DGFIP][2017]EE+CO[DGFIP][2017]FE,max(TFRDPROVP[DGFIP][2017],TFRDPROV[DGFIP][2017]731));

TFRDV = (1-positif(PREM8_11)) * (FRNV+CO[DGFIP][2017]AE) * positif(FRNV+CO[DGFIP][2017]AE - T10MINSV)
                + null(4-V_IND_TRAIT) * positif(PREM8_11) * min(FRNV+CO[DGFIP][2017]AE,TFRDPROVV)
                + null(5-V_IND_TRAIT) * positif(PREM8_11) * min(FRNV+CO[DGFIP][2017]AE,min(TFRDPROVV,max(TFRDPROVV[DGFIP][2017],TFRDPROVV1731)));

TFRDC = (1-positif(ART1731BIS)) * (FRNC+CO[DGFIP][2017]BE) * positif(FRNC+CO[DGFIP][2017]BE - T10MINSC)
        + null(4-V_IND_TRAIT) * positif(PREM8_11) * min(FRNC+CO[DGFIP][2017]BE,TFRDPROVC)
        + null(5-V_IND_TRAIT) * positif(PREM8_11) * min(FRNC+CO[DGFIP][2017]BE,min(TFRDPROVC,max(TFRDPROVC[DGFIP][2017],TFRDPROVC1731)));



TFR[DGFIP][2017] = (1-positif(PREM8_11)) * (FRN1+CO[DGFIP][2017]CE) * positif(FRN1+CO[DGFIP][2017]CE - T10MINS1)
        + null(4-V_IND_TRAIT) * (positif(PREM8_11) * min(FRN1+CO[DGFIP][2017]CE,TFRDPROV1) * positif(FRN2+FRN3+FRN4+CO[DGFIP][2017]DE+CO[DGFIP][2017]EE+CO[DGFIP][2017]FE)
                                        + positif(PREM8_11) * max(0,TFRDP) * (1-positif(FRN2+FRN3+FRN4+CO[DGFIP][2017]DE+CO[DGFIP][2017]EE+CO[DGFIP][2017]FE)))
        + null(5-V_IND_TRAIT) * (positif(PREM8_11) * min(FRN1+CO[DGFIP][2017]CE,min(TFRDPROV1,max(TFRDPROV1[DGFIP][2017],TFRDPROV11731))) * positif(FRN2+FRN3+FRN4+CO[DGFIP][2017]DE+CO[DGFIP][2017]EE+CO[DGFIP][2017]FE)
                                        + positif(PREM8_11) * max(0,TFRDP) * (1-positif(FRN2+FRN3+FRN4+CO[DGFIP][2017]DE+CO[DGFIP][2017]EE+CO[DGFIP][2017]FE)));



TFR[DGFIP][2017] = (1-positif(PREM8_11)) * (FRN2+CO[DGFIP][2017]DE) * positif(FRN2+CO[DGFIP][2017]DE - T10MINS2)
        + null(4-V_IND_TRAIT) * (positif(PREM8_11) * min(FRN2+CO[DGFIP][2017]DE,TFRDPROV2) * positif(FRN3+FRN4+CO[DGFIP][2017]EE+CO[DGFIP][2017]FE)
                                        + positif(PREM8_11) * max(0,TFRDP-TFR[DGFIP][2017]) * (1-positif(FRN3+FRN4+CO[DGFIP][2017]EE+CO[DGFIP][2017]FE)))
        + null(5-V_IND_TRAIT) * (positif(PREM8_11) * min(FRN2+CO[DGFIP][2017]DE,min(TFRDPROV2,max(TFRDPROV2[DGFIP][2017],TFRDPROV21731))) * positif(FRN3+FRN4+CO[DGFIP][2017]EE+CO[DGFIP][2017]FE)
                                        + positif(PREM8_11) * max(0,TFRDP-TFR[DGFIP][2017]) * (1-positif(FRN3+FRN4+CO[DGFIP][2017]EE+CO[DGFIP][2017]FE)));



TFRD3 = (1-positif(PREM8_11)) * (FRN3+CO[DGFIP][2017]EE) * positif(FRN3+CO[DGFIP][2017]EE - T10MINS3)
        + null(4-V_IND_TRAIT) * (positif(PREM8_11) * min(FRN3+CO[DGFIP][2017]EE,TFRDPROV3) * positif(FRN4+CO[DGFIP][2017]FE)
                                        + positif(PREM8_11) * max(0,TFRDP-TFR[DGFIP][2017]-TFR[DGFIP][2017]) * (1-positif(FRN4+CO[DGFIP][2017]FE)))
        + null(5-V_IND_TRAIT) * (positif(PREM8_11) * min(FRN3+CO[DGFIP][2017]EE,min(TFRDPROV3,max(TFRDPROV3[DGFIP][2017],TFRDPROV31731))) * positif(FRN4+CO[DGFIP][2017]FE)
                                        + positif(PREM8_11) * max(0,TFRDP-TFR[DGFIP][2017]-TFR[DGFIP][2017]) * (1-positif(FRN4+CO[DGFIP][2017]FE)));



TFRD4 =  max(0,TFRDP - TFR[DGFIP][2017] - TFR[DGFIP][2017] - TFRD3);

TIND_10V = positif_ou_nul( T10MINSV - (TFRDV+CO[DGFIP][2017]AE) ) ;
TIND_10C = positif_ou_nul( T10MINSC - (TFRDC+CO[DGFIP][2017]BE) ) ;
TIND_101 = positif_ou_nul( T10MINS1 - (TFR[DGFIP][2017]+CO[DGFIP][2017]CE) ) ;
TIND_102 = positif_ou_nul( T10MINS2 - (TFR[DGFIP][2017]+CO[DGFIP][2017]DE) ) ;
TIND_103 = positif_ou_nul( T10MINS3 - (TFRD3+CO[DGFIP][2017]EE) ) ;
TIND_104 = positif_ou_nul( T10MINS4 - (TFRD4+CO[DGFIP][2017]FE) ) ;
pour i = V,C,1..4:
TFPTi = max(TFRDi, T10MINSi);
pour i = V,C,1..4:
T[DGFIP][2017]0Mi = TIND_MINi *TDFNi 
        + (1 - TIND_MINi)* T10MINSi ; 
pour i = V,C,1..4:
TRE[DGFIP][2017]0i =  TIND_10i * T[DGFIP][2017]0Mi + (1-TIND_10i) * TFPTi ;
TABTS1AJ=positif(SALEXTV+ALLOV+BPCOSAV+GLDGRATV+CARTSV+REMPLAV+CODDAJ+CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V) * arr(TRE[DGFIP][2017]0V*(TSHALLOV)/TTSBV)
        + (1-positif(SALEXTV+ALLOV+BPCOSAV+GLDGRATV+CARTSV+REMPLAV+CODDAJ+CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V)) * TRE[DGFIP][2017]0V;
TABTS1AC=positif(ALLOV+BPCOSAV+GLDGRATV+CARTSV+REMPLAV+CODDAJ+CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V) * arr(TRE[DGFIP][2017]0V*(max(0,SALEXTV-CO[DGFIP][2017]AD))/TTSBV)
        + (1-positif(ALLOV+BPCOSAV+GLDGRATV+CARTSV+REMPLAV+CODDAJ+CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V)) * max(0,TRE[DGFIP][2017]0V-TABTS1AJ);
TABTS1AP=positif(BPCOSAV+GLDGRATV+CARTSV+REMPLAV+CODDAJ+CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V) * arr(TRE[DGFIP][2017]0V*(ALLOV)/TTSBV)
        + (1-positif(BPCOSAV+GLDGRATV+CARTSV+REMPLAV+CODDAJ+CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V)) * max(0,TRE[DGFIP][2017]0V-TABTS1AJ-TABTS1AC);
TABTS3VJ=positif(GLDGRATV+CARTSV+REMPLAV+CODDAJ+CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V) * arr(TRE[DGFIP][2017]0V*(BPCOSAV)/TTSBV)
        + (1-positif(GLDGRATV+CARTSV+REMPLAV+CODDAJ+CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V)) * max(0,TRE[DGFIP][2017]0V-TABTS1AJ-TABTS1AC-TABTS1AP);
TABTS1TT=positif(CARTSV+REMPLAV+CODDAJ+CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V) * arr(TRE[DGFIP][2017]0V*(GLDGRATV)/TTSBV)
        + (1-positif(CARTSV+REMPLAV+CODDAJ+CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V)) * max(0,TRE[DGFIP][2017]0V-TABTS1AJ-TABTS1AC-TABTS1AP-TABTS3VJ);
TABTSRAJ=positif(REMPLAV+CODDAJ+CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V) * arr(TRE[DGFIP][2017]0V*(CARTSV)/TTSBV)
        + (1-positif(REMPLAV+CODDAJ+CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V)) * max(0,TRE[DGFIP][2017]0V-TABTS1AJ-TABTS1AC-TABTS1AP-TABTS3VJ-TABTS1TT);
TABTSRAP=positif(REMPLAV+CODDAJ+CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V) * arr(TRE[DGFIP][2017]0V*(REMPLAV)/TTSBV)
        + (1-positif(REMPLAV+CODDAJ+CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V)) * max(0,TRE[DGFIP][2017]0V-TABTS1AJ-TABTS1AC-TABTS1AP-TABTS3VJ-TABTS1TT-TABTSRAJ);
TABTSV = TABTS1AJ + TABTS1AC +TABTS1AP +TABTS3VJ +TABTS1TT +TABTSRAJ+TABTSRAP;
TABTS1BJ=positif(SALEXTC+ALLOC+BPCOSAC+GLDGRATC+CARTSC+REMPLAC+CODDBJ+CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C) * arr(TRE[DGFIP][2017]0C*(TSHALLOC)/TTSBC)
        + (1-positif(SALEXTC+ALLOC+BPCOSAC+GLDGRATC+CARTSC+REMPLAC+CODDBJ+CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C)) * TRE[DGFIP][2017]0C;
TABTS1BC=positif(ALLOC+BPCOSAC+GLDGRATC+CARTSC+REMPLAC+CODDBJ+CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C) * arr(TRE[DGFIP][2017]0C*(max(0,SALEXTC-CO[DGFIP][2017]BD))/TTSBC)
        + (1-positif(ALLOC+BPCOSAC+GLDGRATC+CARTSC+REMPLAC+CODDBJ+CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C)) * max(0,TRE[DGFIP][2017]0C-TABTS1BJ);
TABTS1BP=positif(BPCOSAC+GLDGRATC+CARTSC+REMPLAC+CODDBJ+CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C) * arr(TRE[DGFIP][2017]0C*(ALLOC)/TTSBC)
        + (1-positif(BPCOSAC+GLDGRATC+CARTSC+REMPLAC+CODDBJ+CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C)) * max(0,TRE[DGFIP][2017]0C-TABTS1BJ-TABTS1BC);
TABTS3VK=positif(GLDGRATC+CARTSC+REMPLAC+CODDBJ+CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C) * arr(TRE[DGFIP][2017]0C*(BPCOSAC)/TTSBC)
        + (1-positif(GLDGRATC+CARTSC+REMPLAC+CODDBJ+CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C)) * max(0,TRE[DGFIP][2017]0C-TABTS1BJ-TABTS1BC-TABTS1BP);
TABTS1UT=positif(CARTSC+REMPLAC+CODDBJ+CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C) * arr(TRE[DGFIP][2017]0C*(GLDGRATC)/TTSBC)
        + (1-positif(CARTSC+REMPLAC+CODDBJ+CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C)) * max(0,TRE[DGFIP][2017]0C-TABTS1BJ-TABTS1BC-TABTS1BP-TABTS3VK);
TABTSRBJ=positif(REMPLAC+CODDBJ+CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C) * arr(TRE[DGFIP][2017]0C*(CARTSC)/TTSBC)
        + (1-positif(REMPLAC+CODDBJ+CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C)) * max(0,TRE[DGFIP][2017]0C-TABTS1BJ-TABTS1BC-TABTS1BP-TABTS3VK-TABTS1UT);
TABTSRBP=positif(CODDBJ+CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C) * arr(TRE[DGFIP][2017]0C*(REMPLAC)/TTSBC)
        + (1-positif(CODDBJ+CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C)) * max(0,TRE[DGFIP][2017]0C-TABTS1BJ-TABTS1BC-TABTS1BP-TABTS3VK-TABTS1UT-TABTSRBJ);
TABTSC = TABTS1BJ + TABTS1BC +TABTS1BP +TABTS3VK +TABTS1UT +TABTSRBJ+TABTSRBP;
regle 99992500:
application : iliad , batch  ;
TABDOMDAJ = positif(CODDAJ) *
           (positif(CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V) * arr(TRE[DGFIP][2017]0V*CODDAJ/TTSBV)
           + (1-positif(CODEAJ+GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V)) * max(0,TRE[DGFIP][2017]0V-TABTSV))+0;
TABDOMEAJ = positif(CODEAJ) *
           (positif(GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V) * arr(TRE[DGFIP][2017]0V*CODEAJ/TTSBV)
           + (1-positif(GL[DGFIP][2017]V+GL[DGFIP][2017]V+GLD3V)) * max(0,TRE[DGFIP][2017]0V-TABTSV-TABDOMDAJ))+0;
TABDOMDBJ = positif(CODDBJ) *
           (positif(CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C) * arr(TRE[DGFIP][2017]0C*CODDBJ/TTSBC)
           + (1-positif(CODEBJ+GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C)) * max(0,TRE[DGFIP][2017]0C-TABTSC))+0;
TABDOMEBJ = positif(CODEBJ) *
           (positif(GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C) * arr(TRE[DGFIP][2017]0C*CODEBJ/TTSBC)
           + (1-positif(GL[DGFIP][2017]C+GL[DGFIP][2017]C+GLD3C)) * max(0,TRE[DGFIP][2017]0C-TABTSC-TABDOMDBJ))+0;
TABGL1V = positif(GL[DGFIP][2017]V) *
           (positif(GL[DGFIP][2017]V+GLD3V) * arr(TRE[DGFIP][2017]0V*GL[DGFIP][2017]V/TTSBV)
           + (1-positif(GL[DGFIP][2017]V+GLD3V)) * max(0,TRE[DGFIP][2017]0V-TABTSV-TABDOMDAJ-TABDOMEAJ))+0;
TABGL1C = positif(GL[DGFIP][2017]C) *
           (positif(GL[DGFIP][2017]C+GLD3C) * arr(TRE[DGFIP][2017]0C*GL[DGFIP][2017]C/TTSBC)
           + (1-positif(GL[DGFIP][2017]C+GLD3C)) * max(0,TRE[DGFIP][2017]0C-TABTSC-TABDOMDBJ-TABDOMEBJ))+0;
TABGL2V = positif(GL[DGFIP][2017]V) *
           (positif(GLD3V) * arr(TRE[DGFIP][2017]0V*GL[DGFIP][2017]V/TTSBV)
           + (1-positif(GLD3V)) * max(0,TRE[DGFIP][2017]0V-TABTSV-TABDOMDAJ-TABDOMEAJ-TABGL1V))+0;
TABGL2C = positif(GL[DGFIP][2017]C) *
           (positif(GLD3C) * arr(TRE[DGFIP][2017]0C*GL[DGFIP][2017]C/TTSBC)
           + (1-positif(GLD3C)) * max(0,TRE[DGFIP][2017]0C-TABTSC-TABDOMDBJ-TABDOMEBJ-TABGL1C))+0;
TABGL3V = positif(GLD3V) * max(0,TRE[DGFIP][2017]0V-TABTSV-TABDOMDAJ-TABDOMEAJ-TABGL1V-TABGL2V)+0;
TABGL3C = positif(GLD3C) * max(0,TRE[DGFIP][2017]0C-TABTSC-TABDOMDBJ-TABDOMEBJ-TABGL1C-TABGL2C)+0;
TABTS1CJ=arr(TRE[DGFIP][2017]01*(TSHALLO1)/TTSB1);
TABTS1CC=positif(ALLO1+CARTS[DGFIP][2017]+REMPLA[DGFIP][2017]) * arr(TRE[DGFIP][2017]01*(max(0,SALEXT1-CO[DGFIP][2017]CD))/TTSB1)
        + (1-positif(ALLO1+CARTS[DGFIP][2017]+REMPLA[DGFIP][2017])) * max(0,TRE[DGFIP][2017]01-TABTS1CJ);
TABTS1CP=positif(CARTS[DGFIP][2017]+REMPLA[DGFIP][2017]) * arr(TRE[DGFIP][2017]01*(ALLO1)/TTSB1)
        + (1-positif(CARTS[DGFIP][2017]+REMPLA[DGFIP][2017])) * max(0,TRE[DGFIP][2017]01-TABTS1CJ-TABTS1CC);
TABTSRCJ=positif(REMPLA[DGFIP][2017]) * arr(TRE[DGFIP][2017]01*(CARTS[DGFIP][2017])/TTSB1)
        + (1-positif(REMPLA[DGFIP][2017])) * max(0,TRE[DGFIP][2017]01-TABTS1CJ-TABTS1CC-TABTS1CP);
TABTSRCP=max(0,TRE[DGFIP][2017]01 -TABTS1CJ  -TABTS1CC-TABTS1CP -TABTSRCJ);
TABTS1DJ=arr(TRE[DGFIP][2017]02*(TSHALLO2)/TTSB2);
TABTS1DC=positif(ALLO2+CARTS[DGFIP][2017]+REMPLA[DGFIP][2017]) * arr(TRE[DGFIP][2017]02*(max(0,SALEXT2-CO[DGFIP][2017]DD))/TTSB2)
        + (1-positif(ALLO2+CARTS[DGFIP][2017]+REMPLA[DGFIP][2017])) * max(0,TRE[DGFIP][2017]02-TABTS1DJ);
TABTS1DP=positif(CARTS[DGFIP][2017]+REMPLA[DGFIP][2017]) * arr(TRE[DGFIP][2017]02*(ALLO2)/TTSB2)
        + (1-positif(CARTS[DGFIP][2017]+REMPLA[DGFIP][2017])) * max(0,TRE[DGFIP][2017]02-TABTS1DJ-TABTS1DC);
TABTSRDJ=positif(REMPLA[DGFIP][2017]) * arr(TRE[DGFIP][2017]02*(CARTS[DGFIP][2017])/TTSB2)
        + (1-positif(REMPLA[DGFIP][2017])) * max(0,TRE[DGFIP][2017]02-TABTS1DJ-TABTS1DC-TABTS1DP);
TABTSRDP=max(0,TRE[DGFIP][2017]02- TABTS1DJ  -TABTS1DC-TABTS1DP -TABTSRDJ);
TABTS1EJ=arr(TRE[DGFIP][2017]03*(TSHALLO3)/TTSB3);
TABTS1EC=positif(ALLO3+CARTSP3+REMPLAP3) * arr(TRE[DGFIP][2017]03*(max(0,SALEXT3-CO[DGFIP][2017]ED))/TTSB3)
        + (1-positif(ALLO3+CARTSP3+REMPLAP3)) * max(0,TRE[DGFIP][2017]03-TABTS1EJ);
TABTS1EP=positif(CARTSP3+REMPLAP3) * arr(TRE[DGFIP][2017]03*(ALLO3)/TTSB3)
        + (1-positif(CARTSP3+REMPLAP3)) * max(0,TRE[DGFIP][2017]03-TABTS1EJ-TABTS1EC);
TABTSREJ=positif(REMPLAP3) * arr(TRE[DGFIP][2017]03*(CARTSP3)/TTSB3)
        + (1-positif(REMPLAP3)) * max(0,TRE[DGFIP][2017]03-TABTS1EJ-TABTS1EC-TABTS1EP);
TABTSREP=max(0,TRE[DGFIP][2017]03- TABTS1EJ  -TABTS1EC-TABTS1EP -TABTSREJ);
TABTS1FJ=arr(TRE[DGFIP][2017]04*(TSHALLO4)/TTSB4);
TABTS1FC=positif(ALLO4+CARTSP4+REMPLAP4) * arr(TRE[DGFIP][2017]04*(max(0,SALEXT4-CO[DGFIP][2017]FD))/TTSB4)
        + (1-positif(ALLO4+CARTSP4+REMPLAP4)) * max(0,TRE[DGFIP][2017]04-TABTS1FJ);
TABTS1FP=positif(CARTSP4+REMPLAP4) * arr(TRE[DGFIP][2017]04*(ALLO4)/TTSB4)
        + (1-positif(CARTSP4+REMPLAP4)) * max(0,TRE[DGFIP][2017]04-TABTS1FJ-TABTS1FC);
TABTSRFJ=positif(REMPLAP4) * arr(TRE[DGFIP][2017]04*(CARTSP4)/TTSB4)
        + (1-positif(REMPLAP4)) * max(0,TRE[DGFIP][2017]04-TABTS1FJ-TABTS1FC-TABTS1FP);
TABTSRFP=max(0,TRE[DGFIP][2017]04 - TABTS1FJ  -TABTS1FC-TABTS1FP -TABTSRFJ);
regle 99992600:
application : iliad , batch  ;
TABGLTV = somme (x=1..3: TABGLxV)+TABDOMDAJ + TABDOMEAJ;
TABGLTC = somme (x=1..3: TABGLxC)+TABDOMDBJ + TABDOMEBJ;
regle 899999999:
application : iliad , batch  ;
TTSN1AJ = TSHALLOV - TABTS1AJ;
TTSN1AC = max(0,SALEXTV-CO[DGFIP][2017]AD)- TABTS1AC;
TTSN1AP = ALLOV - TABTS1AP;
TTSN3VJ = BPCOSAV - TABTS3VJ;
TTSN1TT = GLDGRATV - TABTS1TT;
TTSNRAJ = (CARTSV - TABTSRAJ) ;
TTSNRAP = (REMPLAV - TABTSRAP);
TTSNDAJ = (CODDAJ - TABDOMDAJ);
TTSNEAJ = (CODEAJ - TABDOMEAJ);
TTSNGL1V = (GL[DGFIP][2017]V - TABGL1V);
TTSNGL2V = (GL[DGFIP][2017]V - TABGL2V);
TTSNGL3V = (GLD3V - TABGL3V);
TTSN1BJ = TSHALLOC - TABTS1BJ;
TTSN1BC = max(0,SALEXTC-CO[DGFIP][2017]BD)- TABTS1BC;
TTSN1BP = ALLOC - TABTS1BP;
TTSN3VK = BPCOSAC - TABTS3VK;
TTSN1UT = GLDGRATC - TABTS1UT;
TTSNRBJ = (CARTSC - TABTSRBJ);
TTSNRBP = (REMPLAC - TABTSRBP);
TTSNDBJ = (CODDBJ - TABDOMDBJ);
TTSNEBJ = (CODEBJ - TABDOMEBJ);
TTSNGL1C = (GL[DGFIP][2017]C - TABGL1C);
TTSNGL2C = (GL[DGFIP][2017]C - TABGL2C);
TTSNGL3C = (GLD3C - TABGL3C);
TTSN1CJ = TSHALLO1 - TABTS1CJ;
TTSN1CC = max(0,SALEXT1-CO[DGFIP][2017]CD)- TABTS1CC;
TTSN1CP = ALLO1 - TABTS1CP;
TTSNRCJ = (CARTS[DGFIP][2017] - TABTSRCJ);
TTSNRCP = (REMPLA[DGFIP][2017] - TABTSRCP);
TTSN1DJ = TSHALLO2 - TABTS1DJ;
TTSN1DC = max(0,SALEXT2-CO[DGFIP][2017]DD)- TABTS1DC;
TTSN1DP = ALLO2 - TABTS1DP;
TTSNRDJ = (CARTS[DGFIP][2017] - TABTSRDJ);
TTSNRDP = (REMPLA[DGFIP][2017] - TABTSRDP);
TTSN1EJ = TSHALLO3 - TABTS1EJ;
TTSN1EC = max(0,SALEXT3-CO[DGFIP][2017]ED)- TABTS1EC;
TTSN1EP = ALLO3 - TABTS1EP;
TTSNREJ = (CARTSP3 - TABTSREJ);
TTSNREP = (REMPLAP3 - TABTSREP);
TTSN1FJ = TSHALLO4 - TABTS1FJ;
TTSN1FC = max(0,SALEXT4-CO[DGFIP][2017]FD)- TABTS1FC;
TTSN1FP = ALLO4 - TABTS1FP;
TTSNRFJ = (CARTSP4 - TABTSRFJ);
TTSNRFP = (REMPLAP4 - TABTSRFP);

CUMSALEXTEF = TTSN1AC + TTSN1BC + TTSN1CC + TTSN1DC + TTSN1EC + TTSN1FC ;
regle 99992700:
application : iliad , batch  ;
pour i = V,C,1,2,3,4:
TPLRi = min ( MIN_DEDPR , TEXPRi );
pour i = V,C,1,2,3,4:
TAPBi = max( TPLRi , (TEXPRi*TX_DEDPER/100));
pour i = V,C,1,2,3,4:
TIND_APBi = positif_ou_nul(TPLRi- (TEXPRi * TX_DEDPER/100));
TPL_PB = arr(PLAF_DEDPRFOYER -somme (i=V,C,1..4: TAPBi * TIND_APBi));
regle 99992800:
application : iliad , batch  ;
TABPRV = arr ( (1 - TIND_APBV) * 
             min(TAPBV,(TPL_PB * TAPBV / somme(x=V,C,1..4:TAPBx * (1 - TIND_APBx))))
           + TIND_APBV * TAPBV );
TABPRC = arr ( (1 - TIND_APBC) * 
             min(TAPBC,
                       positif(TEXPR1+TEXPR2+TEXPR3+TEXPR4)*(TPL_PB * TAPBC / somme(x=V,C,1..4:TAPBx * (1 - TIND_APBx)))
                     + (1-positif(TEXPR1+TEXPR2+TEXPR3+TEXPR4))* (max(0,TPL_PB - TABPRV)))
           + TIND_APBC * TAPBC );
TABPR1 = arr ( (1 - TIND_APB1) * 
             min(TAPB1,
                       positif(TEXPR2+TEXPR3+TEXPR4)*(TPL_PB * TAPB1 / somme(x=V,C,1..4:TAPBx * (1 - TIND_APBx)))
                     + (1-positif(TEXPR2+TEXPR3+TEXPR4))* (max(0,TPL_PB - TABPRV-TABPRC)))
           + TIND_APB1 * TAPB1 );
TABPR2 = arr ( (1 - TIND_APB2) * 
             min(TAPB2,
                       positif(TEXPR3+TEXPR4)*(TPL_PB * TAPB2 / somme(x=V,C,1..4:TAPBx * (1 - TIND_APBx)))
                     + (1-positif(TEXPR3+TEXPR4))* (max(0,TPL_PB - TABPRV-TABPRC-TABPR1)))
           + TIND_APB2 * TAPB2 );
TABPR3 = arr ( (1 - TIND_APB3) * 
             min(TAPB3,
                       positif(TEXPR4)*(TPL_PB * TAPB3 / somme(x=V,C,1..4:TAPBx * (1 - TIND_APBx)))
                     + (1-positif(TEXPR4))* (max(0,TPL_PB - TABPRV-TABPRC-TABPR1-TABPR2)))
           + TIND_APB3 * TAPB3 );
TABPR4 = arr ( (1 - TIND_APB4) * 
             min(TAPB4,(max(0,TPL_PB -TABPRV-TABPRC-TABPR1-TABPR2-TABPR3)))
           + TIND_APB4 * TAPB4 );
regle 99992900:
application : iliad , batch  ;
TAPRV  =  TIND_APBV * TABPRV 
       + (1-TIND_APBV)* min ( TABPRV , TPL_PB); 
TAPRC  =  TIND_APBC * TABPRC 
       + (1-TIND_APBC)* min ( TABPRC , TPL_PB - (1-TIND_APBV)*TAPRV ); 
TAPR1  =  TIND_APB1 * TABPR1 
       + (1-TIND_APB1)* min ( TABPR1 , TPL_PB - (1-TIND_APBV)*TAPRV 
			- (1-TIND_APBC)*TAPRC);
TAPR2  =  TIND_APB2 * TABPR2
       + (1-TIND_APB2)* min ( TABPR2 , TPL_PB - (1-TIND_APBV)*TAPRV 
                       - (1-TIND_APBC)*TAPRC - (1-TIND_APB1)*TAPR1 ); 
TAPR3  =  TIND_APB3 * TABPR3
       + (1-TIND_APB3)* min ( TABPR3 , TPL_PB - (1-TIND_APBV)*TAPRV 
                       - (1-TIND_APBC)*TAPRC - (1-TIND_APB1)*TAPR1  
                       - (1-TIND_APB2)*TAPR2 ); 
TAPR4  =  TIND_APB4 * TABPR4 
       + (1-TIND_APB4)* min ( TABPR4 , TPL_PB - (1-TIND_APBV)*TAPRV 
                       - (1-TIND_APBC)*TAPRC - (1-TIND_APB1)*TAPR1  
                       - (1-TIND_APB2)*TAPR2 - (1-TIND_APB3)*TAPR3 ); 
regle 99992110:
application : iliad , batch  ;
pour i = V,C,1,2,3,4:
TPRNNi = TEXPRi - TAPRi;
regle 99992120:
application : iliad , batch  ;
TTSNTV =  TTSN1AJ+TTSN1AC+TTSN1AP+TTSN3VJ+TTSN1TT+TTSNRAJ+TTSNRAP
        +TTSNDAJ+TTSNEAJ+TTSNGL1V+TTSNGL2V+TTSNGL3V ;
TTSNTC = TTSN1BJ+TTSN1BC+TTSN1BP+TTSN3VK+TTSN1UT+TTSNRBJ+TTSNRBP
       +TTSNDBJ+TTSNEBJ+TTSNGL1C+TTSNGL2C+TTSNGL3C ;
TTSNT1 =  TTSN1CJ +TTSN1CC+ TTSN1CP + TTSNRCJ + TTSNRCP;
TTSNT2 =  TTSN1DJ +TTSN1DC+ TTSN1DP + TTSNRDJ + TTSNRDP;
TTSNT3 =  TTSN1EJ+TTSN1EC+ TTSN1EP+ TTSNREJ+ TTSNREP ;
TTSNT4 =  TTSN1FJ+TTSN1FC+ TTSN1FP+ TTSNRFJ+ TTSNRFP ;
regle 99992130:
application : iliad , batch  ;
pour i =V,C,1,2,3,4:
TTSNi = positif (-TTSNTi) * min (0 , TTSNTi + TPRNNi)
     + positif_ou_nul (TTSNTi) * TTSNTi;
pour i =V,C,1,2,3,4:
TPRNi = positif (-TTSNTi) * positif (TTSNTi + TPRNNi) * (TTSNTi + TPRNNi)
       + positif_ou_nul (TTSNTi) * TPRNNi;

regle 99992210:
application : iliad , batch  ;
pour i = V,C;x=1..3:
TGLNAVxi = max (GLDxi - TABGLxi,0);
TGLDOMAVDAJV = max (CODDAJ - TABDOMDAJ,0);
TGLDOMAVEAJV = max (CODEAJ - TABDOMEAJ,0);
TGLDOMAVDBJC = max (CODDBJ - TABDOMDBJ,0);
TGLDOMAVEBJC = max (CODEBJ - TABDOMEBJ,0);
TGLN1V = max (GL[DGFIP][2017]V - TABGL1V,0);
TGLN2V = max (GL[DGFIP][2017]V - TABGL2V,0);
TGLN3V = max (GLD3V - TABGL3V,0);
TGLN4V = max(CODDAJ - TABDOMDAJ,0)+max(CODEAJ - TABDOMEAJ,0);
TGLN1C = max (GL[DGFIP][2017]C - TABGL1C,0);
TGLN2C = 
