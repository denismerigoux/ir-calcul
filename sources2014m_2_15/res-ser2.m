#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2015 
#au titre des revenus perçus en 2014. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************

 #
 #
 #
 # #####   ######   ####    #####     #     #####  
 # #    #  #       #          #       #       #   
 # #    #  #####    ####      #       #       #  
 # #####   #            #     #       #       # 
 # #   #   #       #    #     #       #       # 
 # #    #  ######   ####      #       #       # 
 #
 #      #####   #####   #####   #
 #          #   #   #   #   #   #
 #      #####   #   #   #   #   #
 #      #       #   #   #   #   #
 #      #####   #####   #####   #
 #
 #
 #
 #
 #                     RES-SER2.m
 #                    =============
 #
 #
 #                      zones restituees par l'application
 #
 #
regle 9071 :
application : iliad , batch ;
IDRS = INDTXMIN*IMI + 
       INDTXMOY*IMO + 
       (1-INDTXMIN) * (1-INDTXMOY) * max(0,IPHQ2 - ADO1) ;
regle 907100 :
application : iliad , batch, bareme ;
RECOMP = max(0 ,( IPHQANT2 - IPHQ2 )*(1-INDTXMIN) * (1-INDTXMOY)) 
         * (1 - positif(IPMOND+INDTEFF)) ;
regle 907101 :
application : iliad , batch ;
IDRSANT = INDTXMIN*IMI + INDTXMOY*IMO 
         + (1-INDTXMIN) * (1-INDTXMOY) * max(0,IPHQANT2 - ADO1) ;
IDRS2 = (1 - positif(IPMOND+INDTEFF))  * 
        ( 
         IDRSANT + ( positif(ABADO)*ABADO + positif(ABAGU)*ABAGU )
                  * positif(IDRSANT)
         + IPHQANT2 * (1 - positif(IDRSANT))
         + positif(RE168+TAX1649) * IAM[DGFIP][2017]
        )
   + positif(IPMOND+INDTEFF) 
         * ( IDRS*(1-positif(IPHQ2)) + IPHQ2 * positif(IPHQ2) );

IDRS3 = IDRT ;
regle 90710 :
application : iliad , batch ;
PLAFQF = positif(IS521 - PLANT - IS511) * ( positif(abs(TEFF)) * positif(IDRS) + (1 - positif(abs(TEFF))) );
regle 907105 :
application : iliad , batch ;
REVMETRO = max(0,RG - PRODOM - PROGUY);
regle 90711 :
application : iliad , batch ;

RGPAR =   positif(positif(PRODOM)+positif(CODDAJ)+positif(CODDBJ)) * 1 
       +  positif(positif(PROGUY)+positif(CODEAJ)+positif(CODEBJ)) * 2
       +  positif(positif(PROGUY)+positif(CODEAJ)+positif(CODEBJ))*positif(positif(PRODOM)+positif(CODDAJ)+positif(CODDBJ)) 
       ;

regle 9074 :
application : iliad , batch ;
IBAEX = (IPQT2) * (1 - INDTXMIN) * (1 - INDTXMOY);
regle 9080 :
application : iliad , batch ;

PRELIB = PPLIB + RCMLIB ;

regle 9091 :
application : iliad , batch ;
IDEC = DEC11 * (1 - positif(V_CR2 + V_CNR + IPVLOC));
regle 9092 :
application : iliad , batch ;
IPROP = ITP ;
regle 9093 :
application : iliad , batch ;

IREP = REI ;

regle 90981 :
application : batch, iliad ;
RETIR = RETIR2 + arr(BTOINR * TXINT/100) ;

RETTAXA = RETTAXA2 + arr(max(0,TAXASSUR-TAXA9YI- min(TAXASSUR+0,max(0,INE-IRB+AVFISCOPTER))+min(0,IRN - IRANT)) * TXINT/100) ;
RETPCAP = RETPCA[DGFIP][2017]+arr(max(0,IPCAPTAXT-CAP9YI- min(IPCAPTAXT+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR))+min(0,IRN - IRANT+TAXASSUR)) * TXINT/100) ;
RETLOY = RETLOY2+arr(max(0,TAXLOY-LOY9YI- min(TAXLOY+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR-IPCAPTAXT))
                           +min(0,IRN - IRANT+TAXASSUR+IPCAPTAXT)) * TXINT/100) ;
RETHAUTREV = RETCHR2 + arr(max(0,IHAUTREVT-CHR9YI+min(0,IRN - IRANT+TAXASSUR+IPCAPTAXT+TAXLOY)) * TXINT/100) ;

RETCS = RETCS2 + arr(max(0, CSG-CS9YP-CSGIM) * TXINT/100)* positif_ou_nul(CSTOTSSPENA - SEUIL_61) ;

RETRD = RETR[DGFIP][2017] + arr(max(0, RDSN-RD9YP-CRDSIM) * TXINT/100) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

RETPS = RETPS2 + arr(max(0, PRS-PS9YP-PRSPROV) * TXINT/100) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

RETCVN = RETCVN2 + arr(max(0, CVNSALC-CVN9YP - COD8YT) * TXINT/100) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);
RETREGV = RETREGV2 + arr(max(0, BREGV-REGV9YP) * TXINT/100) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

RETCDIS = RETCDIS2 + arr(max(0, CDIS-CDIS9YP - CDISPROV) * TXINT/100) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

RETGLOA = RETGLOA2 + arr(max(0, CGLOA-GLO9YP-COD8YL) * TXINT/100) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

RETRSE1 = RETRSE12 + arr(max(0, RSE1N-RSE19YP-CSPROVYD) * TXINT/100) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

RETRSE2 = RETRSE22 + arr((max(0, max(0, RSE8TV - CIRSE8TV - CSPROVYF) + max(0, RSE8SA -CIRSE8SA - CSPROVYN) - RSE29YP)) * TXINT/100
                        ) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

RETRSE3 = RETRSE32 + arr(max(0, RSE3N-RSE39YP-CSPROVYG) * TXINT/100) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

RETRSE4 = RETRSE42 + arr((max(0, max(0, RSE8TX - CIRSE8TX - CSPROVYH) + max(0, RSE8SB -CIRSE8SB - CSPROVYP) - RSE49YP)) * TXINT/100
                        ) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

RETRSE5 = RETRSE52 + arr(max(0, RSE5N-RSE59YP-CSPROVYE) * TXINT/100) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

regle 90984 :
application : batch, iliad ;
MAJOIRTARDIF_A1 = MAJOIRTARDIF_A - MAJOIR17_2TARDIF_A;
MAJOTAXATARDIF_A1 = MAJOTAXATARDIF_A - MAJOTA17_2TARDIF_A;
MAJOCAPTARDIF_A1 = MAJOCAPTARDIF_A - MAJOC[DGFIP][2017]7_2TARDIF_A;
MAJOLOYTARDIF_A1 = MAJOLOYTARDIF_A - MAJOLO17_2TARDIF_A;
MAJOHRTARDIF_A1 = MAJOHRTARDIF_A - MAJOHR17_2TARDIF_A;
MAJOIRTARDIF_[DGFIP][2017] = MAJOIRTARDIF_D - MAJOIR17_2TARDIF_D;
MAJOTAXATARDIF_[DGFIP][2017] = MAJOTAXATARDIF_D - MAJOTA17_2TARDIF_D;
MAJOCAPTARDIF_[DGFIP][2017] = MAJOCAPTARDIF_D - MAJOC[DGFIP][2017]7_2TARDIF_D;
MAJOLOYTARDIF_[DGFIP][2017] = MAJOLOYTARDIF_D - MAJOLO17_2TARDIF_D;
MAJOHRTARDIF_[DGFIP][2017] = MAJOHRTARDIF_D - MAJOHR17_2TARDIF_D;
MAJOIRTARDIF_[DGFIP][2017] = MAJOIRTARDIF_P - MAJOIR17_2TARDIF_P;
MAJOLOYTARDIF_[DGFIP][2017] = MAJOLOYTARDIF_P - MAJOLO17_2TARDIF_P;
MAJOHRTARDIF_[DGFIP][2017] = MAJOHRTARDIF_P - MAJOHR17_2TARDIF_P;
MAJOIRTARDIF_R1 = MAJOIRTARDIF_R - MAJOIR17_2TARDIF_R;
MAJOTAXATARDIF_R1 = MAJOTAXATARDIF_R - MAJOTA17_2TARDIF_R;
MAJOCAPTARDIF_R1 = MAJOCAPTARDIF_R - MAJOC[DGFIP][2017]7_2TARDIF_R;
MAJOLOYTARDIF_R1 = MAJOLOYTARDIF_R - MAJOLO17_2TARDIF_R;
MAJOHRTARDIF_R1 = MAJOHRTARDIF_R - MAJOHR17_2TARDIF_R;
NMAJ1 = max(0,MAJO1728IR + arr(BTO * COPETO/100)  
		+ FLAG_TRTARDIF * MAJOIRTARDIF_[DGFIP][2017]
		+ FLAG_TRTARDIF_F 
		* (positif(PROPIR_A) * MAJOIRTARDIF_[DGFIP][2017]
		  + (1 - positif(PROPIR_A) ) * MAJOIRTARDIF_[DGFIP][2017])
		- FLAG_TRTARDIF_F * (1 - positif(PROPIR_A))
				    * ( positif(FLAG_RECTIF) * MAJOIRTARDIF_R1
				     + (1 - positif(FLAG_RECTIF)) * MAJOIRTARDIF_A1)
		);
NMAJTAXA1 = max(0,MAJO1728TAXA + arr(max(0,TAXASSUR- min(TAXASSUR+0,max(0,INE-IRB+AVFISCOPTER))+min(0,IRN-IRANT)) * COPETO/100)  
		+ FLAG_TRTARDIF * MAJOTAXATARDIF_[DGFIP][2017]
		+ FLAG_TRTARDIF_F * MAJOTAXATARDIF_[DGFIP][2017]
        	- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOTAXATARDIF_R1
				     + (1 - positif(FLAG_RECTIF)) * MAJOTAXATARDIF_A1)
		);
NMAJPCA[DGFIP][2017] = max(0,MAJO1728PCAP + arr(max(0,IPCAPTAXT- min(IPCAPTAXT+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR))+min(0,IRN-IRANT+TAXASSUR)) * COPETO/100)
                + FLAG_TRTARDIF * MAJOCAPTARDIF_[DGFIP][2017]
                + FLAG_TRTARDIF_F * MAJOCAPTARDIF_[DGFIP][2017]
                - FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOCAPTARDIF_R1
                + (1 - positif(FLAG_RECTIF)) * MAJOCAPTARDIF_A1)
                );
NMAJLOY1 = max(0,MAJO1728LOY + arr(max(0,TAXLOY- min(TAXLOY+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR-IPCAPTAXT))+min(0,IRN-IRANT+TAXASSUR+IPCAPTAXT)) * COPETO/100)
                + FLAG_TRTARDIF * MAJOLOYTARDIF_[DGFIP][2017]
		+ FLAG_TRTARDIF_F 
		* (positif(PROPLOY_A) * MAJOLOYTARDIF_[DGFIP][2017]
		  + (1 - positif(PROPLOY_A) ) * MAJOLOYTARDIF_[DGFIP][2017])
		- FLAG_TRTARDIF_F * (1 - positif(PROPLOY_A))
				    * ( positif(FLAG_RECTIF) * MAJOLOYTARDIF_R1
				     + (1 - positif(FLAG_RECTIF)) * MAJOLOYTARDIF_A1)

                );
NMAJCHR1 = max(0,MAJO1728CHR + arr(max(0,IHAUTREVT+min(0,IRN-IRANT+TAXASSUR+IPCAPTAXT+TAXLOY)) * COPETO/100)
                + FLAG_TRTARDIF * MAJOHRTARDIF_[DGFIP][2017]
		+ FLAG_TRTARDIF_F 
		* (positif(PROPIR_A) * MAJOHRTARDIF_[DGFIP][2017]
		  + (1 - positif(PROPIR_A) ) * MAJOHRTARDIF_[DGFIP][2017])
		- FLAG_TRTARDIF_F * (1 - positif(PROPIR_A))
				    * ( positif(FLAG_RECTIF) * MAJOHRTARDIF_R1
				     + (1 - positif(FLAG_RECTIF)) * MAJOHRTARDIF_A1)
                );
NMAJC1 = max(0,MAJO1728CS + arr((CSG - CSGIM) * COPETO/100)  
		+ FLAG_TRTARDIF * MAJOCSTARDIF_D
		+ FLAG_TRTARDIF_F 
		* (positif(PROPCS_A) * MAJOCSTARDIF_P 
		  + (1 - positif(PROPCS_A) ) * MAJOCSTARDIF_D)
		- FLAG_TRTARDIF_F * (1 - positif(PROPCS_A))
				    * ( positif(FLAG_RECTIF) * MAJOCSTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOCSTARDIF_A)
		) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);
NMAJR1 = max(0,MAJO1728RD + arr((RDSN - CRDSIM) * COPETO/100) 
		+ FLAG_TRTARDIF * MAJORDTARDIF_D
		+ FLAG_TRTARDIF_F 
		* (positif(PROPRD_A) * MAJORDTARDIF_P 
		  + (1 - positif(PROPRD_A) ) * MAJORDTARDIF_D)
		- FLAG_TRTARDIF_F * (1 - positif(PROPCS_A))
				    * ( positif(FLAG_RECTIF) * MAJORDTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORDTARDIF_A)
		) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);
NMAJ[DGFIP][2017] = max(0,MAJO1728PS + arr((PRS - PRSPROV) * COPETO/100)  
		+ FLAG_TRTARDIF * MAJOPSTARDIF_D
		+ FLAG_TRTARDIF_F 
		* (positif(PROPPS_A) * MAJOPSTARDIF_P 
		  + (1 - positif(PROPPS_A) ) * MAJOPSTARDIF_D)
		- FLAG_TRTARDIF_F * (1 - positif(PROPPS_A))
				    * ( positif(FLAG_RECTIF) * MAJOPSTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOPSTARDIF_A)
		) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

NMAJCVN1 = max(0,MAJO1728CVN + arr((CVNSALC - COD8YT) * COPETO/100)
		+ FLAG_TRTARDIF * MAJOCVNTARDIF_D
		+ FLAG_TRTARDIF_F  * MAJOCVNTARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOCVNTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOCVNTARDIF_A)
		) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

NMAJREGV1 = max(0,MAJO1728REGV + arr(BREGV * COPETO/100)) * (1 - V_CNR) ;


NMAJCDIS1 = max(0,MAJO1728CDIS + arr((CDIS - CDISPROV) * COPETO/100)  * (1 - V_CNR)
		+ FLAG_TRTARDIF * MAJOCDISTARDIF_D
		+ FLAG_TRTARDIF_F  * MAJOCDISTARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOCDISTARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOCDISTARDIF_A)
		) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

NMAJGLO1 = max(0,MAJO1728GLO + arr((CGLOA-COD8YL) * COPETO/100)
                + FLAG_TRTARDIF * MAJOGLOTARDIF_D
                + FLAG_TRTARDIF_F  * MAJOGLOTARDIF_D
                - FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOGLOTARDIF_R
                                     + (1 - positif(FLAG_RECTIF)) * MAJOGLOTARDIF_A)
              ) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

NMAJRSE11 = max(0,MAJO1728RSE1 + arr((RSE1N - CSPROVYD) * COPETO/100)  
		+ FLAG_TRTARDIF * MAJORSE1TARDIF_D
		+ FLAG_TRTARDIF_F  * MAJORSE1TARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJORSE1TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORSE1TARDIF_A)
		) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

NMAJRSE21 = max(0,MAJO1728RSE2 + arr(( max(0, RSE8TV - CIRSE8TV - CSPROVYF) + max(0, RSE8SA -CIRSE8SA - CSPROVYN )) * COPETO/100) * (1 - V_CNR)
		+ FLAG_TRTARDIF * MAJORSE2TARDIF_D
		+ FLAG_TRTARDIF_F  * MAJORSE2TARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJORSE2TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORSE2TARDIF_A)
		) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

NMAJRSE31 = max(0,MAJO1728RSE3 + arr((RSE3N - CSPROVYG)* COPETO/100) 
		+ FLAG_TRTARDIF * MAJORSE3TARDIF_D
		+ FLAG_TRTARDIF_F  * MAJORSE3TARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJORSE3TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORSE3TARDIF_A)
		) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

NMAJRSE41 = max(0,MAJO1728RSE4 + arr((max(0, RSE8TX - CIRSE8TX - CSPROVYH) + max(0, RSE8SB -CIRSE8SB - CSPROVYP)) * COPETO/100) 
		+ FLAG_TRTARDIF * MAJORSE4TARDIF_D
		+ FLAG_TRTARDIF_F  * MAJORSE4TARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJORSE4TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORSE4TARDIF_A)
		) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

NMAJRSE51 = max(0,MAJO1728RSE5 + arr((RSE5N - CSPROVYE) * COPETO/100) 
		+ FLAG_TRTARDIF * MAJORSE5TARDIF_D
		+ FLAG_TRTARDIF_F  * MAJORSE5TARDIF_D
		- FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJORSE5TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJORSE5TARDIF_A)
		) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);


NMAJ3 = max(0,MAJO1758AIR + arr(BTO * COPETO/100) * positif(null(CMAJ-10)+null(CMAJ-17)) 
		+ FLAG_TRTARDIF * MAJOIR17_2TARDIF_D
		+ FLAG_TRTARDIF_F 
		* (positif(PROPIR_A) * MAJOIR17_2TARDIF_P 
		  + (1 - positif(PROPIR_A) ) * MAJOIR17_2TARDIF_D)
		- FLAG_TRTARDIF_F * (1 - positif(PROPIR_A))
				    * ( positif(FLAG_RECTIF) * MAJOIR17_2TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOIR17_2TARDIF_A)
		);
NMAJTAXA3 = max(0,MAJO1758ATAXA + arr(max(0,TAXASSUR+min(0,IRN-IRANT)) * COPETO/100)
					* positif(null(CMAJ-10)+null(CMAJ-17)) 
		+ FLAG_TRTARDIF * MAJOTA17_2TARDIF_D
		);
NMAJPCAP3 = max(0,MAJO1758APCAP + arr(max(0,IPCAPTAXT+min(0,IRN-IRANT+TAXASSUR)) * COPETO/100)
                * positif(null(CMAJ-10)+null(CMAJ-17))
                + FLAG_TRTARDIF * MAJOC[DGFIP][2017]7_2TARDIF_D
		);
NMAJLOY3 = max(0,MAJO1758ALOY + arr(max(0,TAXLOY+min(0,IRN-IRANT+TAXASSUR+IPCAPTAXT)) * COPETO/100) * positif(null(CMAJ-10)+null(CMAJ-17)) 
		+ FLAG_TRTARDIF * MAJOLO17_2TARDIF_D
		+ FLAG_TRTARDIF_F 
		* (positif(PROPLOY_A) * MAJOLO17_2TARDIF_P 
		  + (1 - positif(PROPLOY_A) ) * MAJOLO17_2TARDIF_D)
		- FLAG_TRTARDIF_F * (1 - positif(PROPLOY_A))
				    * ( positif(FLAG_RECTIF) * MAJOLO17_2TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOLO17_2TARDIF_A)
		);

NMAJCHR3 = max(0,MAJO1758ACHR + arr(max(0,IHAUTREVT+min(0,IRN-IRANT+TAXASSUR+IPCAPTAXT+TAXLOY)) * COPETO/100) * positif(null(CMAJ-10)+null(CMAJ-17)) 
		+ FLAG_TRTARDIF * MAJOHR17_2TARDIF_D
		+ FLAG_TRTARDIF_F 
		* (positif(PROPHR_A) * MAJOHR17_2TARDIF_P 
		  + (1 - positif(PROPHR_A) ) * MAJOHR17_2TARDIF_D)
		- FLAG_TRTARDIF_F * (1 - positif(PROPHR_A))
				    * ( positif(FLAG_RECTIF) * MAJOHR17_2TARDIF_R
				     + (1 - positif(FLAG_RECTIF)) * MAJOHR17_2TARDIF_A)
		);

NMAJ4    =      somme (i=03..06,30,32,55: MAJOIRi);
NMAJTAXA4  =    somme (i=03..06,55: MAJOTAXAi);
NMAJPCAP4 =  somme(i=03..06,55:MAJOCAPi);
NMAJLOY4 = somme(i=03..06,55:MAJOLOYi);
NMAJCHR4 =  somme(i=03..06,30,32,55:MAJOHRi);

NMAJC4 =  somme(i=03..06,30,32,55:MAJOCSi) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);
NMAJR4 =  somme(i=03..06,30,32,55:MAJORDi) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);
NMAJP4 =  somme(i=03..06,30,32,55:MAJOPSi) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);
NMAJCVN4 =  somme(i=03..06,55:MAJOCVNi) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);
NMAJCDIS4 =  somme(i=03..06,55:MAJOCDISi) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);
NMAJGLO4 =  somme(i=03..06,55:MAJOGLOi) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);
NMAJRSE14 =  somme(i=03..06,55:MAJORSE1i) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);
NMAJRSE24 =  somme(i=03..06,55:MAJORSE2i) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);
NMAJRSE34 =  somme(i=03..06,55:MAJORSE3i) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);
NMAJRSE44 =  somme(i=03..06,55:MAJORSE4i) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);
NMAJRSE54 =  somme(i=03..06,55:MAJORSE5i) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);
regle isf 9094 :
application : batch, iliad ;
MAJOISFTARDIF_A1 = MAJOISFTARDIF_A - MAJOIS[DGFIP][2017]7TARDIF_A;
MAJOISFTARDIF_[DGFIP][2017] = MAJOISFTARDIF_D - MAJOIS[DGFIP][2017]7TARDIF_D;
MAJOISFTARDIF_R1 = MAJOISFTARDIF_R - MAJOIS[DGFIP][2017]7TARDIF_R;
NMAJIS[DGFIP][2017]BIS = max(0,MAJO1728ISF + arr(ISF4BASE * COPETO/100)
                   + FLAG_TRTARDIF * MAJOISFTARDIF_D
                   + FLAG_TRTARDIF_F * MAJOISFTARDIF_D
                   - FLAG_TRTARDIF_F * ( positif(FLAG_RECTIF) * MAJOISFTARDIF_R
					 + (1 - positif(FLAG_RECTIF)) * MAJOISFTARDIF_A)
                 );
regle 90101 :
application : iliad , batch ;

IAVIM = IRB + PTOT + TAXASSUR + PTAXA + IPCAPTAXTOT + PPCAP + TAXLOY + PTAXLOY + CHRAPRES + PHAUTREV ;

IAVIM2 = IRB + PTOT ;

regle 90113 :
application : iliad , batch ;
CDBA = positif_ou_nul(SEUIL_IMPDEFBA-SHBA-(REVTP-BA1)
      -REVQTOTQHT);
AGRBG = SHBA + (REVTP-BA1) + REVQTOTQHT ;

regle 901130 :
application : iliad , batch ;
DBAIP =  TOTDAGRI;

regle 901131 :
application : iliad , batch ;

RBAT = max (0 , BANOR) ;

regle 901132 :
application : iliad , batch ;
DEFIBA = (min(max(1+SEUIL_IMPDEFBA-SHBA-(REVTP-BA1)
      -REVQTOTQHT,0),1)) * min( 0 , BANOR ) ;
regle 901133 :
application :  iliad, batch ;
NAPALEG = abs(NAPT) ;

INDNAP = 1 - positif_ou_nul(NAPT) ;

GAINDBLELIQ = max(0 , V_ANC_NAP - NAPT) * positif(null(MESGOUV - 1) + null(MESGOUV - 2) + null(MESGOUV2 - 4) + null(MESGOUV2 - 5)) ;


GAINPOURCLIQ = (1 - null(V_ANC_NAP*(1-2*V_IND_NAP))) * (V_ANC_NAP*(1-2*V_IND_NAP) - NAPT)/ V_ANC_NAP*(1-2*V_IND_NAP)  * (1 - V_CNR2);

ANCNAP = V_ANC_NAP * (1-2*V_IND_NAP) ;


INDPPEMENS = positif( ( positif(IRESTIT - 180) 
		       + positif((-1)*ANCNAP - 180) 
                       + positif(IRESTIT - IRNET - 180) * null(V_IND_TRAIT-5)
		      ) * positif(PPETOTX - PPERSA - 180) )
	           * (1 - V_CNR) ;

BASPPEMENS = INDPPEMENS * min(max(IREST,(-1)*ANCNAP*positif((-1)*ANCNAP)),PPETOTX-PPERSA) * null(V_IND_TRAIT-4) 
            + INDPPEMENS * max(0,min(IRESTIT-IRNET,PPETOTX-PPERSA)) * null(V_IND_TRAIT-5) ;

regle 90114 :
application : iliad , batch ;
IINET = max(0, NAPTEMPCX- TOTIRPSANT);
IINETIR = max(0 , NAPTIR) ;

regle 901140 :
application : bareme  ;

IINET = IRNET * positif ( IRNET - SEUIL_61 ) ;

regle 9011410 :
application : bareme , iliad , batch ;
IRNET2 =  (IAR + PIR - IRANT) * (1 - INDTXMIN)  * (1 - INDTXMOY)
         + min(0, IAR + PIR - IRANT) * (INDTXMIN + INDTXMOY)
         + max(0, IAR + PIR - IRANT) *
                                   (INDTXMIN * positif(IAVIMBIS - SEUIL_TXMIN)
                                  + INDTXMOY * positif(IAVIMO - SEUIL_TXMIN))
         ;
regle 901141 :
application : iliad , batch ;

IRNETTER = max ( 0 ,   IRNET2
                       + (TAXASSUR + PTAXA - min(TAXASSUR+PTAXA+0,max(0,INE-IRB+AVFISCOPTER))
                        - max(0,TAXASSUR + PTAXA  - min(TAXASSUR + PTAXA + 0,max(0,INE-IRB+AVFISCOPTER))+ min(0,IRNET2)))
                       + (IPCAPTAXT + PPCAP - min(IPCAPTAXT + PPCAP,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA))
                        - max(0,IPCAPTAXT+PPCAP -min(IPCAPTAXT+PPCAP,max(0,INE-IRB+AVFISCOPTER- TAXASSUR - PTAXA ))+ min(0,TAXANEG)))
                       + (TAXLOY + PTAXLOY - min(TAXLOY + PTAXLOY,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA-IPCAPTAXT-PPCAP))
                        - max(0,TAXLOY+PTAXLOY -min(TAXLOY+PTAXLOY,max(0,INE-IRB+AVFISCOPTER- TAXASSUR - PTAXA-IPCAPTAXT-PPCAP ))+ min(0,PCAPNEG)))
                       + (IHAUTREVT + PHAUTREV - max(0,IHAUTREVT+PHAUTREV + min(0,LOYELEVNEG)))
                )
                       ;
IRNETBIS = max(0 , IRNETTER - PIR * positif(SEUIL_12 - IRNETTER + PIR) 
				  * positif(SEUIL_12 - PIR) 
				  * positif_ou_nul(IRNETTER - SEUIL_12)) ;

regle 901143 :
application : iliad , batch ;
IRNET =  null(NRINET + IMPRET + (RASAR * V_CR2) + 0) * IRNETBIS * positif_ou_nul(IRB - min(max(0,IRB-AVFISCOPTER),INE))
          + positif(NRINET + IMPRET + (RASAR * V_CR2) + 0)
                    *
                    (
                    ((positif(IRE) + positif_ou_nul(IAVIM - SEUIL_61) * (1 - positif(IRE)))
                    *
                    max(0, CHRNEG + NRINET + IMPRET + (RASAR * V_CR2) + (IRNETBIS * positif(positif_ou_nul(IAVIM - SEUIL_61)) 
		                                                                  * positif_ou_nul(IRB - min(max(0,IRB-AVFISCOPTER),INE)))     
                      ) * (1 - positif(IRESTIT)))
                    + ((1 - positif_ou_nul(IAVIM - SEUIL_61)) * (1 - positif(IRE)) * max(0, CHRNEG + NRINET + IMPRET + (RASAR * V_CR2)))
                    ) ;
regle 901144 :
application : iliad , batch ;
TOTNET = max (0,NAPTIR);
regle 9011411 :
application : iliad , batch ;
TAXANEG = min(0 , TAXASSUR + PTAXA - min(TAXASSUR + PTAXA + 0 , max(0,INE-IRB+AVFISCOPTER)) + min(0 , IRNET2)) ;
TAXNET = positif(TAXASSUR)
	  * max(0 , TAXASSUR + PTAXA  - min(TAXASSUR + PTAXA + 0,max(0,INE-IRB+AVFISCOPTER)) + min(0 , IRNET2)) ;
TAXANET = null(NRINET + IMPRET + (RASAR * V_CR2) + 0) * TAXNET
	   + positif(NRINET + IMPRET + (RASAR * V_CR2) + 0)
             * (positif_ou_nul(IAM[DGFIP][2017] - SEUIL_61) * TAXNET + (1 - positif_ou_nul(IAM[DGFIP][2017]  - SEUIL_61)) * 0) ;

regle 90114111 :
application : iliad , batch ;
PCAPNEG =  min(0,IPCAPTAXT+PPCAP -min(IPCAPTAXT+PPCAP,max(0,INE-IRB+AVFISCOPTER- TAXASSUR - PTAXA ))+ min(0,TAXANEG)) ;
PCAPTAXNET = positif(IPCAPTAXT)
                * max(0,IPCAPTAXT+PPCAP -min(IPCAPTAXT+PPCAP,max(0,INE-IRB+AVFISCOPTER- TAXASSUR - PTAXA ))+ min(0,TAXANEG)) ;
PCAPNET = null(NRINET + IMPRET + (RASAR * V_CR2) + 0) * PCAPTAXNET
	   + positif(NRINET + IMPRET + (RASAR * V_CR2) + 0)
			* ( positif_ou_nul(IAM[DGFIP][2017]  - SEUIL_61) * PCAPTAXNET + (1 - positif_ou_nul(IAM[DGFIP][2017] - SEUIL_61)) * 0 ) ;

regle 90114112 :
application : iliad , batch ;
LOYELEVNEG =  min(0,TAXLOY + PTAXLOY -min(TAXLOY + PTAXLOY,max(0,INE-IRB+AVFISCOPTER- TAXASSUR - PTAXA-IPCAPTAXT-PPCAP ))+ min(0,PCAPNEG)) ;
LOYELEVNET = positif(LOYELEV)
                * max(0,TAXLOY+PTAXLOY -min(TAXLOY+PTAXLOY,max(0,INE-IRB+AVFISCOPTER- TAXASSUR - PTAXA-IPCAPTAXT-PPCAP ))+ min(0,PCAPNEG)) ;
TAXLOYNET = null(NRINET + IMPRET + (RASAR * V_CR2) + 0) * LOYELEVNET
                + positif(NRINET + IMPRET + (RASAR * V_CR2) + 0)
                * ( positif_ou_nul(IAM[DGFIP][2017] - SEUIL_61) * LOYELEVNET + (1 - positif_ou_nul(IAM[DGFIP][2017] - SEUIL_61)) * 0 ) ;


regle 901141111 :
application : iliad , batch ;
CHRNEG = min(0 , IHAUTREVT + PHAUTREV + min(0 , LOYELEVNEG)) ;
CHRNET = positif(IHAUTREVT)
                * max(0,IHAUTREVT+PHAUTREV + min(0,LOYELEVNEG))
               ;
HAUTREVNET = (null(NRINET + IMPRET + (RASAR * V_CR2) + 0) * CHRNET
              +
              positif(NRINET + IMPRET + (RASAR * V_CR2) + 0)
              * ( positif_ou_nul(IAM[DGFIP][2017] - SEUIL_61) * CHRNET
              + (1 - positif_ou_nul(IAM[DGFIP][2017] - SEUIL_61)) * 0 )
              ) 
              ;
regle 9011412 :
application : bareme ;

IRNET = max(0 , IRNET2 + RECOMP) ;

regle 9011413 :
application : iliad , batch ;

IRPROV = min (IRANT , IAR + PIR) * positif(IRANT) ;

regle 9012401 :
application : batch , iliad ;
NAPPSAVIM = (PRS + PPRS ) ;
NAPCSAVIM = (CSG + PCSG ) ;
NAPRDAVIM = (RDSN + PRDS) ;
NAPCVNAVIM = (CVNSALC + PCVN) ;
NAPREGVAVIM = (BREGV + PREGV) ;
NAPCDISAVIM = (CDIS + PCDIS) ;
NAPGLOAVIM = (CGLOA + PGLOA-COD8YL) ;
NAPRSE1AVIM = (RSE1N + PRSE1) ;
NAPRSE2AVIM = (RSE2N + PRSE2) ;
NAPRSE3AVIM = (RSE3N + PRSE3) ;
NAPRSE4AVIM = (RSE4N + PRSE4) ;
NAPRSE5AVIM = (RSE5N + PRSE5) ;
NAPCRPAVIM = max(0 , NAPPSAVIM + NAPCSAVIM + NAPRDAVIM + NAPCVNAVIM  + NAPREGVAVIM + NAPCDISAVIM 
                     + NAPGLOAVIM + NAPRSE1AVIM + NAPRSE2AVIM + NAPRSE3AVIM + NAPRSE4AVIM + NAPRSE5AVIM);
NAPCRAC = PRSAC+CSGAC+RDSNAC + CDIS + RSE1N + RSE2N + RSE3N + RSE4N + RSE5N ;
regle 90114010 :
application : batch , iliad ;
NAPCRPIAM[DGFIP][2017] = PRS+CSG+RDSN +CVNSALC + CDIS + CGLOA + RSE1N + RSE2N + RSE3N + RSE4N + RSE5N ;
regle 9011402 :
application : batch , iliad ;
NAPCS      =  positif(SEUIL_61 - VARPS61) * 0 +  (1- positif(SEUIL_61 - VARPS61)) *  CSNET  ;
NAPRD      =  positif(SEUIL_61 - VARPS61) * 0 +  (1- positif(SEUIL_61 - VARPS61)) *  RDNET  ;
NAPPS      =  positif(SEUIL_61 - VARPS61) * 0 +  (1- positif(SEUIL_61 - VARPS61)) *  PRSNET  ;
NAPCVN     =  positif(SEUIL_61 - VARPS61) * 0 +  (1- positif(SEUIL_61 - VARPS61)) *  CVNNET  ;
NAPREGV     =  positif(SEUIL_61 - VARPS61) * 0 +  (1- positif(SEUIL_61 - VARPS61)) * REGVNET  ;
NAPCDIS    =  positif(SEUIL_61 - VARPS61) * 0 +  (1- positif(SEUIL_61 - VARPS61)) *  CDISNET  ;
NAPGLOA    =  positif(SEUIL_61 - VARPS61) * 0 +  (1- positif(SEUIL_61 - VARPS61)) *  CGLOANET  ;
NAPRSE1    =  positif(SEUIL_61 - VARPS61) * 0 +  (1- positif(SEUIL_61 - VARPS61)) *  RSE1NET  ;
NAPRSE2    =  positif(SEUIL_61 - VARPS61) * 0 +  (1- positif(SEUIL_61 - VARPS61)) *  RSE2NET  ;
NAPRSE3    =  positif(SEUIL_61 - VARPS61) * 0 +  (1- positif(SEUIL_61 - VARPS61)) *  RSE3NET  ;
NAPRSE4    =  positif(SEUIL_61 - VARPS61) * 0 +  (1- positif(SEUIL_61 - VARPS61)) *  RSE4NET  ;
NAPRSE5    =  positif(SEUIL_61 - VARPS61) * 0 +  (1- positif(SEUIL_61 - VARPS61)) *  RSE5NET  ;
NAPCR[DGFIP][2017] = max(0 , NAPPS + NAPCS + NAPRD + NAPCVN + NAPCDIS + NAPGLOA + NAPRSE1 + NAPRSE2 + NAPRSE3 + NAPRSE4 + NAPRSE5);
regle 9011407 :
application : iliad , batch ;
IKIRN = KIR ;

IMPTHNET = max(0 , (IRB * positif_ou_nul(IRB-SEUIL_61)-INE-IRE)
		       * positif_ou_nul((IRB*positif_ou_nul(IRB-SEUIL_61)-INE-IRE)-SEUIL_12)) 
	     * (1 - V_CNR) ;

regle 90115 :
application : iliad , batch ;
IRESTIT = abs(min(0 , IRN + PIR + NRINET + IMPRET + RASAR
                    + (TAXASSUR + PTAXA - min(TAXASSUR+PTAXA+0,max(0,INE-IRB+AVFISCOPTER)))
                    + (IPCAPTAXT + PPCAP - min(IPCAPTAXT + PPCAP,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA)))
                    + (TAXLOY + PTAXLOY - min(TAXLOY + PTAXLOY,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA-IPCAPTAXT-PPCAP)))
                    + ((IHAUTREVT + PHAUTREV) 
                      -min((IHAUTREVT + PHAUTREV),max(0,INE-IRB+AVFISCOPTER-TAXASSUR-PTAXA-IPCAPTAXT-PPCAP-TAXLOY - PTAXLOY)))
                    + null(4-V_IND_TRAIT)* max(0 ,  TOTCR - CSGIM - CRDSIM - PRSPROV - COD8YT - CDISPROV -COD8YL-CSPROVYD
                                                          -CSPROVYE - CSPROVYF - CSPROVYN - CSPROVYG - CSPROVYH - CSPROVYP )
                             * positif_ou_nul((TOTCR - CSGIM - CRDSIM - PRSPROV - COD8YT - CDISPROV -COD8YL-CSPROVYD
                                                     - CSPROVYE-CSPROVYF- CSPROVYN-CSPROVYG-CSPROVYH - CSPROVYP) - SEUIL_61) 
                    + null(5-V_IND_TRAIT) * max(0 , (TOTCR - CSGIM - CRDSIM - PRSPROV - COD8YT - CDISPROV -COD8YL-CSPROVYD
                                                           - CSPROVYE-CSPROVYF- CSPROVYN -CSPROVYG-CSPROVYH- CSPROVYP))
                          * positif_ou_nul((TOTCR - CSGIM - CRDSIM - PRSPROV - COD8YT - CDISPROV -COD8YL-CSPROVYD
                                                  -CSPROVYE-CSPROVYF- CSPROVYN-CSPROVYG-CSPROVYH- CSPROVYP) - SEUIL_61) 
                 )
             ) ;
regle 90115001 :
application : iliad , batch ;
IRESTITIR = abs(min(0 , IRN + PIR + NRINET + IMPRET + RASAR
                    + (TAXASSUR + PTAXA - min(TAXASSUR+PTAXA+0,max(0,INE-IRB+AVFISCOPTER)))
                    + (IPCAPTAXT + PPCAP - min(IPCAPTAXT + PPCAP,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA)))
                    + (TAXLOY + PTAXLOY - min(TAXLOY + PTAXLOY,max(0,INE-IRB+AVFISCOPTER -TAXASSUR-PTAXA-IPCAPTAXT-PPCAP)))
                    + ((IHAUTREVT + PHAUTREV) -min((IHAUTREVT + PHAUTREV),max(0,INE-IRB+AVFISCOPTER-TAXASSUR-PTAXA-IPCAPTAXT-PPCAP-TAXLOY - PTAXLOY)))
                 )
             ) ;
regle 901151 :
application : iliad , batch ;
IREST = max(0,max(0,-(NAPTEMPCX)) - max(0,-(TOTIRPSANT)));
regle 9011511 :
application : iliad , batch ;
IRESTIR = max(0 , IRESTITIR - RECUMBISIR);
IINETCALC = max(0,NAPTEMP - TOTIRPSANT);
VARNON = IRPSCUM -RECUM - TOTIRPSANT;
NONMER  =  positif(20 - V_NOTRAIT) * (
                                           positif(SEUIL_8 - RECUM) * positif(SEUIL_12 - IRPSCUM) * IRPSCUM
                                          + (1-positif(SEUIL_8 - RECUM) * positif(SEUIL_12 - IRPSCUM)) * 0
                                     )
        + (1-positif(20-V_NOTRAIT)) * (
                          positif(SEUIL_8 - RECUM) * positif(SEUIL_12 - IRPSCUM) * (
                                                                                              positif(SEUIL_12 - abs(TOTIRPSANT))* max(0,IRPSCUM-RECUM-TOTIRPSANT)
                                                                                            + (1-positif(SEUIL_12 - abs(TOTIRPSANT))) * IRPSCUM
                                                                                   )
                   + (1-positif(SEUIL_8 - RECUM) * positif(SEUIL_12 - IRPSCUM)) * (
                                                                                           positif(positif(SEUIL_12-VARNON) * positif(VARNON)
                                                                                                  + positif(SEUIL_8-abs(VARNON)) * (1-positif(VARNON)))
                                                                                                    * max(0,IRPSCUM-RECUM-TOTIRPSANT)
                                                                                       +(1-positif(positif(SEUIL_12-VARNON) * positif(VARNON)
                                                                                                 + positif(SEUIL_8-abs(VARNON)) * (1-positif(VARNON))))
                                                                                                    * 0
                                                                                  )
                                      );


NONREST  =  positif(20 - V_NOTRAIT) * (
                                           positif(SEUIL_8 - RECUM) * positif(SEUIL_12 - IRPSCUM) * RECUM
                                        + (1-positif(SEUIL_8 - RECUM) * positif(SEUIL_12 - IRPSCUM)) * 0 
                                      )
        + (1-positif(20-V_NOTRAIT)) * (
                          positif(SEUIL_8 - RECUM) * positif(SEUIL_12 - IRPSCUM) * (
                                                                                              positif(SEUIL_12 - abs(TOTIRPSANT))* max(0,TOTIRPSANT - (IRPSCUM-RECUM))
                                                                                            + (1-positif(SEUIL_12 - abs(TOTIRPSANT))) * RECUM
                                                                                   )
                   + (1-positif(SEUIL_8 - RECUM) * positif(SEUIL_12 - IRPSCUM)) * (
                                                                                           positif(positif(SEUIL_12-VARNON) * positif(VARNON)
                                                                                                  + positif(SEUIL_8-abs(VARNON)) * (1-positif(VARNON)))
                                                                                                      * max(0,TOTIRPSANT - (IRPSCUM-RECUM))
                                                                                       +(1-positif(positif(SEUIL_12-VARNON) * positif(VARNON)
                                                                                                 + positif(SEUIL_8-abs(VARNON)) * (1-positif(VARNON))))
                                   
