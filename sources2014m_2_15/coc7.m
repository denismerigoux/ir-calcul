#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2015 
#au titre des revenus perçus en 2014. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
verif 9611:
application : iliad , batch ;
si
	( 
	CMAJ != 7 et ((APPLI_BATCH = 1 et APPLI_COLBERT = 0)
                    ou APPLI_OCEANS = 1
                    ou (APPLI_ILIAD = 1 et
                          non ( V_CALCULIR+0=1
                               ou (V_NOTRAIT+0) dans (16,23,26,33,36,43,46,53,56)
                              )
                        )
	             )
	)
	ou
	( 
	CMAJ2 != 7 et CMAJ2 != 0 et ((APPLI_BATCH = 1 et APPLI_COLBERT = 0)
                                   ou APPLI_OCEANS = 1
                                   ou (APPLI_ILIAD = 1 et
                                       non ( V_CALCULIR+0=1
                                            ou (V_NOTRAIT+0) dans (16,23,26,33,36,43,46,53,56)
                              )
                        )
	             )
        )
        
alors erreur A96101;
verif 9612:
application : batch , iliad ;
si
       (
       CMAJ non dans ( 7..8 ) et CMAJ non dans (10..11) et CMAJ non dans (17..18)  
     et (  (APPLI_ILIAD = 1 et
                   ( V_CALCULIR + 0 = 1 ou (V_NOTRAIT + 0) dans (16,23,26,33,36,43,46,53,56))
           )
           ou APPLI_COLBERT = 1)
       )
       ou
       (
       CMAJ2 non dans ( 7..8 ) et CMAJ2 non dans (10..11) et CMAJ2 non dans (17..18) et CMAJ2 != 0
     et (  (APPLI_ILIAD = 1 et
                   ( V_CALCULIR + 0 = 1 ou (V_NOTRAIT + 0) dans (16,23,26,33,36,43,46,53,56))
           )
           ou APPLI_COLBERT = 1)
       )
alors erreur A96102;
verif isf 9613:
application : batch , iliad ;

si  
       (
       		( CMAJ_ISF non dans ( 7,8,17,18,34 )  et V_IND_TRAIT+0 = 4 )
   
                ou

       		( CMAJ_ISF non dans ( 0,7,8,17,18,34 )  et V_IND_TRAIT+0 = 5 )
       )
alors erreur A96103;
verif 9621:
application : iliad , batch ;
si
          ( present(CMAJ)=1 et present(MOISAN)=0 )
          ou
          ( present(CMAJ2)=1 et present(MOISAN2)=0 )

alors erreur A96201 ;
verif 9622:
application : iliad , batch ;
si
          (  present(CMAJ)=0 et present(MOISAN)=1)
          ou
          (  present(CMAJ2)=0 et present(MOISAN2)=1)

alors erreur A96202 ;
verif isf 9623:
application : iliad , batch ;
si
          ( present(CMAJ_ISF)=1 et present(MOISAN_ISF)=0 )

alors erreur A96203 ;
verif isf 9624:
application : iliad , batch ;
si
          (  present(CMAJ_ISF)=0 et present(MOISAN_ISF)=1)

alors erreur A96204;
verif 9631:
application : iliad , batch ;
si
        (V_IND_TRAIT > 0 )
       et
        (
        inf(MOISAN/10000) non dans (01..12)
        ou
        inf(MOISAN2/10000) non dans (00..12)
        )
alors erreur A96301;
verif 9632:
application : iliad ;

si (APPLI_COLBERT=0) et (
   (
 arr( (MOISAN/10000 - inf(MOISAN/10000))*10000 ) != V_ANREV+1
et
 arr( (MOISAN/10000 - inf(MOISAN/10000))*10000 ) != V_ANREV+2
et
 arr( (MOISAN/10000 - inf(MOISAN/10000))*10000 ) != V_ANREV+3
et
 arr( (MOISAN/10000 - inf(MOISAN/10000))*10000 ) != V_ANREV+4
   )
   ou
   (
 arr( (MOISAN2/10000 - inf(MOISAN2/10000))*10000 ) != V_ANREV+1
et
 arr( (MOISAN2/10000 - inf(MOISAN2/10000))*10000 ) != V_ANREV+2
et
 arr( (MOISAN2/10000 - inf(MOISAN2/10000))*10000 ) != V_ANREV+3
et
 arr( (MOISAN2/10000 - inf(MOISAN2/10000))*10000 ) != V_ANREV+4
et
 arr( (MOISAN2/10000 - inf(MOISAN2/10000))*10000 ) != 0
   ))
alors erreur A96302 ;
verif 7317:
application : batch ;

si
   APPLI_COLBERT = 1
   et
   (
    arr( (MOISAN/10000 - inf(MOISAN/10000))*10000 ) != V_ANREV+1
    et
    arr( (MOISAN/10000 - inf(MOISAN/10000))*10000 ) != V_ANREV+2
    et
    arr( (MOISAN/10000 - inf(MOISAN/10000))*10000 ) != V_ANREV+3
   )

alors erreur A96302 ;
verif isf 9633:
application : iliad , batch ;
si
        (
                (V_IND_TRAIT+0 = 4 et inf(MOISAN_ISF/10000) non dans (01..12) )
                ou
                (V_IND_TRAIT+0 = 5 et inf(MOISAN_ISF/10000) non dans (01..12) et MOISAN_ISF != 0 )
        )
alors erreur A96303 ;
verif isf 9634:
application : iliad , batch ;
si (APPLI_OCEANS = 0) et
   (
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV+1
et
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV+2
et
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV+3
et
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV+4
   )

alors erreur A96304 ;
verif isf 73171:
application : batch ;
si (APPLI_COLBERT=1) et  (
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV+1
et
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV+2
et
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV+3
   )
alors erreur A96304 ;
verif 9641:
application : iliad , batch ;
si
	(
       ((inf( DATDEPETR/1000000 ) non dans (01..31)) et V_IND_TRAIT+0 = 4)
ou
       ((inf( DATDEPETR/1000000 ) non dans (01..31)) et V_IND_TRAIT = 5 et DATDEPETR != 0)
	) 
alors erreur A96401;
verif 9642:
application : iliad , batch ;
si
	(
       ((inf( (DATDEPETR/1000000 - inf(DATDEPETR/1000000))*100 ) non dans (01..12)) et (V_IND_TRAIT+0= 4))
ou
       ((inf( (DATDEPETR/1000000 - inf(DATDEPETR/1000000))*100 ) non dans (01..12)) 
		et V_IND_TRAIT = 5 et DATDEPETR != 0)
	) 
alors erreur A96402;
verif 9643:
application : iliad , batch ;
si (
 ((arr( (DATDEPETR/10000 - inf(DATDEPETR/10000))*10000 ) != V_ANREV) et V_IND_TRAIT+0 = 4)
ou
 ((arr( (DATDEPETR/10000 - inf(DATDEPETR/10000))*10000 ) != V_ANREV) 
		et V_IND_TRAIT = 5 et DATDEPETR != 0)
   ) 
alors erreur A96403;
verif 9651:
application : iliad , batch ;
si
	(
       ((inf( DATRETETR/1000000) non dans (01..31)) et V_IND_TRAIT+0 = 4)
	ou
       ((inf( DATRETETR/1000000) non dans (01..31)) et V_IND_TRAIT = 5 et DATRETETR != 0)
	) 
alors erreur A96501;
verif 9652:
application : iliad , batch ;
si
	(
       ((inf( (DATRETETR/1000000 - inf(DATRETETR/1000000))*100 ) non dans (01..12)) et V_IND_TRAIT+0 = 4)
	ou
       ((inf( (DATRETETR/1000000 - inf(DATRETETR/1000000))*100 ) non dans (01..12)) 
	et V_IND_TRAIT = 5 et DATRETETR != 0)
	) 
alors erreur A96502 ;
verif 9653:
application : iliad , batch ;
si (
 ((arr( (DATRETETR/10000 - inf(DATRETETR/10000))*10000 ) != V_ANREV) et V_IND_TRAIT+0 = 4)
ou
 ((arr( (DATRETETR/10000 - inf(DATRETETR/10000))*10000 ) != V_ANREV) 
            et V_IND_TRAIT = 5 et DATRETETR != 0)
   ) 
alors erreur A96503 ;
verif 966:
application : iliad , batch ;

si
   DATDEPETR > 0 
   et 
   DATRETETR > 0
	 
alors erreur A966 ;
verif isf 967:
application : iliad , batch ;

si
   V_ZDC = 4
   et
   positif(V_0AZ + 0) = 1
   et
   positif(ISFBASE + 0) = 1

alors erreur A967 ;
verif isf 9801:
application : iliad , batch ;

si
   V_NOTRAIT + 0 < 14
   et
   V_IND_TRAIT + 0 = 4
   et
   ISFBASE <= LIM_ISFINF

alors erreur A98001 ;
verif isf 9802:
application :  iliad , batch ;

si
   V_NOTRAIT + 0 < 14
   et
   V_IND_TRAIT + 0 = 4
   et
   ISFBASE >= LIM_ISFSUP

alors erreur A98002 ;
verif isf 9803:
application : iliad ;

si
   ((V_NOTRAIT + 0 = 14) ou (V_NOTRAIT+0 = 16))
   et
   present(ISFBASE) = 1
   et
   ISFBASE + 0 <= LIM_ISFINF

alors erreur A98003 ;
verif isf 9804:
application : iliad ;

si
   ISFBASE + 0 != 0
   et
   V_NOTRAIT + 0 > 20
   et
   ISFBASE + 0 <= LIM_ISFINF

alors erreur A98004 ;
verif isf 9805:
application : iliad ;

si
   V_NOTRAIT + 0 > 13
   et
   ISFBASE + 0 >= LIM_ISFSUP

alors erreur A98005 ;
verif isf 981:
application : iliad , batch ;


si
   present(ISFBASE) = 0
   et
   (ISFPMEDI + ISFPMEIN + ISFFIP + ISFFCPI + ISFDONF + ISFPLAF + ISFVBPAT + ISFDONEURO + ISFETRANG + ISFCONCUB + ISFPART + 0) > 0

alors erreur A981 ;
verif isf 982:
application : batch ,iliad ;

si
   V_IND_TRAIT + 0 > 0
   et
   positif(ISF_LIMINF + 0) + positif(ISF_LIMSUP + 0) = 2

alors erreur A982 ;
verif isf 983:
application : batch , iliad ;

si (APPLI_OCEANS=0) et
      (
                  (V_IND_TRAIT + 0 = 4)
                  et
                  (
                  positif(ISFCONCUB + 0 ) = 1
                  et
                        (positif(V_0AM + V_0AO + 0 ) = 1
                         ou
                                (positif(V_0AC + V_0AD + V_0AV + 0 )=1
                                 et
                                 positif(V_0AB + 0)= 1
                                )
                        )
                  )
        )
alors erreur A983 ;
verif isf 984:
application : batch , iliad ;

si
      (
                  (V_IND_TRAIT + 0 = 4)
                  et
                  (
                  positif(ISFPART + 0 ) = 1
                  et
                        (positif(V_0AM + V_0AO + 0 ) = 1
                         ou
                                (positif(V_0AC + V_0AD + V_0AV + 0 )=1
                                 et
                                 positif(V_0AB + 0)= 0
                                )
                        )
                   )
        )
alors erreur A984 ;
verif isf 985:
application : batch , iliad ;

si
      positif(ISF_LIMINF + ISF_LIMSUP + 0) = 1
      et
      ISFBASE > LIM_ISFINF
      et
      ISFBASE < LIM_ISFSUP

alors erreur A985 ;
verif isf 986:
application : batch , iliad ;

si
   APPLI_OCEANS = 0
   et
   V_NOTRAIT > 13
   et
   ISFCONCUB + 0 > 0
   et
   ISFPART + 0 > 0

alors erreur A986 ;
verif isf 9871:
application : batch , iliad ;

si
   APPLI_OCEANS = 0
   et
   V_NOTRAIT + 0 = 14
   et
   V_ETCVL + 0 = 1
   et
   ISFCONCUB + ISFPART + 0 = 0

alors erreur A98701 ;
verif isf 9872:
application : batch , iliad ;

si
   APPLI_OCEANS = 0
   et
   V_NOTRAIT + 0 = 14
   et
   present(V_ETCVL) = 1
   et
   V_ETCVL + 0 = 0
   et
   ISFCONCUB + ISFPART + 0 > 0

alors erreur A98702 ;
verif 990:
application : iliad ;

si
   positif(present(RE168) + present(TAX1649)) = 1
   et
   present(IPVLOC) = 1

alors erreur A990 ;
verif 991:
application : iliad ;

si
   positif(FLAGDERNIE+0) = 1
   et
   positif(null(V_NOTRAIT - 23) + null(V_NOTRAIT - 33) + null(V_NOTRAIT - 43) + null(V_NOTRAIT - 53) + null(V_NOTRAIT - 63)) = 1
   et
   NAPCR61 > V_ANTCR

alors erreur A991 ;
verif 9921:
application : iliad ;
si
                      ((DEFRI = 1)  et (PREM8_11 >0) et (VARR30R32>0))
alors erreur A992 ;
verif 9922:
application : iliad ;
si
                      ((DEFRI = 1) et (PREM8_11 =0) et (CODERAPHOR>0))
alors erreur A992 ;
verif 9923:
application : iliad ;
si
                     ( ((DEFRITS = 1) ou (DEFRIGLOBSUP = 1) ou (DEFRIGLOBINF = 1))et
                   (
  SALEXTV
 + CO[DGFIP][2017]AD
 + CO[DGFIP][2017]AE
 + PPEXTV
 + CO[DGFIP][2017]AH
 + CO[DGFIP][2017]BH
 + CO[DGFIP][2017]CH
 + CO[DGFIP][2017]DH
 + CO[DGFIP][2017]EH
 + CO[DGFIP][2017]FH
 + AUTOBICVV
 + AUTOBICPV
 + AUTOBNCV
 + AUTOBICVC
 + AUTOBICPC
 + AUTOBNCC
 + AUTOBICVP
 + AUTOBICPP
 + AUTOBNCP
 + XHONOAAV
 + XHONOV
 + XHONOAAC
 + XHONOC
 + XHONOAAP
 + XHONOP > 0) )

alors erreur A992 ;
verif 10000:
application : iliad ;

   si positif( 4BACREC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 4BACREC ;
   si positif( 4BACREP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 4BACREP ;
   si positif( 4BACREV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 4BACREV ;
   si positif( 4BAHREC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 4BAHREC ;
   si positif( 4BAHREP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 4BAHREP ;
   si positif( 4BAHREV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 4BAHREV ;
   si positif( ABDETMOINS ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 ABDETMOINS ;
   si positif( ABDETPLUS ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 ABDETPLUS ;
   si positif( ABIMPMV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 ABIMPMV ;
   si positif( ABIMPPV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 ABIMPPV ;
   si positif( ABPVNOSURSIS ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 ABPVNOSURSIS ;
   si positif( ACODELAISINR ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 ACODELAISINR ;
   si positif( ALLECS ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 ALLECS ;
   si positif( ALLO1 ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 ALLO1 ;
   si positif( ALLO2 ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 ALLO2 ;
   si positif( ALLO3 ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 ALLO3 ;
   si positif( ALLO4 ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 ALLO4 ;
   si positif( ALLOC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 ALLOC ;
   si positif( ALLOV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 ALLOV ;
   si positif( ANOCEP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 ANOCEP ;
   si positif( ANOPEP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 ANOPEP ;
   si positif( ANOVEP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 ANOVEP ;
   si positif( ASCAPA ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 ASCAPA ;
   si positif( AUTOBICPC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 AUTOBICPC ;
   si positif( AUTOBICPP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 AUTOBICPP ;
   si positif( AUTOBICPV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 AUTOBICPV ;
   si positif( AUTOBICVC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 AUTOBICVC ;
   si positif( AUTOBICVP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 AUTOBICVP ;
   si positif( AUTOBICVV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 AUTOBICVV ;
   si positif( AUTOBNCC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 AUTOBNCC ;
   si positif( AUTOBNCP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 AUTOBNCP ;
   si positif( AUTOBNCV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 AUTOBNCV ;
   si positif( AUTOVERSLIB ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 AUTOBNCV ;
   si positif( AUTOVERSSUP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 AUTOVERSSUP ;
   si positif( AVETRAN ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 AVETRAN ;
   si positif( BA1AC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BA1AC ;
   si positif( BA1AP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BA1AP ;
   si positif( BA1AV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BA1AV ;
   si positif( BACDEC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BACDEC ;
   si positif( BACDEP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BACDEP ;
   si positif( BACDEV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BACDEV ;
   si positif( BACREC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BACREC ;
   si positif( BACREP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BACREP ;
   si positif( BACREV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BACREV ;
   si positif( BAEXC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAEXC ;
   si positif( BAEXP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAEXP ;
   si positif( BAEXV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BAEXV ;
   si positif( BA[DGFIP][2017]AC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BA[DGFIP][2017]AC ;
   si positif( BA[DGFIP][2017]AP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BA[DGFIP][2017]AP ;
   si positif( BA[DGFIP][2017]AV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BA[DGFIP][2017]AV ;
   si positif( BAFC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAFC ;
   si positif( BAFORESTC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAFORESTC ;
   si positif( BAFORESTP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BAFORESTP ;
   si positif( BAFORESTV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAFORESTV ;
   si positif( BAFP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAFP ;
   si positif( BAFPVC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BAFPVC ;
   si positif( BAFPVP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAFPVP ;
   si positif( BAFPVV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAFPVV ;
   si positif( BAFV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BAFV ;
   si positif( BAHDEC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAHDEC ;
   si positif( BAHDEP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAHDEP ;
   si positif( BAHDEV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BAHDEV ;
   si positif( BAHEXC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAHEXC ;
   si positif( BAHEXP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAHEXP ;
   si positif( BAHEXV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BAHEXV ;
   si positif( BAHREC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAHREC ;
   si positif( BAHREP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAHREP ;
   si positif( BAHREV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BAHREV ;
   si positif( BAILOC98 ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAILOC98 ;
   si positif( BANOCGAC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BANOCGAC ;
   si positif( BANOCGAP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BANOCGAP ;
   si positif( BANOCGAV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BANOCGAV ;
   si positif( BAPERPC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAPERPC ;
   si positif( BAPERPP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BAPERPP ;
   si positif( BAPERPV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BAPERPV ;
   si positif( BASRET ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BASRET ;
   si positif( B[DGFIP][2017]AC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 B[DGFIP][2017]AC ;
   si positif( B[DGFIP][2017]AP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 B[DGFIP][2017]AP ;
   si positif( B[DGFIP][2017]AV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 B[DGFIP][2017]AV ;
   si positif( B[DGFIP][2017]AC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 B[DGFIP][2017]AC ;
   si positif( B[DGFIP][2017]AP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 B[DGFIP][2017]AP ;
   si positif( B[DGFIP][2017]AV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 B[DGFIP][2017]AV ;
   si positif( BICDEC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICDEC ;
   si positif( BICDEP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICDEP ;
   si positif( BICDEV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICDEV ;
   si positif( BICDNC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BICDNC ;
   si positif( BICDNP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICDNP ;
   si positif( BICDNV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BICDNV ;
   si positif( BICEXC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BICEXC ;
   si positif( BICEXP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICEXP ;
   si positif( BICEXV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICEXV ;
   si positif( BICHDEC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BICHDEC ;
   si positif( BICHDEP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICHDEP ;
   si positif( BICHDEV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BICHDEV ;
   si positif( BICHREC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BICHREC ;
   si positif( BICHREP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICHREP ;
   si positif( BICHREV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICHREV ;
   si positif( BICNOC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BICNOC ;
   si positif( BICNOP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICNOP ;
   si positif( BICNOV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICNOV ;
   si positif( BICNPEXC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BICNPEXC ;
   si positif( BICNPEXP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICNPEXP ;
   si positif( BICNPEXV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICNPEXV ;
   si positif( BICNPHEXC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BICNPHEXC ;
   si positif( BICNPHEXP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICNPHEXP ;
   si positif( BICNPHEXV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICNPHEXV ;
   si positif( BICPMVCTC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BICPMVCTC ;
   si positif( BICPMVCTP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICPMVCTP ;
   si positif( BICPMVCTV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICPMVCTV ;
   si positif( BICREC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BICREC ;
   si positif( BICREP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICREP ;
   si positif( BICREV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BICREV ;
   si positif( BIGREST ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BIGREST ;
   si positif( BIHDNC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BIHDNC ;
   si positif( BIHDNP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BIHDNP ;
   si positif( BIHDNV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BIHDNV ;
   si positif( BIHEXC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BIHEXC ;
   si positif( BIHEXP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BIHEXP ;
   si positif( BIHEXV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BIHEXV ;
   si positif( BIHNOC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BIHNOC ;
   si positif( BIHNOP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BIHNOP ;
   si positif( BIHNOV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BIHNOV ;
   si positif( BIPERPC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BIPERPC ;
   si positif( BIPERPP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BIPERPP ;
   si positif( BIPERPV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BIPERPV ;
   si positif( BN1AC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BN1AC ;
   si positif( BN1AP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BN1AP ;
   si positif( BN1AV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BN1AV ;
   si positif( BNCAABC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCAABC ;
   si positif( BNCAABP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCAABP ;
   si positif( BNCAABV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCAABV ;
   si positif( BNCAADC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCAADC ;
   si positif( BNCAADP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCAADP ;
   si positif( BNCAADV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCAADV ;
   si positif( BNCCRC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCCRC ;
   si positif( BNCCRFC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCCRFC ;
   si positif( BNCCRFP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCCRFP ;
   si positif( BNCCRFV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCCRFV ;
   si positif( BNCCRP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCCRP ;
   si positif( BNCCRV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCCRV ;
   si positif( BNCDEC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCDEC ;
   si positif( BNCDEP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCDEP ;
   si positif( BNCDEV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCDEV ;
   si positif( BNCEXC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCEXC ;
   si positif( BNCEXP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCEXP ;
   si positif( BNCEXV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCEXV ;
   si positif( BNCN[DGFIP][2017]AC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCN[DGFIP][2017]AC ;
   si positif( BNCN[DGFIP][2017]AP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCN[DGFIP][2017]AP ;
   si positif( BNCN[DGFIP][2017]AV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCN[DGFIP][2017]AV ;
   si positif( BNCNPC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCNPC ;
   si positif( BNCNPDCT ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCNPDCT ;
   si positif( BNCNPDEC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCNPDEC ;
   si positif( BNCNPDEP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCNPDEP ;
   si positif( BNCNPDEV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCNPDEV ;
   si positif( BNCNPP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCNPP ;
   si positif( BNCNPPVC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCNPPVC ;
   si positif( BNCNPPVP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCNPPVP ;
   si positif( BNCNPPVV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCNPPVV ;
   si positif( BNCNPREXAAC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCNPREXAAC ;
   si positif( BNCNPREXAAP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCNPREXAAP ;
   si positif( BNCNPREXAAV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCNPREXAAV ;
   si positif( BNCNPREXC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCNPREXC ;
   si positif( BNCNPREXP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCNPREXP ;
   si positif( BNCNPREXV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCNPREXV ;
   si positif( BNCNPV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCNPV ;
   si positif( BNCPMVCTC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCPMVCTC ;
   si positif( BNCPMVCTP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCPMVCTP ;
   si positif( BNCPMVCTV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCPMVCTV ;
   si positif( BNCPRO1AC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCPRO1AC ;
   si positif( BNCPRO1AP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCPRO1AP ;
   si positif( BNCPRO1AV ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCPRO1AV ;
   si positif( BNCPROC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCPROC ;
   si positif( BNCPRODEC ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0
      alors erreur A99301 BNCPRODEC ;
   si positif( BNCPRODEP ) = 1 et positif(ANNUL2042) = 1 et APPLI_OCEANS = 0 
      alors erreur A99301 BNCPRODEP ;
   si positif( BNCPRODEV 
