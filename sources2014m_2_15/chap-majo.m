#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2015 
#au titre des revenus perçus en 2014. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
regle isf 232:
application : iliad ;
SUPISF[X] = positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X)
            * max(ISF4BASE,0)
            + (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
	     * max(0,ISF4BASE - (TISF4BASE[FLAG_DERSTTR]));

regle 23111:
application : iliad ;
IRBASE = IRN - IRANT ;

TAXABASE_MAJO = TAXASSUR * positif(IAM[DGFIP][2017] + 1 - SEUIL_61);
CAPBASE_MAJO = IPCAPTAXT * positif(IAM[DGFIP][2017] + 1 - SEUIL_61);
LOYBASE_MAJO = TAXLOY * positif(IAM[DGFIP][2017]  + 1 - SEUIL_61);
HRBASE_MAJO = IHAUTREVT * positif(IAM[DGFIP][2017]  + 1 - SEUIL_61);

CSBASE_MAJO = (CSG - CSGIM) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

RDBASE_MAJO = (RDSN - CRDSIM) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

PSBASE_MAJO = (PRS - PRSPROV) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

CVNBASE_MAJO = (CVNSALC - COD8YT) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

REGVBASE_MAJO = BREGV * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

CDISBASE_MAJO = (CDIS - CDISPROV) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

GLOBASE_MAJO = (CGLOA - COD8YL) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

RSE1BASE_MAJO = (RSE1N - CSPROVYD) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

RSE2BASE_MAJO = (max(0, RSE8TV - CIRSE8TV - CSPROVYF) + max(0, RSE8SA - CIRSE8SA - CSPROVYN)
                ) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

RSE3BASE_MAJO = (RSE3N - CSPROVYG) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

RSE4BASE_MAJO = (max(0, RSE8TX - CIRSE8TX - CSPROVYH) + max(0, RSE8SB -CIRSE8SB - CSPROVYP)
                ) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

RSE5BASE_MAJO = (RSE5N - CSPROVYE) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

regle corrective 23112:
application :   iliad ;
TOT_BASE_MAJO = IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + HRBASE_MAJO + LOYBASE_MAJO ;

TOT_REF = TIRBASE[FLAG_DERSTTR] +TTAXABASE[FLAG_DERSTTR] + TLOYBASE[FLAG_DERSTTR]
	 +TPCAPBASE[FLAG_DERSTTR]+TCHRBASE[FLAG_DERSTTR];


TAXA_ISO = TAXASSUR * positif(IAM[DGFIP][2017] + 1 - SEUIL_61) ; 
CAP_ISO  = IPCAPTAXT * positif(IAM[DGFIP][2017] + 1 - SEUIL_61) ; 
HR_ISO   = IHAUTREVT  * positif(IAM[DGFIP][2017] + 1 - SEUIL_61) ; 

PENA_RESTIT = max(0, IRBASE - TIRBASE[FLAG_DERSTTR]);

NOPENA_RESTIT = max(0 , min( IRBASE - TIRBASE[FLAG_DERSTTR] ,
			     max( 0, IRBASE + TTAXABASE[FLAG_DERSTTR]+TPCAPBASE[FLAG_DERSTTR]
					    + TLOYBASE[FLAG_DERSTTR]  +TCHRBASE[FLAG_DERSTTR])
                           )
	   );

SUPIR[X] =  null(X) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
                   * min( max(0, max(TIRBASE[FLAG_DERSTTR] , IRBASE)) ,
                          max(0, IRBASE2042_FIC )
                        )


	      + ( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		  * positif(null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))
		  * PENA_RESTIT


              + ( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
                  * (1 - positif((null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))))
                  * (1 - positif(null(X-1)))
	          * ((1 - positif(TARDIFEVT2)*null(X-2))
                      * NOPENA_RESTIT 
                      + positif(TARDIFEVT2) * null(X-2) * TIRBASE[FLAG_DERSTTR]
	            )

           + (1 - positif((null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13)))) 
              *  null(X-1)*positif( null(CSTRATE1 - 1) + null(CSTRATE1 - 7) + null(CSTRATE1 - 8)
			           +null(CSTRATE1 - 10)+ null(CSTRATE1 - 11)+ null(CSTRATE1 - 17)
                                   +null(CSTRATE1 - 18)) 
			               * NOPENA_RESTIT

           + null(X-1)*positif( null(CSTRATE1 - 3)
	                       +null(CSTRATE1 - 4)
			       +null(CSTRATE1 - 5)
			       +null(CSTRATE1 - 6)
		               +null(CSTRATE1 - 55)) 
                              		* PENA_RESTIT ; 


SU[DGFIP][2017]IR[X] = null(X) * null(CODE_2042 - 17) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
                    * max(IRBASE,0)
	      + ((positif(null(X-14)+null(X-15)+null(X-18)+null(X-20)+null(X-22))
                    * PENA_RESTIT 
	          )
	          + (1 - positif(null(X-14)+null(X-15)+null(X-18)+null(X-20)))* 0)
                 * (1 - positif(null(X-1))) 
           + null(X-1)*positif( null(CSTRATE1 - 1)
                               +null(CSTRATE1 - 17)
	                       +null(CSTRATE1 - 2)
			       +null(CSTRATE1 - 10)
		               +null(CSTRATE1 - 30)) 
	             * PENA_RESTIT;

SUPCS[X] =  positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X)
             * min( max(0, max (TCSBASE[0] ,CSBASE_MAJO)) ,
                           max(0, CSBASE2042_FIC )
                  )
          +  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
             * max(0, CSBASE_MAJO - TCSBASE[FLAG_DERSTTR]) ;


SUPPS[X] =  positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X)
             * min( max(0, max (TPSBASE[0] , PSBASE_MAJO)) ,
                           max(0, PSBASE2042_FIC )
                  )
          +  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
             * max(0, PSBASE_MAJO - TPSBASE[FLAG_DERSTTR]) ;


SUPRD[X] =  positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X)
             * min( max(0, max (TRDBASE[0] ,RDBASE_MAJO)) ,
                           max(0, RDBASE2042_FIC )
                  )
          +  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
             * max(0, RDBASE_MAJO - TRDBASE[FLAG_DERSTTR]) ;


SUPGLO[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
           * max(0, GLOBASE_MAJO - TGLOBASE[FLAG_DERSTTR]) ;

SUPCDIS[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
           * max(0, CDISBASE_MAJO - TCDISBASE[FLAG_DERSTTR]) ;

SUPCVN[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
           * max(0, CVNBASE_MAJO - TCVNBASE[FLAG_DERSTTR]) ;

SUPREGV[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
           * max(0, REGVBASE_MAJO - TREGVBASE[FLAG_DERSTTR]) ;

SUPRSE1[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
           * max(0, RSE1BASE_MAJO - TRSE1BASE[FLAG_DERSTTR]) ;


SUPRSE2[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
           * max(0, RSE2BASE_MAJO - TRSE2BASE[FLAG_DERSTTR]) ;


SUPRSE3[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
           * max(0, RSE3BASE_MAJO - TRSE3BASE[FLAG_DERSTTR]) ;


SUPRSE4[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
           * max(0, RSE4BASE_MAJO - TRSE4BASE[FLAG_DERSTTR]) ;

SUPRSE5[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
           * max(0, RSE5BASE_MAJO - TRSE5BASE[FLAG_DERSTTR]) ;

SUPTAXA[X] =  null(X) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
                      * max( TAXABASE_MAJO , 0 ) 


	      +( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		 * positif(null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))
		 * max( 0, TAXABASE_MAJO - TTAXABASE[FLAG_DERSTTR])


              + ( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		* (1 - positif((null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))))
                * max( 0, min( TAXABASE_MAJO - TTAXABASE[FLAG_DERSTTR],
				     max( 0, IRBASE + TAXABASE_MAJO + TPCAPBASE[FLAG_DERSTTR] + TLOYBASE[FLAG_DERSTTR] + TCHRBASE[FLAG_DERSTTR] )
                             )
                     );



SU[DGFIP][2017]TAXA[X] = null(X) * null(CODE_2042 - 17) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
                      * max( 0, min( TAXABASE_MAJO - TTAXABASE[FLAG_DERSTTR],
				     max( 0, IRBASE + TAXABASE_MAJO + TPCAPBASE[FLAG_DERSTTR] + TLOYBASE[FLAG_DERSTTR] + TCHRBASE[FLAG_DERSTTR] )
                                   )
                           )


	      + positif(20 - V_NOTRAIT) * positif(null(X-14))
                      * max( 0, min( TAXABASE_MAJO - TTAXABASE[FLAG_DERSTTR],
				     max( 0, IRBASE + TAXABASE_MAJO + TPCAPBASE[FLAG_DERSTTR] + TLOYBASE[FLAG_DERSTTR] + TCHRBASE[FLAG_DERSTTR] )
                                   )
                           )

	      + (1-positif(20 - V_NOTRAIT)) * positif(null(X-14))
		* max( 0, TAXABASE_MAJO - TTAXABASE[FLAG_DERSTTR])
	        
	      + positif(null(X-15)+null(X-18)+null(X-22))
		* max( 0, TAXABASE_MAJO - TTAXABASE[FLAG_DERSTTR])

	     + (1 - positif(null(X-14)+null(X-15)+null(X-18)+null(X-22))) * 0
	     ;


SUPCAP[X] =  null(X) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
                     * max( 0 , CAPBASE_MAJO )

	      +( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		 * positif(null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))
                 * max( 0, CAPBASE_MAJO - TPCAPBASE[FLAG_DERSTTR] )

	+( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		* (1 - positif((null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))))
	        * max(0, min( CAPBASE_MAJO - TPCAPBASE[FLAG_DERSTTR] , 
	                      max( 0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + TLOYBASE[FLAG_DERSTTR] + TCHRBASE[FLAG_DERSTTR] )
                            )
                     );
                
SU[DGFIP][2017]CAP[X] = null(X) * null(CODE_2042 - 17) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
	             * max(0, min( CAPBASE_MAJO - TPCAPBASE[FLAG_DERSTTR] , 
	                           max( 0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + TLOYBASE[FLAG_DERSTTR] + TCHRBASE[FLAG_DERSTTR] )
                                 )
                           )


              + positif(20 - V_NOTRAIT) * positif(null(X-14))            
	             * max(0, min( CAPBASE_MAJO - TPCAPBASE[FLAG_DERSTTR] , 
	                           max( 0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + TLOYBASE[FLAG_DERSTTR] + TCHRBASE[FLAG_DERSTTR] )
                                 )
                           )
              + (1-positif(20 - V_NOTRAIT)) * positif(null(X-14))
		* max(0, CAPBASE_MAJO - TPCAPBASE[FLAG_DERSTTR])
                      
              + positif(null(X-15)+null(X-18)+null(X-20)+null(X-22))
	         * max(0, CAPBASE_MAJO - TPCAPBASE[FLAG_DERSTTR]) 
                 
              + (1 - positif(null(X-14)+null(X-15)+null(X-18)+null(X-20)+null(X-22))) * 0
	      ;

SUPLOY[X] =  null(X) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
                     * max( 0 , LOYBASE_MAJO )

	      +( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		 * positif(null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))
                 * max(0 , LOYBASE_MAJO - TLOYBASE[FLAG_DERSTTR])


	+( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		* (1 - positif((null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))))
                * max(0 , min( LOYBASE_MAJO - TLOYBASE[FLAG_DERSTTR] ,
		               max( 0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + LOYBASE_MAJO + TCHRBASE[FLAG_DERSTTR] )
                             )
                      );

SU[DGFIP][2017]LOY[X] = null(X) * null(CODE_2042 - 17) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
                     * max(0 , min( LOYBASE_MAJO - TLOYBASE[FLAG_DERSTTR] ,
				    max( 0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + LOYBASE_MAJO + TCHRBASE[FLAG_DERSTTR] )
                                  )
                          )

              + positif(20 - V_NOTRAIT) * positif(null(X-14))
                     * max(0 , min( LOYBASE_MAJO - TLOYBASE[FLAG_DERSTTR] ,
				    max( 0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + LOYBASE_MAJO + TCHRBASE[FLAG_DERSTTR] )
                                  )
                          )

              + (1-positif(20 - V_NOTRAIT)) * positif(null(X-14))
                * max(0 , LOYBASE_MAJO - TLOYBASE[FLAG_DERSTTR])

              + positif(null(X-15)+null(X-18)+null(X-20)+null(X-22))
                * max(0 , LOYBASE_MAJO - TLOYBASE[FLAG_DERSTTR])
		
              + (1 - positif(null(X-14)+null(X-15)+null(X-18)+null(X-20)+null(X-22))) * 0
	      ;

SUPHR[X] =  null(X) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
                    * max( 0 , HRBASE_MAJO )


	      +( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		 * positif(null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))
		 * max( 0, HRBASE_MAJO - TCHRBASE[FLAG_DERSTTR] )


              + ( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		* (1 - positif((null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))))
                * max(0 , min( HRBASE_MAJO - TCHRBASE[FLAG_DERSTTR] ,
			       max( 0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + LOYBASE_MAJO + HRBASE_MAJO )
                             )
                     );


SU[DGFIP][2017]HR[X] = null(X) * null(CODE_2042 - 17) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
                     * max(0 , min( HRBASE_MAJO - TCHRBASE[FLAG_DERSTTR] ,
				    max( 0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + LOYBASE_MAJO + HRBASE_MAJO )
                                  )
                          )

              + positif(20 - V_NOTRAIT) * positif(null(X-14))
                     * max(0 , min( HRBASE_MAJO - TCHRBASE[FLAG_DERSTTR] ,
				    max( 0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + LOYBASE_MAJO + HRBASE_MAJO )
                                  )
                          )

              + (1-positif(20 - V_NOTRAIT)) * positif(null(X-14))
	             * max(0 , HRBASE_MAJO - TCHRBASE[FLAG_DERSTTR])

              + positif(null(X-15)+null(X-18)+null(X-20)+null(X-22))
                * max(0 , HRBASE_MAJO - TCHRBASE[FLAG_DERSTTR])
		
              + (1 - positif(null(X-14)+null(X-15)+null(X-18)+null(X-20)+null(X-22))) * 0
	      ;

regle corrective 23113:
application : iliad;
TMAJOIR[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPIR[X] * TAUX_STRATE/100 ));
T2MAJOIR[X] = (1 - null(1 - IND_RJLJ))
	     * (

	     (positif(null(X - 0) * null(CODE_2042 - 17) 
		 +null(X-14)+null(X-15)+null(X-18)+null(X-20)+null(X-22))
        	*(positif(null(X-21)+null(X-22))*TL_IR*arr(SU[DGFIP][2017]IR[X] * TX1758A/100)
	          +(1-null(X-22)) * arr(SU[DGFIP][2017]IR[X] * TX1758A/100)))


	      + null(X-1) 
	                  * positif(null(X - 1) * null(CSTRATE1 - 17)
                               +null(CSTRATE1 - 1)
	                       +null(CSTRATE1 - 2)
			       +null(CSTRATE1 - 10)
		               +null(CSTRATE1 - 30)) * arr(SU[DGFIP][2017]IR[X] * TX1758A/100)

                 ); 

MAJOIR_ST = MAJOIRST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOIR[X] + T2MAJOIR[X];
TMAJOCS[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPCS[X] * TAUX_STRATE/100 ));
MAJOCS_ST = MAJOCSST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOCS[X] ;

TMAJOPS[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPPS[X] * TAUX_STRATE/100 ));
MAJOPS_ST = MAJOPSST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOPS[X] ;


TMAJORD[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPRD[X] * TAUX_STRATE/100 ));
MAJORD_ST = MAJORDST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJORD[X] ;

TMAJOCVN[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPCVN[X] * TAUX_STRATE/100 ));
MAJOCVN_ST = MAJOCVNST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOCVN[X] ;

TMAJOREGV[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPREGV[X] * TAUX_STRATE/100 ));
MAJOREGV_ST = MAJOREGVST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOREGV[X] ;

TMAJOCDIS[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPCDIS[X] * TAUX_STRATE/100 ));

MAJOCDIS_ST = MAJOCDISST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOCDIS[X] ;
TMAJOGLO[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPGLO[X] * TAUX_STRATE/100 ));
MAJOGLO_ST = MAJOGLOST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOGLO[X] ;
TMAJORSE1[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPRSE1[X] * TAUX_STRATE/100 ));
MAJORSE1_ST = MAJORSE1ST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJORSE1[X] ;
TMAJORSE2[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPRSE2[X] * TAUX_STRATE/100 ));
MAJORSE2_ST = MAJORSE2ST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJORSE2[X] ;
TMAJORSE3[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPRSE3[X] * TAUX_STRATE/100 ));
MAJORSE3_ST = MAJORSE3ST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJORSE3[X] ;
TMAJORSE4[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPRSE4[X] * TAUX_STRATE/100 ));
MAJORSE4_ST = MAJORSE4ST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJORSE4[X] ;
TMAJORSE5[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPRSE5[X] * TAUX_STRATE/100 ));
MAJORSE5_ST = MAJORSE5ST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJORSE5[X] ;
TMAJOTAXA[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPTAXA[X] * TAUX_STRATE/100 ));
T2MAJOTAXA[X] = (1 - null(1 - IND_RJLJ))

	     * (positif(null(X - 0) * null(CODE_2042 - 17) 
	        + null(X-14)+null(X-15)+null(X-18)+null(X-22))
		*(null(X-22)*TL_TAXAGA*arr(SU[DGFIP][2017]TAXA[X] * TX1758A/100)
		  +(1-null(X-22)) * arr(SU[DGFIP][2017]TAXA[X] * TX1758A/100)));

MAJOTAXA_ST = MAJOTAXAST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOTAXA[X] + T2MAJOTAXA[X];
TMAJOHR[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPHR[X] * TAUX_STRATE/100 ));
T2MAJOHR[X] = (1 - null(1 - IND_RJLJ))

	     * (positif(null(X - 0) * null(CODE_2042 - 17) 
	        + null(X-14)+null(X-15)+null(X-18)+null(X-20)+null(X-22))
		*(positif(null(X-20)+null(X-22))*TL_CHR*arr(SU[DGFIP][2017]HR[X] * TX1758A/100)
		  +(1-positif(null(X-20)+null(X-22))) * arr(SU[DGFIP][2017]HR[X] * TX1758A/100)));
MAJOHR_ST = MAJOHRST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOHR[X] + T2MAJOHR[X];
TMAJOCAP[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPCAP[X] * TAUX_STRATE/100 ));
T2MAJOCAP[X] = (1 - null(1 - IND_RJLJ))

	     * (positif(null(X - 0) * null(CODE_2042 - 17) 
	        + null(X-14)+null(X-15)+null(X-18)+null(X-22))
		*(null(X-22)*TL_CAP*arr(SU[DGFIP][2017]CAP[X] * TX1758A/100)
		  +(1-null(X-22)) * arr(SU[DGFIP][2017]CAP[X] * TX1758A/100)));
MAJOCAP_ST = MAJOCAPST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOCAP[X] + T2MAJOCAP[X];
TMAJOLOY[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPLOY[X] * TAUX_STRATE/100 ));
T2MAJOLOY[X] = (1 - null(1 - IND_RJLJ))

	     * (positif(null(X - 0) * null(CODE_2042 - 17) 
	        + null(X-14)+null(X-15)+null(X-18)+null(X-22))
		*(null(X-22)*TL_LOY*arr(SU[DGFIP][2017]LOY[X] * TX1758A/100)
		  +(1-null(X-22)) * arr(SU[DGFIP][2017]LOY[X] * TX1758A/100)));
MAJOLOY_ST = MAJOLOYST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOLOY[X] + T2MAJOLOY[X];
regle isf 233:
application : iliad;
TMAJOISF[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPISF[X] * TAUX_STRATE/100 ));
MAJOISF_ST = MAJOISFST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOISF[X] ;
regle corrective 23114:
application : iliad;

MAJOIR07_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                            min(min(IRBASE2042_FIC,IRBASE),
                                max(0, IRBASE)
                               )
                            )
			* STR_TR16 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              ); 
MAJOIR08_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                            min(min(IRBASE2042_FIC,IRBASE),
                                max(0, IRBASE)
                               )
                            )
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              ); 
MAJOIR17_1TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                            min(min(IRBASE2042_FIC,IRBASE),
                                max(0, IRBASE)
                               )
                            )

			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              ); 
MAJOIR17_2TARDIF = max(0,arr(FLAG_TRTARDIF * TX1758A/100 *
                            min(min(IRBASE2042_FIC,IRBASE),
                                max(0, IRBASE)
                               )
                            )
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              ); 
MAJOIR_TARDIF = somme(x = 07,08: MAJOIR0x_TARDIF) 
		+ MAJOIR17_1TARDIF + MAJOIR17_2TARDIF;
MAJOCS07_TARDIF = max(0,arr(FLAG_TRTARDIF * CSBASE_MAJO * TAUX_2042/100)
			* STR_TR16 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOCS08_TARDIF = max(0,arr(FLAG_TRTARDIF * CSBASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOCS17_TARDIF = max(0,arr(FLAG_TRTARDIF * CSBASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOCS_TARDIF = somme(x = 07,08,17 : MAJOCSx_TARDIF);
MAJOPS07_TARDIF = max(0,arr(FLAG_TRTARDIF * PSBASE_MAJO * TAUX_2042/100)
			* STR_TR16 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOPS08_TARDIF = max(0,arr(FLAG_TRTARDIF * PSBASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOPS17_TARDIF = max(0,arr(FLAG_TRTARDIF * PSBASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOPS_TARDIF = somme(x = 07,08,17 : MAJOPSx_TARDIF);
MAJOR[DGFIP][2017]7_TARDIF = max(0,arr(FLAG_TRTARDIF * RDBASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOR[DGFIP][2017]8_TARDIF = max(0,arr(FLAG_TRTARDIF * RDBASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOR[DGFIP][2017]7_TARDIF = max(0,arr(FLAG_TRTARDIF * RDBASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJORD_TARDIF = somme(x = 07,08,17 : MAJORDx_TARDIF);

MAJOCVN07_TARDIF = max(0,arr(FLAG_TRTARDIF * CVNBASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );

MAJOCVN08_TARDIF = max(0,arr(FLAG_TRTARDIF * CVNBASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );

MAJOCVN17_TARDIF = max(0,arr(FLAG_TRTARDIF * CVNBASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );

MAJOCVN_TARDIF = somme(x = 07,08,17 : MAJOCVNx_TARDIF);

MAJOREGV07_TARDIF = max(0,arr(FLAG_TRTARDIF * REGVBASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );

MAJOREGV08_TARDIF = max(0,arr(FLAG_TRTARDIF * REGVBASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );

MAJOREGV17_TARDIF = max(0,arr(FLAG_TRTARDIF * REGVBASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );

MAJOREGV_TARDIF = somme(x = 07,08,17 : MAJOREGVx_TARDIF);

MAJOCDIS07_TARDIF = max(0,arr(FLAG_TRTARDIF * CDISBASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	               );

MAJOCDIS08_TARDIF = max(0,arr(FLAG_TRTARDIF * CDISBASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	               );

MAJOCDIS17_TARDIF = max(0,arr(FLAG_TRTARDIF * CDISBASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	               );

MAJOCDIS_TARDIF = somme(x = 07,08,17 : MAJOCDISx_TARDIF);

MAJOGLO07_TARDIF = max(0,arr(FLAG_TRTARDIF * GLOBASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );

MAJOGLO08_TARDIF = max(0,arr(FLAG_TRTARDIF * GLOBASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );

MAJOGLO17_TARDIF = max(0,arr(FLAG_TRTARDIF * GLOBASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );

MAJOGLO_TARDIF = somme(x = 07,08,17 : MAJOGLOx_TARDIF);

MAJORSE107_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE1BASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );

MAJORSE108_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE1BASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE117_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE1BASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );

MAJORSE1_TARDIF = somme(x = 07,08,17 : MAJORSE1x_TARDIF);

MAJORSE207_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE2BASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE208_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE2BASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE217_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE2BASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE2_TARDIF = somme(x = 07,08,17 : MAJORSE2x_TARDIF);
MAJORSE307_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE3BASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE308_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE3BASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE317_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE3BASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE3_TARDIF = somme(x = 07,08,17 : MAJORSE3x_TARDIF);
MAJORSE407_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE4BASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE408_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE4BASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE417_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE4BASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE4_TARDIF = somme(x = 07,08,17 : MAJORSE4x_TARDIF);
MAJORSE507_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE5BASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE508_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE5BASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE517_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE5BASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE5_TARDIF = somme(x = 07,08,17 : MAJORSE5x_TARDIF);
MAJOHR07_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                            min(min(HRBASE2042_FIC,HRBASE_MAJO),
			        max(0, IRBASE+TAXABASE_MAJO+CAPBASE_MAJO+LOYBASE_MAJO+HRBASE_MAJO)
                               )
                           )
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );

MAJOHR08_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                            min(min(HRBASE2042_FIC,HRBASE_MAJO),
			        max(0, IRBASE+TAXABASE_MAJO+CAPBASE_MAJO+LOYBASE_MAJO+HRBASE_MAJO)
                               )
                           )
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJOHR17_1TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                            min(min(HRBASE2042_FIC,HRBASE_MAJO),
			        max(0, IRBASE+TAXABASE_MAJO+CAPBASE_MAJO+LOYBASE_MAJO+HRBASE_MAJO)
                               )
                           )

			* STR_TR15
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );
MAJOHR17_2TARDIF = max(0,arr(FLAG_TRTARDIF * TX1758A/100 *
                            min(min(HRBASE2042_FIC,HRBASE_MAJO),
			        max(0, IRBASE+TAXABASE_MAJO+CAPBASE_MAJO+LOYBASE_MAJO+HRBASE_MAJO)
                               )
                           )

			* STR_TR15
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );
MAJOHR_TARDIF = somme(x = 07,08 : MAJOHR0x_TARDIF) 
		+ MAJOHR17_1TARDIF + MAJOHR17_2TARDIF;
MAJOCA[DGFIP][2017]7_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                             min(min(CAPBASE2042_FIC,CAPBASE_MAJO),
			         max(0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO)
                                )
                            )
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	               );

MAJOCA[DGFIP][2017]8_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
			     min(min(CAPBASE2042_FIC,CAPBASE_MAJO),
			         max(0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO)
                                )
                             )
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	               );

MAJOCA[DGFIP][2017]7_1TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                              min(min(CAPBASE2042_FIC,CAPBASE_MAJO),
			          max(0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO)
                                 )
                             )
			* STR_TR15
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	               );

MAJOCA[DGFIP][2017]7_2TARDIF = max(0,arr(FLAG_TRTARDIF * TX1758A/100 *
                              min(min(CAPBASE2042_FIC,CAPBASE_MAJO),
			          max(0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO)
                                 )
                             )
			* STR_TR15
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	               );
MAJOCAP_TARDIF = somme(x = 07,08 : MAJOCA[DGFIP][2017]x_TARDIF) 
		+ MAJOCA[DGFIP][2017]7_1TARDIF + MAJOCA[DGFIP][2017]7_2TARDIF;
MAJOLO07_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                             min(min(LOYBASE2042_FIC,LOYBASE_MAJO),
			         max(0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + LOYBASE_MAJO)
                                )
                            )
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	               );

MAJOLO08_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
			     min(min(LOYBASE2042_FIC,LOYBASE_MAJO),
			         max(0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + LOYBASE_MAJO)
                                )
                             )
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	               );
