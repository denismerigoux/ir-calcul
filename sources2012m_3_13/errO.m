#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2011 
#au titre des revenus perçus en 2010. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
# Millesime 12 Application oceans
A000:anomalie :"A":"000":"00":"SAISIE D UN MONTANT NEGATIF":"N";
A00101:anomalie :"A":"001":"01":"LE NBRE DE PERS. ACCUEILLIES NE PEUT EXCEDER 9 (6EV)":"N";
A00102:anomalie :"A":"001":"02":"LE NOMBRE D'EXPLOITATIONS NE PEUT EXCEDER 9 (7FG).":"N";
A00103:anomalie :"A":"001":"03":"LE NOMBRE D'ENFANTS SCOLARISES NE PEUT EXCEDER 9(7EA,7EC,7EF,7EB,7ED,7EG)":"N";
A00104:anomalie :"A":"001":"04":"LE NOMBRE D'ASCENDANTS DE PLUS DE 65 ANS NE PEUT EXCEDER 9 (7DL)":"N";
A00105:anomalie :"A":"001":"05":"LE REVENU BRUT GLOBAL IR OU LA BASE CSG,CRDS,PS,C.S NE PEUT EXCEDER 8 CHIFFRES":"N";
A00106:anomalie :"A":"001":"06":"LES PRIMES LOYERS IMPAYES NE PEUVENT ETRE > A 99.999 E":"N";
A00107:anomalie :"A":"001":"07":"LES MONTANTS DECLARES A L'ISF NE PEUVENT EXCEDER 8 CARACTERES":"O";
A00108:anomalie :"A":"001":"08":"MONTANT VARIABLE RESTITUEE SUPERIEUR A 8 CARACTERES":"N";
A004:anomalie :"A":"004":"00":"CASE B COCHEE ET LIGNE X INCOMPATIBLES AVEC SITUATION M OU O":"N";
A005:anomalie :"A":"005":"00":"CASE B NON COCHEE ET LIGNE X INCOMPATIBLES AVEC SITUATION C, D OU V":"N";
A01001:anomalie :"A":"010":"01":"INCOHERENCE ENTRE LA SITUATION M ET LES CODES E, G, N, W, L.":"N";
A01002:anomalie :"A":"010":"02":"INCOHERENCE ENTRE LA SITUATION O ET LES CODES E, G, N, W, L":"N";
A01003:anomalie :"A":"010":"03":"INCOHERENCE ENTRE LA SITUATION V ET LES CODES F, S.":"N";
A01004:anomalie :"A":"010":"04":"INCOHERENCE ENTRE LA SITUATION C ET LES CODES F, S.":"N";
A01005:anomalie :"A":"010":"05":"INCOHERENCE ENTRE LA SITUATION D ET LES CODES F, S.":"N";
A01006:anomalie :"A":"010":"06":"INCOHERENCE ENTRE LA SITUATION C ET LE CODE G":"N";
A01007:anomalie :"A":"010":"07":"INCOHERENCE ENTRE LA SITUATION D ET LE CODE G":"N";
A01008:anomalie :"A":"010":"08":"INCOHERENCE ENTRE LA SITUATION DE VEUF ET LE CODE G":"N";
A01009:anomalie :"A":"010":"09":"INCOHERENCE ENTRE LA SITUATION M ET LE CODE T":"N";
A01010:anomalie :"A":"010":"10":"INCOHERENCE ENTRE LA SITUATION O ET LE CODE T.":"N";
A01011:anomalie :"A":"010":"11":"CODES P, F, S, E, G, N, W, L, T SAISIS SANS CODES M, O, C, D, V":"N";
A01012:anomalie :"A":"010":"12":"CODES SITUATION DE FAMILLE NON VALIDES":"N";
A011:anomalie :"A":"011":"00":"LE NOMBRE D' ENF. RATTACHES CASES N+P NE PEUT ETRE < A 2":"N";
A01201:anomalie :"A":"012":"01":"LE NOMBRE D'ENF. LIGNE G NE PEUT ETRE > AU NOMBRE PORTE LIGNE F":"N";
A01202:anomalie :"A":"012":"02":"LE NOMBRE D'ENF. LIGNE I NE PEUT ETRE > AU NOMBRE PORTE LIGNE H":"N";
A013:anomalie :"A":"013":"00":"LA DATE DE NAISSANCE DU DECLARANT OU DU CONJOINT EST INVRAISEMBLABLE":"N";
A015:discordance :"A":"015":"00":"ATTENTION, CALCUL NON EFFECTUE PAR L'ESI":"N";
A019:anomalie :"A":"019":"00":"ATTENTION CALCUL NON EFFECTUE PAR L'ESI":"N";
A02301:discordance :"A":"023":"01":"LE JOUR DE L EVENEMENT EST DIFFERENT DE 01 A 31":"N";
A02302:discordance :"A":"023":"02":"LE MOIS DE L EVENEMENT EST DIFFERENT DE 01 A 12":"N";
A02303:discordance :"A":"023":"03":"L'ANNEE DE L EVENEMENT EST DIFFERENT DE 2012":"N";
A02401:discordance :"A":"024":"00":"PRISE EN CHARGE DU JOUR SANS SAISE DU MOIS OU INVERSEMENT":"N";
A02402:discordance :"A":"024":"00":"LA DATE DE L EVENEMENT SAISIE EST INCOMPLETE":"N";
A030:discordance :"A":"030":"00":"REV. OU CHARGE AU NOM DE PERSONNES A CHARGE SANS DECLARATION DE P.A.C.":"N";
A031:discordance :"A":"031":"00":"REV. OU CHARGE AU NOM DU CJT OU PARTEN. POUR UN CELIBATAIRE OU UN DIVORCE":"N";
A041:anomalie :"A":"041":"00":"Code revenu 8VV, saisir code penalite 99, 07, 08, 10, 11, 17 ou 18 ":"N";
A042:anomalie :"A":"042":"00":"Code revenu 8VW, saisir code penalite 99, 07, 08, 10, 11, 17 ou 18 ":"N";
A043:anomalie :"A":"043":"00":"Revenus 8VV et 8VW saisis simultanement, supprimer un des deux codes":"N";
A044:anomalie :"A":"044":"00":"Code revenu 8VX, saisir le code penalite 32":"N";
A045:anomalie :"A":"045":"00":"Code penalite 32 accepte uniquement avec le code 8VX":"N";
A046:discordance :"A":"046":"00":"CODE REVENU 8WW, SAISIR LE CODE PENALITE 30":"N";
A047:discordance :"A":"047":"00":"CODE PENALITE 30 ACCEPTE UNIQUEMENT AVEC LE CODE REVENU 8WW":"N";
A048:discordance :"A":"048":"00":"CE CODE PENALITE NE S'APPLIQUE QU'AUX BIC, BNC,BA":"N";
A078:anomalie :"A":"078":"00":"Code inoperant dans les applications ILIAD-IS et ILIAD-CTX":"N";
A079:discordance :"A":"079":"00":"UTILISATION DU CODE 8ZB UNIQUEMENT POUR DES CONTRIBUABLES NON RESIDENTS":"N";
A080:discordance :"A":"080":"00":"CODE 8ZB INCOMPATIBLE AVEC REGIME AUTRE QUE LE TAUX MINIMUM":"N";
A085:discordance :"A":"085":"00":"UTILISATION CODES 8ZO ET 8ZI UNIQUEMENT DANS LES DIRECTIONS B31 OU 060":"N";
A086:discordance :"A":"086":"00":"LE CODE 8ZO EST PRIS EN CHARGE SANS CODE 8ZI OU RECIPROQUEMENT":"N";
A087:discordance :"A":"087":"00":"CODE 8ZK INCOMPATIBLE AVEC TAUX MINIMUM OU VALEUR LOCATIVE":"N";
A088:discordance :"A":"088":"00":"INCOMPAT.ENTRE TX EFF. OU PRORATA DOM ET TX MIN. OU VALEUR LOCATIVE":"N";
A089:discordance :"A":"089":"00":"CODE 8ZT INCOMPATIBLE AVEC REGIME AUTRE QUE LE TAUX MINIMUM":"N";
A090:discordance :"A":"090":"00":"LE MONTANT 8ZT EST > AU REVENU NET IMPOSABLE DU .1":"N";
A091:discordance :"A":"091":"00":"CODE 8ZW OU 8ZY INCOMPATIBLE AVEC REGIME AUTRE QUE LE TAUX MINIMUM":"N";
A092:discordance :"A":"092":"00":"LA LIGNE 8TM EST REMPLIE SANS PRISE EN CHARGE DU CODE 8ZW OU 8ZY OU INV.":"N";
A093:discordance :"A":"093":"00":"INCOMPATIBILITE ENTRE 8ZW ET 8ZY (TAUX MOYEN)":"N";
A094:discordance :"A":"094":"00":"REGIME D'IMPOSITION INCOMPATIBLE AVEC CODE 8ZH (NON RESIDENTS).":"N";
A095:discordance :"A":"095":"00":"NON RESIDENTS : LA VALEUR LOCATIVE (8ZH) DOIT ETRE INDIQUEE.":"N";
A096:discordance :"A":"096":"00":"CODES 8YO ET 8YI : UNIQUEMENT CONTRIBUABLES NE RELEVANT PAS DU SIP NON RESIDENT":"N";
A097:anomalie :"A":"097":"00":"CODE 6QW INCOMPATIBLE AVEC TAUX MINIMUM OU VALEUR LOCATIVE":"N";
A098:anomalie :"A":"098":"00":"LIGNE 3SE INCOMPATIBLE AVEC REGIME AUTRE QUE TX MINIMUM ET VALEUR LOCATIVE":"N";
A13901:discordance :"A":"139":"01":"LE NB D'ANNEES D'ECHEANCE N'EST PAS COMPRIS ENTRE 2 ET 45":"N";
A13902:discordance :"A":"139":"02":"PENSIONS AU QUOTIENT : REVOIR LA CODIFICATION":"N";
A14001:discordance :"A":"140":"01":"LE NB D'ANNEES D'ECHEANCE N'EST PAS COMPRIS ENTRE 2 ET 45":"N";
A14002:discordance :"A":"140":"02":"SALAIRES AU QUOTIENT : REVOIR LA CODIFICATION":"N";
A14101:discordance :"A":"141":"01":"LE NOMBRE D'ANNEES DE COTISATIONS EST SUPERIEUR A 25":"N";
A14102:discordance :"A":"141":"02":"IMPOSITION DES FOOTBALLEURS: REVOIR LA CODIFICATION":"N";
A143:discordance :"A":"143":"00":"PRESENCE DE FRAIS REELS SANS REVENU CORRESPONDANT":"N";
A14401:discordance :"A":"144":"01":"LE MONTANT 1AJ NE PEUT ETRE INFERIEUR AU MONTANT DE 1NY":"N";
A14402:discordance :"A":"144":"02":"LE MONTANT 1BJ NE PEUT ETRE INFERIEUR AU MONTANT DE 1OY":"N";
A146:discordance :"A":"146":"00":"CODE DEMANDEUR D'EMPLOI DE PLUS D'UN AN SANS SALAIRE CORRESPONDANT":"N";
A148:discordance :"A":"148":"00":"SAISIE DU CODE 1LZ OU 1MZ SANS CODE 8ZK EN ABSENCE DE TAUX EFFECTIF":"N";
A149:anomalie :"A":"149":"00":"CODE CREDIT D'IMPOT JEUNE NON ADMIS POUR CETTE SITUATION DE FAMILLE":"N";
A150:anomalie :"A":"150":"00":"CODE CREDIT D'IMPOT JEUNE INCOMPATIBLE AVEC DEPART A L'ETRANGER (9YD).":"N";
A153:discordance :"A":"153":"00":"INDICATION D'UNE ACTIVITE A TEMPS PLEIN ET PARTIEL POUR LA MEME PERSONNE":"N";
A21801:discordance :"A":"218":"01":"CREDIT D'IMPOT (2AB) > 80 EUROS SANS REVENU CORRESPONDANT":"N";
A21802:discordance :"A":"218":"02":"CREDIT D'IMPOT (2BG) > 80 EUROS SANS REVENU CORRESPONDANT":"N";
A222:discordance :"A":"222":"00":"LE MONTANT 2BH NE PEUT ETRE > AU TOTAL 2DC + 2CH + 2TS + 2TR":"N";
A22301:discordance :"A":"223":"01":"LE NB D ANNEES D ECHEANCE N EST PAS COMPRIS ENTRE 2 ET 20":"N";
A22302:discordance :"A":"223":"02":"RCM AU QUOTIENT : REVOIR LA CODIFICATION":"N";
A320:discordance :"A":"320":"00":"INCOMPATIBILITE ENTRE 3VH(PERTES) ET 3VG (GAINS), 3VE(GAINS DOM), 3VM, 3VT (PEA)":"N";
A321:discordance :"A":"321":"00":"INCOMPATIBILITE ENTRE 3VA(ABATT. SUR PV) ET 3VB (ABATT. SUR MOINS VALUE)":"N";
A32201:discordance :"A":"322":"01":"MONTANT LIGNE 3VE POUR UN CONTRIBUABLE NON DOMICILIE DANS UN DOM":"N";
A32202:discordance :"A":"322":"02":"MONTANT LIGNE 3VV POUR UN CONTRIBUABLE NON DOMICILIE DANS UN DOM":"N";
A323:discordance :"A":"323":"00":"INCOMPATIBILITE ENTRE 3VQ (ABATT. SUR P.V) ET 3VR (ABATT. SUR MOINS VALUE)":"N";
A420:discordance :"A":"420":"00":"LE REGIME MICRO FONCIER (4BE) EST INCOMPATIBLE AVEC LES LIGNES 4BA,4BB,4BC":"N";
A421:discordance :"A":"421":"00":"LE MONTANT PORTE LIGNE 4BE NE PEUT DEPASSER 15000 E":"N";
A422:discordance :"A":"422":"00":"MONTANT PORTE LIGNE 4BF SANS REVENU OU DEFICIT FONCIER CORRESPONDANT":"N";
A423:discordance :"A":"423":"00":"INCOHERENCE ENTRE SAISIE LIGNE 4 BY ET LIGNE 4 BD (SANS 4 BA, 4 BB, 4 BC)":"N";
A424:discordance :"A":"424":"00":"LIGNE 4 BY SAISIE AVEC CODE QUOTIENT (RBA , SBA) : TRAITEMENT PAR 1551 MI":"N";
A42501:discordance :"A":"425":"01":"LE NB D ANNEES D ECHEANCE N EST PAS COMPRIS ENTRE 2 ET 30":"N";
A42502:discordance :"A":"425":"02":"REVENUS FONCIERS AU QUOTIENT : REVOIR LA CODIFICATION":"N";
A42601:discordance :"A":"426":"01":"LE NB D ANNEES D ECHEANCE N EST PAS COMPRIS ENTRE 2 ET 14":"N";
A42602:discordance :"A":"426":"02":"REINTEGRATION AMORTISSEMENT AU QUOTIENT : REVOIR LA CODIFICATION":"N";
A534:discordance :"A":"534":"00":"FORFAIT A FIXER POUR CONTRIBUABLE RESTITUABLE (IMP. OU REST. PART. EN N-1)":"N";
A538:discordance :"A":"538":"00":"REVENUS A IMPOSER AUX CONTRIBUTIONS SOCIALES SANS REVENU CORRESPONDANT":"N";
A542:discordance :"A":"542":"00":"INDICATION D'UNE ACTIVITE SUR L'ANNEE COMPLETE AVEC SAISIE NOMBRE DE JOURS":"N";
A600:discordance :"A":"600":"00":"COTISATIONS EPARGNE RETRAITE 6RS, 6RT, 6RU SANS PLAFOND CORRESPONDANT":"N";
A601:discordance :"A":"601":"00":"LE MONTANT APS, APT MIS A JOUR (VOUS,CJT)NE PEUT DEPASSER 28 282 EUROS":"N";
A602:discordance :"A":"602":"00":"RACHATS DE COTISATIONS LIGNE 6SS, 6ST, 6SU SANS COTISATION CORRESPONDANTE":"N";
A603:discordance :"A":"603":"00":"LES LIGNES 6PS, 6PT, 6PU SONT REMPLIES SANS MISE A JOUR DU PLAFOND":"N";
A700:discordance :"A":"700":"00":"MONTANT PORTE LIGNE 7FF SANS REVENU CORRESPONDANT":"N";
A701:discordance :"A":"701":"00":"LE MONTANT PORTE LIGNE 7UE + 7VB NE PEUT DEPASSER 521 E":"N";
A702:discordance :"A":"702":"00":"LE MONTANT 7UM NE PEUT ETRE SUPERIEUR AU REVENU 2TR":"N";
A703:discordance :"A":"703":"00":"CREDIT INTERETS PRET ETUDIANT (7UK,7TD) AVEC VOUS ET CONJOINT NES AVANT 1979":"N";
A704:discordance :"A":"704":"00":"SAISIE DU CODE 7VO SANS PRISE EN CHARGE DE MONTANT LIGNE 7TD ET INVERSEMENT":"N";
A705:discordance :"A":"705":"00":"LE CHIFFRE DE LA LIGNE 7LY NE PEUT ETRE SUPERIEUR A 15":"N";
A706:discordance :"A":"706":"00":"LE CHIFFRE LIGNE 7MY EST SUPERIEUR AU CHIFFRE DE 7LY":"N";
A70701:discordance :"A":"707":"01":"TOTAL LIGNES 7 EA + 7 EC + 7 EF > TOTAL PAC (CODES F, J, N)":"N";
A70702:discordance :"A":"707":"02":"TOTAL LIGNES 7 EB + 7 ED + 7 EG > TOTAL PAC (CODES H,P)":"N";
A708:discordance :"A":"708":"00":"LE MONTANT DU REPORT NE PEUT DEPASSER 33 333 E":"N";
A709:discordance :"A":"709":"00":"SAISIE DE PLUS D'UNE LIGNE : 7XC,7XF,7XI,7XP,7XN,7XL,7XJ,7XQ,7XV,7XM":"N";
A710:discordance :"A":"710":"00":"SAISIE DES LIGNES 7DB + 7DF INCOMPATIBLES":"N";
A71101:discordance :"A":"711":"01":"SAISIE DU CODE 7DG SANS PRISE EN CHARGE DE MONTANT LIGNE 7DB,7DD OU 7DF":"N";
A71102:discordance :"A":"711":"02":"SAISIE DU CODE 7DL SANS PRISE EN CHARGE DE MONTANT LIGNE 7DD OU INVERSEMENT":"N";
A71103:discordance :"A":"711":"03":"SAISIE DU CODE 7DQ SANS PRISE EN CHARGE DE MONTANT LIGNE 7DB,7DD OU 7DF":"N";
A712:discordance :"A":"712":"00":"LE MONTANT PORTE LIGNE 7WN NE PEUT DEPASSER LE MONTANT 7WO":"N";
A713:discordance :"A":"713":"00":"PRESENCE D UN MONTANT LIGNE 7WN SANS MONTANT 7WO OU RECIPROQUEMENT":"N";
A714:discordance :"A":"714":"00":"REPORT 2011 (7WP) INCOMPATIBLE AVEC VERSEMENTS 2012 (7WO ET 7WN)":"N";
A715:discordance :"A":"715":"00":"LE MONTANT LIGNE 7WP NE PEUT DEPASSER 30 500 E":"N";
A716:discordance :"A":"716":"00":"LE MONTANT 7WM EST INFERIEUR AU MONTANT 7WN":"N";
A71701:discordance :"A":"717":"01":"SAISIE DE PLUS D UNE LIGNE PARMI LES LIGNES 7JA A 7JR":"N";
A71702:discordance :"A":"717":"02":"SAISIE DE PLUS D UNE LIGNE PARMI LES LIGNES 7NA A 7NT":"N";
A71703:discordance :"A":"717":"03":"SAISIE DE PLUS D UNE LIGNE PARMI LES LIGNES 7HJ,7HK,7HN ET 7HO":"N";
A71704:discordance :"A":"717":"04":"INCOMPATIBILITE ENTRE LIGNES 7HL ET 7HM":"N";
A718:discordance :"A":"718":"00":"LA LIGNE 7UO EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A719:discordance :"A":"719":"00":"LA LIGNE 7US EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A72001:anomalie :"A":"720":"01":"SENS INCOMPATIBLE AVEC LA SITUATION INITIALE, SAISIR R AU LIEU DE M":"N";
A72002:anomalie :"A":"720":"02":"SENS INCOMPATIBLE AVEC LA SITUATION INITIALE, SAISIR R AU LIEU DE C":"N";
A72003:anomalie :"A":"720":"03":"SENS INCOMPATIBLE AVEC LA SITUATION INITIALE, SAISIR C OU M AU LIEU DE R":"N";
A721:anomalie :"A":"721":"00":"SENS P IMPOSSIBLE POUR UN CODE ABSENT DE LA 2042":"N";
A72201:anomalie :"A":"722":"01":"LE MONTANT DU RAPPEL EST > A LA VALEUR DU CODE CELLULE AVANT CONTROLE":"N";
A72202:anomalie :"A":"722":"02":"LE MONTANT DE LA MINO EST > A LA VALEUR DU CODE CELLULE AVANT CONTROLE":"N";
A72203:anomalie :"A":"722":"03":"LE MONTANT DE LA CORRECTION EST > A LA VALEUR DU CODE CELLULE DE LA 2042":"N";
A72204:anomalie :"A":"722":"04":"LE MONTANT DE LA RECTIF EST > A LA VALEUR DU CODE CELLULE DE LA 2042":"N";
A72206:anomalie :"A":"722":"06":"LE MONTANT DE LA CORRECTION EST > AUX RAPPELS EFFECTUES SUR CE CODE":"N";
A72207:discordance :"D":"722":"07":"LE MONTANT DE LA CORRECTION EST > AUX RAPPELS EFFECTUES SUR CE CODE":"N";
A72301:anomalie :"A":"723":"01":"LE MONTANT DU C02 AVEC 2042_RECT EST SUPERIEUR A CELUI DU R02 AVEC INDICATEUR":"N";
A72302:anomalie :"A":"723":"02":"ANNULATION OU MINORATION DE R02 ET 2042_RECT S'EFFECTUE AVEC C02 et 2042_RECT":"N";
A724:anomalie :"A":"724":"00":"Majorations 80% apres 2 MED supprimees a compter du 01/01/2006":"N";
A725:anomalie :"A":"725":"00":"PROCEDURE DE REGULARISATION INTERDITE EN CAS DE NON DEPOT OU DEPOT TARDIF":"N";
A73001:discordance :"A":"730":"01":"TRAVAUX FORESTIERS : INCOMPATIBILITE ENTRE LIGNES 7UU ET 7TE":"N";
A73002:discordance :"A":"730":"02":"TRAVAUX FORESTIERS : INCOMPATIBILITE ENTRE LIGNES 7UV ET 7TF":"N";
A73003:discordance :"A":"730":"03":"TRAVAUX FORESTIERS : INCOMPATIBILITE ENTRE LIGNES 7UW ET 7TG":"N";
A731:discordance :"A":"731":"00":"LE CHIFFRE DE LA LIGNE 7VO NE PEUT ETRE SUPERIEUR A 5":"N";
A733:discordance :"A":"733":"00":"LE CREDIT D IMPOT DE LA LIGNE 7SZ NE PEUT EXCEDER 20 000 E":"N";
A73501:discordance :"A":"735":"01":"CASE 7WE OU 7WG COCHEE SANS PRISE EN CHARGE LIGNES 7TT A 7TY OU 7SD A 7SW":"N";
A73502:discordance :"A":"735":"02":"LES CASES 7WE ET 7WG NE PEUVENT ETRE SIMUTANEMENT COCHEES":"N";
A73601:discordance :"A":"736":"01":"SAISIE DE PLUS D UNE LIGNE PARMI LES LIGNES 7ID, 7IE, 7IF ET 7IG":"N";
A73602:discordance :"A":"736":"02":"SAISIE DE PLUS D UNE LIGNE PARMI LES LIGNES 7IJ, 7IL, 7IN, 7IV":"N";
A73603:discordance :"A":"736":"03":"INCOMPATIBILITE ENTRE LIGNES 7IM ET 7IW":"N";
A73701:discordance :"A":"737":"01":"CASE 7 XD COCHEE SANS PRISE EN CHARGE MONTANT LIGNE 7 XC":"N";
A73702:discordance :"A":"737":"02":"CASE 7 XE COCHEE SANS PRISE EN CHARGE MONTANT LIGNE 7 XL":"N";
A738:discordance :"A":"738":"00":"CASE 7 UT COCHEE SANS PRISE EN CHARGE MONTANT LIGNE 7 UP":"N";
A739:discordance :"A":"739":"00":"CASE 7 QA COCHEE SANS PRISE EN CHARGE LIGNE CORRESPONDANTE":"N";
A74001:discordance :"A":"740":"01":"LE TOTAL LIGNES 7 PR + 7 PW NE PEUT ETRE SUPERIEUR A 300 000 E":"N";
A74002:discordance :"A":"740":"02":"LE MONTANT LIGNE 7 RI NE PEUT ETRE SUPERIEUR A 270 000 E":"N";
A74003:discordance :"A":"740":"03":"LE TOTAL LIGNES 7RO + 7RT + 7RY + 7NY NE PEUT ETRE SUPERIEUR A 229 500 E":"N";
A74004:discordance :"A":"740":"04":"LE TOTAL 7PR+7PW+7RI+7RO+7RT+7RY+7NY NE PEUT EXCEDER LES LIMITES ADMISES":"N";
A741:discordance :"A":"741":"00":"LE MONTANT DES REPORTS NE PEUT DEPASSER 99 999 E":"N";
A74201:discordance :"A":"742":"01":"INCOMPATIBILITE ENTRE LIGNES 7HR ET 7HS":"N";
A74202:discordance :"A":"742":"02":"INCOMPATIBILITE ENTRE LIGNES 7HT ET 7HU":"N";
A74203:discordance :"A":"742":"03":"SAISIE DE PLUS D UNE LIGNE PARMI LES LIGNES 7HV, 7HW, 7HX , 7HZ":"N";
A74204:discordance :"A":"742":"04":"INCOMPATIBILITE ENTRE LIGNES 7HD ET 7HE":"N";
A74205:discordance :"A":"742":"05":"SAISIE DE PLUS D UNE LIGNE PARMI LES LIGNES 7HA , 7HB , 7HG , 7HH":"N";
A743:discordance :"A":"743":"00":"LE MONTANT PORTE LIGNE 7IK, 7IP, 7IQ, 7IR NE PEUT DEPASSER 99 999 E":"N";
A744:discordance :"A":"744":"00":"LE MONTANT PORTE LIGNE 7WR NE PEUT DEPASSER 60 000 E":"N";
A745:discordance :"A":"745":"00":"INCOMPATIBILITE ENTRE LIGNES 7IP ET 7IQ":"N";
A74601:discordance :"A":"746":"01":"LE MONTANT LIGNE 7PR NE PEUT EXCEDER LE MONTANT LIGNE 7PQ":"N";
A74602:discordance :"A":"746":"02":"LE MONTANT LIGNE 7PW NE PEUT EXCEDER LE MONTANT LIGNE 7PV":"N";
A74603:discordance :"A":"746":"03":"LE MONTANT LIGNE 7RI NE PEUT EXCEDER LE MONTANT LIGNE 7RH":"N";
A74604:discordance :"A":"746":"04":"LE MONTANT LIGNE 7RO NE PEUT EXCEDER LE MONTANT LIGNE 7RN":"N";
A74605:discordance :"A":"746":"05":"LE MONTANT LIGNE 7RT NE PEUT EXCEDER LE MONTANT LIGNE 7RS":"N";
A74606:discordance :"A":"746":"06":"LE MONTANT LIGNE 7RY NE PEUT EXCEDER LE MONTANT LIGNE 7RX":"N";
A74607:discordance :"A":"746":"07":"LE MONTANT LIGNE 7NY NE PEUT EXCEDER LE MONTANT LIGNE 7NX":"N";
A747:discordance :"A":"747":"00":"MONTANT LIGNE 7FL POUR UN CONTRIBUABLE NON DOMICILIE DANS UN DOM":"N";
A74801:discordance :"A":"748":"01":"CASE 7WH COCHEE SANS MONTANT LIGNE 7SD A 7ST ":"N";
A74802:discordance :"A":"748":"02":"CASE 7WK COCHEE SANS MONTANT LIGNE 7SD A 7SW ":"N";
A74901:discordance :"A":"749":"01":"CASE 7WF 7WQ 7WS COCHEE SANS MONTANT LIGNE 7SJ ET RECIPROQUEMENT":"N";
A74902:discordance :"A":"749":"02":"CASE 7WU 7WV COCHEE SANS MONTANT LIGNE 7SK ET RECIPROQUEMENT":"N";
A74903:discordance :"A":"749":"03":"CASE 7WW 7WX COCHEE SANS MONTANT LIGNE 7SL ET RECIPROQUEMENT":"N";
A74904:discordance :"A":"749":"04":"CASE 7WA 7WB COCHEE SANS MONTANT LIGNE 7SG ET RECIPROQUEMENT":"N";
A74905:discordance :"A":"749":"05":"CASE 7VE 7VF COCHEE SANS MONTANT LIGNE 7SH ET RECIPROQUEMENT":"N";
A75001:discordance :"A":"750":"01":"SAISIE DE PLUS D UNE LIGNE PARMI LES LIGNES 7WF, 7WQ, 7WS":"N";
A75002:discordance :"A":"750":"02":"INCOMPATIBILITE ENTRE LES LIGNES 7WU ET 7WV":"N";
A75003:discordance :"A":"750":"03":"INCOMPATIBILITE ENTRE LES LIGNES 7WW ET 7WX":"N";
A75004:discordance :"A":"750":"04":"INCOMPATIBILITE ENTRE LES LIGNES 7WA ET 7WB":"N";
A75005:discordance :"A":"750":"05":"INCOMPATIBILITE ENTRE LES LIGNES 7VE ET 7VF":"N";
A75101:discordance :"A":"751":"01":"CASE 7WT COCHEE SANS MONTANT LIGNE 7SJ":"N";
A75102:discordance :"A":"751":"02":"CASE 7WC COCHEE SANS MONTANT LIGNE 7SG":"N";
A75103:discordance :"A":"751":"03":"CASE 7VG COCHEE SANS MONTANT LIGNE 7SH":"N";
A752:discordance :"A":"752":"00":"INCOMPATIBILITE ENTRE 7TT A 7TY ET 7SD A 7SW":"N";
A760:discordance :"A":"760":"00":"LE SENS DU RAPPEL SAISI N'APPARTIENT PAS AUX VALEURS POSSIBLES":"N";
A770:discordance :"A":"770":"00":"SI LE CODE RAPPEL EST P, LE CODE MAJO DOIT ETRE 07,08,10,11,17,18 ou 31":"N";
A780:discordance :"A":"780":"00":"MOIS OU ANNEE DE LA DATE DE NOTIFICATION INVRAISEMBLABLE":"N";
A800:discordance :"A":"800":"00":"LE MONTANT 8ZP OU 8ZV EST > AU REVENU BRUT GLOBAL":"N";
A80201:discordance :"A":"802":"01":"INCOMPATIBILITE ENTRE 8ZR ET 8ZN (TX EFFECTIF)":"N";
A80202:discordance :"A":"802":"02":"INCOMPATIBILITE ENTRE TX EFFECTIF ET PRORATA METRO-DOM":"N";
A80203:discordance :"A":"802":"03":"INCOMPATIBILITE ENTRE TX EFFECTIF ET PRORATA METRO-DOM":"N";
A804:discordance :"A":"804":"00":"INCOMPATIBILITE ENTRE METRO/DOM ET REV. AU QUOTIENT":"N";
A805:discordance :"A":"805":"00":"SAISIE DE REVENUS EXCEP. OU DIFF. LIGNE XX SANS CODIFICATION":"N";
A807:discordance :"A":"807":"00":"SAISIE CODE 8XT SANS LIGNE 1AT OU 1BT":"N";
A821:discordance :"A":"821":"00":"IMPOSITION RETOUR EN FRANCE : CODE 8 YO SAISI SANS CODE 8 YI":"N";
A854:anomalie :"A":"854":"00":"SAISIE DU CODE 8YV INCOMPATIBLE AVEC UN TRAITEMENT CORRECTIF":"N";
A855:anomalie :"A":"855":"00":"SAISIE DU CODE 8YW INCOMPATIBLE AVEC UN TRAITEMENT CORRECTIF":"N";
A859:discordance :"A":"859":"00":"LA LIGNE 8WC EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A860:discordance :"A":"860":"00":"LA LIGNE 8WV EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A862:discordance :"A":"862":"00":"LA LIGNE 8 UY EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A863:anomalie :"A":"863":"00":"LE CODE 8UX EST SAISI SANS REVENU AUTO-ENTREPRENEUR":"N";
A86601:discordance :"A":"866":"01":"CSG A 7,5% PROVISOIRE (8YD) >  CSG DUE":"N";
A86602:discordance :"A":"866":"02":"CSG A 7,5% PROVISOIRE (8YE) > CSG DUE":"N";
A86603:discordance :"A":"866":"03":"CSG A 6,6% PROVISOIRE (8YF) > CSG DUE":"N";
A86604:discordance :"A":"866":"04":"CSG A 6,2% PROVISOIRE (8YG) > CSG DUE":"N";
A86605:discordance :"A":"866":"05":"CSG A 3,8% PROVISOIRE (8YH) > CSG DUE":"N";
A867:anomalie :"A":"867":"00":"CONTRIBUTION SALARIALE PROVISOIRE 8 % (8YP) > A CONTRIBUTION DUE":"N";
A868:anomalie :"A":"868":"00":"CONTRIBUTION SALARIALE PROVISOIRE 30 % (8YW) > A CONTRIBUTION DUE":"N";
A869:anomalie :"A":"869":"00":"CONTRIBUTION SALARIALE PROVISOIRE 2,5 % (8YV) > A CONTRIBUTION DUE":"N";
A870:discordance :"A":"870":"00":"SAISIE DU CODE 8ZM SANS PRISE EN CHARGE DU CODE 8ZL":"N";
A871:anomalie :"A":"871":"00":"MONTANT CRDS PROVISOIRE (8ZJ) > AU MONTANT DE CRDS DU":"N";
A872:anomalie :"A":"872":"00":"MONTANT DU PS PROVISOIRE (8ZS) > AU MONTANT DU PS DU":"N";
A873:anomalie :"A":"873":"00":"MONTANT CSG PROVISOIRE (8ZL) > AU MONTANT DE CSG DU":"N";
A874:discordance :"A":"874":"00":"LA LIGNE 8TA EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A875:anomalie :"A":"875":"00":"MONTANT IMPOT PROVISOIRE (8ZG) > AU MONTANT DE l'IMPOT DU ":"N";
A877:discordance :"A":"877":"00":"LES LIGNES 8TB, 8TC SONT REMPLIES SANS REVENU CORRESPONDANT":"N";
A87801:discordance :"A":"878":"01":"SAISIE CODE 8XA SANS REVENU LIGNE 8TQ":"N";
A87802:discordance :"A":"878":"02":"SAISIE CODE 8XB SANS REVENU LIGNE 8TR":"N";
A87803:discordance :"A":"878":"03":"SAISIE CODE 8XC SANS REVENU LIGNE 8TV":"N";
A87804:discordance :"A":"878":"04":"SAISIE CODE 8XD SANS REVENU LIGNE 8TW":"N";
A87805:discordance :"A":"878":"05":"SAISIE CODE 8XE SANS REVENU LIGNE 8TX":"N";
A879:discordance :"A":"879":"00":"LA LIGNE 8TG, 8TS EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A880:discordance :"A":"880":"00":"LA LIGNE 8TE EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A881:discordance :"A":"881":"00":"LA LIGNE 8UZ EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A88201:discordance :"A":"882":"01":"LIGNE 8TI REMPLIE SANS PRISE EN CHARGE DU CODE 8ZR OU 8ZN OU INVERSEMENT":"N";
A88202:discordance :"A":"882":"02":"SAISIE DU CODE 8YC SANS PRISE EN CHARGE DE LA LIGNE 8TI":"N";
A883:discordance :"A":"883":"00":"LA LIGNE 8TK EST REMPLIE SANS PRISE EN CHARGE DE 8XR, 8XP, 8XQ OU 8XT":"N";
A884:discordance :"A":"884":"00":"PRISE EN CHARGE DU CODE 8XR SANS REVENU CORRESPONDANT":"N";
A885:discordance :"A":"885":"00":"PRISE EN CHARGE DU CODE 8XP OU 8XQ SANS PLUS-VALUE CORRESPONDANTE":"N";
A886:discordance :"A":"886":"00":"SAISIE DU CODE 8TU SANS PRISE EN CHARGE DU CODE 8XR, 8XP OU 8XQ":"N";
A888:discordance :"A":"888":"00":"LA LIGNE 8TH EST REMPLIE SANS REVENU DES LIGNES 1AJ, 1BJ, 1AP, 1BP":"N";
A890:discordance :"A":"890":"00":"LA LIGNE 8TZ EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A891:discordance :"A":"891":"00":"LA LIGNE 8WB EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A893:discordance :"A":"893":"00":"LA LIGNE 8WD EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A894:discordance :"A":"894":"00":"LA LIGNE 8WE EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A895:discordance :"A":"895":"00":"LA LIGNE 8WA EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A896:discordance :"A":"896":"00":"LA LIGNE 8WR EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A898:discordance :"A":"898":"00":"LA LIGNE 8WT EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A899:discordance :"A":"899":"00":"LA LIGNE 8WU EST REMPLIE SANS REVENU CORRESPONDANT":"N";
A910:anomalie :"A":"910":"00":"PRESENCE D'UNE CASE A COCHER : CE CODE NE PEUT PRENDRE QU'UNE VALEUR DE 0 OU 1":"N";
A950:discordance :"A":"950":"00":"NATIMP OU PPENATREST DIFFERENT DES VALEURS ADMISES":"N";
A951:discordance :"A":"951":"00":"CODINI DIFFERENT DES VALEURS ADMISES":"N";
A954:discordance :"A":"954":"00":"NATIMPIR DIFFERENT DES VALEURS ADMISES":"N";
A955:discordance :"A":"955":"00":"NATCRP DIFFERENT DES VALEURS ADMISES":"N";
A960:discordance :"A":"960":"00":"L'AMENDE NE PEUT ETRE INFERIEURE A 15 E":"N";
A96101:discordance :"A":"961":"01":"LE CODE MAJORATION 9YT EST DIFFERENT DE 07":"N";
A96102:discordance :"A":"961":"02":"LE CODE MAJORATION 9YT N'EST PAS VALIDE (07 A 11,17 ET 18)":"N";
A96103:discordance :"A":"961":"03":"LE CODE MAJORATION 9XT N'EST PAS VALIDE (07, 08, 17 ET 18)":"O";
A96201:discordance :"A":"962":"01":"LA DATE DE DEPOT DE LA DECLARATION N'EST PAS INDIQUEE":"N";
A96202:discordance :"A":"962":"02":"LE CODE MAJORATION N'EST PAS INDIQUE":"N";
A96203:discordance :"A":"962":"03":"ISF : LA DATE DE DEPOT DE LA DECLARATION N'EST PAS INDIQUEE":"O";
A96204:discordance :"A":"962":"04":"ISF : LE CODE MAJORATION N'EST PAS INDIQUE":"O";
A96301:discordance :"A":"963":"01":"LE MOIS DU DEPOT EST DIFFERENT DE 01 A 12":"N";
A96302:discordance :"A":"963":"02":"L'ANNEE DU DEPOT EST DIFFERENTE DE 2013, 2014 OU 2015":"N";
A96303:discordance :"A":"963":"03":"ISF : LE MOIS DU DEPOT EST DIFFERENT DE 01 A 12":"O";
A96304:discordance :"A":"963":"04":"ISF : L'ANNEE DU DEPOT EST DIFFERENTE DE 2013, 2014 OU 2015":"O";
A96401:discordance :"A":"964":"01":"LE JOUR DU DEPART A L'ETRANGER EST DIFFERENT DE 01 A 31":"N";
A96402:discordance :"A":"964":"02":"LE MOIS DU DEPART A L'ETRANGER EST DIFFERENT DE 01 A 12":"N";
A96403:discordance :"A":"964":"03":"L'ANNEE DU DEPART A L'ETRANGER EST DIFFERENTE DE 2012":"N";
A96501:discordance :"A":"965":"01":"LE JOUR DU RETOUR EN FRANCE EST DIFFERENT DE 01 A 31":"N";
A96502:discordance :"A":"965":"02":"LE MOIS DU RETOUR EN FRANCE EST DIFFERENT DE 01 A 12":"N";
A96503:discordance :"A":"965":"03":"L'ANNEE DU RETOUR EN FRANCE EST DIFFERENTE DE 2012":"N";
A966:discordance :"A":"966":"00":"UNE SEULE DATE DE DEPART OU DE RETOUR PEUT ETRE PRISE EN CHARGE":"N";
A96801:anomalie :"A":"968":"01":"CODE 9YT AVEC VALEUR A 0 ET 9YU AVEC VALEUR DIFFERENTE DE 0  DANS LE MEME EVT":"N";
A96802:anomalie :"A":"968":"02":"CODE 9YU AVEC VALEUR A 0 ET 9YT AVEC VALEUR DIFFERENTE DE 0  DANS LE MEME EVT":"N";
A96803:anomalie :"A":"968":"03":"CODE 9XT AVEC VALEUR A 0 ET 9XU AVEC VALEUR DIFFERENTE DE 0  DANS LE MEME EVT":"N";
A96804:anomalie :"A":"968":"04":"CODE 9XU AVEC VALEUR A 0 ET 9XT AVEC VALEUR DIFFERENTE DE 0  DANS LE MEME EVT":"N";
A96901:anomalie :"A":"969":"01":"R99 SUR LE CODE 9YT ET ABSENCE DE R99 SUR LE CODE 9YU DANS LE MEME EVT":"N";
A96902:anomalie :"A":"969":"02":"R99 SUR LE CODE 9YU ET ABSENCE DE R99 SUR LE CODE 9YT DANS LE MEME EVT":"N";
A96903:anomalie :"A":"969":"03":"R99 SUR LE CODE 9XT ET ABSENCE DE R99 SUR LE CODE 9XU DANS LE MEME EVT":"N";
A96904:anomalie :"A":"969":"04":"R99 SUR LE CODE 9XU ET ABSENCE DE R99 SUR LE CODE 9XT DANS LE MEME EVT":"N";
A970:anomalie :"A":"970":"00":"PRESENCE D'UN TAUX DE PENALITE POUR UNE MINORATION":"N";
A971:anomalie :"A":"971":"00":"ABSENCE D'UN TAUX DE PENALITE POUR UN RAPPEL":"N";
A98001:discordance :"A":"980":"01":"BASE ISF 9HI <= 1 300 000 E : SUPPRIMER 9HI ET SAISIR 9GY = X":"O";
A98002:discordance :"A":"980":"02":"BASE ISF 9HI >= 2 570 000 E : SUPPRIMER 9HI ET SAISIR 9GZ = X":"O";
A98003:discordance :"A":"980":"03":"LA BASE ISF NE PEUT ETRE INFERIEURE OU EGALE A 1 300 000":"O";
A98004:discordance :"A":"980":"04":"LA BASE ISF, INFERIEURE OU EGALE A 1 300 000 E, EST DIFFERENTE DE 0 E":"O";
A98005:discordance :"A":"980":"05":"LA BASE ISF NE PEUT ETRE SUPERIEURE OU EGALE A 2 750 000 E":"O";
A981:discordance :"A":"981":"00":"LA BASE NETTE IMPOSABLE ISF DE LA LIGNE 9HI DOIT ETRE SAISIE":"O";
A982:discordance :"A":"982":"00":"ISF : SAISIE SIMULTANEE DES CODES 9 GY ET 9 GZ":"O";
A983:discordance :"A":"983":"00":"ISF : INCOMPATIBILITE ENTRE 9GL ET SITUATION M, O OU C, D, V AVEC CASE B":"O";
A984:discordance :"A":"984":"00":"ISF : LIGNE 9GM INCOMPATIBLE AVEC SITUATION M, O OU C, D, V SANS OPTION CASE B":"O";
A985:discordance :"A":"985":"00":"ISF : 9 GY OU 9 GZ SAISI POUR UNE BASE 9HI ADMISE":"O";
A986:discordance :"A":"986":"00":"ISF : 9 GL et 9GM incompatibles":"O";
A98701:discordance :"A":"987":"01":"ISF: ETAT CIVIL REMPLI SANS CODE 9GL OU 9GM":"O";
A98702:anomalie :"A":"987":"02":"ISF: CODE 9GL OU 9GM SANS ETAT CIVIL":"O";
A989:discordance :"A":"989":"00":"ISF : Utiliser la meme penalite art. 1728 que lors du traitement primitif.":"O";
A990:discordance :"A":"990":"00":" CODE 8VV INCOMPATIBLE AVEC REGIME VALEUR LOCATIVE":"N";
A99301:anomalie :"A":"993":"01":"ANNULATION 2042: RAMENER LES REVENUS A 0.":"N";
A99302:anomalie :"A":"993":"02":"SUPPRESSION du code 9YA impossible, veuillez ressaisir une declaration":"N";
AS0101:discordance :"A":"S01":"01":"INCOMPATIBILITE ENTRE DATE DE DECES DU CJT ET SITUATION DE FAMILLE C":"N";
AS0102:discordance :"A":"S01":"02":"INCOMPATIBILITE ENTRE DATE DE MARIAGE AVEC OPTION B ET SITUATION M OU O":"N";
AS0103:discordance :"A":"S01":"03":"INCOMPATIBILITE ENTRE DATE DE MARIAGE SANS OPTION B ET SITUATION C, D OU V":"N";
AS0104:discordance :"A":"S01":"04":"INCOMPATIBILITE ENTRE DATE DE DIVORCE ET SITUATION M OU O":"N";
AS0105:discordance :"A":"S01":"05":"INCOMPATIBILITE ENTRE DATES DE DIVORCE ET DE DECES DU CJT ET SITUATION M":"N";
AS0106:discordance :"A":"S01":"06":"INCOMPATIBILITE ENTRE DATE DE DECES DU CJT ET SITUATION D":"N";
AS02:discordance :"A":"S02":"00":"ANNEE DE NAISSANCE INVRAISEMBLABLE POUR UNE PERSONNE A CHARGE":"N";
AS11:discordance :"A":"S11":"00":"DATE DE NAISSANCE ABSENTE":"N";
DD55:discordance :"D":"D55":"00":"LES LIGNES 8TR, 8TQ, 8TV, 8TW, 8TX SONT REMPLIES SANS REVENU CORRESPONDANT":"N";
DD57:discordance :"D":"D57":"00":"LE MONTANT 7LF (REPORT) EST > AU MONTANT CALCULE DE N-1":"N";
DD58:discordance :"D":"D58":"00":"LE MONTANT 7IZ (REPORT) EST > AU MONTANT CALCULE DE N-1":"N";
DD5901:discordance :"D":"D59":"01":"GAINS PORTES LIGNE 3VS SANS GAINS LIGNE 3VI, 3VJ":"N";
DD5902:discordance :"D":"D59":"02":"GAINS PORTES LIGNE 3SS SANS GAINS LIGNE 3SI, 3VK":"N";
DD6001:discordance :"D":"D60":"01":"GAINS PORTES LIGNE 3VO SANS GAINS LIGNE 3VD, 3VI, 3VF, 3VJ":"N";
DD6002:discordance :"D":"D60":"02":"GAINS PORTES LIGNE 3SO SANS GAINS LIGNE 3SD, 3SI, 3SF, 3VK":"N";
DD6101:discordance :"D":"D61":"01":"GAINS PORTES LIGNE 3VN SANS GAINS LIGNE 3VD, 3VI, 3VF, 3VJ":"N";
DD6102:discordance :"D":"D61":"02":"GAINS PORTES LIGNE 3SN SANS GAINS LIGNE 3SD, 3SI, 3SF, 3VK":"N";
DD62:discordance :"D":"D62":"00":"CASE 7WH COCHEE SANS DEPENSES AVEC BOUQUET DE TRAVAUX":"N";
DD63:discordance :"D":"D63":"00":"MONTANT 8ZR (RNI MONDIAL) INFERIEUR A REVENU IMPOSABLE":"N";
I015:informative :"I":"015":"00":"LE MONTANT 2AA,2AL,2AM,2AN,2AQ,2AR (REPORT) EST > AU MONTANT CALCULE DE N-1":"N";
IM1101:informative :"I":"M11":"01":" NB DE P.A.C. CODE F DIFFERENT NB DATES DE NAISSANCE":"N";
IM41:informative :"I":"M41":"00":"ANNULATION OU MINORATION DE R02 ET 2042_RECT S'EFFECTUE AVEC C02 et 2042_RECT":"N";
