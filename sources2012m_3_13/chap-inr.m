#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2013 
#au titre des revenus percus en 2012. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
 #
 #     CHAPITRE 2. CALCUL DU NET A PAYER
 #
 #
 #
regle corrective 10801:
application : iliad;
TXINR = max(0,(NBMOIS * TXMOISRETARD2) + max(0,(NBMOIS2 * TXMOISRETARD2)));

TXINRRED = max(0,(NBMOIS * TXMOISRETARD2*TXMOISRED*2) + max(0,(NBMOIS2 * TXMOISRETARD2 * TXMOISRED * 2)));

regle corrective 1081:
application : iliad ;
IND_PASSAGE = positif(FLAG_DEFAUT + FLAG_RETARD) + IND_PASSAGE_A;
IND_PASSR9901 = 1 + IND_PASSR9901_A;
IRNIN_PA = IRNIN_INR * null(1 - IND_PASSAGE) + IRNIN_PA_A;
TXINR_PA = TXINR * null(1 - IND_PASSAGE) + TXINR_PA_A;
INRIR_RETDEF = (1 - IND_RJLJ) * FLAG_DEFAUT * ( 
             arr(IRNIN_INR * TXINR / 100) * positif(IRNIN_INR) * null(1 - IND_PASSAGE) 
             + INRIR_RETDEF_A* (1-positif(ACODELAISINR))
		  + arr(max(0,IRNIN_PA - ACODELAISINR) * TXINR_PA/100) * positif(IND_PASSAGE -1)* positif(ACODELAISINR)
                                );
INR_IR_TARDIF = ((arr(IRNIN_INR * TXINR/100) * positif(IRNIN_INR) * null(1-IND_PASSAGE)+ INR_IR_TARDIF_A*(1-positif(ACODELAISINR)))
		  + arr(max(0,IRNIN_PA - ACODELAISINR) * TXINR_PA/100) * positif(IND_PASSAGE -1)* positif(ACODELAISINR)) * FLAG_RETARD * (1-IND_RJLJ);
CSG_PA = CSG * null(1 - IND_PASSAGE) + CSG_PA_A;
INRCSG_RETDEF = (1 - IND_RJLJ) * (
                arr((CSG-CSGIM) * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRCSG_RETDEF_A;
INR_CSG_TARDIF = (arr((CSG-CSGIM) * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_CSG_TARDIF_A) * (1-IND_RJLJ);
PRS_PA = PRS * null(1 - IND_PASSAGE) + PRS_PA_A;
INRPRS_RETDEF = (1 - IND_RJLJ) * (
             arr((PRS-PRSPROV) * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRPRS_RETDEF_A;
INR_PS_TARDIF = (arr((PRS-PRSPROV) * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_PS_TARDIF_A) * (1-IND_RJLJ);
CRDS_PA = RDSN * null(1 - IND_PASSAGE) + CRDS_PA_A;
INRCRDS_RETDEF = (1 - IND_RJLJ) * (
             arr((RDSN-CRDSIM) * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRCRDS_RETDEF_A;
INR_CRDS_TARDIF = (arr((RDSN-CRDSIM) * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_CRDS_TARDIF_A) * (1-IND_RJLJ);
TAXA_PA = TAXABASE * null(1 - IND_PASSAGE) + TAXA_PA_A;
INRTAXA_RETDEF = (1 - IND_RJLJ) * (
               arr(TAXABASE * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRTAXA_RETDEF_A;
INR_TAXAGA_TARDIF = (arr(TAXABASE * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_TAXA_TARDIF_A) * (1-IND_RJLJ);
CHR_PA = CHRBASE * null(1 - IND_PASSAGE) + CHR_PA_A;
INRCHR_RETDEF = (1 - IND_RJLJ) * (
               arr(CHRBASE * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRCHR_RETDEF_A;
INR_CHR_TARDIF = (arr(CHRBASE * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_CHR_TARDIF_A) * (1-IND_RJLJ);
PCAP_PA = PCAPBASE * null(1 - IND_PASSAGE) + PCAP_PA_A;
INRPCAP_RETDEF = (1 - IND_RJLJ) * (
               arr(PCAPBASE * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRPCAP_RETDEF_A;
INR_PCAP_TARDIF = (arr(PCAPBASE * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_PCAP_TARDIF_A) * (1-IND_RJLJ);
GAIN_PA = GAINBASE * null(1 - IND_PASSAGE) + GAIN_PA_A;
INRGAIN_RETDEF = (1 - IND_RJLJ) * (
               arr(GAINBASE * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRGAIN_RETDEF_A;
INR_GAIN_TARDIF = (arr(GAINBASE * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_GAIN_TARDIF_A) * (1-IND_RJLJ);
RSE1_PA = RSE1BASE * null(1 - IND_PASSAGE) + RSE1_PA_A;
INRRSE1_RETDEF = (1 - IND_RJLJ) * (
               arr(RSE1BASE * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRRSE1_RETDEF_A;
INR_RSE1_TARDIF = (arr(RSE1BASE * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_RSE1_TARDIF_A) * (1-IND_RJLJ);
RSE2_PA = RSE2BASE * null(1 - IND_PASSAGE) + RSE2_PA_A;
INRRSE2_RETDEF = (1 - IND_RJLJ) * (
               arr(RSE2BASE * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRRSE2_RETDEF_A;
INR_RSE2_TARDIF = (arr(RSE2BASE * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_RSE2_TARDIF_A) * (1-IND_RJLJ);
RSE3_PA = RSE3BASE * null(1 - IND_PASSAGE) + RSE3_PA_A;
INRRSE3_RETDEF = (1 - IND_RJLJ) * (
               arr(RSE3BASE * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRRSE3_RETDEF_A;
INR_RSE3_TARDIF = (arr(RSE3BASE * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_RSE3_TARDIF_A) * (1-IND_RJLJ);
RSE4_PA = RSE4BASE * null(1 - IND_PASSAGE) + RSE4_PA_A;
INRRSE4_RETDEF = (1 - IND_RJLJ) * (
               arr(RSE4BASE * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRRSE4_RETDEF_A;
INR_RSE4_TARDIF = (arr(RSE4BASE * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_RSE4_TARDIF_A) * (1-IND_RJLJ);
CSAL_PA = CSALBASE * null(1 - IND_PASSAGE) + CSAL_PA_A;
INRCSAL_RETDEF = (1 - IND_RJLJ) * (
               arr(CSALBASE * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRCSAL_RETDEF_A;
INR_CSAL_TARDIF = (arr(CSALBASE * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_CSAL_TARDIF_A) * (1-IND_RJLJ);
CDIS_PA = CDISBASE * null(1 - IND_PASSAGE) + CDIS_PA_A;
INRCDIS_RETDEF = (1 - IND_RJLJ) * (
               arr(CDISBASE * TXINR / 100) * FLAG_DEFAUT * null(IND_PASSAGE - 1)
                                )
             + INRCDIS_RETDEF_A;
INR_CDIS_TARDIF = (arr(CDISBASE * TXINR/100) * FLAG_RETARD * null(1-IND_PASSAGE)+INR_CDIS_TARDIF_A) * (1-IND_RJLJ);
regle corrective 10811:
application :  iliad ;
IRNIN_TLDEC_1=IRNIN_INR;
CSG_TLDEC_1=CSG;
PRS_TLDEC_1=PRS;
RDS_TLDEC_1=RDSN;
TAXA_TLDEC_1=TAXASSUR;
CHR_TLDEC_1=IHAUTREVT;
PCAP_TLDEC_1=IPCAPTAXT;
GAIN_TLDEC_1=CGAINSAL;
RSE1_TLDEC_1=RSE1;
RSE2_TLDEC_1=RSE2;
RSE3_TLDEC_1=RSE3;
RSE4_TLDEC_1=RSE4;
CSAL_TLDEC_1=CSAL;
CDIS_TLDEC_1=CDIS;
regle corrective 108112:
application : iliad ;
INRIR_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
			 null(2-FLAG_INR) * positif(IRNIN_INR - IRNIN_R99R)
                       * (
            (positif(IRNIN_INR - max(IRNIN_REF * (1-present(IRNIN_NTLDEC_198)),IRNIN_NTLDEC_198+0) )
            * arr((IRNIN_INR - max(IRNIN_REF * (1-present(IRNIN_NTLDEC_198)),IRNIN_NTLDEC_198+0)) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(IRNIN_INR - max(IRNIN_REF * (1-present(IRNIN_NTLDEC_198)),IRNIN_NTLDEC_198+0))
            * positif(IRNIN_INR-max(IRNIN_RECT * (1-present(IRNIN_NTLDEC_198)),IRNIN_NTLDEC_198+0))
            * arr((IRNIN_INR - max(IRNIN_REF * (1-present(IRNIN_NTLDEC_198)),IRNIN_NTLDEC_198+0)) * (TXINR / 100))
            * positif(FLAG_DEFAUT+FLAG_RETARD) * positif(IND_PASSAGE - 1))
                             )
                   +  null(3-FLAG_INR) * positif(IRNIN_INR - IRNIN_REF)
                       * (
            (positif(IRNIN_INR - max(IRNIN_REF * (1-present(IRNIN_NTLDEC_198)),IRNIN_NTLDEC_198+0) )
            * arr((min(IRNIN_INR,IRNIN_TLDEC_1) - max(IRNIN_REF * (1-present(IRNIN_NTLDEC_198)),IRNIN_NTLDEC_198+0)) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(IRNIN_INR - max(IRNIN_REF * (1-present(IRNIN_NTLDEC_198)),IRNIN_NTLDEC_198+0))
            * positif(IRNIN_INR-max(IRNIN_RECT * (1-present(IRNIN_NTLDEC_198)),IRNIN_NTLDEC_198+0))
            * arr((min(IRNIN_INR,IRNIN_TLDEC_1) - max(IRNIN_REF * (1-present(IRNIN_NTLDEC_198)),IRNIN_NTLDEC_198+0)) * (TXINR / 100))
            * positif(FLAG_DEFAUT+FLAG_RETARD) * positif(IND_PASSAGE - 1))
                             )
            + INRIR_RETDEF * null(IND_PASSAGE - 1)
                                                )
                                               ;
INRCSG_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
			 null(2 - FLAG_INR) * positif(CSG-CSG_R99R) 
			* (
            (positif(CSG * positif(CSG+PRS+RDSN-SEUIL_61) - max(CSG_NTLDEC_198,CSG_REF* (1-present(CSG_NTLDEC_198))))
            * arr((CSG - max(CSG_NTLDEC_198,CSG_REF* (1-present(CSG_NTLDEC_198)))-CSGIM) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CSG* positif(CSG+PRS+RDSN-SEUIL_61)  - max(CSG_NTLDEC_198,CSG_REF* (1-present(CSG_NTLDEC_198))))
            * arr((CSG - max(CSG_NTLDEC_198,CSG_REF* (1-present(CSG_NTLDEC_198)))-CSGIM) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                              )
                      + null(3 - FLAG_INR) * positif(CSG-CSG_REF) 
			* (
            (positif(CSG * positif(CSG+PRS+RDSN-SEUIL_61) - max(CSG_NTLDEC_198,CSG_REF* (1-present(CSG_NTLDEC_198))))
            * arr((min(CSG,CSG_TLDEC_1) - max(CSG_NTLDEC_198,CSG_REF* (1-present(CSG_NTLDEC_198)))-CSGIM) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CSG* positif(CSG+PRS+RDSN-SEUIL_61)  - max(CSG_NTLDEC_198,CSG_REF* (1-present(CSG_NTLDEC_198))+0))
            * arr((min(CSG,CSG_TLDEC_1) - max(CSG_NTLDEC_198,CSG_REF)* (1-present(CSG_NTLDEC_198))-CSGIM) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
			    )
            + INRCSG_RETDEF * null(IND_PASSAGE - 1)
                              )
             ;
INRPRS_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
			   null(2 - FLAG_INR) * positif(PRS-PRS_R99R) 
			   * (
            (positif(PRS* positif(CSG+PRS+RDSN-SEUIL_61)  - max(PRS_NTLDEC_198,PRS_REF* (1-present(PRS_NTLDEC_198))+0)) 
            * arr((PRS  - max(PRS_NTLDEC_198,PRS_REF* (1-present(PRS_NTLDEC_198)))-PRSPROV) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(PRS * positif(CSG+PRS+RDSN-SEUIL_61) - max(PRS_NTLDEC_198,PRS_REF* (1-present(PRS_NTLDEC_198))+0))
            * arr((PRS - max(PRS_NTLDEC_198,PRS_REF* (1-present(PRS_NTLDEC_198)))-PRSPROV) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                        )
                        + null(3 - FLAG_INR) * positif(PRS-PRS_REF) 
			   * (
            (positif(PRS* positif(CSG+PRS+RDSN-SEUIL_61)  - max(PRS_NTLDEC_198,PRS_REF* (1-present(PRS_NTLDEC_198))+0)) 
            * arr((min(PRS,PRS_TLDEC_1)  - max(PRS_NTLDEC_198,PRS_REF* (1-present(PRS_NTLDEC_198)))-PRSPROV) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(PRS * positif(CSG+PRS+RDSN-SEUIL_61) - max(PRS_NTLDEC_198,PRS_REF* (1-present(PRS_NTLDEC_198))+0))
            * arr((min(PRS,PRS_TLDEC_1) - max(PRS_NTLDEC_198,PRS_REF* (1-present(PRS_NTLDEC_198)))-PRSPROV) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                        )
            + INRPRS_RETDEF * null(IND_PASSAGE - 1)
                            )
             ;
INRCRDS_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		       null(2 - FLAG_INR) * positif(RDSN - RDS_R99R) 
		      * (
            (positif(RDSN * positif(CSG+PRS+RDSN-SEUIL_61) - max(CRDS_NTLDEC_198,RDS_REF* (1-present(CRDS_NTLDEC_198))+0))
            * arr((RDSN - max(CRDS_NTLDEC_198,RDS_REF* (1-present(CRDS_NTLDEC_198)))-CRDSIM) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(RDSN * positif(CSG+PRS+RDSN-SEUIL_61) - max(CRDS_NTLDEC_198,RDS_REF* (1-present(CRDS_NTLDEC_198))+0))
            * arr((RDSN -max(CRDS_NTLDEC_198,RDS_REF* (1-present(CRDS_NTLDEC_198)))-CRDSIM) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                         )
                    +  null(3 - FLAG_INR) * positif(RDSN - RDS_REF) 
		      * (
            (positif(RDSN * positif(CSG+PRS+RDSN-SEUIL_61) - max(CRDS_NTLDEC_198,RDS_REF* (1-present(CRDS_NTLDEC_198))+0))
            * arr((min(RDSN,RDS_TLDEC_1) - max(CRDS_NTLDEC_198,RDS_REF* (1-present(CRDS_NTLDEC_198)))-CRDSIM) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(RDSN * positif(CSG+PRS+RDSN-SEUIL_61) - max(CRDS_NTLDEC_198,RDS_REF* (1-present(CRDS_NTLDEC_198))+0))
            * arr((min(RDSN,RDS_TLDEC_1) -max(CRDS_NTLDEC_198,RDS_REF* (1-present(CRDS_NTLDEC_198)))-CRDSIM) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                         )
            + INRCRDS_RETDEF * null(IND_PASSAGE - 1)
                            )
             ;
INRTAXA_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		     null(2 - FLAG_INR) * positif(TAXABASE - TAXA_R99R) 
		     * (
             (positif(TAXABASE - max(TAXA_NTLDEC_198,TAXA_REF* (1-present(TAXA_NTLDEC_198))+0))
            * arr((TAXABASE - max(TAXA_NTLDEC_198,TAXA_REF* (1-present(TAXA_NTLDEC_198)))) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(TAXABASE - max(TAXA_NTLDEC_198,TAXA_REF* (1-present(TAXA_NTLDEC_198))+0))
            * arr((TAXABASE - max(TAXA_NTLDEC_198,TAXA_REF* (1-present(TAXA_NTLDEC_198)))) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
		     + null(3 - FLAG_INR) * positif(TAXABASE - TAXA_REF) 
		     * (
             (positif(TAXABASE - max(TAXA_NTLDEC_198,TAXA_REF* (1-present(TAXA_NTLDEC_198))+0))
            * arr((min(TAXABASE,TAXA_TLDEC_1) - max(TAXA_NTLDEC_198,TAXA_REF* (1-present(TAXA_NTLDEC_198)))) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(TAXABASE - max(TAXA_NTLDEC_198,TAXA_REF* (1-present(TAXA_NTLDEC_198))+0))
            * arr((min(TAXABASE,TAXA_TLDEC_1) - max(TAXA_NTLDEC_198,TAXA_REF* (1-present(TAXA_NTLDEC_198)))) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
            + INRTAXA_RETDEF * null(IND_PASSAGE - 1)
                            )
	     ; 
INRCSAL_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		       null(2 - FLAG_INR) * positif(CSALBASE - CSAL_R99R) 
		       * (
             (positif(CSALBASE - max(CSAL_NTLDEC_198,CSAL_REF* (1-present(CSAL_NTLDEC_198))+0))
            * arr((CSALBASE - max(CSAL_NTLDEC_198,CSAL_REF* (1-present(CSAL_NTLDEC_198)))-CSALPROV) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CSALBASE - max(CSAL_NTLDEC_198,CSAL_REF* (1-present(CSAL_NTLDEC_198))+0)) 
            * arr((CSALBASE - max(CSAL_NTLDEC_198,CSAL_REF* (1-present(CSAL_NTLDEC_198)))-CSALPROV) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
		       + null(3 - FLAG_INR) * positif(CSALBASE - CSAL_REF) 
		       * (
             (positif(CSALBASE - max(CSAL_NTLDEC_198,CSAL_REF+0)-CSALPROV* (1-present(CSAL_NTLDEC_198)))
            * arr((min(CSALBASE,CSAL_TLDEC_1) - max(CSAL_NTLDEC_198,CSAL_REF* (1-present(CSAL_NTLDEC_198)))-CSALPROV) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CSALBASE - max(CSAL_NTLDEC_198,CSAL_REF-CSALPROV* (1-present(CSAL_NTLDEC_198))+0))
            * arr((min(CSALBASE,CSAL_TLDEC_1) - max(CSAL_NTLDEC_198,CSAL_REF* (1-present(CSAL_NTLDEC_198)))-CSALPROV) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
            + INRCSAL_RETDEF * null(IND_PASSAGE - 1)
                            )
	     ; 
INRCDIS_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		       null(2 - FLAG_INR) * positif(CDISBASE - CDIS_R99R) 
		       * (
             (positif(CDISBASE - max(CDIS_NTLDEC_198,CDIS_REF* (1-present(CDIS_NTLDEC_198))+0))
            * arr((CDISBASE - max(CDIS_NTLDEC_198,CDIS_REF* (1-present(CDIS_NTLDEC_198)))-CDISPROV) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CDISBASE - max(CDIS_NTLDEC_198,CDIS_REF* (1-present(CDIS_NTLDEC_198))+0)) 
            * arr((CDISBASE - max(CDIS_NTLDEC_198,CDIS_REF* (1-present(CDIS_NTLDEC_198)))-CDISPROV) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
		       + null(3 - FLAG_INR) * positif(CDISBASE - CDIS_REF) 
		       * (
             (positif(CDISBASE - max(CDIS_NTLDEC_198,CDIS_REF+0))
            * arr((min(CDISBASE,CDIS_TLDEC_1) - max(CDIS_NTLDEC_198,CDIS_REF* (1-present(CDIS_NTLDEC_198)))-CDISPROV) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CDISBASE - max(CDIS_NTLDEC_198,CDIS_REF* (1-present(CDIS_NTLDEC_198))+0))
            * arr((min(CDISBASE,CDIS_TLDEC_1) - max(CDIS_NTLDEC_198,CDIS_REF* (1-present(CDIS_NTLDEC_198)))-CDISPROV) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
            + INRCDIS_RETDEF * null(IND_PASSAGE - 1)
                            )
	     ; 
INRCHR_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		       null(2 - FLAG_INR) * positif(CHRBASE - CHR_R99R) 
		       * (
             (positif(CHRBASE - max(CHR_NTLDEC_198,CHR_REF* (1-present(CHR_NTLDEC_198))+0))
            * arr((CHRBASE - max(CHR_NTLDEC_198,CHR_REF* (1-present(CHR_NTLDEC_198)))) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CHRBASE - max(CHR_NTLDEC_198,CHR_REF* (1-present(CHR_NTLDEC_198))+0)) 
            * arr((CHRBASE - max(CHR_NTLDEC_198,CHR_REF* (1-present(CHR_NTLDEC_198)))) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
		       + null(3 - FLAG_INR) * positif(CHRBASE - CHR_REF) 
		       * (
             (positif(CHRBASE - max(CHR_NTLDEC_198,CHR_REF+0))
            * arr((min(CHRBASE,CHR_TLDEC_1) - max(CHR_NTLDEC_198,CHR_REF* (1-present(CHR_NTLDEC_198)))) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CHRBASE - max(CHR_NTLDEC_198,CHR_REF* (1-present(CHR_NTLDEC_198))+0))
            * arr((min(CHRBASE,CHR_TLDEC_1) - max(CHR_NTLDEC_198,CHR_REF* (1-present(CHR_NTLDEC_198)))) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
            + INRCHR_RETDEF * null(IND_PASSAGE - 1)
                            )
	     ; 
INRPCAP_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		       null(2 - FLAG_INR) * positif(PCAPBASE - PCAP_R99R) 
		       * (
             (positif(PCAPBASE - max(PCAP_NTLDEC_198,PCAP_REF* (1-present(PCAP_NTLDEC_198))+0))
            * arr((PCAPBASE - max(PCAP_NTLDEC_198,PCAP_REF* (1-present(PCAP_NTLDEC_198)))) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(PCAPBASE - max(PCAP_NTLDEC_198,PCAP_REF* (1-present(PCAP_NTLDEC_198))+0)) 
            * arr((PCAPBASE - max(PCAP_NTLDEC_198,PCAP_REF* (1-present(PCAP_NTLDEC_198)))) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
		       + null(3 - FLAG_INR) * positif(PCAPBASE - PCAP_REF) 
		       * (
             (positif(PCAPBASE - max(PCAP_NTLDEC_198,PCAP_REF+0))
            * arr((min(PCAPBASE,PCAP_TLDEC_1) - max(PCAP_NTLDEC_198,PCAP_REF* (1-present(PCAP_NTLDEC_198)))) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(PCAPBASE - max(PCAP_NTLDEC_198,PCAP_REF* (1-present(PCAP_NTLDEC_198))+0))
            * arr((min(PCAPBASE,PCAP_TLDEC_1) - max(PCAP_NTLDEC_198,PCAP_REF* (1-present(PCAP_NTLDEC_198)))) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
            + INRPCAP_RETDEF * null(IND_PASSAGE - 1)
                            )
	     ; 
INRGAIN_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		       null(2 - FLAG_INR) * positif(GAINBASE - GAIN_R99R) 
		       * (
             (positif(GAINBASE - max(GAIN_NTLDEC_198,GAIN_REF* (1-present(GAIN_NTLDEC_198))+0))
            * arr((GAINBASE - max(GAIN_NTLDEC_198,GAIN_REF* (1-present(GAIN_NTLDEC_198)))-GAINPROV) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(GAINBASE - max(GAIN_NTLDEC_198,GAIN_REF* (1-present(GAIN_NTLDEC_198))+0)) 
            * arr((GAINBASE - max(GAIN_NTLDEC_198,GAIN_REF* (1-present(GAIN_NTLDEC_198)))-GAINPROV) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
		       + null(3 - FLAG_INR) * positif(GAINBASE - GAIN_REF) 
		       * (
             (positif(GAINBASE - max(GAIN_NTLDEC_198,GAIN_REF+0))
            * arr((min(GAINBASE,GAIN_TLDEC_1) - max(GAIN_NTLDEC_198,GAIN_REF* (1-present(GAIN_NTLDEC_198)))-GAINPROV) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(GAINBASE - max(GAIN_NTLDEC_198,GAIN_REF* (1-present(GAIN_NTLDEC_198))+0))
            * arr((min(GAINBASE,GAIN_TLDEC_1) - max(GAIN_NTLDEC_198,GAIN_REF* (1-present(GAIN_NTLDEC_198)))-GAINPROV) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
            + INRGAIN_RETDEF * null(IND_PASSAGE - 1)
                            )
	     ; 
INRRSE1_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		       null(2 - FLAG_INR) * positif(RSE1BASE - RSE1_R99R) 
		       * (
             (positif(RSE1BASE - max(RSE1_NTLDEC_198,RSE1_REF* (1-present(RSE1_NTLDEC_198))+0))
            * arr((RSE1BASE - max(RSE1_NTLDEC_198,RSE1_REF* (1-present(RSE1_NTLDEC_198)))) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(RSE1BASE - max(RSE1_NTLDEC_198,RSE1_REF* (1-present(RSE1_NTLDEC_198))+0)) 
            * arr((RSE1BASE - max(RSE1_NTLDEC_198,RSE1_REF* (1-present(RSE1_NTLDEC_198)))) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
		       + null(3 - FLAG_INR) * positif(RSE1BASE - RSE1_REF) 
		       * (
             (positif(RSE1BASE - max(RSE1_NTLDEC_198,RSE1_REF+0))
            * arr((min(RSE1BASE,RSE1_TLDEC_1) - max(RSE1_NTLDEC_198,RSE1_REF* (1-present(RSE1_NTLDEC_198)))) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(RSE1BASE - max(RSE1_NTLDEC_198,RSE1_REF* (1-present(RSE1_NTLDEC_198))+0))
            * arr((min(RSE1BASE,RSE1_TLDEC_1) - max(RSE1_NTLDEC_198,RSE1_REF* (1-present(RSE1_NTLDEC_198)))) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
            + INRRSE1_RETDEF * null(IND_PASSAGE - 1)
                            )
	     ; 
INRRSE2_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		       null(2 - FLAG_INR) * positif(RSE2BASE - RSE2_R99R) 
		       * (
             (positif(RSE2BASE - max(RSE2_NTLDEC_198,RSE2_REF* (1-present(RSE2_NTLDEC_198))+0))
            * arr((RSE2BASE - max(RSE2_NTLDEC_198,RSE2_REF* (1-present(RSE2_NTLDEC_198)))) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(RSE2BASE - max(RSE2_NTLDEC_198,RSE2_REF* (1-present(RSE2_NTLDEC_198))+0)) 
            * arr((RSE2BASE - max(RSE2_NTLDEC_198,RSE2_REF* (1-present(RSE2_NTLDEC_198)))) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
		       + null(3 - FLAG_INR) * positif(RSE2BASE - RSE2_REF) 
		       * (
             (positif(RSE2BASE - max(RSE2_NTLDEC_198,RSE2_REF+0))
            * arr((min(RSE2BASE,RSE2_TLDEC_1) - max(RSE2_NTLDEC_198,RSE2_REF* (1-present(RSE2_NTLDEC_198)))) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(RSE2BASE - max(RSE2_NTLDEC_198,RSE2_REF* (1-present(RSE2_NTLDEC_198))+0))
            * arr((min(RSE2BASE,RSE2_TLDEC_1) - max(RSE2_NTLDEC_198,RSE2_REF* (1-present(RSE2_NTLDEC_198)))) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
            + INRRSE2_RETDEF * null(IND_PASSAGE - 1)
                            )
	     ; 
INRRSE3_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		       null(2 - FLAG_INR) * positif(RSE3BASE - RSE3_R99R) 
		       * (
             (positif(RSE3BASE - max(RSE3_NTLDEC_198,RSE3_REF* (1-present(RSE3_NTLDEC_198))+0))
            * arr((RSE3BASE - max(RSE3_NTLDEC_198,RSE3_REF* (1-present(RSE3_NTLDEC_198)))) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(RSE3BASE - max(RSE3_NTLDEC_198,RSE3_REF* (1-present(RSE3_NTLDEC_198))+0)) 
            * arr((RSE3BASE - max(RSE3_NTLDEC_198,RSE3_REF* (1-present(RSE3_NTLDEC_198)))) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
		       + null(3 - FLAG_INR) * positif(RSE3BASE - RSE3_REF) 
		       * (
             (positif(RSE3BASE - max(RSE3_NTLDEC_198,RSE3_REF+0))
            * arr((min(RSE3BASE,RSE3_TLDEC_1) - max(RSE3_NTLDEC_198,RSE3_REF* (1-present(RSE3_NTLDEC_198)))) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(RSE3BASE - max(RSE3_NTLDEC_198,RSE3_REF* (1-present(RSE3_NTLDEC_198))+0))
            * arr((min(RSE3BASE,RSE3_TLDEC_1) - max(RSE3_NTLDEC_198,RSE3_REF* (1-present(RSE3_NTLDEC_198)))) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
            + INRRSE3_RETDEF * null(IND_PASSAGE - 1)
                            )
	     ; 
INRRSE4_NTL = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
		       null(2 - FLAG_INR) * positif(RSE4BASE - RSE4_R99R) 
		       * (
             (positif(RSE4BASE - max(RSE4_NTLDEC_198,RSE4_REF* (1-present(RSE4_NTLDEC_198))+0))
            * arr((RSE4BASE - max(RSE4_NTLDEC_198,RSE4_REF* (1-present(RSE4_NTLDEC_198)))) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(RSE4BASE - max(RSE4_NTLDEC_198,RSE4_REF* (1-present(RSE4_NTLDEC_198))+0)) 
            * arr((RSE4BASE - max(RSE4_NTLDEC_198,RSE4_REF* (1-present(RSE4_NTLDEC_198)))) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
		       + null(3 - FLAG_INR) * positif(RSE4BASE - RSE4_REF) 
		       * (
             (positif(RSE4BASE - max(RSE4_NTLDEC_198,RSE4_REF+0))
            * arr((min(RSE4BASE,RSE4_TLDEC_1) - max(RSE4_NTLDEC_198,RSE4_REF* (1-present(RSE4_NTLDEC_198)))) * (TXINR / 100))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(RSE4BASE - max(RSE4_NTLDEC_198,RSE4_REF* (1-present(RSE4_NTLDEC_198))+0))
            * arr((min(RSE4BASE,RSE4_TLDEC_1) - max(RSE4_NTLDEC_198,RSE4_REF* (1-present(RSE4_NTLDEC_198)))) * (TXINR / 100))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                             )
            + INRRSE4_RETDEF * null(IND_PASSAGE - 1)
                            )
	     ; 
regle corrective 108111:
application : iliad ;
INRIR_NTL_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
			 null(2-FLAG_INR) * positif(IRNIN_INR - IRNIN_R99R)
                       * (
            (positif(IRNIN_INR - max(IRNIN_REF+0,IRNIN_NTLDEC)) 
            * arr((IRNIN_INR - max(IRNIN_REF,IRNIN_NTLDEC)) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(IRNIN_INR - max(IRNIN_NTLDEC,IRNIN_REF+0))
            * positif(IRNIN_INR-max(IRNIN_NTLDEC,IRNIN_RECT))
            * arr((IRNIN_INR - max(IRNIN_NTLDEC,IRNIN_REF)) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT+FLAG_RETARD) * positif(IND_PASSAGE - 1))
                             )
                   +  null(3-FLAG_INR) * positif(IRNIN_INR - IRNIN_REF)
                       * (
            (positif(IRNIN_INR - max(IRNIN_REF+0,IRNIN_NTLDEC)) 
            * arr((min(IRNIN_INR,IRNIN_TLDEC_1) - max(IRNIN_REF,IRNIN_NTLDEC)) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(IRNIN_INR - max(IRNIN_NTLDEC,IRNIN_REF+0))
            * positif(IRNIN_INR-max(IRNIN_NTLDEC,IRNIN_RECT))
            * arr((min(IRNIN_INR,IRNIN_TLDEC_1) - max(IRNIN_NTLDEC,IRNIN_REF)) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT+FLAG_RETARD) * positif(IND_PASSAGE - 1))
                             )
                                                )
                                               ;
INRCSG_NTL_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
			 null(2 - FLAG_INR) * positif(CSG-CSG_R99R) 
			* (
            (positif(CSG * positif(CSG+PRS+RDSN-SEUIL_61) - max(CSG_NTLDEC,CSG_REF+0))
            * arr((CSG - max(CSG_NTLDEC,CSG_REF)-CSGIM) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CSG* positif(CSG+PRS+RDSN-SEUIL_61)  - max(CSG_NTLDEC,CSG_REF+0))
            * arr((CSG - max(CSG_NTLDEC,CSG_REF)-CSGIM) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                              )
                      + null(3 - FLAG_INR) * positif(CSG-CSG_REF) 
			* (
            (positif(CSG * positif(CSG+PRS+RDSN-SEUIL_61) - max(CSG_NTLDEC,CSG_REF+0))
            * arr((min(CSG,CSG_TLDEC_1) - max(CSG_NTLDEC,CSG_REF)-CSGIM) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(CSG* positif(CSG+PRS+RDSN-SEUIL_61)  - max(CSG_NTLDEC,CSG_REF+0))
            * arr((min(CSG,CSG_TLDEC_1) - max(CSG_NTLDEC,CSG_REF)-CSGIM) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                              )
                            )
             ;
INRPRS_NTL_1 = (1 - IND_RJLJ) * (1-FLAG_NINR) * (
			   null(2 - FLAG_INR) * positif(PRS-PRS_R99R) 
			   * (
            (positif(PRS* positif(CSG+PRS+RDSN-SEUIL_61)  - max(PRS_NTLDEC,PRS_REF+0)) 
            * arr((PRS  - max(PRS_NTLDEC,PRS_REF)-PRSPROV) * (TXINRRED / 200))
            * null(FLAG_DEFAUT + FLAG_RETARD))
            +
            (positif(PRS * positif(CSG+PRS+RDSN-SEUIL_61) - max(PRS_NTLDEC,PRS_REF+0))
            * arr((PRS - max(PRS_NTLDEC,PRS_REF)-PRSPROV) * (TXINRRED / 200))
            * positif(FLAG_DEFAUT + FLAG_RETARD) * positif(IND_PASSAGE - 1))
                                                        )
                        + null(3 - FLAG_INR) * positif(PRS-PRS_REF) 
			   * (
            (positif(PRS* positif(CSG+PRS+RDSN-SEUIL_61)  - max(PRS_NTLDEC,PRS_REF+0)) 
            * arr((min(PRS,PRS_TLDEC_1)  - max(PRS_NTLDEC,PRS_REF)-PRSPROV) * (TXINRRED / 20