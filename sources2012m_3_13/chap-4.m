#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2013 
#au titre des revenus percus en 2012. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************

 #
                                                                
  ####   #    #    ##    #####      #     #####  #####   ######      #    
 #    #  #    #   #  #   #    #     #       #    #    #  #           #    #
 #       ######  #    #  #    #     #       #    #    #  #####       #    #
 #       #    #  ######  #####      #       #    #####   #           ######
 #    #  #    #  #    #  #          #       #    #   #   #                #
  ####   #    #  #    #  #          #       #    #    #  ######           #
 #
 #
 #
 #
 #
 #
 #                      CALCUL DE L'IMPOT BRUT
 #
 #
 #
 #
 #
 #
 #
regle 401:
application : bareme, iliad , batch  ;
IRB = IAMD2; 
IRB2 = IAMD2 + TAXASSUR + IPCAPTAXTOT + TAXLOY + CHRAPRES;
regle 40101:
application : iliad , batch  ;
KIR =   IAMD3 ;
regle 4011:
application : bareme , iliad , batch  ;
IAMD1 = IBM13 ;
IAMD2 = IBM23 ;
IAMD2TH = positif_ou_nul(IBM23 - SEUIL_61)*IBM23;
regle 40110:
application : bareme , iliad , batch  ;
IAMD3 = IBM33 - min(ACP3, IMPIM3);
regle 402112:
application : iliad , batch  ;
ANG3 = IAD32 - IAD31;
regle 40220:
application : iliad , batch  ;
ACP3 = max (0 ,
 somme (a=1..4: min(arr(CHENFa * TX_DPAEAV/100) , SEUIL_AVMAXETU)) - ANG3)
        * (1 - positif(V_CR2 + IPVLOC)) * positif(ANG3) * positif(IMPIM3);
regle 403:
application : bareme ,iliad , batch  ;

IBM13 = IAD11 + ITP + REI + AUTOVERSSUP + TAXASSUR + IPCAPTAXTOT  + TAXLOY + CHRAPRES + AVFISCOPTER ;

IBM23 = IAD11 + ITP + REI + AUTOVERSSUP + AVFISCOPTER ;

regle 404:
application : bareme , iliad , batch  ;
IBM33 = IAD31 + ITP + REI;
regle 4041:
application : iliad , batch  ;
TX_RED0 =  inf ( (100 - ((TX_RABGUY * V_EAG) + (TX_RABDOM * V_EAD))
	     ) * TX16  / 100 * positif(V_EAD + V_EAG)
	   );
TX_RED1 =  inf ( (100 - ((TX_RABGUY * V_EAG) + (TX_RABDOM * V_EAD))
	     ) * TX19 / 100 * positif(V_EAD + V_EAG)
	   );
DOMAVTO = arr(BN1 + SPEPV + BI12F + BA1) * ((TX16 - TX_RED0)/100) * positif(V_EAD)
          + arr(BN1 + SPEPV + BI12F + BA1) * ((TX16 - TX_RED0)/100) * positif(V_EAG);
DOMABDB = max(PLAF_RABDOM - ABADO , 0) * positif(V_EAD)
          + max(PLAF_RABGUY - ABAGU , 0) * positif(V_EAG);
DOMDOM = max(DOMAVTO - DOMABDB , 0) * positif(V_EAD + V_EAG);

ITP = arr((BPTP2 * TX225/100) 
       + (BPTPVT * TX19/100) 
       + (BPTP4 * TX30/100) 
       + ((BPTPD + BPTPG) * TX_RED0/100) 
       + (BPTP3 * TX16/100) 
       + (BPTP40 * TX41/100)
       + DOMDOM * positif(V_EAD + V_EAG)
       + (BPTP18 * TX18/100)
       + (BTP3G * TX24/100)
       + (BTP3N * TX24/100)
       + (BPTP5 * TX24/100)
       + (BPTPSJ * TX19/100)
       + (BPTPSA * TX19/100)
       + (BPTPSB * TX24/100)
       + (BPTPSC * TX19/100)
       + (BPTPWG * TX19/100)
	  )
       * (1-positif(IPVLOC)) * (1 - positif(present(TAX1649)+present(RE168))); 
regle 40412:
application : iliad , batch  ;
PTP = BTP3A + BTP3N + BTP3G+ BPTP2 +BPTPVT+BPTP18 +BPTP4+BPTPSK+BPTP40
	 + BPTP5+BPTPWG +BPTPSA +BPTPSB +BPTPSC;
regle 40413:
application : iliad , batch  ;
BTP3A = (BN1 + SPEPV + BI12F + BA1) * (1 - positif( IPVLOC ));
BPTPD = BTP3A * positif(V_EAD)*(1-positif(present(TAX1649)+present(RE168)));
BPTPG = BTP3A * positif(V_EAG)*(1-positif(present(TAX1649)+present(RE168)));
BPTP3 = BTP3A * (1 - positif(V_EAD + V_EAG))*(1-positif(present(TAX1649)+present(RE168)));
BTP3N = (BPVKRI) * (1 - positif( IPVLOC ));
BTP3G = (BPVRCM) * (1 - positif( IPVLOC ));
BTP2 = PEA * (1 - positif( IPVLOC ));
BPTP2 = BTP2*(1-positif(present(TAX1649)+present(RE168)));
BTPVT = GAINPEA * (1 - positif( IPVLOC ));
BPTPVT = BTPVT*(1-positif(present(TAX1649)+present(RE168)));

BTP18 = (BPV18V + BPV18C) * (1 - positif( IPVLOC ));
BPTP18 = BTP18 * (1-positif(present(TAX1649)+present(RE168))) ;

BPTP4 = (BPCOPTV + BPCOPTC + BPVSK) * (1 - positif(IPVLOC)) * (1 - positif(present(TAX1649) + present(RE168))) ;
BTPSK = BPVSK * (1 - positif( IPVLOC ));
BPTPSK = BTPSK * (1-positif(present(TAX1649)+present(RE168))) ;

BTP40 = (BPV40V + BPV40C) * (1 - positif( IPVLOC )) ;
BPTP40 = BTP40 * (1-positif(present(TAX1649)+present(RE168))) ;

BTP5 = PVIMPOS * (1 - positif( IPVLOC ));
BPTP5 = BTP5 * (1-positif(present(TAX1649)+present(RE168))) ;
BPTPWG = PVSURSIWG * (1 - positif( IPVLOC ))* (1-positif(present(TAX1649)+present(RE168)));
BTPSJ = BPVSJ * (1 - positif( IPVLOC ));
BPTPSJ = BTPSJ * (1-positif(present(TAX1649)+present(RE168))) ;
BTPSA = PVDIRTAX * (1 - positif( IPVLOC ));
BPTPSA = BTPSA * (1-positif(present(TAX1649)+present(RE168))) ;
BTPSB = PVTAXSB * (1 - positif( IPVLOC ));
BPTPSB = BTPSB * (1-positif(present(TAX1649)+present(RE168))) ;
BTPSC = PVTAXSC * (1 - positif( IPVLOC ));
BPTPSC = BTPSC * (1-positif(present(TAX1649)+present(RE168))) ;
BPTP19 = (BPVSJ + PVDIRTAX + PVTAXSC + GAINPEA + PVSURSIWG) * (1 - positif( IPVLOC )) * (1 - positif(present(TAX1649) + present(RE168))) ;

BPTP24 = (BTP3G + BTPSB + BTP3N + BTP5*(1-positif(null(5-V_REGCO)+null(6-V_REGCO)))) * (1 - positif(present(TAX1649) + present(RE168))) ;

BPTPDIV = ((PVTAXSB + PVTAXSC) * (1-positif(IPVLOC)) + BTP5) * (1-positif(present(TAX1649)+present(RE168))) ;

regle 4042:
application : iliad , batch  ;


REI = IPREP+IPPRICORSE;

regle 40421:
application : iliad , batch  ;


PPERSATOT = RSAFOYER + RSAPAC1 + RSAPAC2 ;

PPERSA = min(PPETOT,PPERSATOT) * (1 - V_CNR) ;

PPEFINAL = PPETOT - PPERSA ;

regle 405:
application : bareme , iliad , batch  ;


IAD11 = ( max(0,IDOM11-DEC11-RED) *(1-positif(V_CR2+IPVLOC))
        + positif(V_CR2+IPVLOC) *max(0 , IDOM11 - RED) )
                                * (1-positif(RE168+TAX1649))
        + positif(RE168+TAX1649) * IDOM16;

regle 406:
application : bareme , iliad , batch  ;
IAD31 = ((IDOM31-DEC31)*(1-positif(V_CR2+IPVLOC)))
        +(positif(V_CR2+IPVLOC)*IDOM31);
IAD32 = ((IDOM32-DEC32)*(1-positif(V_CR2+IPVLOC)))
        +(positif(V_CR2+IPVLOC)*IDOM32);

regle 4052:
application : bareme , iliad , batch  ;

IMPIM3 =  IAD31 ;

regle 4061:
application : bareme , iliad , batch  ;
pour z = 1,2:
DEC1z = min (max( arr(SEUIL_DECOTE/2 - (IDOM1z/2)),0),IDOM1z) * (1 - V_CNR);

pour z = 1,2:
DEC3z = min (max( arr(SEUIL_DECOTE/2 - (IDOM3z/2)),0),IDOM3z) * (1 - V_CNR);

DEC6 = min (max( arr(SEUIL_DECOTE/2 - (IDOM16/2)),0),IDOM16) * (1 - V_CNR);

regle 4066:
application : iliad   , batch ;
ART1731BIS = positif ( null (CODE_2042 - 3)
	                 +null (CODE_2042 - 4)
	                 +null (CODE_2042 - 5)
	                 +null (CODE_2042 - 6)
	                 +null ((CODE_2042 + CMAJ)- 8)
	                 +null ((CODE_2042 + CMAJ) - 11)
	                 +null (CODE_2042 - 31)
	                 +null (CODE_2042 - 55));
      
regle 407:
application : iliad   , batch ;
      
RED =  RCOTFOR + RSURV + RCOMP + RHEBE + RREPA + RDIFAGRI + RDONS
       + RCELTOT
       + RRESTIMO  
       + RFIPC + RFIPDOM + RAIDE + RNOUV 
       + RTOURNEUF + RTOURTRA + RTOURREP 
       + RTOUREPA + RTOURES + RTOUHOTR  
       + RLOGDOM + RLOGSOC + RDOMSOC1 + RLOCENT + RCOLENT
       + RRETU + RINNO + RRPRESCOMP + RFOR 
       + RSOUFIP + RRIRENOV + RSOCREPR + RRESIMEUB + RRESINEUV + RRESIVIEU + RLOCIDEFG
       + RREDMEUB + RREDREP + RILMIX + RINVRED + RILMIH + RILMIZ + RMEUBLE 
       + RPROREP + RREPNPRO + RREPMEU + RILMIC + RILMIB + RILMIA 
       + RIDOMPROE3 + RIDOMPROE4 
       + RPATNAT2 + RPATNAT1 + RPATNAT
       + RFORET + RCREAT + RCINE  ;

REDTL = ASURV + ACOMP ;

CIMPTL = ATEC + ADEVDUR + TOTBGE ;

regle 4070:
application : bareme ;
RED = V_9UY;
regle 4025:
application : iliad , batch  ;

PLAFDOMPRO1 = max(0 , RRI1-RLOGDOM-RTOURNEUF-RTOURTRA-RTOURES-RTOURREP-RTOUHOTR-RTOUREPA-RCOMP-RCREAT-RRETU
		          -RDONS-RNOUV-RLOGSOC-RDOMSOC1-RCELTOT-RLOCNPRO-RPATNAT2-RPATNAT1-RPATNAT) ;


RIDOMPROE4 = min(REPINVDOMPRO2 , PLAFDOMPRO1) * (1 - V_CNR) * (1 - ART1731BIS) ;
              
REPDOMENTR4 = positif(REPINVDOMPRO2 - PLAFDOMPRO1) * (REPINVDOMPRO2 - PLAFDOMPRO1)* (1 - V_CNR) ;


PLAFDOMPRO3 = positif(PLAFDOMPRO1 - REPINVDOMPRO2) * (PLAFDOMPRO1 - REPINVDOMPRO2) * (1 - V_CNR) ; 

RIDOMPROE3 = min(REPINVDOMPRO3 , PLAFDOMPRO3) * (1 - V_CNR) * (1 - ART1731BIS) ;
                  
REPOMENTR3 = positif(REPINVDOMPRO3 - PLAFDOMPRO3) * (REPINVDOMPRO3 - PLAFDOMPRO3) * (1 - V_CNR) ;


RIDOMPROTOT = RIDOMPROE3 + RIDOMPROE4 ;


RINVEST = RIDOMPROE3  + RIDOMPROE4 ;

RIDOMPRO = REPINVDOMPRO2 + REPINVDOMPRO3 ;

DIDOMPRO = RIDOMPRO * (1 - V_CNR) * (1 - ART1731BIS) ;

regle 40749:
application : iliad , batch  ;

DFORET = FORET ;

AFORET = max(min(DFORET,LIM_FORET),0) * (1-V_CNR) * (1 - ART1731BIS) ;

RAFORET = arr(AFORET*TX_FORET/100) * (1-V_CNR) ;

RFORET =  max( min( RAFORET , IDOM11-DEC11-RCOTFOR-RREPA-RFIPDOM-RAIDE-RDIFAGRI) , 0 ) ;

regle 4075:
application : iliad , batch ;

DFIPC = FIPCORSE ;

AFIPC = max( min(DFIPC , LIM_FIPCORSE * (1 + BOOL_0AM)) , 0) * (1 - V_CNR) * (1 - ART1731BIS) ;

RFIPCORSE = arr(AFIPC * TX_FIPCORSE/100) * (1 - V_CNR) ;

RFIPC = max( min( RFIPCORSE , IDOM11-DEC11-RCOTFOR-RREPA-RFIPDOM-RAIDE-RDIFAGRI-RFORET) , 0) ;

regle 40751:
application : iliad , batch ;

DFIPDOM = FIPDOMCOM ;

AFIPDOM = max( min(DFIPDOM , LIMFIPDOM * (1 + BOOL_0AM)) , 0) * (1 - V_CNR) * (1 - ART1731BIS) ;

RFIPDOMCOM = arr(AFIPDOM * TXFIPDOM/100) * (1 - V_CNR) ;

RFIPDOM = max( min( RFIPDOMCOM , IDOM11-DEC11-RCOTFOR-RREPA) , 0) ;

regle 4076:
application : iliad , batch  ;
BSURV = min( RDRESU , PLAF_RSURV + PLAF_COMPSURV * (EAC + V_0DN) + PLAF_COMPSURVQAR * (V_0CH + V_0DP) );
RRS = arr( BSURV * TX_REDSURV / 100 ) * (1 - V_CNR);
DSURV = RDRESU;
ASURV = BSURV * (1-V_CNR) * ( 1 - ART1731BIS);
RSURV = max( min( RRS , IDOM11-DEC11-RCOTFOR-RREPA-RFIPDOM-RAIDE-RDIFAGRI-RFORET-RFIPC
			-RCINE-RRESTIMO-RSOCREPR-RRPRESCOMP-RHEBE ) , 0 ) * ( 1 - ART1731BIS);

regle 4100:
application : iliad , batch ;

RRCN = arr(  min( CINE1 , min( max(SOFIRNG,RNG) * TX_CINE3/100 , PLAF_CINE )) * TX_CINE1/100
        + min( CINE2 , max( min( max(SOFIRNG,RNG) * TX_CINE3/100 , PLAF_CINE ) - CINE1 , 0)) * TX_CINE2/100 
       ) * (1 - V_CNR) ;

DCINE = CINE1 + CINE2 ;

ACINE = max(0,min( CINE1 + CINE2 , min( arr(SOFIRNG * TX_CINE3/100) , PLAF_CINE ))) * (1 - V_CNR) * (1 - ART1731BIS) ;

RCINE = max( min( RRCN , IDOM11-DEC11-RCOTFOR-RREPA-RFIPDOM-RAIDE-RDIFAGRI-RFORET-RFIPC) , 0 ) * (1 - ART1731BIS) ;
regle 4176:
application : iliad , batch  ;
BSOUFIP = min( FFIP , LIM_SOUFIP * (1 + BOOL_0AM)) * (1 - ART1731BIS);
RFIP = arr( BSOUFIP * TX_REDFIP / 100 ) * (1 - V_CNR);
DSOUFIP = FFIP;
ASOUFIP = BSOUFIP * (1-V_CNR) ;
RSOUFIP = max( min( RFIP , IDOM11-DEC11-RCOTFOR-RREPA-RFIPDOM-RAIDE-RDIFAGRI-RFORET-RFIPC
			   -RCINE-RRESTIMO-RSOCREPR-RRPRESCOMP-RHEBE-RSURV-RINNO) , 0 );

regle 4200:
application : iliad , batch  ;

BRENOV = min(RIRENOV,PLAF_RENOV) * (1 - ART1731BIS) ;
RENOV = arr( BRENOV * TX_RENOV / 100 ) * (1 - V_CNR) ;

DRIRENOV = RIRENOV ;
ARIRENOV = BRENOV * (1 - V_CNR) ;
RRIRENOV = max( min( RENOV , IDOM11-DEC11-RCOTFOR-RREPA-RFIPDOM-RAIDE-RDIFAGRI-RFORET-RFIPC-RCINE
			     -RRESTIMO-RSOCREPR-RRPRESCOMP-RHEBE-RSURV-RINNO-RSOUFIP) , 0 );

regle 40771:
application : iliad , batch  ;
RFC = min(RDCOM,PLAF_FRCOMPTA * max(1,NBACT)) * present(RDCOM)*(1-V_CNR) * (1-ART1731BIS);
NCOMP = max(1,NBACT)* present(RDCOM) * (1-V_CNR);
DCOMP = RDCOM;
ACOMP = RFC;
regle 10040771:
application :  iliad , batch  ;
RCOMP = max(min( RFC , RRI1-RLOGDOM-RTOURNEUF-RTOURTRA-RTOURES-RTOURREP-RTOUHOTR-RTOUREPA-RCREAT) , 0) ;

regle 4078:
application : iliad , batch  ;


BCEL_2012 = arr( min(( CELLIERJA + CELLIERJD + CELLIERJE + CELLIERJF + CELLIERJH + CELLIERJJ 
		     + CELLIERJK + CELLIERJM + CELLIERJN + 0 ), LIMCELLIER ) /9 );

BCEL_JOQR = arr( min(( CELLIERJO + CELLIERJQ + CELLIERJR + 0 ), LIMCELLIER ) /5 );

BCEL_2011 = arr( min(( CELLIERNA + CELLIERNC + CELLIERND + CELLIERNE + CELLIERNF + CELLIERNH
		     + CELLIERNI + CELLIERNJ + CELLIERNK + CELLIERNM + CELLIERNN + CELLIERNO  + 0 ), LIMCELLIER ) /9 );

BCELCOM2011 = arr( min(( CELLIERNP + CELLIERNR + CELLIERNS + CELLIERNT + 0 ), LIMCELLIER ) /5 );

BCEL_NBGL = arr( min(( CELLIERNB + CELLIERNG + CELLIERNL + 0), LIMCELLIER ) /9 );

BCEL_NQ = arr( min(( CELLIERNQ + 0), LIMCELLIER ) /5 );

BCEL_JBGL = arr( min(( CELLIERJB + CELLIERJG + CELLIERJL + 0), LIMCELLIER ) /9 );

BCEL_JP = arr( min(( CELLIERJP + 0), LIMCELLIER ) /5 );


BCEL_HNO = arr ( min ((CELLIERHN + CELLIERHO + 0 ), LIMCELLIER ) /9 );
BCEL_HJK = arr ( min ((CELLIERHJ + CELLIERHK + 0 ), LIMCELLIER ) /9 );

BCEL_HL = arr ( min ((CELLIERHL + 0 ), LIMCELLIER ) /9 );
BCEL_HM = arr ( min ((CELLIERHM + 0 ), LIMCELLIER ) /9 );


DCELRREDLA = CELRREDLA;
ACELRREDLA = CELRREDLA * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS);

DCELRREDLB = CELRREDLB;
ACELRREDLB = CELRREDLB * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 

DCELRREDLE = CELRREDLE;
ACELRREDLE = CELRREDLE * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS);

DCELRREDLC = CELRREDLC;
ACELRREDLC = CELRREDLC * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS);

DCELRREDLD = CELRREDLD;
ACELRREDLD = CELRREDLD * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS);

DCELRREDLF = CELRREDLF;
ACELRREDLF = CELRREDLF * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS);

DCELREPHS = CELREPHS; 
ACELREPHS = DCELREPHS * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 


DCELREPHR = CELREPHR ;    
ACELREPHR = DCELREPHR * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS);


DCELREPHU = CELREPHU; 
ACELREPHU = DCELREPHU * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 


DCELREPHT = CELREPHT; 
ACELREPHT = DCELREPHT * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 


DCELREPHZ = CELREPHZ; 
ACELREPHZ = DCELREPHZ * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 


DCELREPHX = CELREPHX; 
ACELREPHX = DCELREPHX * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 


DCELREPHW = CELREPHW; 
ACELREPHW = DCELREPHW * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 


DCELREPHV = CELREPHV; 
ACELREPHV = DCELREPHV * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 


DCELREPHF = CELREPHF; 
ACELREPHF = DCELREPHF * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 


DCELREPHE = CELREPHE ;    
ACELREPHE = DCELREPHE * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS);


DCELREPHD = CELREPHD; 
ACELREPHD = DCELREPHD * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 


DCELREPHH = CELREPHH; 
ACELREPHH = DCELREPHH * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 


DCELREPHG = CELREPHG; 
ACELREPHG = DCELREPHG * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 


DCELREPHB = CELREPHB; 
ACELREPHB = DCELREPHB * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 


DCELREPHA = CELREPHA; 
ACELREPHA = DCELREPHA * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 


DCELHM = CELLIERHM ; 
ACELHM = (positif_ou_nul( CELLIERHM) * BCEL_HM) * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 


DCELHL = CELLIERHL ;    
ACELHL = (positif_ou_nul( CELLIERHL) * BCEL_HL) * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS); 


DCELHNO = CELLIERHN + CELLIERHO ;
ACELHNO = (positif_ou_nul( CELLIERHN + CELLIERHO ) * BCEL_HNO) * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS);


DCELHJK = CELLIERHJ + CELLIERHK ;
ACELHJK = (positif_ou_nul( CELLIERHJ + CELLIERHK ) * BCEL_HJK) * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS);


DCELNQ = CELLIERNQ;
ACELNQ = (positif_ou_nul( CELLIERNQ) * BCEL_NQ) * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS);

DCELNBGL =   CELLIERNB + CELLIERNG + CELLIERNL;
ACELNBGL = (positif_ou_nul( CELLIERNB + CELLIERNG + CELLIERNL ) * BCEL_NBGL) * ((V_REGCO+0) dans (1,3,5,6))
									     * (1 - ART1731BIS);

DCELCOM =   CELLIERNP + CELLIERNR + CELLIERNS + CELLIERNT;
ACELCOM = (positif_ou_nul(CELLIERNP + CELLIERNR + CELLIERNS + CELLIERNT) * BCELCOM2011) * ((V_REGCO+0) dans (1,3,5,6))
											* (1 - ART1731BIS);

CELSOMN = CELLIERNA+CELLIERNC+CELLIERND+CELLIERNE+CELLIERNF+CELLIERNH
	 +CELLIERNI+CELLIERNJ+CELLIERNK+CELLIERNM+CELLIERNN+CELLIERNO;  

DCEL = CELSOMN ; 

ACEL = (positif_ou_nul( CELSOMN ) * BCEL_2011) * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS);

DCELJP = CELLIERJP;
ACELJP = (positif_ou_nul( CELLIERJP) * BCEL_JP) * ((V_REGCO+0) dans (1,3,5,6)) * (1 - ART1731BIS);

DCELJBGL =   CELLIERJB + CELLIERJG + CELLIERJL;
ACELJBGL = (positif_ou_nul( CELLIERJB + CELLIERJG + CELLIERJL ) * BCEL_JBGL) * ((V_REGCO+0) dans (1,3,5,6))
									     * (1 - ART1731BIS);

DCELJOQR =   CELLIERJO + CELLIERJQ + CELLIERJR;
ACELJOQR = (positif_ou_nul(CELLIERJO + CELLIERJQ + CELLIERJR) * BCEL_JOQR) * ((V_REGCO+0) dans (1,3,5,6))
									   * (1 - ART1731BIS);

CELSOMJ = CELLIERJA + CELLIERJD + CELLIERJE + CELLIERJF + CELLIERJH 
	  + CELLIERJJ + CELLIERJK + CELLIERJM + CELLIERJN;
 	   

DCEL2012 = CELSOMJ ; 

ACEL2012 = (positif_ou_nul( CELSOMJ ) * BCEL_2012) * ((V_REGCO+0) dans (1,3,5,6))
						   * (1 - ART1731BIS);


RCEL_2011 = (  positif(CELLIERNA + CELLIERNE) * arr (ACEL * (TX22/100))
            + positif(CELLIERNC + CELLIERND + CELLIERNH) * arr (ACEL * (TX25/100))
            + positif(CELLIERNF + CELLIERNJ) * arr (ACEL * (TX13/100))
            + positif(CELLIERNI) * arr (ACEL * (TX15/100))
	    + positif(CELLIERNM + CELLIERNN) * arr (ACEL * (TX40/100))
	    + positif(CELLIERNK + CELLIERNO) * arr (ACEL * (TX36/100))
            ) * ((V_REGCO+0) dans (1,3,5,6));

RCEL_2012 = (  positif(CELLIERJA + CELLIERJE + CELLIERJH) * arr (ACEL2012 * (TX13/100))
            + positif(CELLIERJD) * arr (ACEL2012 * (TX22/100))
            + positif(CELLIERJF + CELLIERJJ) * arr (ACEL2012 * (TX6/100))
            + positif(CELLIERJK + CELLIERJN) * arr (ACEL2012 * (TX24/100))
	    + positif(CELLIERJM) * arr (ACEL2012 * (TX36/100))
            ) * ((V_REGCO+0) dans (1,3,5,6));

RCEL_COM = (  positif(CELLIERNP + CELLIERNT) * arr (ACELCOM * (TX36/100))
               + positif(CELLIERNR + CELLIERNS) * arr (ACELCOM * (TX40/100)) 
              ) * ((V_REGCO+0) dans (1,3,5,6));

RCEL_JOQR = (  positif(CELLIERJQ) * arr (ACELJOQR * (TX36/100))
               + positif(CELLIERJO + CELLIERJR) * arr (ACELJOQR * (TX24/100)) 
              ) * ((V_REGCO+0) dans (1,3,5,6));

RCEL_NBGL = (  positif(CELLIERNB) * arr(ACELNBGL * (TX25/100))
	       + positif(CELLIERNG) * arr(ACELNBGL * (TX15/100))
	       + positif(CELLIERNL) * arr(ACELNBGL * (TX40/100))
              ) * ((V_REGCO+0) dans (1,3,5,6));

RCEL_JBGL = (  positif(CELLIERJB) * arr(ACELJBGL * (TX22/100))
	       + positif(CELLIERJG) * arr(ACELJBGL * (TX13/100))
	       + positif(CELLIERJL) * arr(ACELJBGL * (TX36/100))
              ) * ((V_REGCO+0) dans (1,3,5,6));

RCEL_NQ = ( positif(CELLIERNQ) * arr(ACELNQ * (TX40/100)) ) * ((V_REGCO+0) dans (1,3,5,6));

RCEL_JP = ( positif(CELLIERJP) * arr(ACELJP * (TX36/100)) ) * ((V_REGCO+0) dans (1,3,5,6));


RCEL_HNO = (  positif(CELLIERHN) * arr(ACELHNO * (TX25/100))
	       + positif(CELLIERHO) * arr(ACELHNO * (TX40/100))
              ) * ((V_REGCO+0) dans (1,3,5,6));

RCEL_HJK = (  positif(CELLIERHJ) * arr(ACELHJK * (TX25/100))
	      + positif(CELLIERHK) * arr(ACELHJK * (TX40/100))
             ) * ((V_REGCO+0) dans (1,3,5,6));


RCELDOM = (positif(CELLIERHK + CELLIERHO) * arr (ACELDO * (TX40/100))) 
             * ((V_REGCO+0) dans (1,3,5,6));                           

RCELM = (positif( CELLIERHJ + CELLIERHN ) * arr (ACELMET * (TX25/100)))
             * ((V_REGCO+0) dans (1,3,5,6));                           


RCEL_HM = positif(CELLIERHM) * arr (ACELHM * (TX40/100)) 
             * ((V_REGCO+0) dans (1,3,5,6));                           

RCEL_HL = positif( CELLIERHL ) * arr (ACELHL * (TX25/100))
             * ((V_REGCO+0) dans (1,3,5,6));                           
RCELREP_HS = positif(CELREPHS) * arr (ACELREPHS * (TX40/100)) * ((V_REGCO+0) dans (1,3,5,6));

RCELREP_HR = positif( CELREPHR ) * arr (ACELREPHR * (TX25/100)) * ((V_REGCO+0) dans (1,3,5,6));

RCELREP_HU = positif( CELREPHU ) * arr (ACELREPHU * (TX40/100)) * ((V_REGCO+0) dans (1,3,5,6));

RCELREP_HT = positif( CELREPHT ) * arr (ACELREPHT * (TX25/100)) * ((V_REGCO+0) dans (1,3,5,6));

RCELREP_HZ = positif( CELREPHZ ) * arr (ACELREPHZ * (TX40/100)) * ((V_REGCO+0) dans (1,3,5,6));

RCELREP_HX = positif( CELREPHX ) * arr (ACELREPHX * (TX25/100)) * ((V_REGCO+0) dans (1,3,5,6));

RCELREP_HW = positif( CELREPHW ) * arr (ACELREPHW * (TX40/100)) * ((V_REGCO+0) dans (1,3,5,6));

RCELREP_HV = positif( CELREPHV ) * arr (ACELREPHV * (TX25/100)) * ((V_REGCO+0) dans (1,3,5,6));

regle 2004078:
application : iliad , batch  ;

REDUCAVTCEL =RCOTFOR+RREPA+RRESTIMO+RFIPC+RFIPDOM+RAIDE+RDIFAGRI+RFORET
	     +RCINE+RSOCREPR+RRPRESCOMP+RINNO+RSOUFIP+RRIRENOV+RFOR+RHEBE+
	     RSURV+RLOGDOM+RTOURNEUF+RTOURTRA+RTOURES+RTOURREP+RTOUHOTR+RTOUREPA+
	     RCOMP+RCREAT+RRETU+RDONS+RNOUV;

RCELRREDLA = (max( min( ACELRREDLA , IDOM11-DEC11 - REDUCAVTCEL ) , 0 ))
	    * ((V_REGCO+0) dans (1,3,5,6));

RCELRREDLB = (max( min( ACELRREDLB , IDOM11-DEC11 - REDUCAVTCEL
	      - RCELRREDLA ) , 0 ))
	    * ((V_REGCO+0) dans (1,3,5,6));

RCELRREDLE = (max( min( ACELRREDLE , IDOM11-DEC11 - REDUCAVTCEL
	      - RCELRREDLA-RCELRREDLB ) , 0 ))
	    * ((V_REGCO+0) dans (1,3,5,6));

RCELRREDLC = (max( min( ACELRREDLC , IDOM11-DEC11 - REDUCAVTCEL
	      - RCELRREDLA-RCELRREDLB-RCELRREDLE ) , 0 ))
	    * ((V_REGCO+0) dans (1,3,5,6));

RCELRREDLD = (max( min( ACELRREDLD , IDOM11-DEC11 - REDUCAVTCEL
	      - RCELRREDLA-RCELRREDLB-RCELRREDLE-RCELRREDLC ) , 0 ))
	    * ((V_REGCO+0) dans (1,3,5,6));

RCELRREDLF = (max( min( ACELRREDLF , IDOM11-DEC11 - REDUCAVTCEL
	      - RCELRREDLA-RCELRREDLB-RCELRREDLE-RCELRREDLC-RCELRREDLD ) , 0 ))
	    * ((V_REGCO+0) dans (1,3,5,6));

RCELREPHS = (max( min( RCELREP_HS , IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) ) , 0))
	    * ((V_REGCO+0) dans (1,3,5,6));

RCELREPHR = (max( min( RCELREP_HR , IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      - RCELREPHS ) , 0))
	    * ((V_REGCO+0) dans (1,3,5,6));

RCELREPHU = (max( min( RCELREP_HU, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      - RCELREPHS-RCELREPHR ) , 0))
            * ((V_REGCO+0) dans (1,3,5,6));

RCELREPHT = (max( min( RCELREP_HT, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      - RCELREPHS-RCELREPHR-RCELREPHU ) , 0))
            * ((V_REGCO+0) dans (1,3,5,6));

RCELREPHZ = (max( min( RCELREP_HZ, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      - RCELREPHS-RCELREPHR-RCELREPHU-RCELREPHT ) , 0))
            * ((V_REGCO+0) dans (1,3,5,6));

RCELREPHX = (max( min( RCELREP_HX, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      - RCELREPHS-RCELREPHR-RCELREPHU-RCELREPHT-RCELREPHZ ) , 0))
            * ((V_REGCO+0) dans (1,3,5,6));

RCELREPHW = (max( min( RCELREP_HW, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      - RCELREPHS-RCELREPHR-RCELREPHU-RCELREPHT-RCELREPHZ-RCELREPHX ) , 0))
            * ((V_REGCO+0) dans (1,3,5,6));

RCELREPHV = (max( min( RCELREP_HV, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      - RCELREPHS-RCELREPHR-RCELREPHU-RCELREPHT-RCELREPHZ-RCELREPHX-RCELREPHW ) , 0))
            * ((V_REGCO+0) dans (1,3,5,6));

RCELREPHF = (max( min( ACELREPHF , IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V : RCELREPHi) ) , 0))
	    * ((V_REGCO+0) dans (1,3,5,6));

RCELREPHE = (max( min( ACELREPHE , IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F : RCELREPHi) ) , 0))
	    * ((V_REGCO+0) dans (1,3,5,6));

RCELREPHD = (max( min( ACELREPHD , IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E : RCELREPHi) ) , 0))
	    * ((V_REGCO+0) dans (1,3,5,6));

RCELREPHH = (max( min( ACELREPHH , IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D : RCELREPHi) ) , 0))
	    * ((V_REGCO+0) dans (1,3,5,6));

RCELREPHG = (max( min( ACELREPHG , IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D,H : RCELREPHi) ) , 0))
	    * ((V_REGCO+0) dans (1,3,5,6));

RCELREPHB = (max( min( ACELREPHB , IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G : RCELREPHi) ) , 0))
	    * ((V_REGCO+0) dans (1,3,5,6));

RCELREPHA = (max( min( ACELREPHA , IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G,B : RCELREPHi) ) , 0))
	    * ((V_REGCO+0) dans (1,3,5,6));


RCELHM = (max( min( RCEL_HM, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G,B,A : RCELREPHi) ) , 0))
	  * ((V_REGCO+0) dans (1,3,5,6));

RCELHL = (max( min( RCEL_HL , IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G,B,A : RCELREPHi) 
	      -RCELHM) , 0 ))
	   * ((V_REGCO+0) dans (1,3,5,6));

RCELHNO = (max( min( RCEL_HNO, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G,B,A : RCELREPHi) 
	      -RCELHM-RCELHL) , 0 ))
	   * ((V_REGCO+0) dans (1,3,5,6));

RCELHJK = (max( min( RCEL_HJK , IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G,B,A : RCELREPHi) 
	      -RCELHM-RCELHL-RCELHNO) , 0 ))
	   * ((V_REGCO+0) dans (1,3,5,6));

RCELNQ = (max( min(RCEL_NQ, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G,B,A : RCELREPHi) 
	      -RCELHM-RCELHL-RCELHNO-RCELHJK ) , 0 ))
	 * ((V_REGCO+0) dans (1,3,5,6));

RCELNBGL = (max( min(RCEL_NBGL, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G,B,A : RCELREPHi) 
	      -RCELHM-RCELHL-RCELHNO-RCELHJK-RCELNQ ) , 0 ))
	   * ((V_REGCO+0) dans (1,3,5,6));

RCELCOM = (max( min(RCEL_COM, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G,B,A : RCELREPHi) 
	      -RCELHM-RCELHL-RCELHNO-RCELHJK-RCELNQ-RCELNBGL ) , 0 ))
	  * ((V_REGCO+0) dans (1,3,5,6));

RCEL = (max( min(RCEL_2011, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G,B,A : RCELREPHi) 
	      -RCELHM-RCELHL-RCELHNO-RCELHJK-RCELNQ-RCELNBGL-RCELCOM ) , 0 ))
	     * ((V_REGCO+0) dans (1,3,5,6));

RCELJP = (max( min(RCEL_JP, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G,B,A : RCELREPHi) 
	      -RCELHM-RCELHL-RCELHNO-RCELHJK-RCELNQ-RCELNBGL-RCELCOM-RCEL ) , 0 ))
	 * ((V_REGCO+0) dans (1,3,5,6));

RCELJBGL = (max( min(RCEL_JBGL, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G,B,A : RCELREPHi) 
	      -RCELHM-RCELHL-RCELHNO-RCELHJK-RCELNQ-RCELNBGL-RCELCOM-RCEL-RCELJP ) , 0 ))
	   * ((V_REGCO+0) dans (1,3,5,6));

RCELJOQR = (max( min(RCEL_JOQR, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G,B,A : RCELREPHi) 
	      -RCELHM-RCELHL-RCELHNO-RCELHJK-RCELNQ-RCELNBGL-RCELCOM-RCEL-RCELJP
	      -RCELJBGL) , 0 ))
	  * ((V_REGCO+0) dans (1,3,5,6));

RCEL2012 = (max( min(RCEL_2012, IDOM11-DEC11 - REDUCAVTCEL
              - somme (i=A,B,E,C,D,F : RCELRREDLi) 
	      -somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G,B,A : RCELREPHi) 
	      -RCELHM-RCELHL-RCELHNO-RCELHJK-RCELNQ-RCELNBGL-RCELCOM-RCEL-RCELJP
	      -RCELJBGL-RCELJOQR) , 0 ))
	     * ((V_REGCO+0) dans (1,3,5,6));

RCELTOT = somme(i=A,B,E,C,D,F :  RCELRREDLi)
	  + somme (i=S,R,U,T,Z,X,W,V,F,E,D,H,G,B,A : RCELREPHi)  
	  + RCELHM + RCELHL + RCELHNO + RCELHJK + RCELNQ + RCELNBGL + RCELCOM
	  + RCEL + RCELJP + RCELJBGL + RCELJOQR + RCEL2012 ;

regle 2004079 :
application : iliad , batch  ;


RIVCEL1 =  RCEL_2011 * positif(ACEL); 

RIVCEL2 = RIVCEL1;

RIVCEL3 = RIVCEL1;

RIVCEL4 = RIVCEL1;

RIVCEL5 = RIVCEL1;

RIVCEL6 = RIVCEL1;

RIVCEL7 = RIVCEL1;

RIVCEL8 = (arr((min (CELSOMN,LIMCELLIER) * positif(CELLIERNM+CELLIERNN) * (TX40/100))
          +(min (CELSOMN,LIMCELLIER) * positif(CELLIERNK+CELLIERNO) * (TX36/100))
	  +(min (CELSOMN,LIMCELLIER) * positif(CELLIERNC+CELLIERND+CELLIERNH) * (TX25/100))
	  +(min (CELSOMN,LIMCELLIER) * positif(CELLIERNA+CELLIERNE) * (TX22/100))
	  +(min (CELSOMN,LIMCELLIER) * positif(CELLIERNI) * (TX15/100))
	  +(min (CELSOMN,LIMCELLIER) * positif(CELLIERNF+CELLIERNJ) * (TX13/100)))
	  -( 8 * RIVCEL1))
	  * ((V_REGCO+0) dans (1,3,5,6));
RIV2012CEL1 =  RCEL_2012 * positif(ACEL2012);
RIV2012CEL2 = RIV2012CEL1;
RIV2012CEL3 = RIV2012CEL1;
RIV2012CEL4 = RIV2012CEL1;
RIV2012CEL5 = RIV2012CEL1;
RIV2012CEL6 = RIV2012CEL1;
RIV2012CEL7 = RIV2012CEL1;

RIV2012CEL8 = (arr((min (CELSOMJ,LIMCELLIER) * positif(CELLIERJM) * (TX36/100))
		+(min (CELSOMJ,LIMCELLIER) * positif(CELLIERJK+CELLIERJN) * (TX24/100))
		+(min (CELSOMJ,LIMCELLIER) * positif(CELLIERJD) * (TX22/100))
		+(min (CELSOMJ,LIMCELLIER) * positif(CELLIERJA+CELLIERJE+CELLIERJH) * (TX13/100))
		+(min (CELSOMJ,LIMCELLIER) * positif(CELLIERJF+CELLIERJJ) * (TX6/100)))
	 	-( 8 * RIV2012CEL1))
 		* (1 - V_CNR);


RIVCELNBGL1 =  RCEL_NBGL * positif(ACELNBGL); 
RIVCELNBGL2 = RIVCELNBGL1;
RIVCELNBGL3 = RIVCELNBGL1;
RIVCELNBGL4 = RIVCELNBGL1;
RIVCELNBGL5 = RIVCELNBGL1;
RIVCELNBGL6 = RIVCELNBGL1;
RIVCELNBGL7 = RIVCELNBGL1;

RIVCELNBGL8 = (arr((min (CELLIERNB+CELLIERNG+CELLIERNL,LIMCELLIER) * positif(CELLIERNB) * (TX25/100))
          +(min (CELLIERNB+CELLIERNG+CELLIERNL,LIMCELLIER) * positif(CELLIERNG) * (TX15/100))
	  +(min (CELLIERNB+CELLIERNG+CELLIERNL,LIMCELLIER) * positif(CELLIERNL) * (TX40/100)))
	  -( 8 * RIVCELNBGL1))
	  * (1 - V_CNR);

RIVCELJBGL1 =  RCEL_JBGL * positif(ACELJBGL); 
RIVCELJBGL2 = RIVCELJBGL1;
RIVCELJBGL3 = RIVCELJBGL1;
RIVCELJBGL4 = RIVCELJBGL1;
RIVCELJBGL5 = RIVCELJBGL1;
RIVCELJBGL6 = RIVCELJBGL1;
RIVCELJBGL7 = RIVCELJBGL1;

RIVCELJBGL8 = (arr((min (CELLIERJB+CELLIERJG+CELLIERJL,LIMCELLIER) * positif(CELLIERJB) * (TX22/100))
          +(min (CELLIERJB+CELLIERJG+CELLIERJL,LIMCELLIER) * positif(CELLIERJG) * (TX13/100))
	  +(min (CELLIERJB+CELLIERJG+CELLIERJL,LIMCELLIER) * positif(CELLIERJL) * (TX36/100)))
	  -( 8 * RIVCELJBGL1))
	  * (1 - V_CNR);


RIVCELCOM1 =  RCEL_COM * positif(ACELCOM); 

RIVCELCOM2 = RIVCELCOM1;

RIVCELCOM3 = RIVCELCOM1;

RIVCELCOM4 = (arr((min (CELLIERNP+CELLIERNR+CELLIERNS+CELLIERNT, LIMCELLIER) * positif(CELLIERNP+CELLIERNT) * (TX36/100))
          +(min (CELLIERNP+CELLIERNR+CELLIERNS+CELLIERNT,LIMCELLIER) * positif(CELLIERNR+CELLIERNS) * (TX40/100)))
	  -( 4 * RIVCELCOM1))
	  * (1 - V_CNR);

RIVCELJOQR1 =  RCEL_JOQR * positif(ACELJOQR); 
RIVCELJOQR2 = RIVCELJOQR1;
RIVCELJOQR3 = RIVCELJOQR1;
RIVCELJOQR4 = (arr((min (CELLIERJO + CELLIERJQ + CELLIERJR, LIMCELLIER) * positif(CELLIERJQ) * (TX36/100))
          +(min (CELLIERJO + CELLIERJQ + CELLIERJR, LIMCELLIER) * positif(CELLIERJO+CELLIERJR) * (TX24/100)))
	  -( 4 * RIVCELJOQR1))
	  * (1 - V_CNR);


RIVCELNQ1 =  RCEL_NQ * positif(ACELNQ); 

RIVCELNQ2 = RIVCELNQ1;

RIVCELNQ3 = RIVCELNQ1;

RIVCELNQ4 = (arr((min (CELLIERNQ, LIMCELLIER) * positif(CELLIERNQ) * (TX40/100)))
	  -( 4 * RIVCELNQ1))
	  * (1 - V_CNR);

RIVCELJP1 =  RCEL_JP * positif(ACELJP); 
RIVCELJP2 = RIVCELJP1;
RIVCELJP3 = RIVCELJP1;

RIVCELJP4 = (arr((min (CELLIERJP, LIMCELLIER) * positif(CELLIERJP) * (TX36/100)))
	  -( 4 * RIVCELJP1))
	  * (1 - V_CNR);


RIVCELHJK1 = RCEL_HJK * positif(ACELHJK) ; 

RIVCELHJK2 = RIVCELHJK1;

RIVCELHJK3 = RIVCELHJK1;

RIVCELHJK4 = RIVCELHJK