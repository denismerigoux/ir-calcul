#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2013 
#au titre des revenus percus en 2012. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
regle 111011:
application :  iliad;
CONST0 = 0;
CONST1 = 1;
CONST2 = 2;
CONST3 = 3;
CONST4 = 4;
CONST10 = 10;
CONST20 = 20;
CONST40 = 40;
regle 1110:
application : batch, iliad;
LIG0 = (1 - positif(IPVLOC)) * (1 - positif(RE168 + TAX1649)) * IND_REV ;
LIG1 = (1 - positif(RE168 + TAX1649)) ;
LIG2 = (1 - positif(ANNUL2042)) ;
regle 1110010:
application : batch , iliad ;


LIG0010 = (INDV * INDC * INDP) * (1 - positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55))) * LIG0 * LIG2 ;

LIG0020 = (INDV * (1 - INDC) * (1 - INDP)) * (1 - positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55))) * LIG0 * LIG2 ;

LIG0030 = (INDC * (1 - INDV) * (1 - INDP)) * (1 - positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55))) * LIG0 * LIG2 ;

LIG0040 = (INDP * (1 - INDV) * (1 - INDC)) * (1 - positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55))) * LIG0 * LIG2 ;

LIG0050 = (INDV * INDC * (1 - INDP)) * (1 - positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55))) * LIG0 * LIG2 ;

LIG0060 = (INDV * INDP * (1 - INDC)) * (1 - positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55))) * LIG0 * LIG2 ;

LIG0070 = (INDC * INDP * (1 - INDV)) * (1 - positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55))) * LIG0 * LIG2 ;

LIG10YT = (INDV * INDC * INDP) * positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55)) * LIG0 * LIG2 ;

LIG20YT = (INDV * (1 - INDC) * (1 - INDP)) * positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55)) * LIG0 * LIG2 ;

LIG30YT = (INDC * (1 - INDV) * (1 - INDP)) * positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55)) * LIG0 * LIG2 ;

LIG40YT = (INDP * (1 - INDV) * (1 - INDC)) * positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55)) * LIG0 * LIG2 ;

LIG50YT = (INDV * INDC * (1 - INDP)) * positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55)) * LIG0 * LIG2 ;

LIG60YT = (INDV * INDP * (1 - INDC)) * positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55)) * LIG0 * LIG2 ;

LIG70YT = (INDC * INDP * (1 - INDV)) * positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55)) * LIG0 * LIG2 ;

regle 11110:
application : batch , iliad ;
LIG10V = positif_ou_nul(TSBNV + PRBV + BPCOSAV + GLDGRATV + positif(F10AV * null(TSBNV + PRBV + BPCOSAV + GLDGRATV))) ;
LIG10C = positif_ou_nul(TSBNC + PRBC + BPCOSAC + GLDGRATC + positif(F10AC * null(TSBNC + PRBC + BPCOSAC + GLDGRATC))) ;
LIG10P = positif_ou_nul(somme(i=1..4:TSBNi + PRBi) + positif(F10AP * null(somme(i=1..4:TSBNi + PRBi)))) ;
LIG10 = positif(LIG10V + LIG10C + LIG10P) ;
regle 11000:
application : batch , iliad ;

LIG1100 = positif(T2RV) * (1 - positif(IPVLOC)) ;

LIG899 = positif(RVTOT + LIG1100 + LIG910 + BRCMQ + RCMFR + REPRCM + LIGRCMABT + LIG2RCMABT + LIG3RCMABT + LIG4RCMABT
		  + RCMLIB + LIG29 + LIG30 + RFQ + 2REVF + 3REVF + LIG1130 + VLHAB + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * (1 - positif(LIG0010 + LIG0020 + LIG0030 + LIG0040 + LIG0050 + LIG0060 + LIG0070)) 
		 * (1 - positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55))) ;

LIG900 = positif(RVTOT + LIG1100 + LIG910 + BRCMQ + RCMFR + REPRCM + LIGRCMABT + LIG2RCMABT + LIG3RCMABT + LIG4RCMABT
		  + RCMLIB + LIG29 + LIG30 + RFQ + 2REVF + 3REVF + LIG1130 + VLHAB + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * positif(LIG0010 + LIG0020 + LIG0030 + LIG0040 + LIG0050 + LIG0060 + LIG0070) 
		 * (1 - positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55))) ;

LIG899YT = positif(RVTOT + LIG1100 + LIG910 + BRCMQ + RCMFR + REPRCM + LIGRCMABT + LIG2RCMABT + LIG3RCMABT + LIG4RCMABT
		   + RCMLIB + LIG29 + LIG30 + RFQ + 2REVF + 3REVF + LIG1130 + VLHAB + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * (1 - positif(LIG10YT + LIG20YT + LIG30YT + LIG40YT + LIG50YT + LIG60YT + LIG70YT)) 
		 * positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55)) ;

LIG900YT = positif(RVTOT + LIG1100 + LIG910 + BRCMQ + RCMFR + REPRCM + LIGRCMABT + LIG2RCMABT + LIG3RCMABT + LIG4RCMABT
		   + RCMLIB + LIG29 + LIG30 + RFQ + 2REVF + 3REVF + LIG1130 + VLHAB + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * positif(LIG10YT + LIG20YT + LIG30YT + LIG40YT + LIG50YT + LIG60YT + LIG70YT) 
		 * positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55)) ;

regle 111440:
application : batch , iliad ;
LIG4401 =  positif(V_FORVA) * (1 - positif_ou_nul(BAFV)) * LIG0 ;

LIG4402 =  positif(V_FORCA) * (1 - positif_ou_nul(BAFC)) * LIG0 ;

LIG4403 =  positif(V_FORPA) * (1 - positif_ou_nul(BAFP)) * LIG0 ;

regle 11113:
application : iliad,batch;
LIG13 =  positif(present(BACDEV)+ present(BACREV)
               + present(BAHDEV) +present(BAHREV)
               + present(BACDEC) +present(BACREC)
               + present(BAHDEC)+ present(BAHREC)
               + present(BACDEP)+ present(BACREP)
               + present(BAHDEP)+ present(BAHREP)
               + present(4BAHREV) + present(4BAHREC) + present(4BAHREP)
               + present(4BACREV) + present(4BACREC) + present(4BACREP)
               + present(BAFV) + present(BAFC) + present(BAFP)
	       + present(BAFORESTV) + present(BAFORESTC) 
	       + present(BAFORESTP)
               + present(BAFPVV) + present(BAFPVC) + present(BAFPVP))
	* (1 - positif(IPVLOC)) * LIG1 ;

regle 111135:
application : batch, iliad;
4BAQLV = positif(4BACREV + 4BAHREV) ;
4BAQLC = positif(4BACREC + 4BAHREC) ;
4BAQLP = positif(4BACREP + 4BAHREP) ;
regle 111134:
application : iliad , batch ;

LIG134V = positif(present(BAFV) + present(BAHREV) + present(BAHDEV) + present(BACREV) + present(BACDEV)+ present(BAFPVV)+present(BAFORESTV)) ;
LIG134C = positif(present(BAFC) + present(BAHREC) + present(BAHDEC) + present(BACREC) + present(BACDEC)+ present(BAFPVC)+present(BAFORESTC)) ;
LIG134P = positif(present(BAFP) + present(BAHREP) + present(BAHDEP) + present(BACREP) + present(BACDEP)+ present(BAFPVP)+present(BAFORESTP)) ;
LIG134 = positif(LIG134V + LIG134C + LIG134P+present(DAGRI6)+present(DAGRI5)+present(DAGRI4)+present(DAGRI3)+present(DAGRI2)+present(DAGRI1)) 
		* (1 - positif(IPVLOC)) * (1 - positif(abs(DEFIBA))) * LIG1 ;
LIGDBAIP = positif_ou_nul(DBAIP) * positif(DAGRI1 + DAGRI2 + DAGRI3 + DAGRI4 + DAGRI5 + DAGRI6) * (1 - positif(IPVLOC))
                          * positif(abs(abs(BAHQTOT)+abs(BAQTOT)-(DAGRI6+DAGRI5+DAGRI4+DAGRI3+DAGRI2+DAGRI1))) * LIG1 ;
regle 111136:
application : iliad ,batch;
LIG136 = positif(4BAQV + 4BAQC + 4BAQP) * (1 - positif(IPVLOC)) * LIG1 ;

regle 111590:
application : iliad, batch ;
pour i = V,C,P:
LIG_BICPi =        (
  present ( BICNOi )                          
 + present (BICDNi )                          
 + present (BIHNOi )                          
 + present (BIHDNi )                          
                  ) * LIG0 ;
LIG_BICP = LIG_BICPV + LIG_BICPC + LIG_BICPP ;
LIG_DEFNPI = positif(
   present ( DEFBIC6 ) 
 + present ( DEFBIC5 ) 
 + present ( DEFBIC4 ) 
 + present ( DEFBIC3 ) 
 + present ( DEFBIC2 )
 + present ( DEFBIC1 )
            )
  * LIG0 * LIG2 ;

LIGMLOC = positif(present(MIBMEUV) + present(MIBMEUC) + present(MIBMEUP)
		+ present(MIBGITEV) + present(MIBGITEC) + present(MIBGITEP)
		+ present(LOCGITV) + present(LOCGITC) + present(LOCGITP))
	  * LIG0 * LIG2 ;
 
LIGMLOCAB = positif(MLOCABV + MLOCABC + MLOCABP) * LIG0  * LIG2 ; 

LIGMIBMV = positif(BICPMVCTV + BICPMVCTC + BICPMVCTP) * (1 - null(4 - V_REGCO)) * LIG0 ;

LIGBNCMV = positif(BNCPMVCTV + BNCPMVCTC + BNCPMVCTP) * (1 - null(4 - V_REGCO)) * LIG0 ;

LIGPLOC = positif(LOCPROCGAV + LOCPROCGAC + LOCPROCGAP + LOCDEFPROCGAV + LOCDEFPROCGAC + LOCDEFPROCGAP
		   + LOCPROV + LOCPROC + LOCPROP + LOCDEFPROV + LOCDEFPROC + LOCDEFPROP) 
		   * (1 - null(4 - V_REGCO)) * LIG0 ;

LIGNPLOC = positif(LOCNPCGAV + LOCNPCGAC + LOCNPCGAPAC + LOCDEFNPCGAV + LOCDEFNPCGAC + LOCDEFNPCGAPAC
		   + LOCNPV + LOCNPC + LOCNPPAC + LOCDEFNPV + LOCDEFNPC + LOCDEFNPPAC 
		   + LOCGITCV + LOCGITCC + LOCGITCP + LOCGITHCV + LOCGITHCC + LOCGITHCP)
		   *  (1-null(4 - V_REGCO)) * LIG0 ;

LIGNPLOCF = positif(LOCNPCGAV + LOCNPCGAC + LOCNPCGAPAC + LOCDEFNPCGAV + LOCDEFNPCGAC + LOCDEFNPCGAPAC
		   + LOCNPV + LOCNPC + LOCNPPAC + LOCDEFNPV + LOCDEFNPC + LOCDEFNPPAC 
                   + LNPRODEF10 + LNPRODEF9 + LNPRODEF8 + LNPRODEF6 + LNPRODEF5
                   + LNPRODEF4 + LNPRODEF3 + LNPRODEF2 + LNPRODEF1
		   + LOCGITCV + LOCGITCC + LOCGITCP + LOCGITHCV + LOCGITHCC + LOCGITHCP)
		   *  (1-null(4 - V_REGCO)) * LIG0 ;

LIGDEFNPLOC = positif(TOTDEFLOCNP) *  (1-null(4 - V_REGCO)) ;

LIGLOCNSEUL = positif(LIGNPLOC + LIGDEFNPLOC + LIGNPLOCF) ;

LIGLOCSEUL = 1 - positif(LIGNPLOC + LIGDEFNPLOC + LIGNPLOCF) ;

regle 1115901:
application : iliad,batch;

LIG_BICNPF = 
       positif(
   present (BICDEC)
 + present (BICDEP)
 + present (BICDEV)
 + present (BICHDEC)
 + present (BICHDEP)
 + present (BICHDEV)
 + present (BICHREC)
 + present (BICHREP)
 + present (BICHREV)
 + present (BICREC)
 + present (BICREP)
 + present (BICREV)
 + present ( DEFBIC6 ) 
 + present ( DEFBIC5 ) 
 + present ( DEFBIC4 ) 
 + present ( DEFBIC3 ) 
 + present ( DEFBIC2 )
 + present ( DEFBIC1 )
)
                   * LIG0 * LIG2 ;
regle 11117:
application : iliad,batch;
LIG_BNCNF = positif (present(BNCV) + present(BNCC) + present(BNCP)) ;

LIGNOCEP = (present(NOCEPV) + present(NOCEPC) + present(NOCEPP)) * LIG0 * LIG2 ;

LIGNOCEPIMP = (present(NOCEPIMPV) + present(NOCEPIMPC) + present(NOCEPIMPP)) * LIG0 * LIG2 ;

LIGDAB = positif(present(DABNCNP6) + present(DABNCNP5) + present(DABNCNP4)
		 + present(DABNCNP3) + present(DABNCNP2) + present(DABNCNP1)) 
		* LIG0 * LIG2 ;

LIGDIDAB = present(DIDABNCNP) * LIG0 * LIG2 ;

LIGBNCIF = ( positif (LIGNOCEP) * (1 - positif(LIG3250) + null(BNCIF)) 
             + (null(BNCIF) * positif(LIGBNCDF)) 
	     + null(BNCIF) * (1 - positif_ou_nul(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5-DABNCNP4-DABNCNP3-DABNCNP2-DABNCNP1)))
	    * (1 - positif(LIGSPENPNEG + LIGSPENPPOS)) * LIG0 * LIG2 ;
regle 125:
application : batch, iliad;
LIG910 = positif(present(RCMABD) + present(RCMTNC) + present(RCMAV) + present(RCMHAD) 
	         + present(RCMHAB) + present(REGPRIV) + (1-present(BRCMQ)) *(present(RCMFR))
                ) * LIG0 * LIG2 ;
regle 111266:
application : iliad , batch ;
LIGBPLIB = present(RCMLIB) * LIG0 * (1 - null(4-V_REGCO)) * LIG2 ;
regle 1111130: 
application : iliad , batch ;
LIG1130 = positif(present(REPSOF)) * LIG0 * LIG2 ;
regle 1111950:
application : iliad, batch;
LIG1950 = INDREV1A8 *  positif_ou_nul(REVKIRE) 
                    * (1 - positif(positif_ou_nul(IND_TDR) * (1-(positif_ou_nul(TSELUPPEV + TSELUPPEC))))) 
                    * (1 - LIGPS) ;

LIG1950S = INDREV1A8 *  positif_ou_nul(REVKIRE) 
                     * (1 - positif(positif_ou_nul(IND_TDR) * (1-(positif_ou_nul(TSELUPPEV + TSELUPPEC))))) 
                     * LIGPS ;
regle 11129:
application : batch, iliad;
LIG29 = positif(present(RFORDI) + present(RFDHIS) + present(RFDANT) +
                present(RFDORD)) * (1 - positif(IPVLOC))
                * (1 - positif(LIG30)) * LIG1 * LIG2 * IND_REV ;
regle 11130:
application : iliad, batch ;
LIG30 = positif(RFMIC) * (1 - positif(IPVLOC)) * LIG1 * LIG2 ;
LIGREVRF = positif(present(FONCI) + present(REAMOR)) * (1 - positif(IPVLOC)) * LIG1 * LIG2 ;
regle 11149:
application : batch, iliad;
LIG49 =  INDREV1A8 * positif_ou_nul(DRBG) * LIG2 ;
regle 11152:
application :  iliad, batch;
LIG52 = positif(present(CHENF1) + present(CHENF2) + present(CHENF3) + present(CHENF4) 
                 + present(NCHENF1) + present(NCHENF2) + present(NCHENF3) + present(NCHENF4)) 
	     * LIG1 * LIG2 ;
regle 11158:
application : iliad, batch;
LIG58 = (present(PAAV) + present(PAAP)) * positif(LIG52) * LIG1 * LIG2 ;
regle 111585:
application : iliad, batch;
LIG585 = (present(PAAP) + present(PAAV)) * (1 - positif(LIG58)) * LIG1 * LIG2 ;
LIG65 = positif(LIG52 + LIG58 + LIG585 
                + present(CHRFAC) + present(CHNFAC) + present(CHRDED)
		+ present(DPERPV) + present(DPERPC) + present(DPERPP)
                + LIGREPAR)  
       * LIG1 * LIG2 ;
regle 111555:
application : iliad, batch;
LIGDPREC = present(CHRFAC) * LIG1;

LIGDFACC = (positif(20-V_NOTRAIT+0) * positif(DFACC)
           + (1 - positif(20-V_NOTRAIT+0)) * present(DFACC)) * LIG1 ;
regle 1111390:
application : batch, iliad;
LIG1390 = positif(positif(ABMAR) + (1 - positif(RI1)) * positif(V_0DN)) * LIG1 * LIG2 ;
regle 11168:
application : batch, iliad;
LIG68 = INDREV1A8 * (1 - positif(abs(RNIDF))) * LIG2 ;
regle 111681:
application : iliad, batch;
LIGRNIDF = positif(abs(RNIDF)) * (1 - null(4-V_REGCO)) * LIG1 * LIG2 ;
LIGRNIDF0 = positif(abs(RNIDF0)) * positif(abs(RNIDF)) * (1 - null(4-V_REGCO)) * LIG1 * LIG2 ;
LIGRNIDF1 = positif(abs(RNIDF1)) * positif(abs(RNIDF)) * (1 - null(4-V_REGCO)) * LIG1 * LIG2 ;
LIGRNIDF2 = positif(abs(RNIDF2)) * positif(abs(RNIDF)) * (1 - null(4-V_REGCO)) * LIG1 * LIG2 ;
LIGRNIDF3 = positif(abs(RNIDF3)) * positif(abs(RNIDF)) * (1 - null(4-V_REGCO)) * LIG1 * LIG2 ;
LIGRNIDF4 = positif(abs(RNIDF4)) * positif(abs(RNIDF)) * (1 - null(4-V_REGCO)) * LIG1 * LIG2 ;
LIGRNIDF5 = positif(abs(RNIDF5)) * positif(abs(RNIDF)) * (1 - null(4-V_REGCO)) * LIG1 * LIG2 ;
regle 1111420:
application : iliad,batch;
LIGTTPVQ = positif(
              positif(CARTSV) + positif(CARTSC) + positif(CARTSP1) + positif(CARTSP2)+ positif(CARTSP3)+ positif(CARTSP4)
           +  positif(REMPLAV) + positif(REMPLAC) + positif(REMPLAP1) + positif(REMPLAP2)+ positif(REMPLAP3)+ positif(REMPLAP4)
           +  positif(PEBFV) + positif(PEBFC) + positif(PEBF1) + positif(PEBF2)+ positif(PEBF3)+ positif(PEBF4)
           +  positif(CARPEV) + positif(CARPEC) + positif(CARPEP1) + positif(CARPEP2)+ positif(CARPEP3)+ positif(CARPEP4)
           +  positif(PENSALV) + positif(PENSALC) + positif(PENSALP1) + positif(PENSALP2)+ positif(PENSALP3)+ positif(PENSALP4)
           +  positif(RENTAX) + positif(RENTAX5) + positif(RENTAX6) + positif(RENTAX7)
           +  positif(REVACT) + positif(REVPEA) + positif(PROVIE) + positif(DISQUO) + positif(RESTUC) + positif(INTERE)
           +  positif(FONCI) + positif(REAMOR)
           +  positif(4BACREV) + positif(4BACREC)+positif(4BACREP)+positif(4BAHREV)+positif(4BAHREC)+positif(4BAHREP)
           +  positif(GLD1V) + positif(GLD1C)+positif(GLD2V)+positif(GLD2V)+positif(GLD3V)+positif(GLD3V)
                  ) * LIG1 * LIG2 * (1 - null(4-V_REGCO)) ;

regle 111721:
application : batch, iliad;

LIG1430 = positif(BPTP3) * LIG0 * LIG2 ;

LIG1431 = positif(BPTP18) * LIG0 * LIG2 ;

LIG1432 = positif(BPTP19) * LIG0 * LIG2 ;
regle 111722:
application : batch, iliad;
LIG815 = V_EAD * positif(BPTPD) * LIG0 * LIG2 ;
LIG816 = V_EAG * positif(BPTPG) * LIG0 * LIG2 ;
LIGTXF225 = positif(PEA+0) * LIG0 * LIG2 ;
LIGTXF24 = positif(BPTP24) * LIG0 * LIG2 ;
LIGTXF30 = positif_ou_nul(BPCOPTV + BPCOPTC + BPVSK) * LIG0  * LIG2 ;
LIGTXF40 = positif(BPV40V + BPV40C + 0) * LIG0 * LIG2 ;

regle 111723:
application : batch, iliad ;

LIGCESDOM = positif(BPTPDIV) * positif(PVTAXSB) * positif(V_EAD + 0) * LIG0 * LIG2 ;
LIGCESDOMG = positif(BPTPDIV) * positif(PVTAXSB) * positif(V_EAG + 0) * LIG0 * LIG2 ;

LIGDOMPRO = positif(BPTPDIV) * positif(PVTAXSC) * positif(V_EAD + 0) * LIG0 * LIG2 ;
LIGDOMPROG = positif(BPTPDIV) * positif(PVTAXSC) * positif(V_EAG + 0) * LIG0 * LIG2 ;
regle 11181:
application : batch, iliad;
LIG81 = positif(present(RDDOUP) + present(RDFDOU) + present(DONAUTRE) + present(RDFDAUTRE) + present(REPDON03) 
		+ present(REPDON04) + present(REPDON05) + present(REPDON06) + present(REPDON07))
               * LIG1 * LIG2 ;
regle 1111500:
application : iliad, batch ;

LIG1500 = positif((positif(IPMOND) * present(IPTEFP)) + positif(INDTEFF) * positif(TEFFREVTOT)) 
	      * ((V_REGCO+0) dans (1,3,5,6)) * (1 - positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55))) * LIG1 * LIG2 ;

LIG1510 = positif((positif(IPMOND) * present(IPTEFN)) + positif(INDTEFF) * (1 - positif(TEFFREVTOT))) 
	      * ((V_REGCO+0) dans (1,3,5,6)) * (1 - positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55))) * LIG1 * LIG2 ;

LIG1500YT = positif((positif(IPMOND) * present(IPTEFP)) + positif(INDTEFF) * positif(TEFFREVTOT)) 
	      * ((V_REGCO+0) dans (1,3,5,6)) * positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55)) * LIG1 * LIG2 ;

LIG1510YT = positif((positif(IPMOND) * present(IPTEFN)) + positif(INDTEFF) * (1 - positif(TEFFREVTOT))) 
	      * ((V_REGCO+0) dans (1,3,5,6)) * positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55)) * LIG1 * LIG2 ;

regle 1111522:
application : iliad, batch ;
LIG1522 = (1 - present(IND_TDR)) * (1 - INDTXMIN) * (1 - INDTXMOY) * V_CR2 * LIG2 ;
regle 1111523:
application : batch, iliad;
LIG1523 = (1 - present(IND_TDR)) * null(V_REGCO - 4) * LIG2 ;
regle 11175:
application : iliad, batch ;
LIG75 = (1 - INDTXMIN) * (1 - INDTXMOY) * (1 - LIG1500) * (1 - LIG1510) * INDREV1A8 * LIG2 ;

LIG1545 = (1 - present(IND_TDR)) * INDTXMIN * positif(IND_REV) * LIG2 ;

LIG1760 = (1 - present(IND_TDR)) * INDTXMOY * LIG2 ;

LIG1546 = positif(PRODOM + PROGUY) * (1 - positif(V_EAD + V_EAG)) * LIG2 ;

LIG1550 = (1 - present(IND_TDR)) * INDTXMOY * LIG2 ;

LIG74 = (1 - present(IND_TDR)) * (1 - INDTXMIN) * positif(LIG1500 + LIG1510) * LIG2 ;

regle 11180:
application : batch, iliad ;
LIG80 = positif(present(RDREP) + present(RDFREP) + present(DONETRAN) + present(RDFDONETR)) * LIG1 * LIG2 ;
regle 11182:
application : batch, iliad;
LIG82 = positif(present(RDSYVO) + present(RDSYCJ) + present(RDSYPP) 
		+ present(COSBV) + present(COSBC) + present(COSBP))
	* LIG1 * LIG2 ;
regle 11188:
application : iliad , batch ;
LIGRSOCREPR = positif(present(RSOCREPRISE)) * LIG1 * LIG2 ;
regle 1111740:
application : batch , iliad ;
LIG1740 = positif(RECOMP) * LIG2 ;
regle 1111780:
application : batch , iliad ;
LIG1780 = positif(RDCOM + NBACT) * LIG1 * LIG2 ;
regle 111981:
application : batch, iliad;
LIG98B = positif(LIG80 + LIGFIPC + LIGFIPDOM + present(DAIDE)
                 + LIGREDAGRI + LIGFORET + LIGRESTIMO  
	         + LIGCINE + LIGRSOCREPR + LIGCOTFOR 
	         + present(PRESCOMP2000) + present(RDPRESREPORT) + present(FCPI) 
		 + present(DSOUFIP) + LIGRIRENOV + present(DFOREST) 
		 + present(DHEBE) + present(DSURV)
	         + LIGLOGDOM + LIGACHTOUR  
	         + LIGTRATOUR + LIGLOGRES + LIGREPTOUR + LIGLOCHOTR
	         + LIGREPHA + LIGCREAT + LIG1780 + LIG2040 + LIG81 + LIGLOGSOC
	         + LIGDOMSOC1 
		 + somme (i=A,B,E,C,D,F : LIGCELLi)
		 + somme (i=A,B,D,E,F,H,G,L,M,S,R,U,T,Z,X,W,V : LIGCELHi) 
		 + LIGCELHNO + LIGCELHJK + LIGCELNQ + LIGCELCOM + LIGCELNBGL
		 + LIGCEL + LIGCELJP + LIGCELJBGL + LIGCELJOQR + LIGCEL2012
		 + LIGREDMEUB + LIGREDREP + LIGILMIX + LIGINVRED + LIGILMIH + LIGILMIZ + LIGMEUBLE 
		 + LIGPROREP + LIGREPNPRO + LIGMEUREP + LIGILMIC + LIGILMIB + LIGILMIA 
		 + LIGRESIMEUB + LIGRESINEUV + LIGRESIVIEU + LIGLOCIDEFG
		 + present(DNOUV) + LIGLOCENT + LIGCOLENT + LIGRIDOMPRO
		 + LIGPATNAT + LIGPATNAT1 + LIGPATNAT2) 
           * LIG1 * LIG2 ;

LIGRED = LIG98B * (1 - positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55))) * LIG1 * LIG2 ;

LIGREDYT = LIG98B * positif((CMAJ + 0) dans (3,4,5,6,8,11,31,55)) * LIG1 * LIG2 ;

regle 1111820:
application : batch , iliad ;

LIG1820 = positif(ABADO + ABAGU + RECOMP) * LIG2 ;

regle 111106:
application : iliad , batch ;

LIG106 = positif(RETIR) ;
LIGINRTAX = positif(RETTAXA) ;
LIG10622 = positif(RETIR22) ;
LIGINRTAX22 = positif(RETTAXA22) ;
ZIG_INT22 = positif(RETCS22 + RETPS22 + RETRD22 + RETCVN22) ;

LIGINRPCAP = positif(RETPCAP) ;
LIGINRPCAP2 = positif(RETPCAP22) ;
LIGINRLOY = positif(RETLOY) ;
LIGINRLOY2 = positif(RETLOY22) ;

LIGINRHAUT = positif(RETHAUTREV) ;
LIGINRHAUT2 = positif(RETCHR22) ;
regle 111107:
application : iliad, batch;

LIG_172810 = TYPE2 * positif(NMAJ1) ;

LIGTAXA17281 = TYPE2 * positif(NMAJTAXA1) ;

LIGPCAP17281 = TYPE2 * positif(NMAJPCAP1) ;

LIGCHR17281 = TYPE2 * positif(NMAJCHR1) ;

LIG_NMAJ1 = TYPE2 * positif(NMAJ1) ;
LIG_NMAJ3 = TYPE2 * positif(NMAJ3) ;
LIG_NMAJ4 = TYPE2 * positif(NMAJ4) ;

LIGNMAJTAXA1 = TYPE2 * positif(NMAJTAXA1) ;
LIGNMAJTAXA3 = TYPE2 * positif(NMAJTAXA3) ;
LIGNMAJTAXA4 = TYPE2 * positif(NMAJTAXA4) ;

LIGNMAJPCAP1 = TYPE2 * positif(NMAJPCAP1) ;
LIGNMAJPCAP3 = TYPE2 * positif(NMAJPCAP3) ;
LIGNMAJPCAP4 = TYPE2 * positif(NMAJPCAP4) ;
LIGNMAJLOY1 = TYPE2 * positif(NMAJLOY1) ;
LIGNMAJLOY3 = TYPE2 * positif(NMAJLOY3) ;
LIGNMAJLOY4 = TYPE2 * positif(NMAJLOY4) ;

LIGNMAJCHR1 = TYPE2 * positif(NMAJCHR1) ;
LIGNMAJCHR3 = TYPE2 * positif(NMAJCHR3) ;
LIGNMAJCHR4 = TYPE2 * positif(NMAJCHR4) ;

regle 111109:
application : batch, iliad;
LIG109 = positif(IPSOUR + REGCI + LIGPVETR + LIGCULTURE + LIGMECENAT 
		  + LIGCORSE + LIG2305 + LIGCICAP + LIGREGCI
		  + LIGBPLIB + LIGCIGE + LIGDEVDUR + LIGDDUBAIL
                  + LIGVEHICULE + LIGVEACQ + LIGVEDESTR + LIGCICA + LIGCIGARD + LIG82
		  + LIGPRETUD + LIGSALDOM + LIGHABPRIN
		  + LIGCREFAM + LIGCREAPP + LIGCREBIO  + LIGPRESINT + LIGCREPROSP + LIGINTER
		  + LIGCRETECH + LIGRESTAU + LIGRESERV + LIGCONGA + LIGMETART 
		  + LIGCREFORM + LIGLOYIMP + LIGMOBIL + LIGJEUNE
		  + LIGVERSLIB + LIGCITEC + LIGTABAC
		  + LIGPPEVCP + LIGPPEV + LIGPPEC + LIGPPEP 
		  + LIGPPEVP + LIGPPEVC + LIGPPECP 
		   ) 
               * LIG1 * LIG2 ;

LIGCRED1 = positif(REGCI + LIGPVETR + LIGCICAP + LIGREGCI + 0) 
	    * (1 - positif(IPSOUR + LIGCULTURE + LIGMECENAT + LIGCORSE + LIG2305 + LIGBPLIB + LIGCIGE + LIGDEVDUR + LIGDDUBAIL + LIGVEHICULE
		           + LIGVEACQ + LIGVEDESTR + LIGCICA + LIGCIGARD + LIG82 + LIGPRETUD + LIGSALDOM + LIGHABPRIN + LIGCREFAM + LIGCREAPP 
		           + LIGCREBIO + LIGPRESINT + LIGCREPROSP + LIGINTER + LIGCRETECH + LIGRESTAU + LIGRESERV + LIGCONGA + LIGMETART
		           + LIGCREFORM + LIGLOYIMP + LIGMOBIL + LIGJEUNE + LIGVERSLIB + LIGCITEC + LIGTABAC + 0))
	    ;

LIGCRED2 = (1 - positif(REGCI + LIGPVETR + LIGCICAP + LIGREGCI + 0)) 
	    * positif(IPSOUR + LIGCULTURE + LIGMECENAT + LIGCORSE + LIG2305 + LIGBPLIB + LIGCIGE + LIGDEVDUR + LIGDDUBAIL + LIGVEHICULE 
		      + LIGVEACQ + LIGVEDESTR + LIGCICA + LIGCIGARD + LIG82 + LIGPRETUD + LIGSALDOM + LIGHABPRIN + LIGCREFAM + LIGCREAPP 
		      + LIGCREBIO + LIGPRESINT + LIGCREPROSP + LIGINTER + LIGCRETECH + LIGRESTAU + LIGRESERV + LIGCONGA + LIGMETART
		      + LIGCREFORM + LIGLOYIMP + LIGMOBIL + LIGJEUNE + LIGVERSLIB + LIGCITEC + LIGTABAC + 0)
	    ;

LIGCRED3 = positif(REGCI + LIGPVETR + LIGCICAP + LIGREGCI + 0) 
	    * positif(IPSOUR + LIGCULTURE + LIGMECENAT + LIGCORSE + LIG2305 + LIGBPLIB + LIGCIGE + LIGDEVDUR + LIGDDUBAIL + LIGVEHICULE 
		      + LIGVEACQ + LIGVEDESTR + LIGCICA + LIGCIGARD + LIG82 + LIGPRETUD + LIGSALDOM + LIGHABPRIN + LIGCREFAM + LIGCREAPP 
		      + LIGCREBIO + LIGPRESINT + LIGCREPROSP + LIGINTER + LIGCRETECH + LIGRESTAU + LIGRESERV + LIGCONGA + LIGMETART
		      + LIGCREFORM + LIGLOYIMP + LIGMOBIL + LIGJEUNE + LIGVERSLIB + LIGCITEC + LIGTABAC + 0)
           ;
regle 1112030:
application : batch, iliad ;
LIGNRBASE = positif(present(NRINET) + present(NRBASE)) * LIG1 * LIG2 ;
LIGBASRET = positif(present(IMPRET) + present(BASRET)) * LIG1 * LIG2 ;
regle 1112332:
application :  iliad, batch ;
LIGAVFISC = positif(AVFISCOPTER) * LIG1 * LIG2 ; 
regle 1112040:
application : batch, iliad;
LIG2040 = positif(DNBE + RNBE + RRETU) * LIG1 * LIG2 ;
regle 1112041:
application : iliad, batch ;
LIGRDCSG = positif(positif(V_BTCSGDED) + present(DCSG) + present(RCMSOC)) * LIG1 * LIG2 ;
regle 111973:
application : iliad, batch;
LIG2305 = positif(DIAVF2) * LIG1 * LIG2 ;
LIGCIGE = positif(RDTECH + RDGEQ + RDEQPAHA) * LIG1 * LIG2 ;
regle 111117:
application : batch, iliad;
LIG_IRNET = (1 - positif(V_NOTRAIT - 20)) 
	    + positif(V_NOTRAIT - 20) * positif(ANTIRAFF + TAXANET + PCAPNET  + TAXLOYNET + HAUTREVNET 
						+ TAXANTAFF + PCAPANTAFF + TAXLOYANTAFF + HAUTREVANTAF) ;
regle 1112135:
application : batch, iliad;
LIGANNUL = positif(ANNUL2042) * (1 - LIGPS) ;

LIGANNULS = positif(ANNUL2042) * LIGPS ;
regle 1112050:
application : batch, iliad;
LIG2053 = positif(V_NOTRAIT - 20) * positif(IDEGR) * positif(IREST - SEUIL_8) * (1 - LIGPS) * TYPE2 ;

LIG2053S = positif(V_NOTRAIT - 20) * positif(IDEGR) * positif(IREST - SEUIL_8) * LIGPS * TYPE2 ;
regle 1112051:
application : batch,iliad ;
LIG2051 = (1 - positif(20 - V_NOTRAIT)) 
          * positif (RECUMBIS) ;

LIGBLOC = positif(V_NOTRAIT - 15) ;

LIGSUP = positif((V_NOTRAIT + 0) dans (16,26,36,46,56,66)) ;

LIGDEG = null(IRESTITIR) * positif((V_NOTRAIT + 0) dans (23,33,43,53,63)) ;

LIGRES = positif(V_ANTREIR + 0) * positif((V_NOTRAIT + 0) dans (23,33,43,53,63)) ;

LIGDEGRES = positif(IRESTITIR + 0) * null(V_ANTREIR) * positif((V_NOTRAIT + 0) dans (23,33,43,53,63)) ;

LIGNEMP = (1 - null(NAPTEMP)) ;

LIGEMP = null(NAPTEMP) ;

LIG2052 = (
	   APPLI_ILIAD * (1 - positif(20 - V_NOTRAIT)) * positif(V_ANTIR + LIG_IRNET)
	  ) * ( 1 - APPLI_OCEANS) * (1 - positif(LIG2051)) * TYPE2 ;

LIGTAXANT = (
	     APPLI_ILIAD * (1 - positif(20 - V_NOTRAIT)) * positif(V_TAXANT + LIGTAXANET * positif(TAXANET))
            ) * (1 - positif(LIG2051)) * TYPE2 ;

LIGPCAPANT = (
	      APPLI_ILIAD * (1 - positif(20 - V_NOTRAIT)) * positif(V_PCAPANT + LIGPCAPNET * positif(PCAPNET))
             ) * (1 - positif(LIG2051)) * TYPE2 ;
LIGLOYANT = (
	      APPLI_ILIAD * (1 - positif(20 - V_NOTRAIT)) * positif(V_TAXLOYANT + LIGLOYNET * positif(TAXLOYNET))
             ) * (1 - positif(LIG2051)) * TYPE2 ;

LIGHAUTANT = (
	      APPLI_ILIAD * (1 - positif(20 - V_NOTRAIT)) * positif(V_CHRANT + LIGHAUTNET * positif(HAUTREVNET))
             ) * (1 - positif(LIG2051)) * TYPE2 ;

LIGANTREIR = positif(V_ANTREIR + 0) * null(V_ANTCR) ;

LIGNANTREIR = positif(V_ANTREIR + 0) * positif(V_ANTCR + 0) ;

LIGNONREST = positif(V_NONRESTANT + 0) ;

LIGIINET = LIGSUP * positif(NAPT + 0) ;

LIGIINETC = LIGSUP * null(NAPT) ;

LIGIDEGR = LIGDEG * positif_ou_nul(IDEGR - SEUIL_8) ;

LIGIDEGRC = LIGDEG * positif(SEUIL_8 - IDEGR) ;

LIGIREST = LIGRES * positif_ou_nul(IREST - SEUIL_8) ;

LIGIRESTC = LIGRES * positif(SEUIL_8 - IREST) ;

LIGNMRR = LIGIINETC * positif(V_ANTRE + 0) ;

LIGNMRS = LIGIINETC * null(V_ANTRE) ;

LIGRESINF = LIGIDEGRC * LIGIRESTC ;

regle 1112080:
application : batch, iliad ;

LIG2080 = positif(NATIMP - 71) * LIG2 ;

regle 1112081:
application : batch, iliad ;

LIGTAXADEG = positif(NATIMP - 71) * positif(TAXADEG) * LIG2 ;

LIGPCAPDEG = positif(NATIMP - 71) * positif(PCAPDEG) * LIG2 ;

LIGLOYDEG = positif(NATIMP - 71) * positif(TAXLOYDEG) * LIG2 ;

LIGHAUTDEG = positif(NATIMP - 71) * positif(HAUTREVDEG) * LIG2 ;

regle 1112140:
application : iliad,batch;
INDCTX = si (  (V_NOTRAIT+0 = 23)  
            ou (V_NOTRAIT+0 = 33)   
            ou (V_NOTRAIT+0 = 43)   
            ou (V_NOTRAIT+0 = 53)   
            ou (V_NOTRAIT+0 = 63)  
            )
         alors (1)
         sinon (0)
         finsi;

INDIS = si (  (V_NOTRAIT+0 = 14)
            ou (V_NOTRAIT+0 = 16)
	    ou (V_NOTRAIT+0 = 26)
	    ou (V_NOTRAIT+0 = 36)
	    ou (V_NOTRAIT+0 = 46)
	    ou (V_NOTRAIT+0 = 56)
	    ou (V_NOTRAIT+0 = 66)
           )
        alors (1)
        sinon (0)
	finsi;


LIG2140 = si (( ((V_CR2+0=0) et NATIMP=1 et (IRNET + TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET + NRINET - NAPTOTA + NAPCR >= SEUIL_12)) 
		ou ((V_CR2+0=1) et (NATIMP=1 ou  NATIMP=0)))
		et LIG2141 + 0 = 0
		)
          alors ((((1 - INDCTX) * INDREV1A8 * (1 - (positif(IRANT)*null(NAPT)) ) * LIG2)
                + null(IINET + NAPTOTA) * null(INDREV1A8)) * positif(IND_REV) * (1 - LIGPS))
          finsi;

LIG2140S = si (( ((V_CR2+0=0) et NATIMP=1 et (IRNET + TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET + NRINET - NAPTOTA + NAPCR >= SEUIL_12)) 
		ou ((V_CR2+0=1) et (NATIMP=1 ou  NATIMP=0)))
		et LIG2141 + 0 = 0
		)
           alors ((((1 - INDCTX) * INDREV1A8 * (1 - (positif(IRANT)*null(NAPT)) ) * LIG2)
                + null(IINET + NAPTOTA) * null(INDREV1A8)) * positif(IND_REV) * LIGPS)
           finsi;

LIG21401 = si (( ((V_CR2+0=0) et NATIMP=1 et (IRNET + TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET + NRINET - NAPTOTA + NAPCR >= SEUIL_12)) 
		ou ((V_CR2+0=1) et (NATIMP=1 ou  NATIMP=0)))
		et LIG2141 + 0 = 0
		)
           alors ((((1 - INDCTX) * INDREV1A8 * (1 - (positif(IRANT)*null(NAPT)) ) * LIG2)
                + null(IINET + NAPTOTA) * null(INDREV1A8)) * positif(IND_REV) * positif(20 - V_NOTRAIT))
           finsi ;

LIG21402 = si (( ((V_CR2+0=0) et NATIMP=1 et (IRNET + TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET + NRINET - NAPTOTA + NAPCR >= SEUIL_12)) 
		ou ((V_CR2+0=1) et (NATIMP=1 ou  NATIMP=0)))
		et LIG2141 + 0 = 0
		)
           alors ((((1 - INDCTX) * INDREV1A8 * (1 - (positif(IRANT)*null(NAPT)) ) * LIG2)
                + null(IINET + NAPTOTA) * null(INDREV1A8)) * positif(IND_REV) * positif(V_NOTRAIT - 20))
           finsi ;

LIGTRAIT = LIGPS + LIG2140 ;

regle 112141:
application : batch,  iliad;

LIG2141 = null(IAN + RPEN - IAVT + TAXASSUR + IPCAPTAXT + TAXLOY + IHAUTREVT - IRANT) 
                  * positif(IRANT)
                  * (1 - positif(LIG2501))
		  * null(V_IND_TRAIT - 4)
		  * (1 - LIGPS)
		  * (1 - positif(NRINET + 0)) ;

LIG2141S = null(IAN + RPEN - IAVT + TAXASSUR + IPCAPTAXT + TAXLOY + IHAUTREVT - IRANT) 
                  * positif(IRANT)
                  * (1 - positif(LIG2501))
		  * null(V_IND_TRAIT - 4)
		  * LIGPS
		  * (1 - positif(NRINET + 0)) ;

regle 112145:
application : batch,  iliad;
LIGNETAREC = positif (IINET) * (1 - LIGPS) * positif(ANNUL2042) * TYPE2 ;

LIGNETARECS = positif (IINET) * LIGPS * positif(ANNUL2042) * TYPE2 ;

regle 1112150:
application : iliad , batch ;

LIG2150 = (1 - INDCTX) 
	 * positif(IREST)
         * (1 - positif(LIG2140))
         * (1 - positif(IND_REST50))
	 * (1 - LIGPS)
         * LIG2 ;

LIG2150S = (1 - INDCTX) 
	  * positif(IREST)
          * (1 - positif(LIG2140))
          * (1 - positif(IND_REST50))
	  * LIGPS
          * LIG2 ;

regle 1112160:
application : batch, iliad ;

LIG2161 =  INDCTX 
	  * positif(IREST) 
          * positif_ou_nul(IREST - SEUIL_8) 
	  * (1 - positif(IND_REST50)) ;


LIG2368 = INDCTX 
	 * positif(IREST)
         * positif ( positif(IND_REST50)
                     + positif(IDEGR) )
           ;

regle 1112171:
application : batch , iliad ;

LIG2171 = (1 - INDCTX) 
	 * positif(IREST)
	 * (1 - positif(LIG2140))
         * positif(IND_REST50)  
	 * (1 - LIGPS)
	 * LIG2 ;

LIG2171S = (1 - INDCTX) 
	  * positif(IREST)
	  * (1 - positif(LIG2140))
          * positif(IND_REST50)  
	  * LIGPS
	  * LIG2 ;

regle 11121710:
application : batch, iliad ;

LIGTROP = positif(V_ANTRE+V_ANTCR) * null(IINET)* positif_ou_nul(abs(NAPTOTA)
             - IRESTIT - IRANT) * (1 - positif_ou_nul(abs(NAPTOTA) - IRESTIT
             - IRANT - SEUIL_12))
               * null(IDRS2 - IDEC + IREP)
	       * (1 - LIGPS)
               * (1 - INDCTX);

LIGTROPS = positif(V_ANTRE+V_ANTCR) * null(IINET)* positif_ou_nul(abs(NAPTOTA)
             - IRESTIT - IRANT) * (1 - positif_ou_nul(abs(NAPTOTA) - IRESTIT
             - IRANT - SEUIL_12))
               * null(IDRS2 - IDEC + IREP)
	       * LIGPS
               * (1 - INDCTX);

LIGTROPREST =  positif(V_ANTRE+V_ANTCR) * null(IINET)* positif_ou_nul(abs(NAPTOTA) 
               - IRESTIT - IRANT) * (1 - positif_ou_nul(abs(NAPTOTA) - IRESTIT
               - IRANT - SEUIL_12))
		 * (1 - positif(LIGTROP))
	         * (1 - LIGPS)
                 * (1 - INDCTX);

LIGTROPRESTS =  positif(V_ANTRE+V_ANTCR) * null(IINET)* positif_ou_nul(abs(NAPTOTA) 
                - IRESTIT - IRANT) * (1 - positif_ou_nul(abs(NAPTOTA) - IRESTIT
                - IRANT - SEUIL_12))
		 * (1 - positif(LIGTROP))
	         * LIGPS
                 * (1 - INDCTX);

regle 1113210:
application : batch, iliad ;

LIGRESINF50 = positif(positif(IND_REST50) * positif(IREST) 
                      + positif(IDEGR) * (1 - positif_ou_nul(IDEGR - SEUIL_8)))  
	      * (1 - LIGPS) ;

LIGRESINF50S = positif(positif(IND_REST50) * positif(IREST) 
                      + positif(IDEGR) * (1 - positif_ou_nul(IDEGR - SEUIL_8)))  
	       * LIGPS ;
regle 1112200:
application : batch,iliad ;
LIG2200 = (positif(IDEGR) * positif_ou_nul(IDEGR - SEUIL_8) * (1 - LIGPS) * TYPE2) ;

LIG2200S = (positif(IDEGR) * positif_ou_nul(IDEGR - SEUIL_8) * 