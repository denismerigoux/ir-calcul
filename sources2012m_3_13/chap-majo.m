#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2013 
#au titre des revenus percus en 2012. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
regle isf 232:
application : iliad ;
SUPISF[X] = positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X)
            * max(ISF4BASE,0)
            + (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
	     * max(0,ISF4BASE - (TISF4BASE[FLAG_DERSTTR]));


regle 23111:
application : iliad ;
IRBASE = IRN - IRANT ;
CSBASE_MAJO = (CSG - CSGIM) * positif(CSG + RDSN + PRS + CSAL + CGAINSAL + RSE1
					  + RSE2 + RSE3 + RSE4 - SEUIL_61);
RDBASE_MAJO = (RDSN - CRDSIM) * positif(CSG + RDSN + PRS + CSAL + CGAINSAL + RSE1
					    + RSE2 + RSE3 + RSE4 - SEUIL_61);
PSBASE_MAJO = (PRS - PRSPROV) * positif(CSG + RDSN + PRS + CSAL + CGAINSAL + RSE1
					    + RSE2 + RSE3 + RSE4 - SEUIL_61);
CSALBASE_MAJO = (CSAL - CSALPROV) * positif(CSG + RDSN + PRS + CSAL + CGAINSAL + RSE1
						+ RSE2 + RSE3 + RSE4 - SEUIL_61);
CDISBASE_MAJO = CDIS * positif(CDIS + CSG + RDSN + PRS + CSAL + CGAINSAL + RSE1
                                          + RSE2 + RSE3 + RSE4 - SEUIL_61);
TAXABASE_MAJO = TAXASSUR * positif(IAMD1 + 1 - SEUIL_61);

HRBASE_MAJO = IHAUTREVT * positif(IAMD1 + 1 - SEUIL_61);

CAPBASE_MAJO = IPCAPTAXT * positif(IAMD1 + 1 - SEUIL_61);

GAINBASE_MAJO = (CGAINSAL - GAINPROV) * positif(CSG + RDSN + PRS + CSAL + CGAINSAL + RSE1
						+ RSE2 + RSE3 + RSE4 - SEUIL_61);

RSE1BASE_MAJO = RSE1N * positif(CSG + RDSN + PRS + CSAL + CGAINSAL + RSE1N
                                   + RSE2N + RSE3N + RSE4N - SEUIL_61);

RSE2BASE_MAJO = RSE2N * positif(CSG + RDSN + PRS + CSAL + CGAINSAL + RSE1N
				   + RSE2N + RSE3N + RSE4N - SEUIL_61);

RSE3BASE_MAJO = RSE3N * positif(CSG + RDSN + PRS + CSAL + CGAINSAL + RSE1N
				   + RSE2N + RSE3N + RSE4N - SEUIL_61);

RSE4BASE_MAJO = RSE4N * positif(CSG + RDSN + PRS + CSAL + CGAINSAL + RSE1N
                                   + RSE2N + RSE3N + RSE4N - SEUIL_61);

regle corrective 23112:
application :   iliad ;
TOT_BASE_MAJO = IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + HRBASE_MAJO ;

TOT_REF = TIRBASE[FLAG_DERSTTR] +TTAXABASE[FLAG_DERSTTR]
	 +TPCAPBASE[FLAG_DERSTTR]+TCHRBASE[FLAG_DERSTTR];


TAXA_ISO = TAXASSUR * positif(IAMD1 + 1 - SEUIL_61) ; 
CAP_ISO  = IPCAPTAXT * positif(IAMD1 + 1 - SEUIL_61) ; 
HR_ISO   = IHAUTREVT  * positif(IAMD1 + 1 - SEUIL_61) ; 
SUPIR_PENA_RESTIT = max(0, min( IRBASE + TTAXABASE[FLAG_DERSTTR]+TPCAPBASE[FLAG_DERSTTR]+TCHRBASE[FLAG_DERSTTR]
				- TOT_REF ,
                                 min(
                                      min(IRBASE_IRECT , IRBASE) - TIRBASE[FLAG_DERSTTR] ,
                                      TOT_BASE_MAJO - TOT_REF
                                    )
                               )
                        );


SUPIR[X] =  null(X) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
                   * max( 0, min( 
			          min( IRBASE_IRECT , IRBASE ),
				  TOT_BASE_MAJO 
                                )
                            
                        )
	      +( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		 * positif(null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))
		 * SUPIR_PENA_RESTIT


             + (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
             * (1 - positif((null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))))
             * (1 - positif(null(X-1)))
	     * ((1 - positif(TARDIFEVT2)*null(X-2))
                       * max( 0 , min( max( 0, IRBASE + TTAXABASE[FLAG_DERSTTR]+TPCAPBASE[FLAG_DERSTTR]+TCHRBASE[FLAG_DERSTTR])
			               - max( 0 , TOT_REF ),
			               min(
				       		min(IRBASE_IRECT , IRBASE) - TIRBASE[FLAG_DERSTTR] ,
						max( 0 , TOT_BASE_MAJO ) - max( 0 , TOT_REF )
                                          )
                                     )
                            )
                 + positif(TARDIFEVT2) * null(X-2) * TIRBASE[FLAG_DERSTTR]
	       )

           + (1 - positif((null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13)))) 
              *  null(X-1)*positif( null(CSTRATE1 - 1)
			       +null(CSTRATE1 - 7)
		               +null(CSTRATE1 - 8)
			       +null(CSTRATE1 - 10)
		               +null(CSTRATE1 - 11)
	                       +null(CSTRATE1 - 17)
                               +null(CSTRATE1 - 18)) 
                       * max( 0 , min( max( 0, IRBASE + TTAXABASE[FLAG_DERSTTR]+TPCAPBASE[FLAG_DERSTTR]+TCHRBASE[FLAG_DERSTTR])
			               - max( 0 , TOT_REF ),
			               min(
				       		min(IRBASE_IRECT , IRBASE) - TIRBASE[FLAG_DERSTTR] ,
						max( 0 , TOT_BASE_MAJO ) - max( 0 , TOT_REF )
                                          )
                                     )
                            )


           + null(X-1)*positif( null(CSTRATE1 - 3)
	                       +null(CSTRATE1 - 4)
			       +null(CSTRATE1 - 5)
			       +null(CSTRATE1 - 6)
		               +null(CSTRATE1 - 55)) 
                              		* SUPIR_PENA_RESTIT ; 


SUP2IR[X] = null(X) * null(CODE_2042 - 17) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
                    * max( 0, min( 
			            min( IRBASE_IRECT , IRBASE ),
				    TOT_BASE_MAJO 
                                 )
                         )         

	      + ((positif(null(X-14)+null(X-15)+null(X-18)+null(X-20))
                    * SUPIR_PENA_RESTIT 
	          )
	          + (1 - positif(null(X-14)+null(X-15)+null(X-18)+null(X-20)))* 0)
                 * (1 - positif(null(X-1))) 
           + null(X-1)*positif( null(CSTRATE1 - 1)
                               +null(CSTRATE1 - 17)
	                       +null(CSTRATE1 - 2)
			       +null(CSTRATE1 - 10)
		               +null(CSTRATE1 - 30)) 
	             * SUPIR_PENA_RESTIT;

SUPCS[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
	 * max(0,CSBASE_MAJO - (TCSBASE[FLAG_DERSTTR] * positif(TNAPCR[FLAG_DERSTTR] + NAPCR_P + null(CSBASE_MAJO - TCSBASE[FLAG_DERSTTR]))))
         +  (positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
         * max(CSBASE_MAJO,0); 
SUPPS[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
	 * max(0,PSBASE_MAJO - (TPSBASE[FLAG_DERSTTR] * positif(TNAPCR[FLAG_DERSTTR] + NAPCR_P + null(PSBASE_MAJO - TPSBASE[FLAG_DERSTTR]))))
         +  (positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
         * max(PSBASE_MAJO,0); 
SUPRD[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
	 * max(0,RDBASE_MAJO - (TRDBASE[FLAG_DERSTTR] * positif(TNAPCR[FLAG_DERSTTR] + NAPCR_P + null(RDBASE_MAJO - TRDBASE[FLAG_DERSTTR]))))
         +  (positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
         * max(RDBASE_MAJO,0); 
SUPCSAL[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
	 * max(0,CSALBASE_MAJO - (TCSALBASE[FLAG_DERSTTR] * positif(TNAPCR[FLAG_DERSTTR] + NAPCR_P + null(CSALBASE_MAJO - TCSALBASE[FLAG_DERSTTR]))))
         +  (positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
         * max(CSALBASE_MAJO,0); 
SUPCDIS[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
	 * max(0,CDISBASE_MAJO - (TCDISBASE[FLAG_DERSTTR] * positif(TNAPCR[FLAG_DERSTTR] + NAPCR_P + null(CDISBASE_MAJO - TCDISBASE[FLAG_DERSTTR]))))
         +  (positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
         * max(CDISBASE_MAJO,0); 
SUPGAIN[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
	 * max(0,GAINBASE_MAJO - (TGAINBASE[FLAG_DERSTTR] * positif(TNAPCR[FLAG_DERSTTR] + NAPCR_P + null(GAINBASE_MAJO - TGAINBASE[FLAG_DERSTTR]))))
         +  (positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
         * max(GAINBASE_MAJO,0); 

SUPRSE1[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
	 * max(0,RSE1BASE_MAJO - (TRSE1BASE[FLAG_DERSTTR] * positif(TNAPCR[FLAG_DERSTTR] + NAPCR_P + null(RSE1BASE_MAJO - TRSE1BASE[FLAG_DERSTTR]))))
         +  (positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
         * max(RSE1BASE_MAJO,0); 

SUPRSE2[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
	 * max(0,RSE2BASE_MAJO - (TRSE2BASE[FLAG_DERSTTR] * positif(TNAPCR[FLAG_DERSTTR] + NAPCR_P + null(RSE2BASE_MAJO - TRSE2BASE[FLAG_DERSTTR]))))
         +  (positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
         * max(RSE2BASE_MAJO,0); 

SUPRSE3[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
	 * max(0, min(RSE3BASE2042_FIC , RSE3BASE_MAJO) - (TRSE3BASE[FLAG_DERSTTR] * positif(TNAPCR[FLAG_DERSTTR] + NAPCR_P + null(RSE3BASE_MAJO - TRSE3BASE[FLAG_DERSTTR]))))
         +  (positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
         * max(RSE3BASE_MAJO,0); 

SUPRSE4[X] =  (1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
	 * max(0,RSE4BASE_MAJO - (TRSE4BASE[FLAG_DERSTTR] * positif(TNAPCR[FLAG_DERSTTR] + NAPCR_P + null(RSE4BASE_MAJO - TRSE4BASE[FLAG_DERSTTR]))))
         +  (positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
         * max(RSE4BASE_MAJO,0); 

SUPTAXA[X] =  null(X) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
		      * max(0, min( IRBASE + TAXABASE_MAJO ,
				       min(
				             min( TAXABASE_IRECT , TAXABASE_MAJO) ,
				             TOT_BASE_MAJO
				          )
                                  )
                           )


	      +( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		 * positif(null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))
	         * max(0, min( IRBASE + TAXABASE_MAJO + TPCAPBASE[FLAG_DERSTTR] + TCHRBASE[FLAG_DERSTTR]
			       - TOT_REF ,
				   min(
		                   	min( TAXABASE_IRECT , TAXABASE_MAJO) - TTAXABASE[FLAG_DERSTTR] ,
				   	TOT_BASE_MAJO - TOT_REF
                                      )
                             )
                       )			       


              + ( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		* (1 - positif((null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))))

		    * max( 0 , min( max( 0 , IRBASE + TAXABASE_MAJO + TPCAPBASE[FLAG_DERSTTR] + TCHRBASE[FLAG_DERSTTR] )
				     - max( 0 , TOT_REF ) ,
				    min(
			             min( TAXABASE_IRECT , TAXABASE_MAJO) - TTAXABASE[FLAG_DERSTTR] ,
			             max( 0 , TOT_BASE_MAJO )  - max( 0 , TOT_REF )
				     )
				  )
		         ) ;



SUP2TAXA[X] = null(X) * null(CODE_2042 - 17) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)

		      * max(0, min( IRBASE + TAXABASE_MAJO ,
				       min(
				             min( TAXABASE_IRECT , TAXABASE_MAJO) ,
				             TOT_BASE_MAJO
				          )
                                  )
                           )

	      + (positif(null(X-14)+null(X-15)+null(X-18)+null(X-20))

	         * max(0, min( IRBASE + TAXABASE_MAJO + TPCAPBASE[FLAG_DERSTTR] + TCHRBASE[FLAG_DERSTTR]
			       - TOT_REF ,
				   min(
		                   	min( TAXABASE_IRECT , TAXABASE_MAJO) - TTAXABASE[FLAG_DERSTTR] ,
				   	TOT_BASE_MAJO - TOT_REF
                                      )
                             )
                       )			       
	        )
	     + (1 - positif(null(X-14)+null(X-15)+null(X-18)+null(X-20))) * 0
	     ;


SUPCAP[X] =  null(X) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
		    * max(0, min( IRBASE + TAXABASE_MAJO + CAPBASE_MAJO ,
				     min(
					    min( CAPBASE_IRECT , CAPBASE_MAJO) ,
                                            TOT_BASE_MAJO
					)
                                 )
                         )

	      +( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		 * positif(null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))
	         * max(0, min(IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + TCHRBASE[FLAG_DERSTTR] - TOT_REF,
			      min(
			           min( CAPBASE_IRECT , CAPBASE_MAJO) - TPCAPBASE[FLAG_DERSTTR] , 
				   TOT_BASE_MAJO - TOT_REF
                                 )
                             )
                      )
	+( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		* (1 - positif((null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))))

	        * max( 0 , min( max( 0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + TCHRBASE[FLAG_DERSTTR])
				 - max( 0 , TOT_REF ) ,  
				  min(
				     min( CAPBASE_IRECT , CAPBASE_MAJO) - TPCAPBASE[FLAG_DERSTTR] ,
				     max( 0 , TOT_BASE_MAJO ) - max( 0 , TOT_REF )
                                     )
                              )
                     ) ;

SUP2CAP[X] = null(X) * null(CODE_2042 - 17) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
		     * max(0, min( IRBASE + TAXABASE_MAJO + CAPBASE_MAJO ,
				     min(
					    min( CAPBASE_IRECT , CAPBASE_MAJO) ,
                                            TOT_BASE_MAJO
					)
                                 )
                          )
	      + (positif(null(X-14)+null(X-15)+null(X-18)+null(X-20))
	         * max(0, min(IRBASE + TAXABASE_MAJO + CAPBASE_MAJO + TCHRBASE[FLAG_DERSTTR] - TOT_REF,
			      min(
			           min( CAPBASE_IRECT , CAPBASE_MAJO) - TPCAPBASE[FLAG_DERSTTR] , 
				   TOT_BASE_MAJO - TOT_REF
                                 )
                             )
                      )

	        )
	     + (1 - positif(null(X-14)+null(X-15)+null(X-18)+null(X-20))) * 0
	     ;

SUPHR[X] =  null(X) * positif(FLAG_RETARD) * positif(FLAG_RECTIF)
		    * max(0, 
				     min(
					    min( HRBASE_IRECT , HRBASE_MAJO) ,
                                            TOT_BASE_MAJO
					)
                          )

	      +( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		 * positif(null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))
	         * max(0, min( 
		                min( HRBASE_IRECT, HRBASE_MAJO) - TCHRBASE[FLAG_DERSTTR] , 
                                TOT_BASE_MAJO - TOT_REF
                             )
                      )

              + ( 1 - positif(FLAG_RETARD) * positif(FLAG_RECTIF) * null(X))
		* (1 - positif((null(X-3)+null(X-7)+null(X-8)+null(X-12)+null(X-13))))
                * max( 0 , min( 
                                  min( HRBASE_IRECT, HRBASE_MAJO) - TCHRBASE[FLAG_DERSTTR] ,
			          max( 0 , TOT_BASE_MAJO ) - max( 0 , TOT_REF ) 
                              )
                     );


SUP2HR[X] = null(X) * null(CODE_2042 - 17) * positif(FLAG_RETARD) * positif(FLAG_RECTIF) 
		    * max(0, 
				     min(
					    min( HRBASE_IRECT , HRBASE_MAJO) ,
                                            TOT_BASE_MAJO
					)
                          )

	      + (positif(null(X-14)+null(X-15)+null(X-18)+null(X-20))

	         * max(0, min( 
		                min( HRBASE_IRECT, HRBASE_MAJO) - TCHRBASE[FLAG_DERSTTR] , 
                                TOT_BASE_MAJO - TOT_REF
                             )
                      )
	        )
	     + (1 - positif(null(X-14)+null(X-15)+null(X-18)+null(X-20))) * 0
	        ;
regle corrective 23113:
application : iliad;
TMAJOIR[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPIR[X] * TAUX_STRATE/100 ));
T2MAJOIR[X] = (1 - null(1 - IND_RJLJ))
	     * (
	     (positif(null(X - 0) * null(CODE_2042 - 17) 
		 +null(X-14)+null(X-15)+null(X-18)+null(X-20))
		*(null(X-20)*TL_IR*arr(SUP2IR[X] * TX1758A/100)
		  +(1-null(X-20)) * arr(SUP2IR[X] * TX1758A/100)))

	      + null(X-1) 
	                  * positif(null(X - 1) * null(CSTRATE1 - 17)
                               +null(CSTRATE1 - 1)
	                       +null(CSTRATE1 - 2)
			       +null(CSTRATE1 - 10)
		               +null(CSTRATE1 - 30)) * arr(SUP2IR[X] * TX1758A/100)

                 ); 

MAJOIR_ST = MAJOIRST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOIR[X] + T2MAJOIR[X];
TMAJOCS[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPCS[X] * TAUX_STRATE/100 ));
MAJOCS_ST = MAJOCSST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOCS[X] ;
TMAJOPS[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPPS[X] * TAUX_STRATE/100 ));
MAJOPS_ST = MAJOPSST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOPS[X] ;
TMAJORD[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPRD[X] * TAUX_STRATE/100 ));
MAJORD_ST = MAJORDST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJORD[X] ;
TMAJOCSAL[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPCSAL[X] * TAUX_STRATE/100 ));
MAJOCSAL_ST = MAJOCSALST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOCSAL[X] ;
TMAJOCDIS[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPCDIS[X] * TAUX_STRATE/100 ));
MAJOCDIS_ST = MAJOCDISST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOCDIS[X] ;
TMAJOGAIN[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPGAIN[X] * TAUX_STRATE/100 ));
MAJOGAIN_ST = MAJOGAINST_DEF * (1 - positif(FLAG_1STRATE)) + 
TMAJOGAIN[X] ;
TMAJORSE1[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPRSE1[X] * TAUX_STRATE/100 ));
MAJORSE1_ST = MAJORSE1ST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJORSE1[X] ;
TMAJORSE2[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPRSE2[X] * TAUX_STRATE/100 ));
MAJORSE2_ST = MAJORSE2ST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJORSE2[X] ;
TMAJORSE3[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPRSE3[X] * TAUX_STRATE/100 ));
MAJORSE3_ST = MAJORSE3ST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJORSE3[X] ;
TMAJORSE4[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPRSE4[X] * TAUX_STRATE/100 ));
MAJORSE4_ST = MAJORSE4ST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJORSE4[X] ;
TMAJOTAXA[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPTAXA[X] * TAUX_STRATE/100 ));
T2MAJOTAXA[X] = (1 - null(1 - IND_RJLJ))
	     * (positif(null(X - 0) * null(CODE_2042 - 17) 
	        + null(X-14)+null(X-15)+null(X-18)+null(X-20))
		*(null(X-20)*TL_TAXAGA*arr(SUP2TAXA[X] * TX1758A/100)
		  +(1-null(X-20)) * arr(SUP2TAXA[X] * TX1758A/100)));
MAJOTAXA_ST = MAJOTAXAST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOTAXA[X] + T2MAJOTAXA[X];
TMAJOHR[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPHR[X] * TAUX_STRATE/100 ));
T2MAJOHR[X] = (1 - null(1 - IND_RJLJ))
	     * (positif(null(X - 0) * null(CODE_2042 - 17) 
	        + null(X-14)+null(X-15)+null(X-18)+null(X-20))
		*(null(X-20)*TL_CHR*arr(SUP2HR[X] * TX1758A/100)
		  +(1-null(X-20)) * arr(SUP2HR[X] * TX1758A/100)));
MAJOHR_ST = MAJOHRST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOHR[X] + T2MAJOHR[X];
TMAJOCAP[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPCAP[X] * TAUX_STRATE/100 ));
T2MAJOCAP[X] = (1 - null(1 - IND_RJLJ))
	     * (positif(null(X - 0) * null(CODE_2042 - 17) 
	        + null(X-14)+null(X-15)+null(X-18)+null(X-20))
		*(null(X-20)*TL_CAP*arr(SUP2CAP[X] * TX1758A/100)
		  +(1-null(X-20)) * arr(SUP2CAP[X] * TX1758A/100)));
MAJOCAP_ST = MAJOCAPST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOCAP[X] + T2MAJOCAP[X];
regle isf 233:
application : iliad;
TMAJOISF[X] = (1 - null((1 - IND_RJLJ) + (10 - TAUX_STRATE)))
	     * arr( (SUPISF[X] * TAUX_STRATE/100 ));
MAJOISF_ST = MAJOISFST_DEF * (1 - positif(FLAG_1STRATE)) + 
            TMAJOISF[X] ;
regle corrective 23114:
application : iliad;

MAJOIR07_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                            min(min(IRBASE2042_FIC,IRBASE),
                                max(0, IRBASE)
                               )
                            )
			* STR_TR16 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              ); 
MAJOIR08_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                            min(min(IRBASE2042_FIC,IRBASE),
                                max(0, IRBASE)
                               )
                            )
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              ); 
MAJOIR17_1TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                            min(min(IRBASE2042_FIC,IRBASE),
                                max(0, IRBASE)
                               )
                            )

			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              ); 
MAJOIR17_2TARDIF = max(0,arr(FLAG_TRTARDIF * TX1758A/100 *
                            min(min(IRBASE2042_FIC,IRBASE),
                                max(0, IRBASE)
                               )
                            )
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              ); 
MAJOIR_TARDIF = somme(x = 07,08: MAJOIR0x_TARDIF) 
		+ MAJOIR17_1TARDIF + MAJOIR17_2TARDIF;
MAJOCS07_TARDIF = max(0,arr(FLAG_TRTARDIF * CSBASE_MAJO * TAUX_2042/100)
			* STR_TR16 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOCS08_TARDIF = max(0,arr(FLAG_TRTARDIF * CSBASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOCS17_TARDIF = max(0,arr(FLAG_TRTARDIF * CSBASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOCS_TARDIF = somme(x = 07,08,17 : MAJOCSx_TARDIF);
MAJOPS07_TARDIF = max(0,arr(FLAG_TRTARDIF * PSBASE_MAJO * TAUX_2042/100)
			* STR_TR16 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOPS08_TARDIF = max(0,arr(FLAG_TRTARDIF * PSBASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOPS17_TARDIF = max(0,arr(FLAG_TRTARDIF * PSBASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOPS_TARDIF = somme(x = 07,08,17 : MAJOPSx_TARDIF);
MAJORD07_TARDIF = max(0,arr(FLAG_TRTARDIF * RDBASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJORD08_TARDIF = max(0,arr(FLAG_TRTARDIF * RDBASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJORD17_TARDIF = max(0,arr(FLAG_TRTARDIF * RDBASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJORD_TARDIF = somme(x = 07,08,17 : MAJORDx_TARDIF);
MAJOCSAL07_TARDIF = max(0,arr(FLAG_TRTARDIF * CSALBASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOCSAL08_TARDIF = max(0,arr(FLAG_TRTARDIF * CSALBASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOCSAL17_TARDIF = max(0,arr(FLAG_TRTARDIF * CSALBASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOCSAL_TARDIF = somme(x = 07,08,17 : MAJOCSALx_TARDIF);
MAJOGAIN07_TARDIF = max(0,arr(FLAG_TRTARDIF * GAINBASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOGAIN08_TARDIF = max(0,arr(FLAG_TRTARDIF * GAINBASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOGAIN17_TARDIF = max(0,arr(FLAG_TRTARDIF * GAINBASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOGAIN_TARDIF = somme(x = 07,08,17 : MAJOGAINx_TARDIF);
MAJOCDIS07_TARDIF = max(0,arr(FLAG_TRTARDIF * CDISBASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOCDIS08_TARDIF = max(0,arr(FLAG_TRTARDIF * CDISBASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOCDIS17_TARDIF = max(0,arr(FLAG_TRTARDIF * CDISBASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOCDIS_TARDIF = somme(x = 07,08,17 : MAJOCDISx_TARDIF);
MAJORSE107_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE1BASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE108_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE1BASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE117_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE1BASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE1_TARDIF = somme(x = 07,08,17 : MAJORSE1x_TARDIF);
MAJORSE207_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE2BASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE208_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE2BASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE217_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE2BASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE2_TARDIF = somme(x = 07,08,17 : MAJORSE2x_TARDIF);
MAJORSE307_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE3BASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE308_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE3BASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE317_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE3BASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE3_TARDIF = somme(x = 07,08,17 : MAJORSE3x_TARDIF);
MAJORSE407_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE4BASE_MAJO * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE408_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE4BASE_MAJO * TAUX_2042/100)
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE417_TARDIF = max(0,arr(FLAG_TRTARDIF * RSE4BASE_MAJO * TAUX_2042/100)
			* STR_TR15 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJORSE4_TARDIF = somme(x = 07,08,17 : MAJORSE4x_TARDIF);
MAJOHR07_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                            min(min(HRBASE2042_FIC,HRBASE_MAJO),
			        max(0, IRBASE+TAXABASE_MAJO+CAPBASE_MAJO+HRBASE_MAJO)
                               )
                           )
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );

MAJOHR08_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                            min(min(HRBASE2042_FIC,HRBASE_MAJO),
			        max(0, IRBASE+TAXABASE_MAJO+CAPBASE_MAJO+HRBASE_MAJO)
                               )
                           )
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJOHR17_1TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                            min(min(HRBASE2042_FIC,HRBASE_MAJO),
			        max(0, IRBASE+TAXABASE_MAJO+CAPBASE_MAJO+HRBASE_MAJO)
                               )
                           )

			* STR_TR15
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );
MAJOHR17_2TARDIF = max(0,arr(FLAG_TRTARDIF * TX1758A/100 *
                            min(min(HRBASE2042_FIC,HRBASE_MAJO),
			        max(0, IRBASE+TAXABASE_MAJO+CAPBASE_MAJO+HRBASE_MAJO)
                               )
                           )

			* STR_TR15
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );
MAJOHR_TARDIF = somme(x = 07,08 : MAJOHR0x_TARDIF) 
		+ MAJOHR17_1TARDIF + MAJOHR17_2TARDIF;
MAJOCAP07_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                             min(min(CAPBASE2042_FIC,CAPBASE_MAJO),
			         max(0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO)
                                )
                            )
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	               );

MAJOCAP08_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
			     min(min(CAPBASE2042_FIC,CAPBASE_MAJO),
			         max(0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO)
                                )
                             )
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	               );

MAJOCAP17_1TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                              min(min(CAPBASE2042_FIC,CAPBASE_MAJO),
			          max(0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO)
                                 )
                             )
			* STR_TR15
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	               );

MAJOCAP17_2TARDIF = max(0,arr(FLAG_TRTARDIF * TX1758A/100 *
                              min(min(CAPBASE2042_FIC,CAPBASE_MAJO),
			          max(0, IRBASE + TAXABASE_MAJO + CAPBASE_MAJO)
                                 )
                             )
			* STR_TR15
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	               );
MAJOCAP_TARDIF = somme(x = 07,08 : MAJOCAP0x_TARDIF) 
		+ MAJOCAP17_1TARDIF + MAJOCAP17_2TARDIF;

MAJOTAXA07_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 * 
                             min(min(TAXABASE2042_FIC,TAXABASE_MAJO),
			         max(0, IRBASE + TAXABASE_MAJO)
                                )
                              )
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	                );
MAJOTAXA08_TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
			      min(min(TAXABASE2042_FIC,TAXABASE_MAJO),
                                  max(0, IRBASE + TAXABASE_MAJO)
				 )	   
                              ) 
			* STR_TR11 
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	);
MAJOTA17_1TARDIF = max(0,arr(FLAG_TRTARDIF * TAUX_2042/100 *
                             min(min(TAXABASE2042_FIC,TAXABASE_MAJO),
			         max(0, IRBASE + TAXABASE_MAJO)
                                )
                            ) 
			* STR_TR15
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	               );
MAJOTA17_2TARDIF = max(0,arr(FLAG_TRTARDIF * TX1758A/100 *
                             min(min(TAXABASE2042_FIC,TAXABASE_MAJO),
                                  max(0, IRBASE + TAXABASE_MAJO)
                                )
                            )
			* STR_TR15
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
	              );
MAJOTAXA_TARDIF = somme(x = 07,08 : MAJOTAXA0x_TARDIF) 
		+ MAJOTA17_1TARDIF + MAJOTA17_2TARDIF;
IRNIN_TARDIF = IRBASE * FLAG_TRTARDIF ;
CSG_TARDIF = CSBASE_MAJO * FLAG_TRTARDIF ;
RDS_TARDIF = RDBASE_MAJO* FLAG_TRTARDIF;
PRS_TARDIF = PSBASE_MAJO * FLAG_TRTARDIF;
CSAL_TARDIF = CSALBASE_MAJO * FLAG_TRTARDIF;
CDIS_TARDIF = CDISBASE_MAJO * FLAG_TRTARDIF;
GAIN_TARDIF = GAINBASE_MAJO * FLAG_TRTARDIF;
RSE1_TARDIF = RSE1BASE_MAJO * FLAG_TRTARDIF;
RSE2_TARDIF = RSE2BASE_MAJO * FLAG_TRTARDIF;
RSE3_TARDIF = RSE3BASE_MAJO * FLAG_TRTARDIF;
RSE4_TARDIF = RSE4BASE_MAJO * FLAG_TRTARDIF;
TAXA_TARDIF = TAXABASE_MAJO * FLAG_TRTARDIF;
HR_TARDIF = HRBASE_MAJO * FLAG_TRTARDIF;
CAP_TARDIF = CAPBASE_MAJO * FLAG_TRTARDIF;
regle isf 234:
application : iliad;
MAJOISF07_TARDIF =  max(0,arr(FLAG_TRTARDIF * ISF4BASE * TAUX_2042/100)
			* STR_TR16
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
                     );
MAJOISF08_TARDIF = max(0,arr(FLAG_TRTARDIF * ISF4BASE * TAUX_2042/100)
			* STR_TR11
		        * (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
		     );
MAJOISF17_TARDIF = max(0,arr(FLAG_TRTARDIF * ISF4BASE * TAUX_2042/100)
			* STR_TR15
			* (1 - null((1 -IND_RJLJ) + (10 - TAUX_2042)))
		     );
MAJOISF_TARDIF = somme(x = 07,08,17 : MAJOISFx_TARDIF);
ISF_TARDIF = ISF4BASE * FLAG_TRTARDIF ;
regle corrective 231141:
application : iliad;
FLAG_TRTARDIF_R = FLAG_RETARD * FLAG_RECTIF * FLAG_1STRATE 
		 * (null(CSTRATE99 - 7) + null(CSTRATE99 - 8) + null(CSTRATE99 - 17) );
MAJOIR07TARDIF_R = FLAG_RETARD * FLAG_RECTIF * null(CSTRATE99-7) * TMAJOIR[00];
MAJOIR08TARDIF_R = FLAG_RETARD * FLAG_RECTIF * null(CSTRATE99-8) * TMAJOIR[00];
MAJOIR17_1TARDIF_R = FLAG_RETARD * FLAG_RECTIF * null(CSTRATE99-17) * TMAJOIR[00];
MAJOIR17_2TARDIF_R = FLAG_RETARD * FLAG_RECTIF * null(CSTRATE99-17) * TMAJOIR[00];
MAJOIRTARDIF_R = somme(x = 07,08: MAJOIR0xTARDIF_R) 
		+ MAJOIR17_1TARDIF_R + MAJOIR17_2TARDIF_R;
MAJOCS07TARDIF_R = FLAG_RETARD * FLAG_RECTIF * null(CSTRATE99-7) * TMAJOCS[00];
MAJOCS08TARDIF_R = FLAG_RETARD * FLAG_RECTIF * null(CSTRATE99-8) 