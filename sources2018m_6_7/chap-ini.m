#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2019]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2019 
#au titre des revenus perçus en 2018. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************

# =================================================================================
# Chapitre ini : Préparation de zones pour le calcul
# =================================================================================

regle irisf 1:
application : bareme , iliad ;


BIDON = 1 ;

regle 951040:
application : iliad ;

SOMMEA718 = (

  present( BAFORESTV ) + present( BAFPVV ) + present( BAF1AV ) 
 + present( BAFORESTC ) + present( BAFPVC ) + present( BAF1AC ) 
 + present( BAFORESTP ) + present( BAFPVP ) + present( BAF1AP ) 
 + present( BACREV ) + present( 4BACREV ) + present( BA1AV ) + present( BACDEV ) 
 + present( BACREC ) + present( 4BACREC ) + present( BA1AC ) + present( BACDEC ) 
 + present( BACREP ) + present( 4BACREP ) + present( BA1AP ) + present( BACDEP ) 
 + present( BAHREV ) + present( 4BAHREV ) + present( BAHDEV ) 
 + present( BAHREC ) + present( 4BAHREC ) + present( BAHDEC ) 
 + present( BAHREP ) + present( 4BAHREP ) + present( BAHDEP ) 
 + present(COD5XT) + present(COD5XV) + present(COD5XU) + present(COD5XW)
 + present(COD5XB) + present(COD5XN) + present(COD5YB) + present(COD5YN)
 + present(COD5ZB) + present(COD5ZN)

 + present( MIBVENV ) + present( MIBPRESV ) + present( MIBPVV ) + present( MIB1AV ) + present( MIBDEV ) + present( BICPMVCTV )
 + present( MIBVENC ) + present( MIBPRESC ) + present( MIBPVC ) + present( MIB1AC ) + present( MIBDEC ) + present( BICPMVCTC )
 + present( MIBVENP ) + present( MIBPRESP ) + present( MIBPVP ) + present( MIB1AP ) + present( MIBDEP ) + present( BICPMVCTP )
 + present( BICNOV ) + present( BI1AV ) + present( BICDNV ) 
 + present( BICNOC ) + present( BI1AC ) + present( BICDNC ) 
 + present( BICNOP ) + present( BI1AP ) + present( BICDNP ) 
 + present( BIHNOV ) + present( BIHDNV ) 
 + present( BIHNOC ) + present( BIHDNC ) 
 + present( BIHNOP ) + present( BIHDNP )

 + present( MIBMEUV ) + present( MIBGITEV ) + present( LOCGITV ) + present( MIBNPVENV ) + present( MIBNPPRESV ) 
 + present( MIBNPPVV ) + present( MIBNP1AV ) + present( MIBNPDEV ) 
 + present( MIBMEUC ) + present( MIBGITEC ) + present( LOCGITC ) + present( MIBNPVENC ) + present( MIBNPPRESC ) 
 + present( MIBNPPVC ) + present( MIBNP1AC ) + present( MIBNPDEC ) 
 + present( MIBMEUP ) + present( MIBGITEP ) + present( LOCGITP ) + present( MIBNPVENP ) + present( MIBNPPRESP ) 
 + present( MIBNPPVP ) + present( MIBNP1AP ) + present( MIBNPDEP ) 
 + present( MIBNPDCT ) 
 + present( BICREV ) + present( LOCNPCGAV ) + present( LOCGITCV ) + present( BI2AV ) + present( BICDEV ) + present( LOCDEFNPCGAV )
 + present( BICREC ) + present( LOCNPCGAC ) + present( LOCGITCC ) + present( BI2AC ) + present( BICDEC ) + present( LOCDEFNPCGAC )
 + present( BICREP ) + present( LOCNPCGAPAC ) + present( LOCGITCP ) + present( BI2AP ) + present( BICDEP ) + present( LOCDEFNPCGAPAC )
 + present( BICHREV ) + present( LOCNPV ) + present( LOCGITHCV ) + present( BICHDEV ) + present( LOCDEFNPV )
 + present( BICHREC ) + present( LOCNPC ) + present( LOCGITHCC ) + present( BICHDEC ) + present( LOCDEFNPC )
 + present( BICHREP ) + present( LOCNPPAC ) + present( LOCGITHCP ) + present( BICHDEP ) + present( LOCDEFNPPAC )
 + present ( COD5NW ) + present ( COD5OW ) + present ( COD5PW )

 + present( BNCPROV ) + present( BNCPROPVV ) + present( BNCPRO1AV ) + present( BNCPRODEV ) + present( BNCPMVCTV )
 + present( BNCPROC ) + present( BNCPROPVC ) + present( BNCPRO1AC ) + present( BNCPRODEC ) + present( BNCPMVCTC )
 + present( BNCPROP ) + present( BNCPROPVP ) + present( BNCPRO1AP ) + present( BNCPRODEP ) + present( BNCPMVCTP )
 + present( BNCREV ) + present( BN1AV ) + present( BNCDEV ) 
 + present( BNCREC ) + present( BN1AC ) + present( BNCDEC ) 
 + present( BNCREP ) + present( BN1AP ) + present( BNCDEP ) 
 + present( BNHREV ) + present( BNHDEV ) 
 + present( BNHREC ) + present( BNHDEC ) 
 + present( BNHREP ) + present( BNHDEP ) 

 + present( BNCNPV ) + present( BNCNPPVV ) + present( BNCNP1AV ) + present( BNCNPDEV ) 
 + present( BNCNPC ) + present( BNCNPPVC ) + present( BNCNP1AC ) + present( BNCNPDEC ) 
 + present( BNCNPP ) + present( BNCNPPVP ) + present( BNCNP1AP ) + present( BNCNPDEP ) 
 + present ( BNCAABV ) + present( ANOCEP ) + present( PVINVE ) 
 + present( INVENTV ) + present ( BNCAADV ) + present( DNOCEP ) 
 + present ( BNCAABC ) + present( ANOVEP ) + present( PVINCE ) 
 + present( INVENTC ) + present ( BNCAADC ) + present( DNOCEPC )
 + present ( BNCAABP ) + present( ANOPEP ) + present( PVINPE ) 
 + present ( INVENTP ) + present ( BNCAADP ) + present( DNOCEPP )
 + present(COD5RZ) + present (COD5SZ)+ 0
            ) ;

regle 951050:
application : iliad ;

SOMMEA719 = (

   present( BAEXV ) + present ( BACREV ) + present( 4BACREV ) + present ( BA1AV ) + present ( BACDEV ) 
 + present( BAEXC ) + present ( BACREC ) + present( 4BACREC ) + present ( BA1AC ) + present ( BACDEC ) 
 + present( BAEXP ) + present ( BACREP ) + present( 4BACREP ) + present ( BA1AP ) + present ( BACDEP ) 
 + present( BAHEXV ) + present ( BAHREV ) + present( 4BAHREV ) + present ( BAHDEV ) 
 + present( BAHEXC ) + present ( BAHREC ) + present( 4BAHREC ) + present ( BAHDEC ) 
 + present( BAHEXP ) + present ( BAHREP ) + present( 4BAHREP ) + present ( BAHDEP ) 
 + present(COD5XT) + present(COD5XV) + present(COD5XU) + present(COD5XW)

 + present( BICEXV ) + present ( BICNOV ) 
 + present ( BI1AV ) + present ( BICDNV ) 
 + present( BICEXC ) + present ( BICNOC ) 
 + present ( BI1AC ) + present ( BICDNC ) 
 + present( BICEXP ) + present ( BICNOP ) 
 + present ( BI1AP ) + present ( BICDNP ) 
 + present( BIHEXV ) + present ( BIHNOV ) 
 + present ( BIHDNV ) 
 + present( BIHEXC ) + present ( BIHNOC ) 
 + present ( BIHDNC ) 
 + present( BIHEXP ) + present ( BIHNOP ) 
 + present ( BIHDNP )

 + present( BICNPEXV ) + present ( BICREV ) + present( LOCNPCGAV )
 + present ( BI2AV ) + present ( BICDEV ) + present( LOCDEFNPCGAV )
 + present( BICNPEXC ) + present ( BICREC ) + present( LOCNPCGAC )
 + present ( BI2AC ) + present ( BICDEC ) + present( LOCDEFNPCGAC )
 + present( BICNPEXP ) + present ( BICREP ) + present( LOCNPCGAPAC )
 + present ( BI2AP ) + present ( BICDEP ) + present( LOCDEFNPCGAPAC )
 + present( BICNPHEXV ) + present ( BICHREV ) + present ( LOCNPV )
 + present ( BICHDEV ) + present ( LOCDEFNPV )
 + present( BICNPHEXC ) + present ( BICHREC ) + present ( LOCNPC )
 + present ( BICHDEC ) + present ( LOCDEFNPC )
 + present( BICNPHEXP ) + present ( BICHREP ) + present ( LOCNPPAC )
 + present ( BICHDEP ) + present ( LOCDEFNPPAC )
 + present ( COD5NW ) + present ( COD5OW ) + present ( COD5PW )

 + present( BNCEXV ) + present ( BNCREV ) + present ( BN1AV ) + present ( BNCDEV ) 
 + present( BNCEXC ) + present ( BNCREC ) + present ( BN1AC ) + present ( BNCDEC ) 
 + present( BNCEXP ) + present ( BNCREP ) + present ( BN1AP ) + present ( BNCDEP ) 
 + present( BNHEXV ) + present ( BNHREV ) + present ( BNHDEV ) 
 + present( BNHEXC ) + present ( BNHREC ) + present ( BNHDEC ) 
 + present( BNHEXP ) + present ( BNHREP ) + present ( BNHDEP )

 + present ( BNCNPREXAAV ) + present ( BNCAABV )   + present ( BNCAADV )  + present ( BNCNPREXV ) 
 + present( ANOCEP ) + present( DNOCEP ) + present( PVINVE ) + present( INVENTV )
 + present ( BNCNPREXAAC ) + present ( BNCAABC ) + present ( BNCAADC ) + present ( BNCNPREXC )
 + present( ANOVEP ) + present( DNOCEPC ) + present( PVINCE ) + present( INVENTC )
 + present ( BNCNPREXAAP ) + present ( BNCAABP ) + present ( BNCAADP ) + present ( BNCNPREXP )
 + present( ANOPEP ) + present( DNOCEPP ) + present( PVINPE ) + present( INVENTP )

 + 0
        ) ;

regle 951060:
application : iliad ;

SOMMEA030 =     
                somme(i=1..4: positif(TSHALLOi) + positif(ALLOi)
		+ positif(CARTSPi) + positif(REMPLAPi)
		+ positif(CARTSNBAPi) + positif(REMPLANBPi)
                + positif(PRBRi)
		+ positif(CARPEPi) + positif(CARPENBAPi)
                + positif(PALIi) + positif(FRNi) 
		+ positif(PENSALPi) + positif(PENSALNBPi)
		)
 + positif(BAFORESTP) + positif(BAFPVP)  + positif(BAF1AP)
 + positif(BAEXP) + positif(BACREP) + positif(4BACREP)  
 + positif(BA1AP) + positif(BACDEP)
 + positif(BAHEXP) + positif(BAHREP) + positif(4BAHREP) 
 + positif(BAHDEP) 
 + positif(MIBEXP) + positif(MIBVENP) + positif(MIBPRESP)  + positif(MIBPVP)  + positif(MIB1AP)  + positif(MIBDEP)
 + positif(BICPMVCTP) + positif(BICEXP) + positif(BICNOP) + positif(BI1AP)  
 + positif(BICDNP) 
 + positif(BIHEXP) + positif(BIHNOP) + positif(BIHDNP)  
 + positif(MIBNPEXP)  + positif(MIBNPVENP)  + positif(MIBNPPRESP)  + positif(MIBNPPVP)  + positif(MIBNP1AP)  + positif(MIBNPDEP)
 + positif(BICNPEXP)  + positif(BICREP) + positif(BI2AP)  + positif(BICDEP)  
 + positif(BICNPHEXP) + positif(BICHREP) + positif(BICHDEP) 
 + positif(BNCPROEXP)  + positif(BNCPROP)  + positif(BNCPROPVP)  + positif(BNCPRO1AP)  + positif(BNCPRODEP) + positif(BNCPMVCTP)
 + positif(BNCEXP)  + positif(BNCREP) + positif(BN1AP) 
 + positif(BNCDEP)
 + positif(BNHEXP)  + positif(BNHREP)  + positif(BNHDEP) + positif(BNCCRP)
 + positif(BNCNPP)  + positif(BNCNPPVP)  + positif(BNCNP1AP)  + positif(BNCNPDEP)
 + positif(ANOPEP) + positif(PVINPE) + positif(INVENTP) + positif(DNOCEPP) + positif(BNCCRFP)
 + positif(BNCAABP) + positif(BNCAADP)
 + positif(RCSP) 
 + positif(BAPERPP) 
 + positif(PERPP) + positif(PERP_COTP) + positif(PLAF_PERPP)
 + somme(i=1..4: positif(PEBFi))
 + positif( COTF1 ) + positif( COTF2 ) + positif( COTF3 ) + positif( COTF4 )
 + positif (BNCNPREXAAP) + positif (BNCNPREXP)
 + positif(AUTOBICVP) + positif(AUTOBICPP) 
 + positif(AUTOBNCP) 
 + positif(LOCNPCGAPAC) + positif(LOCGITCP) + positif(LOCGITHCP) 
 + positif(LOCDEFNPCGAPAC)
 + positif(LOCNPPAC) + positif(LOCDEFNPPAC) 
 + positif(XSPENPP)
 + positif(BANOCGAP) + positif(MIBMEUP) + positif(MIBGITEP) + positif(LOCGITP) 
 + positif(SALEXT1) + positif(COD1CE) + positif(COD1CH)
 + positif(SALEXT2) + positif(COD1DE) + positif(COD1DH)
 + positif(SALEXT3) + positif(COD1EE) + positif(COD1EH)
 + positif(SALEXT4) + positif(COD1FE) + positif(COD1FH)
 + positif(RDSYPP)
 + positif(PENIN1) + positif(PENIN2) + positif(PENIN3) + positif(PENIN4)
 + positif(CODRCZ) + positif(CODRDZ) + positif(CODREZ) + positif(CODRFZ)
 + positif(COD1CF) + positif(COD1CG) + positif(COD1CL) + positif(COD1CM)
 + positif(COD1DF) + positif(COD1DG) + positif(COD1DL) + positif(COD1DM)
 + positif(COD1EF) + positif(COD1EG) + positif(COD1EL) + positif(COD1EM)
 + positif(COD1FF) + positif(COD1FG) + positif(COD1FL) + positif(COD1FM)
 + positif(CODRCF) + positif(CODRDF) + positif(CODREF) + positif(CODRFF) + positif(CODRCG) 
 + positif(CODRDG) + positif(CODRGG) + positif(CODRFG) + positif(CODRCL) + positif(CODRDL) 
 + positif(CODREL) + positif(CODRFL) + positif(CODRCM) + positif(CODRDM) + positif(CODREM) 
 + positif(CODRFM) + positif(COD1IB) + positif(COD1JB) 
 + positif(COD1CA) + positif(COD1DA) + positif(COD1EA) + positif(COD1FA) + positif(COD1CX)
 + positif(COD1DX) + positif(COD1EX) + positif(COD1FX) + positif(COD1IE) + positif(COD1JE)
 + positif(COD1KE) + positif(COD1LE) + positif(COD1IF) + positif(COD1JF) + positif(COD1KF)
 + positif(COD1LF) + positif(COD1CD) + positif(COD1DD) + positif(COD1ED) + positif(COD1FD)
 + positif(COD1CN) + positif(COD1CV) + positif(COD1CY) + positif(COD1FN) + positif(COD1FV)
 + positif(COD1WA) + positif(COD1WB) + positif(COD1XA) + positif(COD1XB) + positif(COD1IN)
 + positif(COD1IV) + positif(COD1IY) + positif(COD1JN) + positif(COD1JV) + positif(COD1WC)
 + positif(COD1WD) + positif(COD1XC) + positif(COD1XD) 
 + positif( COD5CK ) + positif( COD5CL )
 + positif( COD5FF ) + positif( COD5FG )
 + positif( COD5GY ) + positif( COD5GZ )
 + positif( COD5MD ) + positif( COD5SZ ) + positif( COD5WR ) + positif( COD5WS )
 + positif( COD5ZA ) + positif( COD5ZB ) + positif( COD5ZC ) 
 + positif( COD5ZJ ) + positif( COD5ZK ) + positif( COD5ZN ) + positif( COD5ZO )
 + positif( COD5ZS ) + positif( COD5ZX ) 
 + positif( INVENTP ) 
 + positif( XSPENPP ) +  positif( BICPMVCTP ) 
 + positif( BNCNPREXP ) + positif( COD5AH ) +  positif( COD5BH ) +  positif( COD5CM ) +  positif( COD5CN ) 
 + positif( COD5CQ )
 + positif(COD5CR) + positif(COD5CU) + positif(COD5CV) + positif(COD5CY) + positif(COD5CZ) 
 + positif(COD5ED) + positif(COD5FB) + positif(COD5FD) + positif(COD5FK) + positif(COD5FL) 
 + positif(COD5FM) + positif(COD5FN) + positif(CODCMC) + positif(CODCMI) + positif(CODCOS) 
 + positif(CODCPC) + positif(CODCPI) + positif(CODCSC) + positif(CODCSF) + positif(CODCSI) 
 + positif(COD5PW) + positif(COD5TP) + positif(COD5VQ) + positif(COD5VV) + positif(COD5VW) 
 + positif(COD5VX) + positif(COD5ZH) + positif(COD5ZI) + positif(COD5ZL) + positif(COD5ZM) 
 + positif(COD5ZP) + positif(COD5ZQ) + positif(COD5ZR) + positif(COD5ZW) + positif(COD5ZY) 
 + positif(COD5ZZ) + positif(COD5ZT) + positif(COD5ZU) + positif(COD5NV) + positif(COD5OV)
 + positif(COD5PV) + positif(COD5RU) + positif(COD5TW) + positif(COD5WW) + positif(COD5SM)
 + positif(COD5SS) + positif(COD5ST) + positif(COD5UL) + positif(COD5VL) + positif(COD5WP)
 + positif(COD5TO) + positif(COD5UM) + positif(COD5UN) + positif(COD5UO) + positif(COD5VO)
 + positif(COD5WO) + positif(COD5RV) + positif(COD5UX) + positif(COD5TZ)
 + positif(COD5WZ) + positif(COD5XG) + positif(COD5YG)
 + positif(COD8UM) + positif( CODZRF )
 + positif ( COD1IA ) + positif ( COD1JA )
 + positif ( COD1KA ) + positif ( COD1LA )
 + positif(CODCAC) + positif(CODCAD) + positif(CODCBC) + positif(CODCBD) + positif(CODBAC)
 + positif(CODBIC) + positif(CODBNC) + positif(CODLMC) + positif(CODPSC) 
 + positif(CODCID) + positif(CODCIE) + positif(CODCIF) + positif(CODCIG) 
 + 0 ;

regle 951070:
application : iliad ;

SOMMEA031 = positif(TSHALLOC + ALLOC + PRBRC + PALIC + GSALC + TSASSUC + XETRANC  
                    + EXOCETC + FRNC + PCAPTAXC + CARTSC + PENSALC + REMPLAC + CARPEC  
                    + GLDGRATC + BPCOSAC + BAFORESTC + BAFPVC + BAF1AC + BAEXC 
		    + BACREC + 4BACREC + BA1AC + BACDEC + BAHEXC + BAHREC + 4BAHREC 
                    + BAHDEC + BAPERPC + BANOCGAC + COD5XU + COD5XW + AUTOBICVC 
		    + AUTOBICPC + AUTOBNCC + MIBEXC + MIBVENC + MIBPRESC + MIBPVC  
                    + MIB1AC + MIBDEC + BICPMVCTC + BICEXC + BICNOC + BI1AC + BICDNC  
                    + BIHEXC + BIHNOC + BIHDNC + MIBNPEXC + MIBNPVENC + MIBNPPRESC  
		    + MIBNPPVC + MIBNP1AC + MIBNPDEC + BICNPEXC + BICREC + LOCNPCGAC 
		    + BI2AC + BICDEC + LOCDEFNPCGAC + MIBMEUC + MIBGITEC + LOCGITC 
		    + LOCGITCC + LOCGITHCC + BICNPHEXC + BICHREC + LOCNPC + BICHDEC 
                    + LOCDEFNPC + BNCPROEXC + BNCPROC + BNCPROPVC + BNCPRO1AC + BNCPRODEC 
		    + BNCPMVCTC + BNCEXC + BNCREC + BN1AC + BNCDEC + BNHEXC + BNHREC 
		    + BNHDEC + BNCCRC + CESSASSC + XSPENPC + BNCNPC + BNCNPPVC + BNCNP1AC 
		    + BNCNPDEC + BNCNPREXAAC + BNCAABC + BNCAADC + BNCNPREXC + ANOVEP
                    + PVINCE + INVENTC + DNOCEPC+ BNCCRFC 
                    + RCSC + PVSOCC   + PEBFC  + PERPC + PERP_COTC + PLAF_PERPC 
                    + PERPPLAFCC + PERPPLAFNUC1 + PERPPLAFNUC2 + PERPPLAFNUC3
                    + CODDBJ + CODEBJ + SALEXTC + COD1BE + COD1BH + RDSYCJ + PENINC + CODRBZ
                    + COD1BF + COD1BL + COD1BM + COD1OX + COD1UP + COD5BD + COD5BI + COD5BK 
                    + COD5BL + COD5BN + COD5EB + COD5EF + COD5EG + COD5EK + COD5EL + COD5EM 
                    + COD5EN + COD5FY + COD5FZ + COD5LD + COD5RZ + COD5VP + COD5VR + COD5VS 
                    + COD5VT + COD5VU + COD5VY + COD5VZ + COD5WM + COD5WN + COD5YA + COD5YB  
                    + COD5YI + COD5YJ + COD5YN + COD5YO + COD5YP + COD5YQ + COD5YR + COD5YS 
                    + COD5YX + COD5YY + COD5YZ + COD5OW + CODCLC + CODCLI + CODCOC + CODCOI 
                    + CODCNS + CODCRC + CODCRF + CODCRI + CARPEC + COD1QM + COD5AI + COD5BO 
                    + COD5BP + COD5BQ + COD5BR + COD5BY + COD5YH + COD5YK + COD5YL + COD8WM 
                    + CODZRA + CODZRB + CODZRE + COD1HA + COD1HB + CODRBF + CODRBG + CODRBL 
                    + CODRBM + COD1BG + COD5DD + COD1BA + COD1HE + COD1BN + COD1BV + COD1BY 
                    + COD1VA + COD1VB + COD1HN + COD1HV + COD1HY + COD1VC + COD1VD + GLDGRATC
                    + COD5WV + COD5TV + COD5SQ + COD5SR + COD5UK + COD5VK + COD5ER + COD5QY 
                    + COD5RY + COD5SY + COD5WU + COD5QU + COD5QV + COD5QW + COD5TU + COD5RT 
                    + COD5BE + COD5WY + COD5TY + COD5YV + COD5ZV + COD5LA + COD5YT + COD5YU
                    + CODCAB + CODCBB + CODCCB + CODCDB + CODCEB + CODCFB + CODBAB + CODBIB
		    + CODBNB + CODLMB + CODPSB + CODCIB + COD8SI
                    + 0 ) ;

regle 951080:
application : iliad ;  

SOMMEA804 = SOMMEANOEXP ;

SOMMEA805 = SOMMEANOEXP + positif(CODDAJ) + positif(CODEAJ) + positif(CODDBJ) + positif(CODEBJ) 
            + positif(CARTSV) + positif(CARTSNBAV) + positif(CARTSC) + positif(CARTSNBAC) ;

regle 951090:
application : iliad ;  

SOMMEA899 = present( BICEXV ) + present( BICNOV ) + present( BI1AV ) + present( BICDNV )
            + present( BICEXC ) + present( BICNOC ) + present( BI1AC ) + present( BICDNC )
	    + present( BICEXP ) + present( BICNOP ) + present( BI1AP ) + present( BICDNP )
	    + present( BIHEXV ) + present( BIHNOV ) + present( BIHDNV )
	    + present( BIHEXC ) + present( BIHNOC ) + present( BIHDNC )
	    + present( BIHEXP ) + present( BIHNOP ) + present( BIHDNP )
	    ;

SOMMEA859 = present( BICEXV ) + present( BICNOV ) + present( BI1AV ) + present( BICDNV )
            + present( BICEXC ) + present( BICNOC ) + present( BI1AC ) + present( BICDNC )
	    + present( BICEXP ) + present( BICNOP ) + present( BI1AP ) + present( BICDNP )
	    + present( BIHEXV ) + present( BIHNOV ) + present( BIHDNV )
	    + present( BIHEXC ) + present( BIHNOC ) + present( BIHDNC )
	    + present( BIHEXP ) + present( BIHNOP ) + present( BIHDNP )
	    ;

SOMMEA860 = present( BICEXV ) + present( BICNOV ) + present( BI1AV ) + present( BICDNV )
            + present( BICEXC ) + present( BICNOC ) + present( BI1AC ) + present( BICDNC )
	    + present( BICEXP ) + present( BICNOP ) + present( BI1AP ) + present( BICDNP )
	    + present( BIHEXV ) + present( BIHNOV ) + present( BIHDNV )
	    + present( BIHEXC ) + present( BIHNOC ) + present( BIHDNC )
	    + present( BIHEXP ) + present( BIHNOP ) + present( BIHDNP )
            + present( BNCEXV ) + present( BNCREV ) + present( BN1AV ) + present( BNCDEV )
	    + present( BNCEXC ) + present( BNCREC ) + present( BN1AC ) + present( BNCDEC )
	    + present( BNCEXP ) + present( BNCREP ) + present( BN1AP ) + present( BNCDEP )
	    + present( BNHEXV ) + present( BNHREV ) + present( BNHDEV )
	    + present( BNHEXC ) + present( BNHREC ) + present( BNHDEC )
	    + present( BNHEXP ) + present( BNHREP ) + present( BNHDEP )
            ;

SOMMEA895 = present(BAEXV) + present(BACREV) + present(4BACREV) + present(BA1AV) + present(BACDEV)
            + present(BAEXC) + present(BACREC) + present(4BACREC) + present(BA1AC) + present(BACDEC)
	    + present(BAEXP) + present(BACREP) + present(4BACREP) + present(BA1AP) + present(BACDEP)
	    + present(BAHEXV) + present(BAHREV) + present(4BAHREV) + present(BAHDEV)
	    + present(BAHEXC) + present(BAHREC) + present(4BAHREC) + present(BAHDEC)
	    + present(BAHEXP) + present(BAHREP) + present(4BAHREP) + present(BAHDEP)
	    + (1 - null(V_FORVA+0)) + present(BAFPVV) + present(BAF1AV)
            + (1 - null(V_FORCA+0)) + present(BAFPVC) + present(BAF1AC)
            + (1 - null(V_FORPA+0)) + present(BAFPVP) + present(BAF1AP) 
            + present(COD5XT) + present(COD5XV) + present(COD5XU) + present(COD5XW)
	    + present( COD5XB ) + present( COD5YB ) + present( COD5ZB )
	    + present( COD5XN ) + present( COD5YN ) + present( COD5ZN )
	    +present(COD5XA) + present(COD5YA) + present(COD5ZA)
	    ;

SOMMEA898 = SOMMEA895 ;

SOMMEA881 =  
	     present(BAEXV) + present(BACREV) + present(4BACREV) + present(BA1AV) + present(BACDEV)
           + present(BAEXC) + present(BACREC) + present(4BACREC) + present(BA1AC) + present(BACDEC)
	   + present(BAEXP) + present(BACREP) + present(4BACREP) + present(BA1AP) + present(BACDEP)
	   + present(BAHEXV) + present(BAHREV) + present(4BAHREV) + present(BAHDEV)
	   + present(BAHEXC) + present(BAHREC) + present(4BAHREC) + present(BAHDEC)
	   + present(BAHEXP) + present(BAHREP) + present(4BAHREP) + present(BAHDEP)
           + present(COD5XT) + present(COD5XV) + present(COD5XU) + present(COD5XW)

	   + present( BICEXV ) + present( BICNOV ) + present( BI1AV ) + present( BICDNV )
	   + present( BICEXC ) + present( BICNOC ) + present( BI1AC )
	   + present( BICDNC ) + present( BICEXP ) + present( BICNOP ) 
	   + present( BI1AP ) + present( BICDNP ) + present( BIHEXV ) + present( BIHNOV )
	   + present( BIHDNV ) + present( BIHEXC )
	   + present( BIHNOC ) + present( BIHDNC ) 
	   + present( BIHEXP ) + present( BIHNOP ) + present( BIHDNP )
	   + present( BICNPEXV ) + present( BICREV ) + present( BI2AV )
	   + present( BICDEV ) + present( BICNPEXC ) + present( BICREC ) 
	   + present( BI2AC ) + present( BICDEC ) + present( BICNPEXP ) + present( BICREP )
	   + present( BI2AP ) + present( BICDEP ) + present( BICNPHEXV )
	   + present( BICHREV ) + present( BICHDEV ) 
	   + present( BICNPHEXC ) + present( BICHREC ) + present( BICHDEC )
	   + present( BICNPHEXP ) + present( BICHREP ) 
	   + present( BICHDEP ) 
	   + present( LOCNPCGAV ) + present( LOCGITCV ) + present( LOCDEFNPCGAV ) 
	   + present( LOCNPCGAC ) + present( LOCGITCC ) + present( LOCDEFNPCGAC ) 
	   + present( LOCNPCGAPAC ) + present( LOCGITCP ) + present( LOCDEFNPCGAPAC )
	   + present( LOCNPV ) + present( LOCGITHCV ) + present( LOCDEFNPV ) 
	   + present( LOCNPC ) + present( LOCGITHCC ) + present( LOCDEFNPC ) 
	   + present( LOCNPPAC ) + present( LOCGITHCP ) + present( LOCDEFNPPAC )
	   + present (COD5NW) + present (COD5OW) + present (COD5PW)
           + present( BAPERPV ) + present( BAPERPC ) + present( BAPERPP)
           + present( BANOCGAV ) + present( BANOCGAC ) + present( BANOCGAP )

	   + present(BNCEXV) + present(BNCREV) + present(BN1AV) + present(BNCDEV) 
	   + present(BNCEXC) + present(BNCREC) + present(BN1AC) + present(BNCDEC)
	   + present(BNCEXP) + present(BNCREP) + present(BN1AP) + present(BNCDEP) 
	   + present(BNHEXV) + present(BNHREV) + present(BNHDEV) 
	   + present(BNHEXC) + present(BNHREC) + present(BNHDEC) 
	   + present(BNHEXP) + present(BNHREP) + present(BNHDEP) 

	   + present(BNCAABV) + present(ANOCEP) + present(INVENTV) 
	   + present(PVINVE) + present(BNCAADV) + present(DNOCEP) 
	   + present(BNCAABC) + present(ANOVEP) + present(INVENTC) 
	   + present(PVINCE) + present(BNCAADC) + present(DNOCEPC)
	   + present(BNCAABP) + present(ANOPEP) + present(INVENTP)
	   + present(PVINPE) + present(BNCAADP) + present(DNOCEPP)
           + present(BNCNPREXAAV) + present(BNCNPREXAAC) + present(BNCNPREXAAP)
           + present(BNCNPREXV) + present(BNCNPREXC) + present(BNCNPREXP)
	   ;

SOMMEA858 = SOMMEA881 + present(TSHALLOV) + present(TSHALLOC) + present(TSASSUV) + present(TSASSUC)
                      + present(RFMIC) + present(RFORDI) + present(RFDORD) + present(RFDHIS)+ present (COD1GF)
		      + present (COD1HF) ;


SOMMEA890 = SOMMEA881  + present( TSHALLOV ) + present( TSHALLOC ) 
		       + present( CARTSV ) + present( CARTSC )
		       + present( CARTSNBAV ) + present( CARTSNBAC ) ;

SOMMEA891 = SOMMEA881 ;

SOMMEA893 = SOMMEA881  + present(TSHALLOV) + present(TSHALLOC) ;

SOMMEA894 = SOMMEA881 ;

SOMMEA896 = SOMMEA881 ;

SOMMEA885 =  present( BA1AV ) + present( BA1AC ) + present( BA1AP )
           + present( BI1AV ) + present( BI1AC ) + present( BI1AP ) 
	   + present( BN1AV ) + present( BN1AC ) + present( BN1AP ) ;

SOMMEA880 =  present ( BICEXV ) + present ( BICNOV ) + present ( BI1AV )
           + present ( BICDNV ) + present ( BICEXC ) + present ( BICNOC )
	   + present ( BI1AC ) + present ( BICDNC ) 
	   + present ( BICEXP ) + present ( BICNOP ) + present ( BI1AP )
	   + present ( BICDNP ) + present ( BIHEXV ) + present ( BIHNOV )
           + present ( BIHDNV ) + present ( BIHEXC )
	   + present ( BIHNOC ) + present ( BIHDNC ) 
	   + present ( BIHEXP ) + present ( BIHNOP ) + present ( BIHDNP )
	   + present( BAEXV ) +  present( BAHEXV ) +  present( BAEXC ) +  present( BAHEXC ) +  present( BAEXP ) 
	   + present( BAHEXP ) +  present( BACREV ) +  present( BAHREV ) +  present( BACREC ) +  present( BAHREC ) 
	   + present( BACREP ) +  present( BAHREP ) +  present( BACDEV ) +  present( BAHDEV ) +  present( BACDEC ) 
	   + present( BAHDEC ) +  present( BACDEP ) +  present( BAHDEP ) +  present( COD5XT ) +  present( COD5XV ) 
	   + present( COD5XU ) +  present( COD5XW ) +  present( BA1AV ) +  present( BA1AC ) +  present( BA1AP ) 
	   + present( BNCEXV ) +  present( BNHEXV ) +  present( BNCEXC ) +  present( BNHEXC ) +  present( BNCEXP ) 
	   + present( BNHEXP ) +  present( BNCREV ) +  present( BNHREV ) +  present( BNCREC ) +  present( BNHREC ) 
	   + present( BNCREP ) +  present( BNHREP ) +  present( BNCDEV ) +  present( BNHDEV ) +  present( BNCDEC ) 
	   + present( BNHDEC ) +  present( BNCDEP ) +  present( BNHDEP ) +  present( BN1AV ) +  present( BN1AC ) 
	   + present( BN1AP )
	   ;

SOMMEA874 = present( CARTSV ) +  present( REMPLAV ) +  present( CARTSC ) +  present( REMPLAC ) +  present( CARTSP1 ) +
		present( REMPLAP1 ) +  present( CARTSP2 ) +  present( REMPLAP2 ) +  present( CARTSP3 ) +  present( REMPLAP3 ) +
		present( CARTSP4 ) +  present( REMPLAP4 ) +  present( COD1AF ) +  present( COD1BF ) +  present( COD1CF ) +
		present( COD1DF ) +  present( COD1EF ) +  present( COD1FF ) + present ( COD1TZ ) + present ( COD1TP ) +  
	       	present ( COD1NX ) + present ( COD1PM ) + present ( COD1UP ) + present ( COD1OX ) + present (COD1QM) +
	        present( CARPEV ) +  present( CODRAZ ) +
		present( CARPEC ) +  present( CODRBZ ) +  present( CARPEP1 ) +  present( CODRCZ ) +  present( CARPEP2 ) +
		present( CODRDZ ) +  present( CARPEP3 ) +  present( CODREZ ) +  present( CARPEP4 ) +  present( CODRFZ ) +
		present( PENSALV ) +  present( PENSALC ) +  present( PENSALP1 ) +  present( PENSALP2 ) +  present( PENSALP3 ) +
		present( PENSALP4 ) +  present( COD1AL ) +  present( COD1BL ) +  present( COD1CL ) +  present( COD1DL ) +
		present( COD1EL ) +  present( COD1FL ) +  present( RENTAX ) +  present( RENTAX5 ) +  present( RENTAX6 ) +
		present( RENTAX7 ) +  present( COD1AR ) +  present( COD1BR ) +  present( COD1CR ) +  present( COD1DR ) +
		present (CODRAR) + present (CODRBR) + present (CODRCR) + present (CODRDR) + 
		present( RVB1 ) + present( RVB2 ) + present( RVB3 ) + present( RVB4) + 
                present( COD5XJ ) +  present( COD5XK ) +  present( COD5YJ ) +
		present( COD5YK ) +  present( COD5ZJ ) +  present( COD5ZK ) +  present( COD5XS ) +  present( COD5XX ) +
		present( COD5YS ) +  present( COD5YX ) +  present( COD5ZS ) +  present( COD5ZX )
	+       present(BPCOSAV) +  present(BPCOSAC) +  present(BPVSJ) + present(BPVSK) 
        +       present(BPV18V) + present(BPV40V) + present(BPCOPTV)	
	+ present (GLDGRATV)+ present (GLDGRATC) + present(COD3TJ)
	;

SOMMEA877 =  present(BAEXV) + present(BACREV) + present(4BACREV) 
	   + present(BA1AV) + present(BACDEV) + present(BAEXC) 
	   + present(BACREC) + present(4BACREC)
	   + present(BA1AC) + present(BACDEC) + present(BAEXP) + present(BACREP) 
	   + present(4BACREP) + present(BA1AP) 
	   + present(BACDEP) + present(BAHEXV) + present(BAHREV)  
	   + present(4BAHREV) + present(BAHDEV) + present(BAHEXC) 
	   + present(BAHREC) + present(4BAHREC)
	   + present(BAHDEC) + present(BAHEXP) + present(BAHREP)  
	   + present(4BAHREP) + present(BAHDEP) + present(BICEXV) 
           + present(COD5XT) + present(COD5XV) + present(COD5XU) + present(COD5XW)

	   + present(BICNOV) + present(BI1AV) + present(BICDNV) 
           + present(BICEXC) + present(BICNOC)  
	   + present(BI1AC) + present(BICDNC) + present(BICEXP) 
           + present(BICNOP) + present(BI1AP) + present(BICDNP) 
           + present(BIHEXV) + present(BIHNOV) + present(BIHDNV) 
           + present(BIHEXC) + present(BIHNOC) + present(BIHDNC) 
           + present(BIHEXP) + present(BIHNOP) + present(BIHDNP) ;

SOMMEA879 =  
	     present(BACREV) + present(4BACREV) + present(BA1AV) + present(BACDEV) 
	   + present(BACREC) + present(4BACREC) + present(BA1AC) + present(BACDEC) 
           + present(BACREP) + present(4BACREP) + present(BA1AP) + present(BACDEP) 
	   + present(BAHREV) + present(4BAHREV) + present(BAHDEV) 
	   + present(BAHREC) + present(4BAHREC) + present(BAHDEC) 
           + present(BAHREP) + present(4BAHREP) + present(BAHDEP) 
           + present(COD5XT) + present(COD5XV) + present(COD5XU) + present(COD5XW)
	   
	   + present( BICNOV ) + present( BI1AV ) 
	   + present( BICDNV ) + present( BICNOC )  
	   + present( BI1AC ) + present( BICDNC ) + present( BICNOP ) 
	   + present( BI1AP ) + present( BICDNP )  
	   + present( BIHNOV ) + present( BIHDNV )  
	   + present( BIHNOC ) + present( BIHDNC )  
	   + present( BIHNOP ) + present( BIHDNP )  
	   + present( BICREV ) + present( BI2AV ) + present( BICDEV ) 
	   + present( BICREC ) + present( BI2AC ) 
	   + present( BICDEC ) + present( BICREP )  
	   + present( BI2AP ) + present( BICDEP ) + present( BICHREV ) 
	   + present( BICHDEV ) + present( BICHREC ) 
	   + present( BICHDEC ) + present( BICHREP ) 
	   + present( BICHDEP ) 
	   + present( LOCNPCGAV ) + present( LOCGITCV ) + present( LOCDEFNPCGAV ) 
	   + present( LOCNPCGAC ) + present( LOCGITCC ) + present( LOCDEFNPCGAC ) 
	   + present( LOCNPCGAPAC ) + present( LOCGITCP ) + present( LOCDEFNPCGAPAC )
	   + present( LOCNPV ) + present( LOCGITHCV ) + present( LOCDEFNPV ) 
	   + present( LOCNPC ) + present( LOCGITHCC ) + present( LOCDEFNPC ) 
	   + present( LOCNPPAC ) + present( LOCGITHCP ) + present( LOCDEFNPPAC )
	   + present (COD5NW) + present (COD5OW) + present (COD5PW)
	   + present(BNCREV) + present(BN1AV) + present(BNCDEV) 
	   + present(BNCREC) + present(BN1AC) + present(BNCDEC) 
	   + present(BNCREP) + present(BN1AP) + present(BNCDEP) 
	   + present(BNHREV) + present(BNHDEV) 
	   + present(BNHREC) + present(BNHDEC) 
	   + present(BNHREP) + present(BNHDEP) 
	   + present(BNCAABV) + present(ANOCEP) + present(INVENTV) 
	   + present(PVINVE) + present(BNCAADV) + present(DNOCEP) 
	   + present(BNCAABC) + present(ANOVEP) + present(INVENTC) 
	   + present(PVINCE) + present(BNCAADC) + present(DNOCEPC)
	   + present(BNCAABP) + present(ANOPEP) + present(INVENTP)
	   + present(PVINPE) + present(BNCAADP) + present(DNOCEPP)
	   ; 

SOMMEA884 = present(PCAPTAXV) + present(PCAPTAXC)
           + present(CARTSV) + present(CARTSC) + present(CARTSP1)
           + present(CARTSP2) + present(CARTSP3) + present(CARTSP4)
           + present(CARTSNBAV) + present(CARTSNBAC) + present(CARTSNBAP1)
           + present(CARTSNBAP2) + present(CARTSNBAP3) + present(CARTSNBAP4)
           + present(REMPLAV) + present(REMPLAC) + present(REMPLAP1)
           + present(REMPLAP2) + present(REMPLAP3) + present(REMPLAP4)
           + present(REMPLANBV) + present(REMPLANBC) + present(REMPLANBP1)
           + present(REMPLANBP2) + present(REMPLANBP3) + present(REMPLANBP4)
           + present(CARPEV) + present(CARPEC) + present(CARPEP1)
           + present(CARPEP2) + present(CARPEP3) + present(CARPEP4)
           + present(CARPENBAV) + present(CARPENBAC) + present(CARPENBAP1)
           + present(CARPENBAP2) + present(CARPENBAP3) + present(CARPENBAP4)
           + present(PENSALV) + present(PENSALC) + present(PENSALP1)
           + present(PENSALP2) + present(PENSALP3) + present(PENSALP4)
           + present(PENSALNBV) + present(PENSALNBC) + present(PENSALNBP1)
           + present(PENSALNBP2) + present(PENSALNBP3) + present(PENSALNBP4)
	   + present(REVACT) + present(DISQUO) + present(REVACTNB) + present(DISQUONB) 
	   + present(RCMHAD)  + present(RCMABD) + present (RCMHAB)  
           + present(CODRAZ) + present(CODRBZ) + present(CODRCZ) + present(CODRDZ) + present(CODREZ) + present(CODRFZ)
	   + present( COD1AF )+ present( COD1BF ) + present( COD1CF )
      	   + present( COD1DF ) + present( COD1EF ) + present( COD1FF )
      	   + present( COD1AL ) + present( COD1BL ) + present( COD1CL )
      	   + present( COD1DL ) + present( COD1EL ) + present( COD1FL )
	   + present (CODRAL) + present (CODRBL) + present (CODRCL) 
	   + present (CODRDL) + present (CODREL) + present (CODRFL)
	   +present (CODRAF) + present (CODRBF) + present (CODRCF) + present (CODRDF)
	   + present (CODREF) + present (CODRFF)
	   + present( COD1AR ) + present( COD1BR ) + present( COD1CR     )
   	   + present( COD1DR ) + present(CODRAR) + present(CODRBR)
           + present (CODRCR) + present (CODRDR)
	   + present(REAMOR) + present (CODRBT)  
	   + present(COD4BK) + present(COD4BL)	
           + present(BPVRCM) + present(PVTAXSB)
           + present(CODRVG)
	   + present(4BACREV) + present(4BAHREV)  
	   + present(4BACREC) + present(4BAHREC)  
	   + present(4BACREP) + present(4BAHREP)  
           + present(COD5XT)  + present(COD5XV) + present(COD5XU) + present(COD5XW)
	   + present( COD5AK ) + present( COD5AL ) + present( COD5BK )
	   + present( COD5BL ) + present( COD5CK ) + present( COD5CL )

	   + present( COD5DF )
	   + present( COD5DG ) + present( COD5EF ) + present( COD5EG ) + present( COD5FF ) + present( COD5FG )
 	   + present( COD5UR )+ present( COD5US ) + present( COD5VR )
	   + present( COD5VS )+ present (COD5WR) + present (COD5WS) 
	   + present( COD5EY ) + present (COD5EZ ) 
	   + present( COD5FY ) + present( COD5FZ )
	   + present( COD5GY ) + present( COD5GZ )
	   + present( COD5XJ ) + present( COD5XK ) + present( COD5YJ ) + present( COD5YK ) + present( COD5ZJ ) + present( COD5ZK )
   	   + present( COD5XS ) + present( COD5XX ) + present( COD5YS ) + present( COD5YX ) + present( COD5ZS ) + present( COD5ZX )
	   ;

SOMMEA538VB =  present( BAFORESTV ) + present( BAFPVV ) + present( BACREV ) + present( 4BACREV ) 
	     + present( BAHREV ) + present( 4BAHREV ) + present( MIBVENV ) + present( MIBPRESV ) 
	     + present( MIBPVV ) + present( BICNOV ) + present( BIHNOV ) 
	     + present( MIBNPVENV ) + present( MIBNPPRESV ) + present( MIBNPPVV ) 
	     + present( BICREV ) + present( BICHREV ) 
             + present( BNCPROV ) + present( BNCPROPVV ) + present( BNCREV ) + present( BNHREV ) 
	     + present( BNCNPV ) + present( BNCNPPVV ) + present( ANOCEP ) + present( BNCAABV ) 
	     + present( INVENTV ) +  present( BACDEV ) +  present( BAHDEV ) + present ( COD5XB);

SOMMEA538CB =  present( BAFORESTC ) + present( BAFPVC ) + present( BACREC ) + present( 4BACREC ) 
	     + present( BAHREC ) + present( 4BAHREC ) + present( MIBVENC ) + present( MIBPRESC )
             + present( MIBPVC ) + present( BICNOC ) + present( BIHNOC )
             + present( MIBNPVENC ) + present( MIBNPPRESC ) + present( MIBNPPVC )
             + present( BICREC ) + present( BICHREC ) 
             + present( BNCPROC ) + present( BNCPROPVC ) + present( BNCREC ) + present( BNHREC )
	     + present( BNCNPC ) + present( BNCNPPVC ) + present( ANOVEP ) + present( BNCAABC ) 
	     + present( INVENTC ) +  present( BACDEC ) +  present( BAHDEC ) + present ( COD5YB);

SOMMEA538PB =  present( BAFORESTP ) + present( BAFPVP ) + present( BACREP ) + present( 4BACREP ) 
	     + present( BAHREP ) + present( 4BAHREP ) + present( MIBVENP ) + present( MIBPRESP )
             + present( MIBPVP ) + present( BICNOP ) + present( BIHNOP )
	     + present( MIBNPVENP ) + present( MIBNPPRESP ) + present( MIBNPPVP )
             + present( BICREP ) + present( BICHREP ) 
	     + present( BNCPROP ) + present( BNCPROPVP ) + present( BNCREP ) + present( BNHREP )
	     + present( BNCNPP ) + present( BNCNPPVP ) + present( ANOPEP ) + present( BNCAABP ) 
	     + present( INVENTP )+  present( BACDEP ) +  present( BAHDEP ) + present ( COD5ZB );

SOMMEA538VP =  present ( BAF1AV ) + present ( BA1AV ) + present ( MIB1AV ) + present ( BI1AV )
             + present ( MIBNP1AV ) + present ( BI2AV ) + present ( BNCPRO1AV ) + present ( BN1AV )
	     + present ( BNCNP1AV ) + present ( PVINVE ) ;


SOMMEA538CP =  present ( BAF1AC ) + present ( BA1AC ) + present ( MIB1AC ) + present ( BI1AC )
             + present ( MIBNP1AC ) + present ( BI2AC ) + present ( BNCPRO1AC ) + present ( BN1AC )
	     + present ( BNCNP1AC ) + present ( PVINCE ) ;

SOMMEA538PP =  present ( BAF1AP ) + present ( BA1AP ) + present ( MIB1AP ) + present ( BI1AP )
             + present ( MIBNP1AP ) + present ( BI2AP ) + present ( BNCPRO1AP ) + present ( BN1AP )
	     + present ( BNCNP1AP ) + present ( PVINPE ) ;

SOMMEA538 = SOMMEA538VB + SOMMEA538CB + SOMMEA538PB + SOMMEA538VP + SOMMEA538CP + SOMMEA538PP ;

SOMMEA090 =  somme(i=V,C,1..4:TSHALLOi + ALLOi + FRNi + PRBRi + PALIi)
            + somme(i=V,C:CARTSi + REMPLAi + PEBFi + CARPEi + PENSALi)
            + somme(i=1..4:CARTSPi + REMPLAPi + PEBFi + CARPEPi + PENSALPi + RVBi)
	    + RENTAX + RENTAX5 + RENTAX6 + RENTAX7
	    + BPCOSAV + BPCOSAC + GLDGRATV + GLDGRATC
	    ; 

SOMMEA862 =  

      present( MIBEXV ) + present( MIBVENV ) + present( MIBPRESV ) 
    + present( MIBPVV ) + present( MIB1AV ) + present( MIBDEV ) + present( BICPMVCTV )
    + present( MIBEXC ) + present( MIBVENC ) + present( MIBPRESC ) 
    + present( MIBPVC ) + present( MIB1AC ) + present( MIBDEC ) + present( BICPMVCTC ) 
    + present( MIBEXP ) + present( MIBVENP ) + present( MIBPRESP ) 
    + present( MIBPVP ) + present( MIB1AP ) + present( MIBDEP ) + present( BICPMVCTP ) 
    + present( BICEXV ) + present( BICNOV ) 
    + present( BI1AV ) + present( BICDNV ) 
    + present( BICEXC ) + present( BICNOC ) 
    + present( BI1AC ) + present( BICDNC ) 
    + present( BICEXP ) + present( BICNOP ) 
    + present( BI1AP ) + present( BICDNP ) 
    + present( BIHEXV ) + present( BIHNOV ) + present( BIHDNV )
    + present( BIHEXC ) + present( BIHNOC ) + present( BIHDNC )
    + present( BIHEXP ) + present( BIHNOP ) + present( BIHDNP )

    + present( MIBMEUV ) + present( MIBGITEV ) + present( LOCGITV ) + present( MIBNPEXV ) + present( MIBNPVENV ) 
    + present( MIBNPPRESV ) + present( MIBNPPVV ) + present( MIBNP1AV ) + present( MIBNPDEV ) 
    + present( MIBMEUC ) + present( MIBGITEC ) + present( LOCGITC ) + present( MIBNPEXC ) + present( MIBNPVENC ) 
    + present( MIBNPPRESC ) + present( MIBNPPVC ) + present( MIBNP1AC ) + present( MIBNPDEC ) 
    + present( MIBMEUP ) + present( MIBGITEP ) + present( LOCGITP ) + present( MIBNPEXP ) + present( MIBNPVENP ) 
    + present( MIBNPPRESP ) + present( MIBNPPVP ) + present( MIBNP1AP ) + present( MIBNPDEP ) 
    + present( MIBNPDCT )
    + present( BICNPEXV ) + present( BICREV ) + present( LOCNPCGAV ) + present( LOCGITCV )
    + present( BI2AV ) + present( BICDEV ) + present( LOCDEFNPCGAV )
    + present( BICNPEXC ) + present( BICREC ) + present( LOCNPCGAC ) + present( LOCGITCC )
    + present( BI2AC ) + present( BICDEC ) + present( LOCDEFNPCGAC )
    + present( BICNPEXP ) + present( BICREP ) + present( LOCNPCGAPAC ) + present( LOCGITCP )
    + present( BI2AP ) + present( BICDEP ) + present( LOCDEFNPCGAPAC )
    + present( BICNPHEXV ) + present( BICHREV ) + present( LOCNPV )
    + present( LOCGITHCV ) + present( BICHDEV ) + present( LOCDEFNPV ) 
    + present( BICNPHEXC ) + present( BICHREC ) + present( LOCNPC ) 
    + present( LOCGITHCC ) + present( BICHDEC ) + present( LOCDEFNPC ) 
    + present( BICNPHEXP ) + present( BICHREP ) + present( LOCNPPAC ) 
    + present( LOCGITHCP ) + present( BICHDEP ) + present( LOCDEFNPPAC )
    + present( COD5RZ ) + present( COD5SZ ) 

    + present( BNCPROEXV ) + present( BNCPROV ) + present( BNCPROPVV ) 
    + present( BNCPRO1AV ) + present( BNCPRODEV ) + present( BNCPMVCTV )
    + present( BNCPROEXC ) + present( BNCPROC ) + present( BNCPROPVC ) 
    + present( BNCPRO1AC ) + present( BNCPRODEC ) + present( BNCPMVCTC )
    + present( BNCPROEXP ) + present( BNCPROP ) + present( BNCPROPVP ) 
    + present( BNCPRO1AP ) + present( BNCPRODEP ) + present( BNCPMVCTP )
    + present( BNCPMVCTV ) 
    + present( BNCEXV ) + present( BNCREV ) + present( BN1AV ) + present( BNCDEV ) 
    + present( BNCEXC ) + present( BNCREC ) + present( BN1AC ) + present( BNCDEC )
    + present( BNCEXP ) + present( BNCREP ) + present( BN1AP ) + present( BNCDEP ) 
    + present( BNHEXV ) + present( BNHREV ) + present( BNHDEV ) 
    + present( BNHEXC ) + present( BNHREC ) + present( BNHDEC ) 
    + present( BNHEXP ) + present( BNHREP ) + present( BNHDEP ) 

    + present( XSPENPV ) + present( BNCNPV ) + present( BNCNPPVV ) + present( BNCNP1AV ) + present( BNCNPDEV )
    + present( XSPENPC ) + present( BNCNPC ) + present( BNCNPPVC ) + present( BNCNP1AC ) + present( BNCNPDEC ) 
    + present( XSPENPP ) + present( BNCNPP ) + present( BNCNPPVP ) + present( BNCNP1AP ) + present( BNCNPDEP ) 
    + present( BNCNPDCT ) 
    + present( BNCNPREXAAV ) + present( BNCAABV ) + present( BNCAADV ) + present( BNCNPREXV )
    + present( ANOCEP ) + present( DNOCEP ) + present( PVINVE ) + present( INVENTV )
    + present( BNCNPREXAAC ) + present( BNCAABC ) + present( BNCAADC ) + present( BNCNPREXC ) 
    + present( ANOVEP ) + present( DNOCEPC ) + present( PVINCE ) + present( INVENTC )
    + present( BNCNPREXAAP ) + present( BNCAABP ) + present( BNCAADP ) + present( BNCNPREXP ) 
    + present( ANOPEP ) + present( DNOCEPP ) + present( PVINPE ) + present( INVENTP )
    + present( COD5LD) + present( COD5MD)   
    
    +present(COD5NW) + present(COD5OW) + present(COD5PW)
    ;

SOMMEDD55 =

 somme(i=V,C,1,2,3,4: present(TSHALLOi) + present(ALLOi) +  present(PRBRi) + present(PALIi) + present(PENINi))
 + present ( CARTSV ) + present ( CARTSC ) + present ( CARTSP1 )
 + present ( CARTSP2 ) + present ( CARTSP3) + present ( CARTSP4 )
 + present ( CARTSNBAV ) + present ( CARTSNBAC ) + present ( CARTSNBAP1 )
 + present ( CARTSNBAP2 ) + present ( CARTSNBAP3) + present ( CARTSNBAP4 )
 + present ( REMPLAV ) + present ( REMPLAC ) + present ( REMPLAP1 )
 + present ( REMPLAP2 ) + present ( REMPLAP3 ) + present ( REMPLAP4 )
 + present ( REMPLANBV ) + present ( REMPLANBC ) + present ( REMPLANBP1 )
 + present ( REMPLANBP2 ) + present ( REMPLANBP3 ) + present ( REMPLANBP4 )
 + present ( CARPEV ) + present ( CARPEC ) + present ( CARPEP1 )
 + present ( CARPEP2 ) + present ( CARPEP3 ) + present ( CARPEP4 )
 + present ( CARPENBAV ) + present ( CARPENBAC ) + present ( CARPENBAP1 )
 + present ( CARPENBAP2 ) + present ( CARPENBAP3 ) + present ( CARPENBAP4 )
 + present ( PENSALV ) + present ( PENSALC ) + present ( PENSALP1 )
 + present ( PENSALP2 ) + present ( PENSALP3 ) + present ( PENSALP4 )
 + present ( PENSALNBV ) + present ( PENSALNBC ) + present ( PENSALNBP1 )
 + present ( PENSALNBP2 ) + present ( PENSALNBP3 ) + present ( PENSALNBP4 )
 + present ( PCAPTAXV ) + present ( PCAPTAXC )
 + present(CODRAZ) + present(CODRBZ) + present(CODRCZ) + present(CODRDZ) + present(CODREZ) + present(CODRFZ)

 + present ( BACREV ) + present ( 4BACREV ) + present ( BA1AV ) + present ( BACDEV )
 + present ( BACREC ) + present ( 4BACREC ) + present ( BA1AC ) + present ( BACDEC )
 + present ( BACREP ) + present ( 4BACREP ) + present ( BA1AP ) + present ( BACDEP )
 + present ( BAHREV ) + present ( 4BAHREV ) + present ( BAHDEV ) 
 + present ( BAHREC ) + present ( 4BAHREC ) + present ( BAHDEC ) 
 + present ( BAHREP ) + present ( 4BAHREP ) + present ( BAHDEP )

 + present ( BICNOV ) + present ( BI1AV )
 + present ( BICDNV ) + present ( BICNOC )
 + present ( BI1AC ) + present ( BICDNC )
 + present ( BICNOP ) 
 + present ( BI1AP ) + present ( BICDNP ) 
 + present ( BIHNOV ) + present ( BIHDNV )
 + present ( BIHNOC ) 
 + present ( BIHDNC ) + present ( BIHNOP )
 + present ( BIHDNP ) 
 + present ( MIBVENV ) + present ( MIBPRESV ) + present ( MIB1AV )
 + present ( MIBDEV ) + present ( MIBVENC ) + present ( MIBPRESC )
 + present ( MIB1AC ) + present ( MIBDEC ) + present ( MIBVENP )
 + present ( MIBPRESP ) + present ( MIB1AP ) + present ( MIBDEP )

 + present(BICREV) + present(LOCNPCGAV) + present ( BI2AV ) + present ( BICDEV ) + present(LOCDEFNPCGAV) 
 + present(BICREC) + present(LOCNPCGAC) + present ( BI2AC ) + present ( BICDEC ) + present(LOCDEFNPCGAC)
 + present(BICREP) + present(LOCNPCGAPAC) + present ( BI2AP ) + present ( BICDEP ) + present(LOCDEFNPCGAPAC)
 + present(BICHREV) + present(LOCNPV) + present(BICHDEV) + present(LOCDEFNPV)
 + present(BICHREC) + present(LOCNPC) + present(BICHDEC) + present(LOCDEFNPC)
 + present(BICHREP) + present(LOCNPPAC) + present(BICHDEP) + present(LOCDEFNPPAC) 
 + present(MIBNPVENV) + present(MIBNPPRESV) + present(MIBNP1AV) + present(MIBNPDEV) 
 + present(MIBNPVENC) + present(MIBNPPRESC) + present(MIBNP1AC) + present(MIBNPDEC) 
 + present(MIBNPVENP) + present(MIBNPPRESP) + present(MIBNP1AP) + present(MIBNPDEP)
 + present(MIBMEUV) + present(MIBGITEV)
 + present(MIBMEUC) + present(MIBGITEC)
 + present(MIBMEUP) + present(MIBGITEP)
 + present(LOCGITCV ) + present(LOCGITCC) + present(LOCGITCP)
 + present(LOCGITHCV) + present(LOCGITHCC) + present(LOCGITHCP)
 + present(LOCGITV) + present(LOCGITC) + present(LOCGITP)

 + present ( BNCREV ) + present ( BN1AV ) + present ( BNCDEV )
 + present ( BNCREC ) + present ( BN1AC ) + present ( BNCDEC )
 + present ( BNCREP ) + present ( BN1AP ) + present ( BNCDEP )
 + present ( BNHREV ) + present ( BNHDEV ) 
 + present ( BNHREC ) + present ( BNHDEC ) 
 + present ( BNHREP ) + present ( BNHDEP )
 + present ( BNCPROV ) + present ( BNCPRO1AV ) + present ( BNCPRODEV )
 + present ( BNCPROC ) + present ( BNCPRO1AC ) + present ( BNCPRODEC )
 + present ( BNCPROP ) + present ( BNCPRO1AP ) + present ( BNCPRODEP )

 + present ( BNCNPV ) + present ( BNCNP1AV ) + present ( BNCNPDEV )
 + present ( BNCNPC ) + present ( BNCNP1AC ) + present ( BNCNPDEC )
 + present ( BNCNPP ) + present ( BNCNP1AP ) + present ( BNCNPDEP )
 + present ( BNCAABV ) + present ( ANOCEP ) + present ( PVINVE ) + present ( BNCAADV ) + present ( DNOCEP )
 + present ( BNCAABC ) + present ( ANOVEP ) + present ( PVINCE ) + present ( BNCAADC ) + present ( DNOCEPC )
 + present ( BNCAABP ) + present ( ANOPEP ) + present ( PVINPE ) + present ( BNCAADP ) + present ( DNOCEPP )
 + present ( INVENTV ) + present ( INVENTC ) + present ( INVENTP )

 ;

SOMMEA802 = present( AUTOBICVV ) + present ( AUTOBICPV ) + present ( AUTOBICVC ) + present ( AUTOBICPC )
	    + present( AUTOBICVP ) + present ( AUTOBICPP ) 
	    + present( AUTOBNCV ) + present ( AUTOBNCC ) + present ( AUTOBNCP ) 
            + present(SALEXTV) + present(SALEXTC) + present(SALEXT1) + present(SALEXT2) + present(SALEXT3) + present(SALEXT4)
            + present(COD1AE) + present(COD1BE) + present(COD1CE) + present(COD1DE) + present(COD1EE) + present(COD1FE)
            + present(COD1AH) + present(COD1BH) + present(COD1CH) + present(COD1DH) + present(COD1EH) + present(COD1FH)
            ;

regle 951100:
application : iliad ;  


SOMMEANOEXP = positif(PEBFV + COTFV + PEBFC + COTFC + PEBF1 + COTF1 + PEBF2 + COTF2 
                      + PEBF3 + COTF3 + PEBF4 + COTF4 + PENSALV + PENSALNBV + PENSALC + PENSALNBC 
                      + PENSALP1 + PENSALNBP1 + PENSALP2 + PENSALNBP2 + PENSALP3 + PENSALNBP3 + PENSALP4 + PENSALNBP4
                      + CARPEV + CARPENBAV + CARPEC + CARPENBAC + CARPEP1 + CARPENBAP1 + CARPEP2 + CARPENBAP2 
                      + CARPEP3 + CARPENBAP3 + CARPEP4 + CARPENBAP4 + CARTSP1 + CARTSNBAP1 + CARTSP2 + CARTSNBAP2 
		      + CARTSP3 + CARTSNBAP3 + CARTSP4 + CARTSNBAP4 + REMPLAV + REMPLANBV + REMPLAC + REMPLANBC 
                      + REMPLAP1 + REMPLANBP1 + REMPLAP2 + REMPLANBP2 + REMPLAP3 + REMPLANBP3 + REMPLAP4 + REMPLANBP4 
                      + RENTAX + RENTAX5 + RENTAX6 + RENTAX7 + RENTAXNB + RENTAXNB5 + RENTAXNB6 + RENTAXNB7
                      + FONCI + FONCINB + REAMOR + REAMORNB + CODRBT + REVACT + REVACTNB + REVPEA + REVPEANB 
                      + PROVIE + PROVIENB + DISQUO + DISQUONB + RESTUC + RESTUCNB + INTERE + INTERENB
                      + 4BACREV + 4BAHREV + 4BACREC + 4BAHREC + 4BACREP + 4BAHREP 
                      + CODRAZ + CODRBZ + CODRCZ + CODRDZ + CODREZ + CODRFZ + CODNAZ + CODNBZ + CODNCZ 
	              + CODNDZ + CODNEZ + CODNFZ + CODRVG + CODNVG + CODRUA + CODNUA + CODRSG + CODRSL 
	              + CODRVA + CODRAF + CODNAF + CODRBF + CODNBF + CODRCF + CODNCF + CODRDF + CODNDF 
	              + CODREF + CODNEF + CODRFF + CODNFF + CODRAG + CODNAG + CODRBG + CODNBG + CODRCG 
	              + CODNCG + CODRDG + CODNDG + CODRGG + CODNGG + CODRFG + CODNFG + CODRAL + CODNAL 
	              + CODRBL + CODNBL + CODRCL + CODNCL + CODRDL + CODNDL + CODREL + CODNEL + CODRFL 
	              + CODNFL + CODRAM + CODNAM + CODRBM + CODNBM + CODRCM + CODNCM + CODRDM + CODNDM 
	              + CODREM + CODNEM + CODRFM + CODNFM + CODRAR + CODNAR + CODRBR + CODNBR + CODRCR 
	              + CODNCR + CODRDR + CODNDR + CODCKC + CODCKI + CODCLC + CODCLI + CODCMC + CODCMI 
	              + CODCNC + CODCNI + CODCOC + CODCOI + CODCPC + CODCPI + CODCNS + CODCOS + CODCQC 
	              + CODCQI + CODCRC + CODCRI + CODCSC + CODCSF + CODCSI + CODCSN
                      + 0) ;

regle 951110:
application : iliad ;  

SOMMEA709 = positif(INVLOCXN) + positif(INVLOCXV) + positif(COD7UY) + positif(COD7UZ) + 0 ;

regle 951130:
application : iliad ;  

SOMMEA700 = 
          (
   present(BAEXV) + present(BACREV) + present(4BACREV) + present(BA1AV) + present(BACDEV) 
 + present(BAEXC) + present(BACREC) + present(4BACREC) + present(BA1AC) + present(BACDEC) 
 + present(BAEXP) + present(BACREP) + present(4BACREP) + present(BA1AP) + present(BACDEP) 
 + present(BAPERPV) + present(BAPERPC) + present(BAPERPP)
 + present(4BAHREV)
 + present(4BAHREC)
 + present(4BAHREP)
 + present(COD5XT) + present(COD5XU) 

 + present(BICEXV) + present(BICNOV) 
 + present(BI1AV) + present(BICDNV) 
 + present(BICEXC) + present(BICNOC) 
 + present(BI1AC) + present(BICDNC) 
 + present(BICEXP) + present(BICNOP) 
 + present(BI1AP) + present(BICDNP) 

 + present(BICNPEXV) + present(BICREV) + present(LOCNPCGAV) + present(LOCGITCV)
 + present(BI2AV) + present(BICDEV) + present(LOCDEFNPCGAV)
 + present(BICNPEXC) + present(BICREC) + present(LOCNPCGAC) + present(LOCGITCC)
 + present(BI2AC) + present(BICDEC) + present(LOCDEFNPCGAC)
 + present(BICNPEXP) + present(BICREP) + present(LOCNPCGAPAC) + present(LOCGITCP)
 + present(BI2AP) + present(BICDEP) + present(LOCDEFNPCGAPAC)

 + present(BNCEXV) + present(BNCREV) + present(BN1AV) + present(BNCDEV)
 + present(BNCEXC) + present(BNCREC) + present(BN1AC) + present(BNCDEC)
 + present(BNCEXP) + present(BNCREP) + present(BN1AP) + present(BNCDEP)

 + present(BNCNPREXAAV) + present(BNCNPREXAAC)
 + present(BNCNPREXAAP) 
 + present(BNCAABV) + present(BNCAADV)  
 + present(PVINVE) + present(INVENTV)
 + present(BNCAABC) + present(BNCAADC)  
 + present(PVINCE) + present(INVENTC)
 + present(BNCAABP) + present(BNCAADP) 
 + present(PVINPE) + present(INVENTP)
          ) ;

regle 951140:
application : iliad ;  

V_CNR  =   (V_REGCO+0) dans (2);
V_CNR2 =   (V_REGCO+0) dans (2,3);
V_EAD  =   (V_REGCO+0) dans (5);
V_EAG  =   (V_REGCO+0) dans (6);
regle 951145:
application : iliad ;  

ANNEEREV = V_ANREV + V_MODUL;
regle 951150:
application : iliad ;  

VARIPTEFN =  (IPTEFN*(1-FLAG_PVRO)+(COD3WG-IPTEFN)*positif(COD3WG-IPTEFN)*positif(IPTEFN)*FLAG_PVRO) * (1-positif(SOMMEMOND_2+PREM8_11));
VARIPTEFP = (IPTEFP + (COD3WG*FLAG_PVRO*positif(IPTEFP))+ max(0,DEFZU*positif(SOMMEMOND_2)*(1-PREM8_11)+DEFZU*PREM8_11 - IPTEFN )) * positif(present(IPTEFP)+present(IPTEFN));

VARDMOND = DMOND* (1-positif(SOMMEMOND_2+PREM8_11));

VARRMOND = (RMOND + max(0,DEFZU*positif(SOMMEMOND_2)*(1-PREM8_11)+DEFZU*PREM8_11 - DMOND)) * positif(present(RMOND)+present(DMOND));

regle 951160:
application : iliad ;  

FLAGRETARD = FLAG_RETARD + 0 ;
FLAGRETARD08 = FLAG_RETARD08 + 0 ;
FLAGDEFAUT = FLAG_DEFAUT + 0 ;
FLAGDEFAUT10 = FLAG_DEFAUT10 + 0 ;
FLAGDEFAUT11 = FLAG_DEFAUT11 + 0 ;

regle 951170:
application : iliad ;  


INDCODDAJ = positif(present(CODDAJ) + present(CODDBJ) + present(CODEAJ) + present(CODEBJ)) ;

regle 951180:
application : iliad ;  


DEFRI = positif(RIDEFRI + DEFRITS + DEFRIBA + DEFRIBIC + DEFRILOC + 
                DEFRIBNC + DEFRIRCM + DEFRIRF + DEFRIGLOB + DEFRIMOND + 0) ;

DEFRIMAJ = positif(DEFRIMAJ_DEF + DEFRI) ;

regle 951190:
application : iliad ;  

SOMMEBAINF = positif(null(SOMMEBA_2+0) + (1-positif(SHBA - SEUIL_IMPDEFBA))) ;
SOMMEBASUP = positif(SOMMEBA_2 * positif(SHBA - SEUIL_IMPDEFBA)) ;
SOMMEGLOB_1 = SOMMEGLOBAL_1 ;
SOMMEGLOB_2 = SOMMEGLOBAL_2 ;
SOMMEBA = SOMMEBA_1 + SOMMEBA_2 ;
SOMMEBIC = SOMMEBIC_1 + SOMMEBIC_2 ;
SOMMELOC = SOMMELOC_1 + SOMMELOC_2 ;
SOMMEBNC = SOMMEBNC_1 + SOMMEBNC_2 ;
SOMMERF = SOMMERF_1 + SOMMERF_2 ;
SOMMERCM = SOMMERCM_1 + SOMMERCM_2 ;

regle 951200:
application : iliad ;

SOMDEFTS =
   FRNV * positif (FRNV - 10MINSV)
 + FRNC * positif (FRNC - 10MINSC)
 + FRN1 * positif (FRN1 - 10MINS1)
 + FRN2 * positif (FRN2 - 10MINS2)
 + FRN3 * positif (FRN3 - 10MINS3)
 + FRN4 * positif (FRN4 - 10MINS4);
SOMDEFBIC =
     BICNOV
   + arr(BIHNOV * MAJREV)
   + BICNOC
   + arr(BIHNOC * MAJREV)
   + BICNOP
   + arr(BIHNOP * MAJREV )
   - BIPN
   +BICPMVCTV +BICPMVCTC +BICPMVCTP;
SOMDEFBNC =
     BNCREV
    + arr(BNHREV * MAJREV)
    + BNCREC
    + arr(BNHREC * MAJREV)
    + BNCREP
    + arr(BNHREP * MAJREV)
    - BNRTOT
+BNCPMVCTV +BNCPMVCTC +BNCPMVCTP;
SOMDEFANT =
   DEFAA5
 + DEFAA4
 + DEFAA3
 + DEFAA2
 + DEFAA1
 + DEFAA0;
SOMDEFICIT =SOMDEFANT+SOMDEFBNC
                          +SOMDEFBANI * (1-positif(SHBA-SEUIL_IMPDEFBA))
                          +SOMDEFTS+SOMDEFBIC+RFDHIS;
SOMDEFICITHTS =SOMDEFANT+SOMDEFBNC
                          +SOMDEFBANI * (1-positif(SHBA-SEUIL_IMPDEFBA))
                          +SOMDEFBIC+RFDHIS;

regle 951210:
application : iliad ;  

DEFRITS = positif(
                positif(max(FRNV,10MINSV)-max(FRDV,10MINSV)) 
              + positif(max(FRNC,10MINSC)-max(FRDC,10MINSC)) 
              + positif(max(FRN1,10MINS1)-max(FRD1,10MINS1)) 
              + positif(max(FRN2,10MINS2)-max(FRD2,10MINS2)) 
              + positif(max(FRN3,10MINS3)-max(FRD3,10MINS3)) 
              + positif(max(FRN4,10MINS4)-max(FRD4,10MINS4))) ;
DEFRIBA =  positif(DEFBANIF)+0;
DEFRIBIC = positif(DEFBICNPF)+0;
DEFRILOC = positif(DEFLOCNPF)+0;
DEFRIBNC = positif(DEFBNCNPF)+0;
DEFRIRCM = positif(DEFRCMIMPU)+0;
DEFRIRF =  positif(DEFRFNONI)+0;
DEFRIGLOB = positif(DFANTIMPU)+0;
DEFRIMOND = positif(positif(TEFFP_2-TEFFP_1)+positif(TEFFN_2*(-1)-TEFFN_1*(-1)) +positif(RMOND_2-RMOND_1)+positif(DMOND_2*(-1)-DMOND_1*(-1)))+0;

regle 951220:
application : iliad ;


ANO1731 = positif(RIDEFRI) *  positif(SOMMERI_1);
