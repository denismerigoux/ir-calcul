#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2019]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2019 
#au titre des revenus perçus en 2018. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************

# =================================================================================
# Chapitre aff : Conditions d'affichage pour l'avis d'imposition
# =================================================================================

regle 901000:
application : iliad ;

CONST0 = 0;
CONST1 = 1;
CONST2 = 2;
CONST3 = 3;
CONST4 = 4;
CONST10 = 10;
CONST20 = 20;
CONST40 = 40;

regle 901010:
application :  iliad ;


LIG0 = (1 - positif(RE168 + TAX1649)) * IND_REV ;

LIG1 = (1 - positif(RE168 + TAX1649)) ;

LIG2 = (1 - positif(ANNUL2042)) ;

LIG12 = LIG1 * LIG2 ;

LIG01 = (1 - (positif(REVFONC) * (1 - INDREV1A8))) * LIG0 * LIG2 ;

LIG3 = positif(positif(CMAJ + 0) 
	+ positif_ou_nul(MAJTX1 - 40) + positif_ou_nul(MAJTX4 - 40)
        + positif_ou_nul(MAJTXPCAP1 - 40) + positif_ou_nul(MAJTXPCAP4 - 40)
        + positif_ou_nul(MAJTXLOY1 - 40) + positif_ou_nul(MAJTXLOY4 - 40)
        + positif_ou_nul(MAJTXCHR1 - 40) + positif_ou_nul(MAJTXCHR4 - 40)
	+ positif_ou_nul(MAJTXC1 - 40) + positif_ou_nul(MAJTXC4 - 40) 
        + positif_ou_nul(MAJTXCVN1 - 40) + positif_ou_nul(MAJTXCVN4 - 40)
	+ positif_ou_nul(MAJTXCDIS1 - 40) + positif_ou_nul(MAJTXCDIS4 - 40)
        + positif_ou_nul(MAJTXGLO1 - 40) + positif_ou_nul(MAJTXGLO4 - 40)
        + positif_ou_nul(MAJTXRSE11 - 40) + positif_ou_nul(MAJTXRSE14 - 40)
        + positif_ou_nul(MAJTXRSE51 - 40) + positif_ou_nul(MAJTXRSE54 - 40)
	+ positif_ou_nul(MAJTXRSE21 - 40) + positif_ou_nul(MAJTXRSE24 - 40)
        + positif_ou_nul(MAJTXRSE31 - 40) + positif_ou_nul(MAJTXRSE34 - 40)
        + positif_ou_nul(MAJTXRSE41 - 40) + positif_ou_nul(MAJTXRSE44 - 40)
        + positif_ou_nul(MAJTXRSE61 - 40) + positif_ou_nul(MAJTXRSE64 - 40)
        + positif_ou_nul(MAJTXRSE71 - 40) + positif_ou_nul(MAJTXRSE74 - 40)
        + positif_ou_nul(MAJTXTAXA4 - 40)) ;

CNRLIG12 = (1 - V_CNR) * LIG12 ;

CNRLIG1 = (1 - V_CNR) * LIG1 ;

regle 901020:
application :  iliad ;


LIG0010 = (INDV * INDC * INDP) * (1 - ART1731BIS) * LIG0 * LIG2 ;

LIG0020 = (INDV * (1 - INDC) * (1 - INDP)) * (1 - ART1731BIS) * LIG0 * LIG2 ;

LIG0030 = (INDC * (1 - INDV) * (1 - INDP)) * (1 - ART1731BIS) * LIG0 * LIG2 ;

LIG0040 = (INDP * (1 - INDV) * (1 - INDC)) * (1 - ART1731BIS) * LIG0 * LIG2 ;

LIG0050 = (INDV * INDC * (1 - INDP)) * (1 - ART1731BIS) * LIG0 * LIG2 ;

LIG0060 = (INDV * INDP * (1 - INDC)) * (1 - ART1731BIS) * LIG0 * LIG2 ;

LIG0070 = (INDC * INDP * (1 - INDV)) * (1 - ART1731BIS) * LIG0 * LIG2 ;

LIG10YT = (INDV * INDC * INDP) * ART1731BIS * LIG0 * LIG2 ;

LIG20YT = (INDV * (1 - INDC) * (1 - INDP)) * ART1731BIS * LIG0 * LIG2 ;

LIG30YT = (INDC * (1 - INDV) * (1 - INDP)) * ART1731BIS * LIG0 * LIG2 ;

LIG40YT = (INDP * (1 - INDV) * (1 - INDC)) * ART1731BIS * LIG0 * LIG2 ;

LIG50YT = (INDV * INDC * (1 - INDP)) * ART1731BIS * LIG0 * LIG2 ;

LIG60YT = (INDV * INDP * (1 - INDC)) * ART1731BIS * LIG0 * LIG2 ;

LIG70YT = (INDC * INDP * (1 - INDV)) * ART1731BIS * LIG0 * LIG2 ;

regle 901030:
application :  iliad ;


LIG10V = positif_ou_nul(TSBNV + PRBV + BPCOSAV + GLDGRATV + positif(F10AV * null(TSBNV + PRBV + BPCOSAV + GLDGRATV))) ;
LIG10C = positif_ou_nul(TSBNC + PRBC + BPCOSAC + GLDGRATC + positif(F10AC * null(TSBNC + PRBC + BPCOSAC + GLDGRATC))) ;
LIG10P = positif_ou_nul(somme(i=1..4:TSBNi + PRBi) + positif(F10AP * null(somme(i=1..4:TSBNi + PRBi)))) ;
LIG10 = positif(LIG10V + LIG10C + LIG10P) ;

regle 901040:
application :  iliad ;

LIG1100 = positif(T2RV) * LIG2 ;

LIG899 = positif(RVTOT + LIG1100 + LIG910 + BRCMQ + (RCMFR + REPRCM) * LIG2OP + LIGRCMABT + LIG2RCMABT + LIGPV3VG + LIGPV3WB 
		  + RCMLIB + LIG29 + LIG30 + RFQ + 2REVF + 3REVF + LIG1130 + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * (1 - positif(LIG0010 + LIG0020 + LIG0030 + LIG0040 + LIG0050 + LIG0060 + LIG0070)) 
		 * (1 - ART1731BIS) * LIG2 ; 

LIG900 = positif(RVTOT + LIG1100 + LIG910 + BRCMQ + (RCMFR + REPRCM) * LIG2OP + LIGRCMABT + LIG2RCMABT + LIGPV3VG + LIGPV3WB 
		  + RCMLIB + LIG29 + LIG30 + RFQ + 2REVF + 3REVF + LIG1130 + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * positif(LIG0010 + LIG0020 + LIG0030 + LIG0040 + LIG0050 + LIG0060 + LIG0070) 
		 * (1 - ART1731BIS) * LIG2 ; 

LIG899YT = positif(RVTOT + LIG1100 + LIG910 + BRCMQ + (RCMFR + REPRCM) * LIG2OP + LIGRCMABT + LIG2RCMABT + LIGPV3VG + LIGPV3WB 
		   + RCMLIB + LIG29 + LIG30 + RFQ + 2REVF + 3REVF + LIG1130 + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * (1 - positif(LIG10YT + LIG20YT + LIG30YT + LIG40YT + LIG50YT + LIG60YT + LIG70YT)) 
		 * ART1731BIS * LIG2 ; 

LIG900YT = positif(RVTOT + LIG1100 + LIG910 + BRCMQ + (RCMFR + REPRCM) * LIG2OP + LIGRCMABT + LIG2RCMABT + LIGPV3VG + LIGPV3WB 
		   + RCMLIB + LIG29 + LIG30 + RFQ + 2REVF + 3REVF + LIG1130 + DFANT + ESFP + RE168 + TAX1649 + R1649 + PREREV)
		 * positif(LIG10YT + LIG20YT + LIG30YT + LIG40YT + LIG50YT + LIG60YT + LIG70YT) 
		 * ART1731BIS * LIG2 ; 

regle 901060:
application : iliad  ;

LIGBAM = positif(COD5XB + COD5YB + COD5ZB) * LIG12 ;

LIGBAMPV = positif(BAFPVV + BAFPVC + BAFPVP) * LIG12 ;

LIGBAMMV = positif(COD5XO + COD5YO + COD5ZO) * LIG12 ;

LIGBAMTOT = positif(LIGBAM + LIGBAMPV + LIGBAMMV) * LIG12 ;

LIGCBOIS = positif(BAFORESTV + BAFORESTC + BAFORESTP) * LIG12 ;

LIG13 =  positif(present(BACDEV)+ present(BACREV) + present(COD5AK) + present(BAHDEV) +present(BAHREV) + present(COD5AL)
                 + present(BACDEC) +present(BACREC) + present(COD5BK) + present(BAHDEC)+ present(BAHREC) + present(COD5BL)
                 + present(BACDEP)+ present(BACREP) + present(COD5CK) + present(BAHDEP)+ present(BAHREP) + present(COD5CL)
                 + present(4BAHREV) + present(4BAHREC) + present(4BAHREP) + present(4BACREV) + present(4BACREC) + present(4BACREP))
	* LIG12 ;

LIGBAMICF1 = (1 - positif(BAFORESTV + BAFORESTC + BAFORESTP + LIG13 + DEFANTBAF + 0)) * LIGBAMTOT ;

LIGBAMICF2 = (1 - LIGBAMICF1) * LIG12 ;

BAFORESTOT = (BAFORESTV + BAFORESTC + BAFORESTP) * (1 - positif(IBAMICF + LIG13 + DEFANTBAF)) ;

regle 901070:
application :  iliad ;

4BAQLV = positif(4BACREV + 4BAHREV) ;
4BAQLC = positif(4BACREC + 4BAHREC) ;
4BAQLP = positif(4BACREP + 4BAHREP) ;

regle 901080:
application : iliad  ;

LIG134V = positif(present(BAHREV) + present(BAHDEV) + present(BACREV) + present(BACDEV) + present(4BAHREV) + present(4BACREV)
                  + present(BAFPVV) + present(COD5AK) + present(COD5AL)
		  + present(COD5XB) + present(COD5XN) + present(COD5XO)) ;

LIG134C = positif(present(BAHREC) + present(BAHDEC) + present(BACREC) + present(BACDEC) + present(4BAHREC) + present(4BACREC)
                  + present(BAFPVC) + present(COD5BK) + present(COD5BL)
		  + present(COD5YB) + present(COD5YN) + present(COD5YO)) ;

LIG134P = positif(present(BAHREP) + present(BAHDEP) + present(BACREP) + present(BACDEP) + present(4BAHREP) + present(4BACREP)
                  + present(BAFPVP) + present(COD5CK) + present(COD5CL)
		  + present(COD5ZB) + present(COD5ZN) + present(COD5ZO)) ;

LIG134 = positif(LIG134V + LIG134C + LIG134P + present(DAGRI6) + present(DAGRI5) + present(DAGRI4) + present(DAGRI3) + present(DAGRI2) + present(DAGRI1)) 
	 * (1 - LIGBAMICF1) * (1 - BAFORESTOT) * LIG12 ;

LIGDBAIP = positif(DEFANTBAF + 0) * LIG12 ;

BAHQTAVIS = (1 - positif_ou_nul(BAHQT)) * positif(SHBA + (REVTP - BA1) + REVQTOTQHT - SEUIL_IMPDEFBA) ;

LIGBAHQ = positif(LIG13 + present(DAGRI6) + present(DAGRI5) + present(DAGRI4) + present(DAGRI3) + present(DAGRI2) + present(DAGRI1) + (LIGCBOIS * LIGBAMTOT)) * LIG12 ;

LIGBAQ = positif(present(4BACREV) + present(4BAHREV) + present(4BACREC) + present(4BAHREC) + present(4BACREP) + present(4BAHREP)) * LIG2 ;

regle 901090:
application : iliad  ;

LIG136 = positif(BAQNODEFV + DEFANTBAQV + BAQNODEFC + DEFANTBAQC + BAQNODEFP + DEFANTBAQP) * LIG12 ;

LIG138 = positif(BATMARGTOT) * LIG12 ;

regle 901100:
application : iliad ;

LIGBICPRO = positif(BICNOV + COD5DF + CODCKC + BICDNV + BIHNOV + COD5DG + CODCKI + BIHDNV
                    + BICNOC + COD5EF + CODCLC + BICDNC + BIHNOC + COD5EG + CODCLI + BIHDNC
		    + BICNOP + COD5FF + CODCMC + BICDNP + BIHNOP + COD5FG + CODCMI + BIHDNP) * LIG0 * LIG2 ;

LIGBICPROQ = positif(CODCKC + CODCKI + CODCLC + CODCLI + CODCMC + CODCMI) * LIG0 * LIG2 ;

LIGMICPV = positif(MIBNPPVV + MIBNPPVC + MIBNPPVP) * LIG0 * LIG2 ;

LIGMICMV = positif(MIBNPDCT + COD5RZ + COD5SZ) * LIG0 * LIG2 ;

LIGBICNP = positif(BICREV + COD5UR + CODCNC + BICDEV + BICHREV + COD5US + CODCNI + BICHDEV
                   + BICREC + COD5VR + CODCOC + BICDEC + BICHREC + COD5VS + CODCOI + BICHDEC
                   + BICREP + COD5WR + CODCPC + BICDEP + BICHREP + COD5WS + CODCPI + BICHDEP) * LIG0 * LIG2 ;

LIGBICNPQ = positif(CODCNC + CODCNI + CODCOC + CODCOI + CODCPC + CODCPI) * LIG0 * LIG2 ;

LIG_DEFNPI = positif(present(DEFBIC6) + present(DEFBIC5) + present(DEFBIC4) 
                     + present(DEFBIC3) + present(DEFBIC2) + present(DEFBIC1)) * LIG0 * LIG2 ;

LIGMLOC = positif(present(MIBMEUV) + present(MIBMEUC) + present(MIBMEUP)
		  + present(MIBGITEV) + present(MIBGITEC) + present(MIBGITEP)
		  + present(LOCGITV) + present(LOCGITC) + present(LOCGITP)
		  + present(COD5NW) + present(COD5OW) + present(COD5PW))
	  * LIG0 * LIG2 ;
 
LIGMLOCAB = positif(MLOCABV + MLOCABC + MLOCABP) * LIG0  * LIG2 ; 

LIGMIBPV = positif(MIBPVV + MIBPVC + MIBPVP) * LIG0 * LIG2 ;

LIGMIBMV = positif(BICPMVCTV + BICPMVCTC + BICPMVCTP) * LIG0 * LIG2 ;

LIGBNCPV = positif(BNCPROPVV + BNCPROPVC + BNCPROPVP) * LIG0 * LIG2 ;

LIGBNCMV = positif(BNCPMVCTV + BNCPMVCTC + BNCPMVCTP) * LIG0 * LIG2 ;

LIGBNCNPPV = positif(BNCNPPVV + BNCNPPVC + BNCNPPVP) * LIG0 * LIG2 ;

LIGBNCNPMV = positif(BNCNPDCT + COD5LD + COD5MD) * LIG0 * LIG2 ;

LIGSPENETPF = positif(BNCPROV + BNCPROC + BNCPROP + BNCPROPVV + BNCPROPVC + BNCPROPVP + BNCPMVCTV + BNCPMVCTC + BNCPMVCTP) * LIG0 * LIG2 ;

LIGBNCPHQ = positif(present(BNHREV) + present(COD5XK) + present(BNCREV) + present(COD5XJ) + present(BNHDEV) + present(BNCDEV)
                    + present(BNHREC) + present(COD5YK) + present(BNCREC) + present(COD5YJ) + present(BNHDEC) + present(BNCDEC)
                    + present(BNHREP) + present(COD5ZK) + present(BNCREP) + present(COD5ZJ) + present(BNHDEP) + present(BNCDEP)) * LIG2 ;

LIGBNCAFFQ = positif(present(CODCQC) + present(CODCQI) + present(CODCRC) + present(CODCRI) + present(CODCSC) + present(CODCSI)) * LIG0 * LIG2 ;

LIGBNCAFF = positif(LIGBNCPHQ + (LIGSPENETPF * LIGBNCAFFQ)) * LIG0 * LIG2 ;

LIGNPLOC = positif(LOCNPCGAV + LOCNPCGAC + LOCNPCGAPAC + LOCDEFNPCGAV + LOCDEFNPCGAC + LOCDEFNPCGAPAC
		   + LOCNPV + LOCNPC + LOCNPPAC + LOCDEFNPV + LOCDEFNPC + LOCDEFNPPAC
		   + LOCGITCV + LOCGITCC + LOCGITCP + LOCGITHCV + LOCGITHCC + LOCGITHCP
		   + COD5EY + COD5EZ + COD5FY + COD5FZ + COD5GY + COD5GZ)
		   * LIG0 * LIG2 ;

LIGNPLOCF = positif(LOCNPCGAV + LOCNPCGAC + LOCNPCGAPAC + LOCDEFNPCGAV + LOCDEFNPCGAC + LOCDEFNPCGAPAC
		   + LOCNPV + LOCNPC + LOCNPPAC + LOCDEFNPV + LOCDEFNPC + LOCDEFNPPAC
                   + LNPRODEF10 + LNPRODEF9 + LNPRODEF8 + LNPRODEF7 + LNPRODEF6 + LNPRODEF5
                   + LNPRODEF4 + LNPRODEF3 + LNPRODEF2 + LNPRODEF1
		   + LOCGITCV + LOCGITCC + LOCGITCP + LOCGITHCV + LOCGITHCC + LOCGITHCP
		   + COD5EY + COD5EZ + COD5FY + COD5FZ + COD5GY + COD5GZ)
		   * LIG0 * LIG2 ;

LIGDEFNPLOC = positif(TOTDEFLOCNP) * LIG2 ;

LIGDFLOCNPF = positif(DEFLOCNPF) * LIG2 ;

LIGLOCNSEUL = positif(LIGNPLOC + LIGDEFNPLOC + LIGNPLOCF) * LIG2 ;

LIGLOCSEUL = (1 - positif(LIGNPLOC + LIGDEFNPLOC + LIGNPLOCF)) * LIG2 ;

regle 901110:
application : iliad  ;

ABICHQF = max(0 , BICHQF) ;

LIG_BICNPF = LIGBICNP * (1 - LIGMIBNPPOS) * LIG0 * LIG2 ;

LIGBICNPFQ = positif(present(CODCNC) + present(CODCNI) + present(CODCOC) + present(CODCOI) + present(CODCPC) + present(CODCPI)) * LIG0 * LIG2 ;

regle 901120:
application : iliad  ;

BNCNF = positif(present(BNHREV) + present(COD5XK) + present(BNCREV) + present(COD5XJ) + present(CODCQC) + present(CODCQI) + present(BNHDEV) + present(BNCDEV)
                    + present(BNHREC) + present(COD5YK) + present(BNCREC) + present(COD5YJ) + present(CODCRC) + present(CODCRI) + present(BNHDEC) + present(BNCDEC)
		    + present(BNHREP) + present(COD5ZK) + present(BNCREP) + present(COD5ZJ) + present(CODCSC) + present(CODCSI) + present(BNHDEP) + present(BNCDEP)) ;

LIGBNCNF = 1 - BNCNF ;

LIGNOCEP = (present(NOCEPV) + present(NOCEPC) + present(NOCEPP)) * LIG0 * LIG2 ;

NOCEPIMPN = max(0 , NOCEPIMP) ;

regle 901121:
application : iliad  ;

LIGNOCEPIMP = positif(present(BNCAABV) + present(COD5XS) + present(BNCAADV) + present(ANOCEP) + present(COD5XX) + present(DNOCEP)
                      + present(BNCAABC) + present(COD5YS) + present(BNCAADC) + present(ANOVEP) + present(COD5YX) + present(DNOCEPC)
		      + present(BNCAABP) + present(COD5ZS) + present(BNCAADP) + present(ANOPEP) + present(COD5ZX) + present(DNOCEPP)
		      + present(BNCNPPVV) + present(BNCNPPVC) + present(BNCNPPVP) + (present(BNCNPDCT) + present(COD5LD) + present(COD5MD)) * LIGNOCEP)
              * (1 - (null(BNCNPHQCV) * null(BNCNPHQCC) * null(BNCNPHQCP))) * (1 - LIGSPENPPOS) * LIG0 * LIG2 ;

LIGNOCEPIMPQ = positif(present(CODCJG) + present(CODCSN) + present(CODCRF) + present(CODCNS) + present(CODCSF) + present(CODCOS)) * LIG0 * LIG2 ;

regle 901122:
application : iliad  ;

LIGDAB = positif(present(DABNCNP6) + present(DABNCNP5) + present(DABNCNP4)
		 + present(DABNCNP3) + present(DABNCNP2) + present(DABNCNP1)) 
		* LIG0 * LIG2 ;

LIGDIDAB = positif_ou_nul(DIDABNCNP) * LIGDAB * LIG0 * LIG2 ;

LIGDEFBNCNPF = positif(DEFBNCNPF) * LIG2 ;
LIGDEFBANIF  = positif(DEFBANIF) * LIG2 ;
LIGDEFBICNPF = positif(DEFBICNPF) * LIG2 ;
LIGDEFRFNONI = positif(DEFRFNONI) * LIG2 ;

regle 901130:
application :  iliad ;

LIG910 = positif((present(RCMABD) + present(RCMTNC) + present(RCMAV) + present(RCMHAD) + present(RCMHAB) 
                  + present(REGPRIV) + present(COD2TT) + present(COD2VV) + present(COD2WW) + present(COD2YY) 
		  + present(COD2ZZ) + present(COD2VN) + present(COD2VO) + present(COD2VP)) * positif(COD2OP)
		 + (present(RCMAV) + present(COD2YY) + present(COD2VN)) * (1 - positif(COD2OP))
		 + ((1 - present(BRCMQ)) * present(RCMFR))) * LIG0 * LIG2 ;

LIG2OP = positif(COD2OP + 0) * LIG2 ;

regle 901140: 
application : iliad  ;

LIG1130 = positif(present(REPSOF)) * LIG0 * LIG2 ;

regle 901150:
application : iliad  ;

LIG1950 = INDREV1A8 * positif_ou_nul(REVKIRE) * (1 - positif_ou_nul(IND_TDR)) * LIG2 ;

regle 901160:
application :  iliad ;

LIG29 = positif(present(RFORDI) + present(RFDHIS) + present(RFDANT) + present(RFDORD)) 
        * (1 - LIG30) * IND_REV * LIG12 ;

regle 901170:
application : iliad  ;

LIG30 = positif(RFMIC) * LIG12 ;
LIGREVRF = positif(present(FONCI) + present(REAMOR)) * LIG12 ;

regle 901180:
application :  iliad ;

LIG49 =  INDREV1A8 * positif_ou_nul(DRBG) * LIG2 ;

regle 901190:
application : iliad  ;

LIG52 = positif(present(CHENF1) + present(CHENF2) + present(CHENF3) + present(CHENF4) 
                + present(NCHENF1) + present(NCHENF2) + present(NCHENF3) + present(NCHENF4)) 
	* LIG12 ;

regle 901200:
application : iliad  ;

LIG58 = positif(present(PAAV) + present(PAAP)) * LIG52 * LIG12 ;

regle 901210:
application : iliad  ;

LIG585 = positif(present(PAAP) + present(PAAV)) * (1 - LIG52) * LIG12 ;

LIG65 = positif(LIG52 + LIG58 + LIG585 
                + present(CHRFAC) + present(CHNFAC) + present(CHRDED)
		+ present(DPERPV) + present(DPERPC) + present(DPERPP)
                + LIGREPAR)  
       * LIG12 ;

regle 901220:
application : iliad  ;

LIGDPREC = present(CHRFAC) * LIG12 ;

LIGDFACC = (positif(20-V_NOTRAIT+0) * positif(DFACC)
           + (1 - positif(20-V_NOTRAIT+0)) * present(DFACC)) * LIG12 ;

regle 901230:
application :  iliad ;

LIG1390 = positif(positif(ABMAR) + (1 - positif(RI1)) * positif(V_0DN)) * LIG12 ;

regle 901240:
application :  iliad ;

LIG68 = INDREV1A8 * (1 - positif(abs(RNIDF))) * LIG2 ;

regle 901250:
application : iliad  ;

LIGTTPVQ = positif(
                   positif(CARTSV) + positif(CARTSC) + positif(CARTSP1) + positif(CARTSP2)+ positif(CARTSP3)+ positif(CARTSP4)
                   + positif(REMPLAV) + positif(REMPLAC) + positif(REMPLAP1) + positif(REMPLAP2)+ positif(REMPLAP3)+ positif(REMPLAP4)
                   + positif(PEBFV) + positif(PEBFC) + positif(PEBF1) + positif(PEBF2)+ positif(PEBF3)+ positif(PEBF4)
                   + positif(CARPEV) + positif(CARPEC) + positif(CARPEP1) + positif(CARPEP2)+ positif(CARPEP3)+ positif(CARPEP4)
                   + positif(CODRAZ) + positif(CODRBZ) + positif(CODRCZ) + positif(CODRDZ) + positif(CODREZ) + positif(CODRFZ) 
                   + positif(PENSALV) + positif(PENSALC) + positif(PENSALP1) + positif(PENSALP2)+ positif(PENSALP3)+ positif(PENSALP4)
                   + positif(RENTAX) + positif(RENTAX5) + positif(RENTAX6) + positif(RENTAX7)
                   + positif(REVACT) + positif(REVPEA) + positif(PROVIE) + positif(DISQUO) + positif(RESTUC) + positif(INTERE)
                   + positif(FONCI) + positif(REAMOR)
                   + positif(4BACREV) + positif(4BACREC) + positif(4BACREP) + positif(4BAHREV) + positif(4BAHREC) + positif(4BAHREP)
                   + positif(CODDAJ) + positif(CODEAJ) + positif(CODDBJ)+ positif(CODEBJ) + positif(CODRVG)
		   + positif(CODRAF) + positif(CODRAG) + positif(CODRBF) + positif(CODRBG) + positif(CODRCF) + positif(CODRCG)
		   + positif(CODRDF) + positif(CODRDG) + positif(CODREF) + positif(CODRGG) + positif(CODRFF) + positif(CODRFG)
		   + positif(CODRAL) + positif(CODRAM) + positif(CODRBL) + positif(CODRBM) + positif(CODRCL) + positif(CODRCM)
		   + positif(CODRDL) + positif(CODRDM) + positif(CODREL) + positif(CODREM) + positif(CODRFL) + positif(CODRFM)
		   + positif(CODRAR) + positif(CODRBR) + positif(CODRCR) + positif(CODRDR)
		   + positif(CODCJG) + positif(CODCKC) + positif(CODCKI) + positif(CODCLC) + positif(CODCLI) + positif(CODCMC)
		   + positif(CODCMI) + positif(CODCNC) + positif(CODCNI) + positif(CODCNS) + positif(CODCOC) + positif(CODCOI)
		   + positif(CODCOS) + positif(CODCPC) + positif(CODCPI) + positif(CODCQC) + positif(CODCQI) + positif(CODCRC)
		   + positif(CODCRF) + positif(CODCRI) + positif(CODCSC) + positif(CODCSF) + positif(CODCSI) + positif(CODCSN)
		   + positif(CODRUA) + positif(CODRSL) + positif(CODRVA)
                  ) * LIG12 ;

regle 901260:
application :  iliad ;

LIGIMPTR = positif(RCMIMPTR) * LIG0 * LIG2 ;

LIG1430 = positif(BPTP3) * LIG0 * LIG2 ;

LIG1431 = positif(BPTP18) * LIG0 * LIG2 ;

LIG1432 = positif(BPT19) * LIG0 * LIG2 ;

regle 901270:
application :  iliad ;

LIG815 = V_EAD * positif(BPTPD) * LIG0 * LIG2 ;
LIG816 = V_EAG * positif(BPTPG) * LIG0 * LIG2 ;
LIGTXF225 = positif(PEA+0) * LIG0 * LIG2 ;
LIGTXF24 = positif(BPT24) * LIG0 * LIG2 ;
LIGTXF30 = positif_ou_nul(BPCOPTV + BPVSK) * LIG0  * LIG2 ;
LIGTXF40 = positif(BPV40V + 0) * LIG0 * LIG2 ;

regle 901290:
application :  iliad ;
 
LIG81 = positif(present(RDDOUP) + present(DONAUTRE) + present(REPDON03) + present(REPDON04) 
                + present(REPDON05) + present(REPDON06) + present(REPDON07) + present(COD7UH)
                + positif(EXCEDANTA))
        * LIG12 ;

LIGCRDIE = positif(REGCI) * LIG12 ;

regle 901300:
application : iliad  ;

LIG1500 = positif((positif(IPMOND) * positif(present(IPTEFP)+positif(VARIPTEFP)*present(DEFZU))) + positif(INDTEFF) * positif(TEFFREVTOT)) 
	      * (1 - positif(DEFRIMOND)) * CNRLIG12 ;

LIG1510 = positif((positif(IPMOND) * present(IPTEFN)) + positif(INDTEFF) * (1 - positif(TEFFREVTOT))) 
	      * (1 - positif(DEFRIMOND)) * CNRLIG12 ;

LIG1500YT = positif((positif(IPMOND) * positif(present(IPTEFP)+positif(VARIPTEFP)*present(DEFZU))) + positif(INDTEFF) * positif(TEFFREVTOT)) 
	     * positif(positif(max(0,IPTEFP+DEFZU-IPTEFN))+positif(max(0,RMOND+DEFZU-DMOND))) * positif(DEFRIMOND) * CNRLIG12 ;

LIG1510YT =  positif(null(max(0,RMOND+DEFZU-DMOND))+null(max(0,IPTEFP+DEFZU-IPTEFN))) * positif(DEFRIMOND) * CNRLIG12 ;

regle 901310:
application : iliad  ;

LIG1522 = (1 - present(IND_TDR)) * (1 - INDTXMIN) * (1 - INDTXMOY) * V_CNR * LIG2 ;

regle 901320:
application :  iliad ;

LIG1523 = (1 - present(IND_TDR)) * LIG2 ;

regle 901330:
application : iliad  ;

LIG75 = (1 - INDTXMIN) * (1 - INDTXMOY) * (1 - (LIG1500+ LIG1500YT)) * (1 - (LIG1510+ LIG1510YT)) * INDREV1A8 * LIG2 ;

LIG1545 = (1 - present(IND_TDR)) * INDTXMIN * positif(IND_REV) * LIG2 ;

LIG1760 = (1 - present(IND_TDR)) * INDTXMOY * LIG2 ;

LIG1546 = positif(PRODOM + PROGUY) * (1 - positif(V_EAD + V_EAG)) * LIG2 ;

LIG1550 = (1 - present(IND_TDR)) * INDTXMOY * LIG2 ;

LIG74 = (1 - INDTXMIN) * positif(LIG1500 + LIG1510 + LIG1500YT + LIG1510YT) * LIG2 ;

LIGBAMARG = positif(BATMARGTOT) * (1 - present(IND_TDR)) * LIG138 * LIG2 ;

regle 901340:
application :  iliad ;

LIG80 = positif(present(RDREP) + present(DONETRAN)) * LIG12 ;

regle 901350:
application : iliad  ;

LIGRSOCREPR = positif(present(RSOCREPRISE)) * LIG12 ;

regle 901360:
application :  iliad ;

LIG1740 = positif(RECOMP) * LIG2 ;

regle 901370:
application :  iliad ;

LIG1780 = positif(RDCOM + NBACT) * LIG12 ;

regle 901380:
application :  iliad ;

LIG98B = positif(LIG80 + LIGFIPC + LIGFIPDOM 
                 + LIGDUFTOT + LIGPINTOT + LIG7CY + LIG7DY + LIG7EY + LIG7FY + LIG7GY
                 + LIGREDAGRI + LIGFORET + LIGRESTIMO  
	         + LIGCINE + LIGPRESSE + LIGRSOCREPR + LIGCOTFOR 
	         + present(PRESCOMP2000) + present(RDPRESREPORT) + present(FCPI) 
		 + present(DSOUFIP) + LIGRIRENOV + present(DFOREST) 
		 + present(DHEBE) + present(DSURV)
	         + LIGLOGDOM + LIGREPTOUR + LIGREPHA + LIGREHAB
		 + LIG1780 + LIG2040 + LIG81 + LIGCRDIE
                 + LIGLOGSOC + LIGDOMSOC1 
		 + LIGCELSOM1 + LIGCELSOM2 + LIGCELSOM3 + LIGCELSOM4
		 + LIGCELSOM56 + LIGCELSOM7
                 + LIGILMNP1 + LIGILMNP2 + LIGILMNP3 + LIGILMNP4
                 + LIGILMPA + LIGILMPB + LIGILMPC + LIGILMPD + LIGILMPE
                 + LIGILMPF + LIGILMPK + LIGILMPG + LIGILMPL + LIGILMPH 
		 + LIGILMPM + LIGILMPI + LIGILMPN + LIGILMPJ + LIGILMPO
                 + LIGILMOA + LIGILMOB + LIGILMOC + LIGILMOD + LIGILMOE
                 + LIGILMOJ + LIGILMOI + LIGILMOH + LIGILMOG + LIGILMOF
		 + LIGILMOO + LIGILMON + LIGILMOM + LIGILMOL + LIGILMOK
		 + LIGILMIY + LIGINVRED
                 + LIGILMJC + LIGILMJI + LIGILMJS
                 + LIGPROREP + LIGREPNPRO + LIGMEUREP + LIGILMIC
                 + LIGILMIB + LIGILMIA + LIGILMJY + LIGILMJX + LIGILMJW
                 + LIGILMJV + LIGRESINEUV + LIGRESIVIEU + LIGLOCIDEFG
		 + LIGCODJTJU + LIGCODOU + LIGCODOV + LIGCODOW
		 + present(DNOUV) + LIGLOCENT + LIGCOLENT + LIGRIDOMPRO
		 + LIGPATNAT3 + LIGPATNAT4 + COD7KZ) 
           * LIG12 ;

LIGRED = LIG98B * (1 - positif(RIDEFRI)) * LIG12 ;

LIGREDYT = LIG98B * positif(RIDEFRI) * LIG12 ;

regle 901390:
application :  iliad ;

LIG1820 = positif(ABADO + ABAGU + RECOMP) * LIG2 ;

LIGIDRS = positif(IDRS4) * positif(LIGRED + LIGREDYT) ;

regle 901400:
application : iliad  ;

LIG106 = positif(RETIR) * LIG2 ;
LIGINRTAX = positif(RETTAXA) * LIG2 ;
LIG10622 = positif(RETIR2224) * LIG2 ;
LIGINRTAX22 = positif(RETTAXA2224) * LIG2 ;
ZIG_INT22 = positif(RETCS2224 + RETPS2224 + RETRD2224) * LIG2 ;

LIGINRPCAP = positif(RETPCAP) * LIG2 ;
LIGINRPCAP2 = positif(RETPCAP2224) * LIG2 ;
LIGINRLOY = positif(RETLOY) * LIG2 ;
LIGINRLOY2 = positif(RETLOY2224) * LIG2 ;

LIGINRHAUT = positif(RETHAUTREV) * LIG2 ;
LIGINRHAUT2 = positif(RETCHR2224) * LIG2 ;

regle 901410:
application : iliad  ;

LIG_172810 = positif(NMAJ1) ;

LIGTAXA17281 = positif(NMAJTAXA1) ;

LIGPCAP17281 = positif(NMAJPCAP1) ;

LIGCHR17281 = positif(NMAJCHR1) ;

LIG_NMAJ1 = positif(NMAJ1) * LIG2 ;
LIG_NMAJ3 = positif(NMAJ3) * LIG2 ;
LIG_NMAJ4 = positif(NMAJ4) * LIG2 ;

LIGNMAJTAXA1 = positif(NMAJTAXA1) * LIG2 ;
LIGNMAJTAXA3 = positif(NMAJTAXA3) * LIG2 ;
LIGNMAJTAXA4 = positif(NMAJTAXA4) * LIG2 ;

LIGNMAJPCAP1 = positif(NMAJPCAP1) * LIG2 ;
LIGNMAJPCAP3 = positif(NMAJPCAP3) * LIG2 ;
LIGNMAJPCAP4 = positif(NMAJPCAP4) * LIG2 ;

LIGNMAJLOY1 = positif(NMAJLOY1) * LIG2 ;
LIGNMAJLOY4 = positif(NMAJLOY4) * LIG2 ;

LIGNMAJCHR1 = positif(NMAJCHR1) * LIG2 ;
LIGNMAJCHR3 = positif(NMAJCHR3) * LIG2 ;
LIGNMAJCHR4 = positif(NMAJCHR4) * LIG2 ;

regle 901420:
application :  iliad ;

LIG109 = positif(LIGIPSOUR + LIGIPAE + LIGPVETR + LIGCIGLO + LIGCICAP + LIGREGCI + LIGCULTURE + LIGMECENAT 
		  + LIGCORSE + LIG2305 + LIGEMPLOI + LIGCI2CK + LIGBPLIB + LIGCIHJA + LIGCIGE + LIGDEVDUR 
                  + LIGCITEC + LIGCICA + LIGCIGARD + LIG82 + LIGPRETUD + LIGSALDOM + LIGCIFORET + LIGHABPRIN
		  + LIGCREFAM + LIGCREAPP + LIGCREBIO + LIGPRESINT + LIGCREFORM 
		  + LIGCONGA + LIGMETART + LIGRESTAU + LIGVERSLIB + LIGCIMR) 
               * LIG12 ;

LIGCRED1 = positif(LIGPVETR + LIGCICAP + LIGREGCI + LIGCIGLO + LIGCIMR + 0) 
	    * (1 - positif(LIGIPSOUR + LIGIPAE + LIGCULTURE + LIGMECENAT + LIGCORSE + LIG2305 + LIGEMPLOI + LIGCI2CK + LIGBPLIB + LIGCIHJA 
	                   + LIGCIGE + LIGDEVDUR + LIGCICA + LIGCIGARD + LIG82 + LIGPRETUD + LIGSALDOM + LIGCIFORET + LIGHABPRIN + LIGCREFAM 
			   + LIGCREAPP + LIGCREBIO + LIGPRESINT + LIGRESTAU + LIGCONGA + LIGMETART + LIGCREFORM + LIGVERSLIB + LIGCITEC + 0))
	    * LIG12 ;

LIGCRED2 = (1 - positif(LIGPVETR + LIGCICAP + LIGREGCI + LIGCIGLO + LIGCIMR + 0)) 
	    * positif(LIGIPSOUR + LIGIPAE + LIGCULTURE + LIGMECENAT + LIGCORSE + LIG2305 + LIGEMPLOI + LIGCI2CK + LIGBPLIB + LIGCIHJA 
	              + LIGCIGE + LIGDEVDUR + LIGCICA + LIGCIGARD + LIG82 + LIGPRETUD + LIGSALDOM + LIGCIFORET + LIGHABPRIN + LIGCREFAM 
		      + LIGCREAPP + LIGCREBIO + LIGPRESINT + LIGRESTAU + LIGCONGA + LIGMETART + LIGCREFORM + LIGVERSLIB + LIGCITEC + 0)
	    * LIG12 ;

LIGCRED3 = positif(LIGPVETR + LIGCICAP + LIGREGCI + LIGCIGLO + LIGCIMR + 0) 
	    * positif(LIGIPSOUR + LIGIPAE + LIGCULTURE + LIGMECENAT + LIGCORSE + LIG2305 + LIGEMPLOI + LIGCI2CK + LIGBPLIB + LIGCIHJA 
	              + LIGCIGE + LIGDEVDUR + LIGCICA + LIGCIGARD + LIG82 + LIGPRETUD + LIGSALDOM + LIGCIFORET + LIGHABPRIN + LIGCREFAM 
		      + LIGCREAPP + LIGCREBIO + LIGPRESINT + LIGRESTAU + LIGCONGA + LIGMETART + LIGCREFORM + LIGVERSLIB + LIGCITEC + 0)
           * LIG12 ;

regle 901430:
application :  iliad ;

LIGIPSOUR = positif(IPSOUR) * LIG12 ;
LIGIPAE = positif(IPAE) * LIG12 ;
LIGPVETR = positif(present(CIIMPPRO) + present(CIIMPPRO2)+present(COD8XX)) * LIG12 ;
LIGCICAP = present(PRELIBXT) * LIG12 ;
LIGREGCI = positif(present(REGCI) + present(COD8XY)) * positif(CICHR) * LIG12 ;
LIGCIGLO = positif(present(COD8XF) + present(COD8XG) + present(COD8XH)) * LIG12 ;

LIGCULTURE = present(CIAQCUL) * LIG12 ;
LIGMECENAT = present(RDMECENAT) * LIG12 ;
LIGCORSE = positif(present(CIINVCORSE) + present(IPREPCORSE) + present(CICORSENOW)) * LIG12 ;
LIG2305 = positif(DIAVF2) * LIG12 ;
LIGEMPLOI = positif(COD8UW + COD8TL) * LIG12 ;
LIGCI2CK = positif(COD2CK) * LIG12 ;
LIGBPLIB = present(RCMLIB) * LIG0 * LIG2 ;
LIGCIHJA = positif(CODHJA) * LIG12 ;
LIGCIGE = positif(RDTECH + RDEQPAHA + COD7WI) * LIG12 ;
LIGDEVDUR = positif(DDEVDUR) * LIG12 ;
LIGCICA = positif(BAILOC98) * LIG12 ;
LIGCIGARD = positif(DGARD) * LIG12 ;
LIG82 = positif(present(RDSYVO) + present(RDSYCJ) + present(RDSYPP) ) * LIG12 ;
LIGPRETUD = positif(PRETUD+PRETUDANT) * LIG12 ;
LIGSALDOM = present(CREAIDE) * LIG12 ;
LIGCIFORET = positif(BDCIFORET) * LIG12 ;
LIGHABPRIN = positif(present(PREHABT) + present(PREHABT1) + present(PREHABT2) + present(PREHABTN) 
                     + present(PREHABTN1) + present(PREHABTN2) + present(PREHABTVT)
                    ) * LIG12 ;
LIGCREFAM = positif(CREFAM) * LIG12 ;
LIGCREAPP = positif(CREAPP) * LIG12 ;
LIGCREBIO = positif(CREAGRIBIO) * LIG12 ;
LIGPRESINT = positif(PRESINTER) * LIG12 ;
LIGRESTAU = positif(CRERESTAU) * LIG12 ;
LIGCONGA = positif(CRECONGAGRI) * LIG12 ;
LIGMETART = positif(CREARTS) * LIG12 ;
LIGCREFORM = positif(CREFORMCHENT) * LIG12 ;
LIGVERSLIB = positif(AUTOVERSLIB) * LIG12 ;
LIGCITEC = positif(DTEC) * LIG12 ;
LIGCIMR = null(4 - V_IND_TRAIT) * positif(CIMR2) * LIG12
          + null(5 - V_IND_TRAIT) * positif(CIMR24TL) * (1 - positif(FLAG_RETARD * null(FLAG_RETARD07) + FLAG_DEFAUT)) * LIG12 ;

regle 901440:
application :  iliad ;

LIGBRAS = positif(BRAS) * LIG12 ;
LIGNRBASE = positif(present(NRINET) + present(NRBASE)) * LIG12 ;
LIGBASRET = positif(present(IMPRET) + present(BASRET)) * LIG12 ;

regle 901450:
application : iliad  ;

LIG251112 = positif(V_NOTRAIT - 999) ;

LIGCOD8EA = positif(COD8EA + 0) * LIG12 ;

LIGAVFISC = positif(AVFISCOPTER) * LIG12 ;

regle 901460:
application :  iliad ;

LIG2040 = positif(DNBE + RNBE + RRETU) * LIG12 ;

regle 901470:
application : iliad  ;

LIGRDCSG = positif(positif(V_BTCSGDED) + present(DCSG) + present(RCMSOC)) * LIG12 ;

regle 901480:
application :  iliad ;

LIGTAXANET = positif((present(CESSASSV) + present(CESSASSC)) * INDREV1A8 + TAXANTAFF) * LIG12 ;

LIGPCAPNET = positif((present(PCAPTAXV) + present(PCAPTAXC)) * INDREV1A8 + PCAPANTAFF) * LIG12 ;

LIGLOYNET = (present(LOYELEV) * INDREV1A8 + TAXLOYANTAFF) * LIG12 ;

LIGHAUTNET = positif(BHAUTREV * INDREV1A8 + CHRPVIMP + HAUTREVANTAF) * LIG12 ;

LIG_IRNET = positif(LIGTAXANET + LIGPCAPNET + LIGLOYNET + LIGHAUTNET) * LIG2 ;

LIGIRNET = positif(IRNET * LIG_IRNET + LIGTAXANET + LIGPCAPNET + LIGLOYNET + LIGHAUTNET) * LIG2 ;

regle 901490:
application :  iliad ;

LIGANNUL = positif(ANNUL2042) ;

regle 901500:
application :  iliad ;

LIG2053 = positif(V_NOTRAIT - 20) * positif(IDEGR) * positif(IREST - SEUIL_8) ;

regle 901510:
application :  iliad ;


LIG2051 = (1 - positif(20 - V_NOTRAIT)) * positif (RECUMBIS) ;

LIGBLOC = positif(V_NOTRAIT - 20) ;

LIGSUP = positif(null(V_NOTRAIT - 26) + null(V_NOTRAIT - 36) + null(V_NOTRAIT - 46) + null(V_NOTRAIT - 56) + null(V_NOTRAIT - 66)) ;

LIGDEG = positif_ou_nul(TOTIRPSANT) * positif(SEUIL_8 - RECUM) 
         * positif(null(V_NOTRAIT - 23) + null(V_NOTRAIT - 33) + null(V_NOTRAIT - 43) + null(V_NOTRAIT - 53) + null(V_NOTRAIT - 63)) ;

LIGRES = (1 - positif(TOTIRPSANT + 0)) * positif_ou_nul(RECUM - SEUIL_8)
         * positif(null(V_NOTRAIT - 23) + null(V_NOTRAIT - 33) + null(V_NOTRAIT - 43) + null(V_NOTRAIT - 53) + null(V_NOTRAIT - 63)) ;

LIGDEGRES = positif(TOTIRPSANT + 0) * positif_ou_nul(RECUM - SEUIL_8) 
            * positif(null(V_NOTRAIT - 23) + null(V_NOTRAIT - 33) + null(V_NOTRAIT - 43) + null(V_NOTRAIT - 53) + null(V_NOTRAIT - 63)) ;

LIGNEMP = positif((1 - null(NAPTEMP)) + null(NAPTEMP) * null(NAPTIR) * null(NAPCRP)) * positif(V_NOTRAIT - 20) ;

LIGEMP = (1 - LIGNEMP) * positif(V_NOTRAIT - 20) ;

LIG2052 = (1 - positif(V_ANTREIR + 0)) * (1 - APPLI_OCEANS) * positif(V_NOTRAIT - 20) ;

LIGTAXANT = APPLI_ILIAD * positif(V_NOTRAIT - 20) * positif(V_TAXANT + LIGTAXANET * positif(TAXANET))
            * (1 - LIG2051) * (1 - APPLI_OCEANS) ;

LIGPCAPANT = APPLI_ILIAD * positif(V_NOTRAIT - 20) * positif(V_PCAPANT + LIGPCAPNET * positif(PCAPNET))
             * (1 - LIG2051) * (1 - APPLI_OCEANS) ;

LIGLOYANT = APPLI_ILIAD * positif(V_NOTRAIT - 20) * positif(V_TAXLOYANT + LIGLOYNET * positif(TAXLOYNET))
             * (1 - LIG2051) * (1 - APPLI_OCEANS) ;

LIGHAUTANT = APPLI_ILIAD * positif(V_NOTRAIT - 20) * positif(V_CHRANT + LIGHAUTNET * positif(HAUTREVNET))
             * (1 - LIG2051) * (1 - APPLI_OCEANS) ;

LIGANTREIR = positif(V_ANTREIR + 0) * (1 - positif(V_ANTCR)) * (1 - APPLI_OCEANS) * positif(V_NOTRAIT - 20) ;

LIGNANTREIR = positif(V_ANTREIR + 0) * positif(V_ANTCR + 0) * (1 - APPLI_OCEANS) * positif(V_NOTRAIT - 20) ;

LIGNONREC = positif(V_NONMERANT + 0) * (1 - APPLI_OCEANS) * positif(V_NOTRAIT - 20) ;

LIGNONREST = positif(V_NONRESTANT + 0) * (1 - APPLI_OCEANS) * positif(V_NOTRAIT - 20) ;

LIGIINET = LIGSUP * (positif(NAPT + 0) + null(IINETCALC)) * positif(V_NOTRAIT - 20) ;

LIGIINETC = LIGSUP * null(NAPT) * positif(IINETCALC + 0) * positif(V_NOTRAIT - 20) ;

LIGIDEGR = positif(LIGDEG + LIGDEGRES) * (positif_ou_nul(IDEGR - SEUIL_8) + null(IDEGR)) * positif(V_NOTRAIT - 20) ;

LIGIDEGRC = positif(LIGDEG + LIGDEGRES) * positif(SEUIL_8 - IDEGR) * positif(IDEGR + 0) * positif(V_NOTRAIT - 20) ;

LIGIREST = positif(LIGRES + LIGDEGRES) * (positif_ou_nul(IREST - SEUIL_8) + null(IREST)) * positif(V_NOTRAIT - 20) ;

LIGIRESTC = positif(LIGRES + LIGDEGRES) * positif(SEUIL_8 - IREST) * positif(IREST + 0) * positif(V_NOTRAIT - 20) ;

LIGNMRR = LIGIINETC * positif(V_ANTRE - V_NONRESTANT + 0) * positif(V_NOTRAIT - 20) ;

LIGNMRS = LIGIINETC * (1 - positif(V_ANTRE - V_NONRESTANT)) * positif(V_NOTRAIT - 20) ;

LIGRESINF = positif(LIGIDEGRC + LIGIRESTC) * positif(V_NOTRAIT - 20) ;

regle 901520:
application :  iliad ;


LIG2080 = positif(NATIMP - 71) * LIG2 ;

regle 901530:
application :  iliad ;


LIGTAXADEG = positif(NATIMP - 71) * positif(TAXADEG) * LIG2 ;

LIGPCAPDEG = positif(NATIMP - 71) * positif(PCAPDEG) * LIG2 ;

LIGLOYDEG = positif(NATIMP - 71) * positif(TAXLOYDEG) * LIG2 ;

LIGHAUTDEG = positif(NATIMP - 71) * positif(HAUTREVDEG) * LIG2 ;

regle 901540:
application : iliad  ;

INDCTX = positif(null(V_NOTRAIT - 23) + null(V_NOTRAIT - 33) + null(V_NOTRAIT - 43) + null(V_NOTRAIT - 53) + null(V_NOTRAIT - 63)) ;

INDIS = positif(null(V_NOTRAIT - 14) + null(V_NOTRAIT - 16) + null(V_NOTRAIT - 26) + null(V_NOTRAIT - 36) + null(V_NOTRAIT - 46) + null(V_NOTRAIT - 56) + null(V_NOTRAIT - 66)) ;


LIG2140 = si (
                ( ( (V_CNR + 0 = 0) et NATIMP = 1 et (IRNET + TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET + NRINET - NAPTOTA + NAPCR >= SEUIL_12)) 
		    ou ((V_CNR + 0 = 1) et (NATIMP = 1 ou  NATIMP = 0))
                    ou ((V_REGCO + 0 = 3) et ((NRINET > 0) et (NRINET < 12) et (CSTOTSSPENA < 61)))
                ) 
		et LIG2141 + 0 = 0
		)
          alors ((((1 - INDCTX) * INDREV1A8 * (1 - (positif(IRANT)*null(NAPT)) ) * LIG2)
                + null(IINET + NAPTOTA) * null(INDREV1A8)) * positif(IND_REV) * positif(20 - V_NOTRAIT))
          finsi;

LIG21401 = si (( ((V_CNR+0=0) et NATIMP=1 et (IRNET + TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET + NRINET - NAPTOTA + NAPCR >= SEUIL_12)) 
		ou ((V_CNR+0=1) et (NATIMP=1 ou  NATIMP=0)))
		et LIG2141 + 0 = 0
		)
           alors ((((1 - INDCTX) * INDREV1A8 * (1 - (positif(IRANT)*null(NAPT)) ) * LIG2)
                + null(IINET + NAPTOTA) * null(INDREV1A8)) * positif(IND_REV) * positif(20 - V_NOTRAIT))
           finsi ;

LIG21402 = si (( ((V_CNR+0=0) et NATIMP=1 et (IRNET + TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET + NRINET - NAPTOTA + NAPCR >= SEUIL_12)) 
		ou ((V_CNR+0=1) et (NATIMP=1 ou  NATIMP=0)))
		et LIG2141 + 0 = 0
		)
           alors ((((1 - INDCTX) * INDREV1A8 * (1 - (positif(IRANT)*null(NAPT)) ) * LIG2)
                + null(IINET + NAPTOTA) * null(INDREV1A8)) * positif(IND_REV) * positif(V_NOTRAIT - 20))
           finsi ;

regle 901550:
application :  iliad ;

LIG2141 = null(IAN + RPEN - IAVT + TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES - IRANT) 
                  * positif(IRANT)
                  * (1 - LIG2501)
		  * null(V_IND_TRAIT - 4)
		  * (1 - positif(NRINET + 0)) ;

regle 901560:
application :  iliad ;

LIGNETAREC = positif (IINET) * (1 - LIGPS) * positif(ANNUL2042) ;

LIGNETARECS = positif (IINET) * LIGPS * positif(ANNUL2042) ;

regle 901570:
application : iliad  ;

LIG2150 = (1 - INDCTX) 
	 * positif(IREST)
         * (1 - LIG2140)
         * (1 - positif(IND_REST50))
	 * positif(20 - V_NOTRAIT)
         * LIG2 ;

regle 901580:
application :  iliad ;

LIG2161 =  INDCTX 
	  * positif(IREST) 
          * positif_ou_nul(IREST - SEUIL_8) 
	  * (1 - positif(IND_REST50)) ;

LIG2368 = INDCTX 
	 * positif(IREST)
         * positif ( positif(IND_REST50)
                     + positif(IDEGR) ) ;

regle 901590:
application :  iliad ;

LIG2171 = (1 - INDCTX) 
	 * positif(IREST)
	 * (1 - LIG2140)
         * positif(IND_REST50)  
	 * positif(20 - V_NOTRAIT)
	 * LIG2 ;

regle 901600:
application :  iliad ;

LIGTROP = positif(V_ANTRE+V_ANTCR) * null(IINET)* positif_ou_nul(abs(NAPTOTA)
             - IRESTIT - IRANT) * (1 - positif_ou_nul(abs(NAPTOTA) - IRESTIT
             - IRANT - SEUIL_12))
               * null(IDRS2 - IDEC + IREP)
	       * (1 - LIGPS)
               * (1 - INDCTX);

LIGTROPS = positif(V_ANTRE+V_ANTCR) * null(IINET)* positif_ou_nul(abs(NAPTOTA)
             - IRESTIT - IRANT) * (1 - positif_ou_nul(abs(NAPTOTA) - IRESTIT
             - IRANT - SEUIL_12))
               * null(IDRS2 - IDEC + IREP)
	       * LIGPS
               * (1 - INDCTX);

LIGTROPREST =  positif(V_ANTRE+V_ANTCR) * null(IINET)* positif_ou_nul(abs(NAPTOTA) 
               - IRESTIT - IRANT) * (1 - positif_ou_nul(abs(NAPTOTA) - IRESTIT
               - IRANT - SEUIL_12))
		 * (1 - LIGTROP)
	         * (1 - LIGPS)
                 * (1 - INDCTX);

LIGTROPRESTS =  positif(V_ANTRE+V_ANTCR) * null(IINET)* positif_ou_nul(abs(NAPTOTA) 
                - IRESTIT - IRANT) * (1 - positif_ou_nul(abs(NAPTOTA) - IRESTIT
                - IRANT - SEUIL_12))
		 * (1 - LIGTROP)
	         * LIGPS
                 * (1 - INDCTX);

regle 901610:
application :  iliad ;

LIGRESINF50 = positif(positif(IND_REST50) * positif(IREST) 
                      + positif(RECUM) * (1 - positif_ou_nul(RECUM - SEUIL_8)))  
	      * positif(SEUIL_8 - IRESTIT) * null(LIGRESINF) * LIG2 ;

regle 901620:
application :  iliad ;

LIG2200 = positif(IDEGR) * positif_ou_nul(IDEGR - SEUIL_8) * (1 - LIGPS) ;

LIG2200S = positif(IDEGR) * positif_ou_nul(IDEGR - SEUIL_8) * LIGPS ;

regle 901630:
application :  iliad ;

LIG2205 = positif(IDEGR) * (1 - positif_ou_nul(IDEGR - SEUIL_8)) * (1 - LIGPS) * LIG2 ;

LIG2205S = positif(IDEGR) * (1 - positif_ou_nul(IDEGR - SEUIL_8)) * LIGPS * LIG2 ;

regle 901640:
application :  iliad ;


IND_NIRED = si ((CODINI=3 ou CODINI=5 ou CODINI=13)
               et ((IAVIM +NAPCRPAVIM)- TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES) = 0 
                   et  V_CNR = 0)
          alors (1 - INDCTX) 
          finsi ;

regle 901650:
application :  iliad ;


IND_IRNMR = si (CODINI=8 et NATIMP=0 et V_CNR = 0)
          alors (1 - INDCTX)  
          finsi;

regle 901660:
application :  iliad ;

IND_IRINF80 = si ( ((CODINI+0=9 et NATIMP+0=0) ou (CODINI +0= 99))
                  et V_CNR=0 
                  et  (IRNET +TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES + NAPCR < SEUIL_12)
                  et  ((IAVIM+NAPCRPAVIM) >= SEUIL_61))
              alors ((1 - positif(INDCTX)) * (1 - positif(IREST))) 
              finsi;

regle 901670:
application :  iliad ;


LIGNIIR = null(IDRS3 - IDEC)
          * null(NRINET + 0)
          * null(NATIMP)
          * null(TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES + NAPCRP)
          * (1 - positif(IREP))
          * (1 - positif(IPROP))
          * (1 - positif(IRESTIT))
          * (1 - positif(IDEGR))
          * (1 - LIGAUCUN)
          * (1 - LIG2141)
          * (1 - LIG2501)
          * (1 - LIG8FV)
          * (1 - LIGNIDB)
          * (1 - LIGTROP)
	  * (1 - LIGTROPREST)
	  * (1 - positif(IMPRET))
	  * (1 - positif(NRINET))
	  * positif(20 - V_NOTRAIT)
          * (1 - V_CNR) * LIG2 ;

LIGNIIRDEG = null(IDRS3 - IDEC)
	       * null(IAMD2)
	       * (1 - positif(IRE))
               * null(TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES + NAPCRP)
               * (1 - V_CNR)
               * (1 - LIG2501)
	       * (1 - LIGTROP)
	       * (1 - LIGTROPREST)
	       * (1 - positif(IMPRET - SEUIL_12))
	       * (1 - positif(NRINET - SEUIL_12))
	       * positif(1 + null(3 - INDIRPS))
	       * positif(V_NOTRAIT - 20)
               * LIG2 ;

regle 901680:
application :  iliad ;


LIGCBAIL = null(IDOM11 - DEC11 - RMENAGE)
            * (1 - positif(IAMD2))
	    * positif_ou_nul(TAXASSUR + IPCAPTAXTOT + TAXLOY + CHRAPRES + NAPCRP - SEUIL_61)
	    * (positif_ou_nul(NAPTIR - SEUIL_12) + positif_ou_nul(NAPCRP - SEUIL_61))
	    * positif_ou_nul(NAPTIR + NAPCRP - SEUIL_12)
	    * (1 - LIGNIDB)
	    * (1 - LIGTROP)
	    * (1 - LIGTROPREST)
	    * (1 - positif(IMPRET))
	    * (1 - positif(NRINET))
            * (1 - V_CNR)
            * LIG2 ;

LIGNITSUP = positif_ou_nul(TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES - SEUIL_61)
             * null(IDRS2-IDEC+IREP)
             * positif_ou_nul(TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET - SEUIL_12)
	     * (1 - LIG0TSUP)
             * (1 - V_CNR)
	     * (1 - LIGTROP)
	     * (1 - LIGTROPREST)
	     * positif(V_NOTRAIT - 20)
	     * (1 - positif(INDCTX))
             * LIG2 ;
                       
LIGNITDEG = positif(TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET)
             * positif_ou_nul(IRB2 - SEUIL_61)
             * positif_ou_nul(TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET - SEUIL_12)
             * null(INDNIRI) * (1 - positif(IAMD2))
             * positif(1 - V_CNR) * INDREV1A8
             * (1 - V_CNR)
	     * (1 - LIGTROP)
	     * (1 - LIGTROPREST)
	     * (1 - positif(IMPRET))
	     * positif(INDCTX)
             * LIG2 ;
                       
regle 901690:
application :  iliad ;

LIGNIDB = null(IDOM11 - DEC11-RMENAGE)
           * positif(SEUIL_61 - TAXASSUR - IPCAPTAXTOT - TAXLOY - CHRAPRES)
           * positif(SEUIL_61 - NAPCRP)
	   * positif(TAXASSUR + IPCAPTAXTOT + TAXLOY + CHRAPRES + NAPCRP)
           * null(IRNETBIS)
	   * (1 - positif(IRESTIT))
           * (1 - positif(IREP))
           * (1 - positif(IPROP))
	   * (1 - LIGTROP)
	   * (1 - LIGTROPREST)
	   * (1 - positif(NRINET))
	   * (1 - positif(IMPRET))
           * (1 - V_CNR)
           * LIG2 ;  

LIGREVSUP = INDREV1A8
	     * positif(REVFONC)
             * (1 - V_CNR)
	     * (1 - LIGTROP)
	     * (1 - LIGTROPREST)
	     * (1 - positif(IMPRET))
	     * positif(V_NOTRAIT - 20)
	     * (1 - positif(INDCTX))
             * LIG2 ;  

LIGREVDEG = INDREV1A8
	     * positif(REVFONC)
             * (1 - V_CNR)
	     * (1 - LIGTROP)
	     * (1 - LIGTROPREST)
	     * (1 - positif(IMPRET))
	     * positif(INDCTX)
             * LIG2 ;  

regle 901700:
application :  iliad ;

LIG0TSUP = INDNIRI
            * null(IRNETBIS)
            * positif_ou_nul(TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES - SEUIL_61)
            * (1 - positif(IREP))
            * (1 - positif(IPROP))
            * (1 - V_CNR)
	    * (1 - LIGTROP)
	    * (1 - LIGTROPREST)
	    * positif(V_NOTRAIT - 20)
	    * (1 - positif(INDCTX))
            * LIG2 ;

LIG0TDEG = INDNIRI
            * null(IRNETBIS)
            * positif_ou_nul(TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES - SEUIL_61)
            * (1 - positif(IREP))
            * (1 - positif(IPROP))
            * (1 - V_CNR)
	    * (1 - LIGTROP)
	    * (1 - LIGTROPREST)
	    * positif(INDCTX)
            * LIG2 ;

regle 901710:
application :  iliad ;


LIGPSNIR = positif(IAVIM) 
           * positif(SEUIL_61 - IAVIM) 
           * positif(SEUIL_61 - (NAPTIR + V_ANTREIR))
           * positif_ou_nul(NAPCRP - SEUIL_61)
           * (positif(IINET) * positif(20 - V_NOTRAIT) + positif(V_NOTRAIT - 20)) 
           * (1 - V_CNR)
           * (1 - LIGNIDB)
	   * (1 - LIGTROP)
	   * (1 - LIGTROPREST)
	   * (1 - positif(IMPRET))
	   * (1 - positif(NRINET))
           * LIG2 ;

LIGIRNPS = positif((positif_ou_nul(IAVIM - SEUIL_61) * positif_ou_nul(NAPTIR - SEUIL_12)) * positif(IAMD2)
                   + positif(IRESTIT + 0))
           * positif(SEUIL_61 - NAPCRP)
           * positif(NAPCRP)
           * (1 - V_CNR)
           * (1 - LIGNIDB)
	   * (1 - LIGTROP)
	   * (1 - LIGTROPREST)
	   * (1 - positif(IMPRET))
	   * (1 - positif(NRINET))
           * LIG2 ;

LIG400DEG = positif(IAVIM + NAPCRPAVIM)
  	     * positif (SEUIL_61 - (IAVIM + NAPCRPAVIM))
	     * null(ITRED)
	     * positif (IRNET)
	     * (1 - positif(IRNET + V_ANTREIR - SEUIL_61))
             * (1 - V_CNR)
	     * (1 - LIGTROP)
	     * (1 - LIGTROPREST)
	     * (1 - positif(IMPRET - SEUIL_12))
	     * (1 - positif(NRINET - SEUIL_12))
	     * positif(V_NOTRAIT - 20)
             * LIG2 ;

regle 901720:
application :  iliad ;


LIG61DEG = positif(ITRED)
	    * positif(IAVIM)  
            * positif(SEUIL_61 - IAVIM)
            * (1 - positif(INDNMR2))
            * (1 - V_CNR)
	    * (1 - LIGTROP)
	    * (1 - LIGTROPREST)
	    * (1 - positif(IMPRET))
            * positif(INDCTX)
            * LIG2 ;

regle 901730:
application :  iliad ;
	

LIGAUCUN = positif((positif(SEUIL_61 - IAVIM) * positif(IAVIM) * (1 - positif(NAPCRP))
                    + positif_ou_nul(IAVIM - SEUIL_61) * positif(NAPTIR) * positif(SEUIL_12 - NAPTIR) * (1 - positif(NAPCRP))
                    + positif(SEUIL_61 - NAPCRP) * positif(NAPCRP) * positif(SEUIL_61 - IAVIM)
                    + (positif_ou_nul(IAVIM - SEUIL_61) + positif_ou_nul(NAPCRP - SEUIL_61)) * positif(NAPCRP) * positif(SEUIL_12 - IRPSCUM))
	           * (1 - positif(IREST))
                   * (1 - LIGNIDB)
	           * (1 - LIGTROP)
	           * (1 - LIGTROPREST)
	           * (1 - positif(IMPRET))
	           * (1 - positif(NRINET))
                   * (1 - V_CNR)
	           * positif(20 - V_NOTRAIT) 
	           * LIG2
                  ) ;

regle 901740:
application :  iliad ;


LIG12ANT = positif (IRANT)
            * positif (SEUIL_12 - TOTNET )
	    * positif( TOTNET)
	    * (1 - LIGTROP)
	    * (1 - LIGTROPREST)
	    * (1 - positif(V_CNR + (1 - V_CNR) * positif(NRINET-SEUIL_12)))
	    * (1 - positif(IMPRET - SEUIL_12)) 
	    * (1 - positif(NRINET - SEUIL_12))
	    * positif(20 - V_NOTRAIT)
            * LIG2 ; 

regle 901750:
application :  iliad ;

LIG12NMR = positif(IRPSCUM)
            * positif(SEUIL_12 - IRPSCUM)
	    * positif(V_NOTRAIT - 20)
            * (1 - V_CNR)
	    * (1 - positif(IMPRET - SEUIL_12)) 
	    * (1 - positif(NRINET - SEUIL_12)) ;

regle 901760:
application :  iliad ;

LIGNIIRAF = null(IAD11)
             * positif(IRESTIT)
             * (1 - positif(INDNIRI))
             * (1 - positif(IREP))
             * (1 - positif(IPROP))
             * (1 - positif_ou_nul(NAPTIR))
	     * (1 - LIGTROP)
	     * (1 - LIGTROPREST)
	     * positif(1 + null(3 - INDIRPS))
             * LIG2 ;

regle 901770:
application :  iliad ;

LIGNIDEG = null(IDRS3-IDEC)
	    * null(IAMD2)
	    * positif(SEUIL_61 - TAXASSUR)
	    * positif(SEUIL_61 - IPCAPTAXT)
	    * positif(SEUIL_61 - TAXLOY)
	    * positif(SEUIL_61 - CHRAPRES)
            * positif(SEUIL_12 - IRNET)
            * (1 - V_CNR)
	    * (1 - LIGDEG61)
	    * (1 - LIGTROP)
	    * (1 - LIGTROPREST)
	    * positif(INDCTX)
            * LIG2 ;

regle 901780:
application :  iliad ;

LIGDEG61 = positif (IRNETBIS)
            * positif (SEUIL_61 - IAMD1) 
            * positif (SEUIL_61 - NRINET) 
	    * positif (IAMD2)
	    * (1 - LIG61DEG)
            * (1 - V_CNR)
	    * (1 - LIGTROP)
	    * (1 - LIGTROPREST)
	    * (1 - positif(IMPRET))
            * positif (INDCTX)
            * LIG2 ;

regle 901790:
application :  iliad ;

LIGDEG12 = positif (IRNET + TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET)
            * positif (SEUIL_12 - IRNET - TAXANET - PCAPNET - TAXLOYNET - HAUTREVNET)
            * (1 - V_CNR)
            * (1 - LIGNIDEG)
            * (1 - LIGDEG61)
            * (1 - LIG61DEG)
	    * (1 - LIGTROP)
	    * (1 - LIGTROPREST)
            * (1 - positif(IMPRET))
	    * positif(INDCTX)
            * LIG2 ;

regle 901800:
application :  iliad ;

LIGDIPLOI = positif(INDREV1A8)
             * positif(null(NATIMP - 1) + positif(NAPTEMP))
             * positif(REVFONC) * (1 - V_CNR)
	     * (1 - LIGTROP)
	     * (1 - LIGTROPREST)
             * LIG12 ;

regle 901810:
application :  iliad ;

LIGDIPLONI = positif(INDREV1A8)
              * positif(null(NATIMP) + positif(IREST) + (1 - positif(NAPTEMP)))
              * positif(REVFONC) * (1 - V_CNR)
	      * (1 - LIGTROP)
	      * (1 - LIGTROPREST)
	      * (1 - LIGDIPLOI)
              * LIG12 ;

regle 901820:
application :  iliad ;

LIG2355 = positif (
		   IND_NI * (1 - positif(V_ANTRE)) + INDNMR1 + INDNMR2
                   + positif(NAT1BIS) *  null(NAPT) * (positif (IRNET + TAXANET + PCAPNET + TAXLOYNET + HAUTREVNET))
		   + positif(SEUIL_12 - (IAN + RPEN - IAVT + TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES - IRANT))
				 * positif_ou_nul(IAN + RPEN - IAVT + TAXASSUR + IPCAPTAXT + TAXLOY + CHRAPRES - IRANT) 
                  )
          * positif(INDREV1A8)
          * (1 - null(NATIMP - 1) + null(NATIMP - 1) * positif(IRANT))  
	  * (1 - LIGPS)
	  * LIG2 ;

regle 901830:
application :  iliad ;

LIG7CY = positif(COD7CY) * LIG12 ;
LIG7DY = positif(COD7DY) * LIG12 ;
LIG7EY = positif(COD7EY) * LIG12 ;
LIG7FY = positif(COD7FY) * LIG12 ;
LIG7GY = positif(COD7GY) * LIG12 ;
LIGRVG = positif(CODRVG + CODRUA) * LIG12 ;

regle 901840:
application :  iliad ;

LIG2380 = si (NATIMP=0 ou NATIMP=21 ou NATIMP=70 ou NATIMP=91)
          alors (IND_SPR * positif_ou_nul(V_8ZT + CODZRE + CODZRF - RBG1) * positif(V_8ZT + CODZRE + CODZRF)
                * (1 - present(BRAS)) * (1 - present(IPSOUR))
                * V_CNR * LIG2)
          finsi ;

regle 901850:
application :  iliad ;

LIG2383 = si ((IAVIM+NAPCRPAVIM)<=(IPSOUR * LIG1 ))
          alors ( positif(RBG1 - V_8ZT + CODZRE + CODZRF) * present(IPSOUR) 
                * V_CNR * LIG2)
          finsi ;

regle 901860:
application : iliad  ;

LIG2501 = (1 - positif(IND_REV)) * (1 - V_CNR) * LIG2 ;

LIG25012 = (1 - positif(IND_REV)) * V_CNR * LIG2 ;

LIG8FV = positif(REVFONC) * (1 - INDREV1A8) ;

regle 901870:
application :  iliad ;

LIG2503 = (1 - positif(IND_REV))
          * (1 - positif_ou_nul(IND_TDR))
          * LIG2
          * (1 - V_CNR) ;

regle 901890:
application :  iliad ;

LIG3700 = positif(LIG4271 + LIG3710 + LIG3720 + LIG3730) * LIG1 ;

regle 901900:
application :  iliad ;

LIG4271 = positif(V_0AB) * LIG12 ;

LIG3710 = positif(20 - V_NOTRAIT) * positif(BOOL_0AZ) * LIG12;

regle 901910:
application :  iliad ;

LIG3720 = (1 - positif(20 - V_NOTRAIT)) * (1 - LIG3730) * LIG12 ;

regle 901920:
application :  iliad ;

LIG3730 = (1 - positif(20 - V_NOTRAIT)) * positif(BOOL_0AZ) * LIG12 ;

regle 901930:
application :  iliad ;

LIG3740 = positif(INDTXMIN) * positif(IND_REV) * LIG12 ;

regle 901940:
application :  iliad ;

LIG3750 = present(V_ZDC) * null(abs(V_ZDC - 1)) * positif(IREST) * LIG12 ;

regle 901950:
application : iliad  ;

LIGPRR2 = positif(PRR2V + PRR2C + PRR2P + PRR2ZV + PRR2ZC + PRR2Z1 + PRR2Z2 + PRR2Z3 + PRR2Z4 
                  + PRR2RAL + PRR2RAM + PRR2RBL + PRR2RBM + PRR2RCL + PRR2RCM + PRR2RDL + PRR2RDM 
		  + PRR2REL + PRR2REM + PRR2RFL + PRR2RFM + PENALIMV + PENALIMC + PENALIMP + 0) * LIG2 ;

regle 901990:
application :  iliad ;

LIG062V = CARPEV + CARPENBAV + PENSALV + PENSALNBV + CODRAZ + CODRAL + CODRAM ;
LIG062C = CARPEC + CARPENBAC + PENSALC + PENSALNBC + CODRBZ + CODRBL + CODRBM ;
LIG062P = somme(i=1..4: CARPEPi + CARPENBAPi) + somme(i=1..4: PENSALPi + PENSALNBPi) + CODRCZ + CODRDZ + CODREZ + CODRFZ 
          + CODRCL + CODRCM  + CODRDL + CODRDM  + CODREL + CODREM  + CODRFL + CODRFM ;

regle 902000:
application :  iliad ;

LIG066 = somme(i=1..4:PEBFi);

regle 902020:
application :  iliad ;

TSDECV = TSHALLOV + COD1PM + COD1TP + COD1NX + COD1AF + COD1AG ;
TSDECC = TSHALLOC + COD1QM + COD1UP + COD1OX + COD1BF + COD1BG ;
TSDECP = TSHALLO1 + TSHALLO2 + TSHALLO3 + TSHALLO4 + COD1CF + COD1CG
         + COD1DF + COD1DG + COD1EF + COD1EG + COD1FF + COD1FG ;

LIG_SAL = positif_ou_nul(TSDECV + TSDECC + TSDECP) * LIG0  * LIG2 ;

LIG_REVASS = positif_ou_nul(ALLOV + ALLOC + ALLOP) * LIG0  * LIG2 ;

LIGREVAC = positif_ou_nul(COD1GB + COD1HB + REVACPAC) * LIG0  * LIG2 ;

LIGREVEMP = positif_ou_nul(COD1AA + COD1BA + REVEMPAC) * LIG0  * LIG2 ;

LIGREVAAS = positif_ou_nul(COD1GF + COD1HF + REVAAPAC) * LIG0  * LIG2 ;

LIG_SALASS = positif(TSBNV + TSBNC + TSBNP + F10AV + F10AC + F10AP)
             * positif_ou_nul(LIG_SAL + LIG_REVASS + LIGREVAC + LIGREVEMP + LIGREVAAS - 2)
             * LIG0 * LIG2 ;

LIG_GATASA = positif_ou_nul(BPCOSAV + BPCOSAC + GLDGRATV + GLDGRATC) * LIG0 * LIG2 ;

LIGF10V = positif(F10AV + F10BV) * LIG0 * LIG2 ;

LIGF10C = positif(F10AC + F10BC) * LIG0 * LIG2 ;

LIGF10P = positif(F10AP + F10BP) * LIG0 * LIG2 ;

PENSDECV = PRBRV + COD1AL + COD1AM ;
PENSDECC = PRBRC + COD1BL + COD1BM ;
PENSDECP = PRBR1 + PRBR2 + PRBR3 + PRBR4 + COD1CL + COD1CM
           + COD1DL + COD1DM + COD1EL + COD1EM + COD1FL + COD1FM ;

LIGPENS = positif(PENSDECV + PENSDECC + PENSDECP) * LIG0 * LIG2 ;

LIGPENSQUO = positif(PRQVO + PRQCJ + PRQPC) * LIG0 * LIG2 ;

LIGINVQUO = positif(PRQZV + PRQZC + PRQZP) * LIG0 * LIG2 ;

LIGALIQUO = positif(PENSALV + PENSALC + PENSALP) * LIG0 * LIG2 ;

LIGFOOTQUO = positif(PEBFV + PEBFC + LIG066) * LIG0 * LIG2 ;

regle 902030:
application :  iliad ;


LIGRCMABT = positif(positif(COD2OP) * (present(RCMABD) + present(RCMTNC) + present(RCMAV) + present(RCMHAD) + present(RCMHAB) 
                                       + present(REGPRIV) + present(RCMFR) + present(DEFRCM) + present(DEFRCM2) + present(DEFRCM3) 
				       + present(DEFRCM4) + present(DEFRCM5) + present(DEFRCM6) + present(COD2TT) + present(COD2VV) 
				       + present(COD2WW) + present(COD2YY) + present(COD2ZZ) + present(COD2VN) + present(COD2VO) 
				       + present(COD2VP)) 
		    + (1 - positif(COD2OP)) * (present(RCMAV) + present(COD2YY) + present(COD2VN)))
             * positif(INDREV1A8) * LIG12 ;

LIG2RCMABT = positif(present(REVACT) + present(REVPEA) + present(PROVIE) + present(DISQUO) + present(RESTUC) + present(INTERE))
                * positif(INDREV1A8) * LIG12 ;

LIGPV3VG = positif(PVBAR3VG) * positif(INDREV1A8) * LIG12 ;

LIGPV3WB = positif(PVBAR3WB) * positif(INDREV1A8) * LIG12 ;

LIGPVMTS = positif(PVMTS) * LIG12 ;

regle 902040:
application :  iliad ;


LIG_REPORT = positif(LIGRNIDF + LIGDFRCM + LIGDRFRP + LIGDEFBA
                     + LIGDLMRN + LIGDEFPLOC + LIGBNCDF + LIG2TUV + LIGR2VQ + LIGDRCVM
		     + LIGBAMVV + LIGBAMVC + LIGBAMVP
                     + somme(i=V,C,P:LIGMIBDREPi + LIGMBDREPNPi + LIGSPEDREPi + LIGSPDREPNPi)
                     + LIGREPREPAR + LIGREPREST

                     + LIGRCELZV + LIGRCELZEF + LIGRCELZIJ + LIGRCELSEF + LIGRCELSIJ + LIGRCELSM
	             + LIGRRCEL1 + LIGRRCEL2 + LIGRRCEL3 + LIGRRCEL4 + LIGRRCEL5 + LIGRRCEL6 
		     + LIGRRCEL7 + LIGRRCELG + LIGRRCELH

                     + LIGRCODOY + LIGRCODOX + LIGRCODOW + LIGRCODOV + LIGRCODOU + LIGRCODJT 
                     + LIGRLOCIDFG + LIGNEUV + LIGVIEU
                     + LIGREPLOC15 + LIGREPLOC12 + LIGREPLOC11 + LIGREPLOC10 + LIGREPLOC9                      
	             + LIGRDUFLOTOT + LIGRPINELTOT
                     + LIGCOMP01

	             + LIGREPQKG + LIGREPQUS + LIGREPQWB + LIGREPRXC + LIGREPXIO
	             + LIGREPPEK + LIGREPPFL + LIGREPPHO + LIGREPPIZ + LIGREPCCS 
                     + LIGREPPJA + LIGREPCD + LIGREPPLB
	             + LIGREPRUP + LIGREPRVQ + LIGREPRWR + LIGREPRYT 
                     + LIGREPSAA + LIGREPSAB + LIGREPSAC + LIGREPSAE + LIGREPSAF
                     + LIGREPSAG + LIGREPSAH + LIGREPSAJ + LIGREPSAM 
                     + LIGREPTBE + LIGREPSAU + LIGREPSAV + LIGREPSAW + LIGREPSAY
                     + LIGREPBX + LIGREPBY + LIGREPBZ + LIGREPCB 

                     + LIGREPCORSE + LIGPME + LIGRSN + LIGPLAFRSN + LIGREPDON
                     + LIGNFOREST + LIGRCIF + LIGRCIFAD + LIGRCIFSIN + LIGRCIFADSN
                     + LIGPATNATR + LIGREPRECH + LIGREPCICE
	             ) * LIG2 ;

regle 902050:
application : iliad  ;

LIGRNIDF = positif(abs(RNIDF)) * LIG12 ;
LIGRNIDF0 = positif(abs(RNIDF0)) * positif(positif(abs(RNIDF))+positif(FLAGRETARD08+FLAGDEFAUT11)) * LIG12 ;
LIGRNIDF1 = positif(abs(RNIDF1)) * positif(positif(abs(RNIDF))+positif(FLAGRETARD08+FLAGDEFAUT11)) * LIG12 ;
LIGRNIDF2 = positif(abs(RNIDF2)) * positif(positif(abs(RNIDF))+positif(FLAGRETARD08+FLAGDEFAUT11)) * LIG12 ;
LIGRNIDF3 = positif(abs(RNIDF3)) * positif(positif(abs(RNIDF))+positif(FLAGRETARD08+FLAGDEFAUT11)) * LIG12 ;
LIGRNIDF4 = positif(abs(RNIDF4)) * positif(positif(abs(RNIDF))+positif(FLAGRETARD08+FLAGDEFAUT11)) * LIG12 ;
LIGRNIDF5 = positif(abs(RNIDF5)) * positif(positif(abs(RNIDF))+positif(FLAGRETARD08+FLAGDEFAUT11)) * LIG12 ;

regle 902060:
application : iliad  ;

LIGDUFREP = positif(DDUFREP) * LIG12 ;

LIGDUFLO = positif(DDUFLO) * LIG12 ;

LIGPIREP = positif(DPIREP) * LIG12 ;

LIGPINEL = positif(DPINEL) * LIG12 ;

LIGDUFTOT = LIGDUFREP + LIGDUFLO ; 

LIGPINTOT = LIGPIREP + LIGPINEL ;

regle 902070:
application : iliad  ;

LIGRDUEKL = positif(RIVDUEKL) * LIG12 ;
LIGRDUEKL1 = LIGRDUEKL * null(RIVDUEKL - RIVDUEKL8) ;
LIGRDUEKL2 = LIGRDUEKL * (1 - null(RIVDUEKL - RIVDUEKL8)) ;

LIGRPIQB = positif(RIVPIQB) * LIG12 ;
LIGRPIQB1 = LIGRPIQB * null(RIVPIQB - RIVPIQB8) ;
LIGRPIQB2 = LIGRPIQB * (1 - null(RIVPIQB - RIVPIQB8)) ;

LIGRPIQD = positif(RIVPIQD) * LIG12 ;
LIGRPIQD1 = LIGRPIQD * null(RIVPIQD - RIVPIQD8) ;
LIGRPIQD2 = LIGRPIQD * (1 - null(RIVPIQD - RIVPIQD8)) ;

LIGRPIQA = positif(RIVPIQA) * LIG12 ;
LIGRPIQA1 = LIGRPIQA * null(RIVPIQA - RIVPIQA5) ;
LIGRPIQA2 = LIGRPIQA * (1 - null(RIVPIQA - RIVPIQA5)) ;

LIGRPIQC = positif(RIVPIQC) * LIG12 ;
LIGRPIQC1 = LIGRPIQC * null(RIVPIQC - RIVPIQC5) ;
LIGRPIQC2 = LIGRPIQC * (1 - null(RIVPIQC - RIVPIQC5)) ;

LIGRPIQN = positif(RIVPIQN) * LIG12 ;
LIGRPIQN1 = LIGRPIQN * null(RIVPIQN - RIVPIQN8) ;
LIGRPIQN2 = LIGRPIQN * (1 - null(RIVPIQN - RIVPIQN8)) ;

LIGRPIQO = positif(RIVPIQO) * LIG12 ;
LIGRPIQO1 = LIGRPIQO * null(RIVPIQO - RIVPIQO5) ;
LIGRPIQO2 = LIGRPIQO * (1 - null(RIVPIQO - RIVPIQO5)) ;

LIGRPIQM = positif(RIVPIQM) * LIG12 ;
LIGRPIQM1 = LIGRPIQM * null(RIVPIQM - RIVPIQM5) ;
LIGRPIQM2 = LIGRPIQM * (1 - null(RIVPIQM - RIVPIQM5)) ;

LIGRPIQU = positif(RIVPIQU) * LIG12 ;
LIGRPIQU1 = LIGRPIQU * null(RIVPIQU - RIVPIQU8) ;
LIGRPIQU2 = LIGRPIQU * (1 - null(RIVPIQU - RIVPIQU8)) ;

LIGRPIQS = positif(RIVPIQS) * LIG12 ;
LIGRPIQS1 = LIGRPIQS * null(RIVPIQS - RIVPIQS8) ;
LIGRPIQS2 = LIGRPIQS * (1 - null(RIVPIQS - RIVPIQS8)) ;

LIGRPIQT = positif(RIVPIQT) * LIG12 ;
LIGRPIQT1 = LIGRPIQT * null(RIVPIQT - RIVPIQT5) ;
LIGRPIQT2 = LIGRPIQT * (1 - null(RIVPIQT - RIVPIQT5)) ;

LIGRPIQR = positif(RIVPIQR) * LIG12 ;
LIGRPIQR1 = LIGRPIQR * null(RIVPIQR - RIVPIQR5) ;
LIGRPIQR2 = LIGRPIQR * (1 - null(RIVPIQR - RIVPIQR5)) ;

LIGRPIQP = positif(RIVPIQP) * LIG12 ;
LIGRPIQP1 = LIGRPIQP * null(RIVPIQP - RIVPIQP8) ;
LIGRPIQP2 = LIGRPIQP * (1 - null(RIVPIQP - RIVPIQP8)) ;

LIGRPIQL = positif(RIVPIQL) * LIG12 ;
LIGRPIQL1 = LIGRPIQL * null(RIVPIQL - RIVPIQL8) ;
LIGRPIQL2 = LIGRPIQL * (1 - null(RIVPIQL - RIVPIQL8)) ;

LIGRPIQJ = positif(RIVPIQJ) * LIG12 ;
LIGRPIQJ1 = LIGRPIQJ * null(RIVPIQJ - RIVPIQJ8) ;
LIGRPIQJ2 = LIGRPIQJ * (1 - null(RIVPIQJ - RIVPIQJ8)) ;

LIGRPIQK = positif(RIVPIQK) * LIG12 ;
LIGRPIQK1 = LIGRPIQK * null(RIVPIQK - RIVPIQK5) ;
LIGRPIQK2 = LIGRPIQK * (1 - null(RIVPIQK - RIVPIQK5)) ;

LIGRPIQI = positif(RIVPIQI) * LIG12 ;
LIGRPIQI1 = LIGRPIQI * null(RIVPIQI - RIVPIQI5) ;
LIGRPIQI2 = LIGRPIQI * (1 - null(RIVPIQI - RIVPIQI5)) ;

LIGRPIQH = positif(RIVPIQH) * LIG12 ;
LIGRPIQH1 = LIGRPIQH * null(RIVPIQH - RIVPIQH8) ;
LIGRPIQH2 = LIGRPIQH * (1 - null(RIVPIQH - RIVPIQH8)) ;

LIGRPIQF = positif(RIVPIQF) * LIG12 ;
LIGRPIQF1 = LIGRPIQF * null(RIVPIQF - RIVPIQF8) ;
LIGRPIQF2 = LIGRPIQF * (1 - null(RIVPIQF - RIVPIQF8)) ;

LIGRPIQG = positif(RIVPIQG) * LIG12 ;
LIGRPIQG1 = LIGRPIQG * null(RIVPIQG - RIVPIQG5) ;
LIGRPIQG2 = LIGRPIQG * (1 - null(RIVPIQG - RIVPIQG5)) ;

LIGRPIQE = positif(RIVPIQE) * LIG12 ;
LIGRPIQE1 = LIGRPIQE * null(RIVPIQE - RIVPIQE5) ;
LIGRPIQE2 = LIGRPIQE * (1 - null(RIVPIQE - RIVPIQE5)) ;

LIGRDUGIH = positif(RIVDUGIH) * LIG12 ;
LIGRDUGIH1 = LIGRDUGIH * null(RIVDUGIH - RIVDUGIH8) ;
LIGRDUGIH2 = LIGRDUGIH * (1 - null(RIVDUGIH - RIVDUGIH8)) ;

LIGRDUFLOTOT = positif(LIGRDUEKL + LIGRDUGIH) ;
LIGRPINELTOT = positif(LIGRPIQA + LIGRPIQB + LIGRPIQC + LIGRPIQD + LIGRPIQE + LIGRPIQF + LIGRPIQG + LIGRPIQH 
                       + LIGRPIQI + LIGRPIQJ + LIGRPIQK + LIGRPIQL + LIGRPIQM + LIGRPIQN + LIGRPIQO + LIGRPIQP 
	               + LIGRPIQU + LIGRPIQS + LIGRPIQT + LIGRPIQR) ;

regle 902080:
application : iliad  ;

LIGCELSOM1 = positif(DCELSOM1) * LIG12 ;

LIGCELSOM2 = positif(DCELSOM2) * LIG12 ;

LIGCELSOM3 = positif(DCELSOM3) * LIG12 ;

LIGCELSOM4 = positif(DCELSOM4) * LIG12 ;

LIGCELSOM56 = positif(DCELSOM5 + DCELSOM6) * LIG12 ;

LIGCELSOM7 = positif(DCELSOM7) * LIG12 ;

regle 902090:
application : iliad  ;


LIGRCELZV = positif(RIVCELZV1) * CNRLIG12 ;
LIGZV1 = LIGRCELZV * null(RIVCELZV1 - RIVCELZV3) ;
LIGZV2 = LIGRCELZV * (1 - null(RIVCELZV1 - RIVCELZV3)) ;

LIGRCELZEF = positif(RIVCELZEF1) * CNRLIG12 ;
LIGZEF1 = LIGRCELZEF * null(RIVCELZEF1 - RIVCELZEF3) ;
LIGZEF2 = LIGRCELZEF * (1 - null(RIVCELZEF1 - RIVCELZEF3)) ;

LIGRCELZIJ = positif(RIVCELZIJ1) * CNRLIG12 ;
LIGZIJ1 = LIGRCELZIJ * null(RIVCELZIJ1 - RIVCELZIJ3) ;
LIGZIJ2 = LIGRCELZIJ * (1 - null(RIVCELZIJ1 - RIVCELZIJ3)) ;

LIGRCELSEF = positif(RIVCELSEF1) * CNRLIG12 ;
LIGSEF1 = LIGRCELSEF * null(RIVCELSEF1 - RIVCELSEF3) ;
LIGSEF2 = LIGRCELSEF * (1 - null(RIVCELSEF1 - RIVCELSEF3)) ;

LIGRCELSIJ = positif(RIVCELSIJ1) * CNRLIG12 ;
LIGSIJ1 = LIGRCELSIJ * null(RIVCELSIJ1 - RIVCELSIJ3) ;
LIGSIJ2 = LIGRCELSIJ * (1 - null(RIVCELSIJ1 - RIVCELSIJ3)) ;

LIGRCELSM = positif(RIVCELSM1) * CNRLIG12 ;
LIGSM1 = LIGRCELSM * null(RIVCELSM1 - RIVCELSM3) ;
LIGSM2 = LIGRCELSM * (1 - null(RIVCELSM1 - RIVCELSM3)) ;



LIGRRCEL1 = positif(RRCELMG + RRCELMH + RRCELLJ + RRCELLP + RRCELLV + RRCELLY + RRCEL2012) * CNRLIG12 ;
LIGRCELMG = positif(RRCELMG) * CNRLIG12 ;
LIGRCELMH = positif(RRCELMH) * CNRLIG12 ;
LIGRCELLJ = positif(RRCELLJ) * CNRLIG12 ;
LIGRCELLP = positif(RRCELLP) * CNRLIG12 ;
LIGRCELLV = positif(RRCELLV) * CNRLIG12 ;
LIGRCELLY = positif(RRCELLY) * CNRLIG12 ;
LIGRRCEL11 = positif(RRCEL2012) * CNRLIG12 ;

LIGRRCEL2 = positif(RRCELLZ + RRCELLX + RRCELLI + RRCELLO + RRCELLU + RRCELLC + RRCEL2011) * CNRLIG12 ;
LIGRCELLZ = positif(RRCELLZ) * CNRLIG12 ;
LIGRCELLX = positif(RRCELLX) * CNRLIG12 ;
LIGRCELLI = positif(RRCELLI) * CNRLIG12 ;
LIGRCELLO = positif(RRCELLO) * CNRLIG12 ;
LIGRCELLU = positif(RRCELLU) * CNRLIG12 ;
LIGRCELLC = positif(RRCELLC) * CNRLIG12 ;
LIGRRCEL22 = positif(RRCEL2011) * CNRLIG12 ;

LIGRRCEL3 = positif(RRCELLS + RRCELLT + RRCELLH + RRCELLL + RRCELLR + RRCELLB + RRCEL2010) * CNRLIG12 ;
LIGRCELLS = positif(RRCELLS) * CNRLIG12 ;
LIGRCELLT = positif(RRCELLT) * CNRLIG12 ;
LIGRCELLH = positif(RRCELLH) * CNRLIG12 ;
LIGRCELLL = positif(RRCELLL) * CNRLIG12 ;
LIGRCELLR = positif(RRCELLR) * CNRLIG12 ;
LIGRCELLB = positif(RRCELLB) * CNRLIG12 ;
LIGRRCEL33 = positif(RRCEL2010) * CNRLIG12 ;

LIGRRCEL4 = positif(RRCELLM + RRCELLN + RRCELLG + RRCELLK + RRCELLQ + RRCELLA + RRCEL2009) * CNRLIG12 ;
LIGRCELLM = positif(RRCELLM) * CNRLIG12 ;
LIGRCELLN = positif(RRCELLN) * CNRLIG12 ;
LIGRCELLG = positif(RRCELLG) * CNRLIG12 ;
LIGRCELLK = positif(RRCELLK) * CNRLIG12 ;
LIGRCELLQ = positif(RRCELLQ) * CNRLIG12 ;
LIGRCELLA = positif(RRCELLA) * CNRLIG12 ;
LIGRRCEL44 = positif(RRCEL2009) * CNRLIG12 ;

LIGRRCEL5 = positif(RRCELZP + RRCELZRS + RRCELE) * CNRLIG12 ;
LIGRCELZP = positif(RRCELZP) * CNRLIG12 ;
LIGRCELZRS = positif(RRCELZRS) * CNRLIG12 ;
LIGRRCELE = positif(RRCELE) * CNRLIG12 ;

LIGRRCEL6 = positif(RRCELZO + RRCELZQT + RRCELF) * CNRLIG12 ;
LIGRCELZO = positif(RRCELZO) * CNRLIG12 ;
LIGRCELZQT = positif(RRCELZQT) * CNRLIG12 ;
LIGRRCELF = positif(RRCELF) * CNRLIG12 ;

LIGRRCEL7 = positif(RRCELZUQ + RRCELG) * CNRLIG12 ;
LIGRCELZUQ = positif(RRCELZUQ) * CNRLIG12 ;
LIGRRCELG = positif(RRCELG) * CNRLIG12 ;

LIGRRCELH = positif(RRCELH) * CNRLIG12 ;

regle 902100:
application : iliad  ;


LIGPATNAT3 = (positif(PATNAT3) + null(PATNAT3) * positif(V_NOTRAIT - 20)) * LIG12 ;
LIGPATNAT4 = (positif(PATNAT4) + null(PATNAT4) * positif(V_NOTRAIT - 20)) * LIG12 ;

LIGPATNATR = positif(REPNATR + REPNATR1 + REPNATR2 + REPNATR3) * LIG12 ; 

regle 902110:
application : iliad  ;


LIGREPXE = positif(REPXE) * CNRLIG12 ;
LIGREPXK = positif(REPXK) * CNRLIG12 ;
LIGREPXP = positif(REPXP) * CNRLIG12 ;
LIGREPXU = positif(REPXU) * CNRLIG12 ;
LIGREPYB = positif(REPYB) * CNRLIG12 ;
LIGREPQKG = positif(REPXE + REPXK + REPXP + REPXU + REPYB) * CNRLIG12 ;

LIGREPXA = positif(REPXA) * CNRLIG12 ;
LIGREPXF = positif(REPXF) * CNRLIG12 ;
LIGREPXL = positif(REPXL) * CNRLIG12 ;
LIGREPXQ = positif(REPXQ) * CNRLIG12 ;
LIGREPQUS = positif(REPXA + REPXF + REPXL + REPXQ) * CNRLIG12 ;

LIGREPXB = positif(REPXB) * CNRLIG12 ;
LIGREPXG = positif(REPXG) * CNRLIG12 ;
LIGREPXM = positif(REPXM) * CNRLIG12 ;
LIGREPXR = positif(REPXR) * CNRLIG12 ;
LIGREPQWB = positif(REPXB + REPXG + REPXM + REPXR) * CNRLIG12 ;

LIGREPXC = positif(REPXC) * CNRLIG12 ;
LIGREPXH = positif(REPXH) * CNRLIG12 ;
LIGREPXN = positif(REPXN) * CNRLIG12 ;
LIGREPXS = positif(REPXS) * CNRLIG12 ;
LIGREPRXC = positif(REPXC + REPXH + REPXN + REPXS) * CNRLIG12 ;

LIGREPXI = positif(REPXI) * CNRLIG12 ;
LIGREPXO = positif(REPXO) * CNRLIG12 ;
LIGREPXT = positif(REPXT) * CNRLIG12 ;
LIGREPYA = positif(REPYA) * CNRLIG12 ;
LIGREPXIO = positif(REPXI + REPXO + REPXT + REPYA) * CNRLIG12 ;

LIGREPAK = positif(REPAK) * CNRLIG12 ;
LIGREPBI = positif(REPBI) * CNRLIG12 ;
LIGREPPEK = positif(REPAK + REPBI) * CNRLIG12 ;

LIGREPAL = positif(REPAL) * CNRLIG12 ;
LIGREPBJ = positif(REPBJ) * CNRLIG12 ;
LIGREPPFL = positif(REPAL + REPBJ) * CNRLIG12 ;

LIGREPAO = positif(REPAO) * CNRLIG12 ;
LIGREPBM = positif(REPBM) * CNRLIG12 ;
LIGREPPHO = positif(REPAO + REPBM) * CNRLIG12 ;

LIGREPPIZ = positif(REPBA) * CNRLIG12 ;

LIGREPCC = positif(REPCC) * CNRLIG12 ;
LIGREPCS = positif(REPCS) * CNRLIG12 ;
LIGREPDS = positif(REPDS) * CNRLIG12 ;
LIGREPES = positif(REPES) * CNRLIG12 ;
LIGREPCCS = positif(REPCC + REPCS + REPDS + REPES) * CNRLIG12 ;

LIGREPPJA = positif(REPBB) * CNRLIG12 ;

LIGREPCD = positif(REPCD) * CNRLIG12 ;
LIGREPCT = positif(REPHCT) * CNRLIG12 ;
LIGREPDT = positif(REPDT) * CNRLIG12 ;
LIGREPET = positif(REPDT) * CNRLIG12 ;
LIGREPCDT = positif(REPCD + REPHCT + REPDT + REPET) * CNRLIG12 ;

LIGREPBG = positif(REPBG) * CNRLIG12 ;
LIGREPCG = positif(REPCG) * CNRLIG12 ;
LIGREPCW = positif(REPCW) * CNRLIG12 ;
LIGREPDW = positif(REPDW) * CNRLIG12 ;
LIGREPEW = positif(REPEW) * CNRLIG12 ;
LIGREPPLB = positif(REPBG + REPCG + REPCW + REPDW + REPEW) * CNRLIG12 ;

LIGREPAP = positif(REPAP) * CNRLIG12 ;
LIGREPBN = positif(REPBN) * CNRLIG12 ;
LIGREPRUP = positif(REPAP + REPBN) * CNRLIG12 ;

LIGREPAQ = positif(REPAQ) * CNRLIG12 ;
LIGREPBO = positif(REPBO) * CNRLIG12 ;
LIGREPRVQ = positif(REPAQ + REPBO) * CNRLIG12 ;

LIGREPHAR = positif(REPHAR) * CNRLIG12 ;
LIGREPBP = positif(REPBP) * CNRLIG12 ;
LIGREPRWR = positif(REPHAR + REPBP) * CNRLIG12 ;

LIGREPAT = positif(REPAT) * CNRLIG12 ;
LIGREPBR = positif(REPBR) * CNRLIG12 ;
LIGREPRYT = positif(REPAT + REPBR) * CNRLIG12 ;

LIGREPSAA = positif(REPAA) * CNRLIG12 ;

LIGREPSAB = positif(REPAB) * CNRLIG12 ;

LIGREPSAC = positif(REPAC) * CNRLIG12 ;

LIGREPSAE = positif(REPAE) * CNRLIG12 ;

LIGREPSAF = positif(REPAF) * CNRLIG12 ;

LIGREPSAG = positif(REPAG) * CNRLIG12 ;

LIGREPSAH = positif(REPAH) * CNRLIG12 ;

LIGREPSAJ = positif(REPAJ) * CNRLIG12 ;

LIGREPAM = positif(REPAM) * CNRLIG12 ;
LIGREPBK = positif(REPBK) * CNRLIG12 ;
LIGREPSAM = positif(REPAM + REPBK) * CNRLIG12 ;

LIGREPBE = positif(REPBE) * CNRLIG12 ;
LIGREPCE = positif(REPCE) * CNRLIG12 ;
LIGREPCU = positif(REPCU) * CNRLIG12 ;
LIGREPDU = positif(REPDU) * CNRLIG12 ;
LIGREPEU = positif(REPEU) * CNRLIG12 ;
LIGREPTBE = positif(REPBE + REPCE + REPCU + REPDU + REPEU) * CNRLIG12 ;

LIGREPAU = positif(REPAU) * CNRLIG12 ;
LIGREPBS = positif(REPBS) * CNRLIG12 ;
LIGREPCI = positif(REPCI) * CNRLIG12 ;
LIGREPDI = positif(REPDI) * CNRLIG12 ;
LIGREPSAU = positif(REPAU + REPBS + REPCI + REPDI) * CNRLIG12 ;

LIGREPAV = positif(REPAV) * CNRLIG12 ;
LIGREPBT = positif(REPBT) * CNRLIG12 ;
LIGREPCJ = positif(REPCJ) * CNRLIG12 ;
LIGREPDJ = positif(REPDJ) * CNRLIG12 ;
LIGREPSAV = positif(REPAV + REPBT + REPCJ + REPDJ) * CNRLIG12 ;

LIGREPAW = positif(REPAW) * CNRLIG12 ;
LIGREPBU = positif(REPBU) * CNRLIG12 ;
LIGREPCK = positif(REPCK) * CNRLIG12 ;
LIGREPDK = positif(REPDK) * CNRLIG12 ;
LIGREPSAW = positif(REPAW + REPBU + REPCK + REPDK) * CNRLIG12 ;

LIGREPAY = positif(REPAY) * CNRLIG12 ;
LIGREPBW = positif(REPBW) * CNRLIG12 ;
LIGREPCM = positif(REPCM) * CNRLIG12 ;
LIGREPDM = positif(REPDM) * CNRLIG12 ;
LIGREPSAY = positif(REPAY + REPBW + REPCM + REPDM) * CNRLIG12 ;

LIGREPBX = positif(REPBX) * CNRLIG12 ;
LIGREPCN = positif(REPCN) * CNRLIG12 ;
LIGREPDN = positif(REPDN) * CNRLIG12 ;
LIGREPEN = positif(REPEN) * CNRLIG12 ;
LIGREPBXN = positif(REPBX + REPCN + REPDN + REPEN) * CNRLIG12 ;

LIGREPBY = positif(REPBY) * CNRLIG12 ;
LIGREPCO = positif(REPCO) * CNRLIG12 ;
LIGREPDO = positif(REPDO) * CNRLIG12 ;
LIGREPEO = positif(REPEO) * CNRLIG12 ;
LIGREPBYO = positif(REPBY + REPCO + REPDO + REPEO) * CNRLIG12 ;

LIGREPBZ = positif(REPBZ) * CNRLIG12 ;
LIGREPCP = positif(REPCP) * CNRLIG12 ;
LIGREPDP = positif(REPDP) * CNRLIG12 ;
LIGREPEP = positif(REPEP) * CNRLIG12 ;
LIGREPBZP = positif(REPBZ + REPCP + REPDP + REPEP) * CNRLIG12 ;

LIGREPCB = positif(REPCB) * CNRLIG12 ;
LIGREPCR = positif(REPCR) * CNRLIG12 ;
LIGREPDR = positif(REPDR) * CNRLIG12 ;
LIGREPER = positif(REPER) * CNRLIG12 ;
LIGREPCBR = positif(REPCB + REPCR + REPDR + REPER) * CNRLIG12 ;


LIGREPDON = positif(REPDONR + REPDONR1 + REPDONR2 + REPDONR3 + REPDONR4) * CNRLIG12 ;
LIGREPDONR1 = positif(REPDONR1) * CNRLIG12 ;
LIGREPDONR2 = positif(REPDONR2) * CNRLIG12 ;
LIGREPDONR3 = positif(REPDONR3) * CNRLIG12 ;
LIGREPDONR4 = positif(REPDONR4) * CNRLIG12 ;
LIGREPDONR = positif(REPDONR) * CNRLIG12 ;
LIGRIDOMPRO = positif(RIDOMPRO) * LIG12 ;

LIGPME1 = positif(REPINVPME1) * CNRLIG12 ;
LIGPME2 = positif(REPINVPME2) * CNRLIG12 ;

LIGRSN = positif(RINVPECR + RINVPECV + RINVPECX + RINVPECF) * CNRLIG12 ;
LIGRSN3 = positif(RINVPECR) * CNRLIG12 ;
LIGRSN2 = positif(RINVPECV) * CNRLIG12 ;
LIGRSN1 = positif(RINVPECX) * CNRLIG12 ;
LIGRSN0 = positif(RINVPECF) * CNRLIG12 ;

LIGPLAFRSN = positif(RPLAFPME18 + RPLAFPME17 + RPLAFPME16 + RPLAFPME15 + RPLAFPME14 + RPLAFPME13) * CNRLIG12 ;
LIGPLAFRSN8 = positif(RPLAFPME18) * CNRLIG12 ;
LIGPLAFRSN7 = positif(RPLAFPME17) * CNRLIG12 ;
LIGPLAFRSN6 = positif(RPLAFPME16) * CNRLIG12 ;
LIGPLAFRSN5 = positif(RPLAFPME15) * CNRLIG12 ;
LIGPLAFRSN4 = positif(RPLAFPME14) * CNRLIG12 ;
LIGPLAFRSN3 = positif(RPLAFPME13) * CNRLIG12 ;

LIGREPFOR3 = positif(REPFOREST3) * CNRLIG12 ;

regle 902120:
application :  iliad ;

EXOVOUS = present ( TSASSUV ) 
          + positif ( XETRANV )
          + positif ( EXOCETV ) 
	  + positif ( COD5XA )
          + positif ( MIBEXV ) 
          + positif ( MIBNPEXV ) 
          + positif ( BNCPROEXV ) 
          + positif ( XSPENPV ) 
          + positif ( XBAV ) 
          + positif ( XBIPV ) 
          + positif ( XBINPV ) 
          + positif ( XBNV ) 
          + positif ( XBNNPV ) 
          + positif ( ABICPDECV ) * ( 1 - V_CNR )
          + positif ( ABNCPDECV ) * ( 1 - V_CNR )
          + positif ( HONODECV ) * ( 1 - V_CNR )
          + positif ( AGRIV )
          + positif ( BNCCREAV ) 
          ;

EXOCJT = present ( TSASSUC ) 
         + positif ( XETRANC )
         + positif ( EXOCETC ) 
	 + positif ( COD5YA )
         + positif ( MIBEXC ) 
         + positif ( MIBNPEXC ) 
         + positif ( BNCPROEXC ) 
         + positif ( XSPENPC ) 
         + positif ( XBAC ) 
         + positif ( XBIPC ) 
         + positif ( XBINPC ) 
         + positif ( XBNC ) 
         + positif ( XBNNPC ) 
         + positif ( ABICPDECC ) * ( 1 - V_CNR )
         + positif ( ABNCPDECC ) * ( 1 - V_CNR )
         + positif ( HONODECC ) * ( 1 - V_CNR )
         + positif ( AGRIC )
         + positif ( BNCCREAC ) 
         ;
 
EXOPAC = positif ( COD5ZA ) 
         + positif ( MIBEXP ) 
         + positif ( MIBNPEXP ) 
         + positif ( BNCPROEXP ) 
         + positif ( XSPENPP ) 
         + positif ( XBAP ) 
         + positif ( XBIPP ) 
         + positif ( XBINPP ) 
         + positif ( XBNP ) 
         + positif ( XBNNPP ) 
         + positif ( ABICPDECP ) * ( 1 - V_CNR )
         + positif ( ABNCPDECP ) * ( 1 - V_CNR )
         + positif ( HONODECP ) * ( 1 - V_CNR )
         + positif ( AGRIP )
         + positif ( BNCCREAP ) 
         ;

regle 902130:
application :  iliad ;

LIGTITREXVCP = positif(EXOVOUS) * positif(EXOCJT) * positif(EXOPAC) * (1 - LIG2501) * LIG12 ;

LIGTITREXV = positif(EXOVOUS) * (1 - positif(EXOCJT)) * (1 - positif(EXOPAC)) * (1 - LIG2501) * LIG12 ;

LIGTITREXC =  (1 - positif(EXOVOUS)) * positif(EXOCJT) * (1 - positif(EXOPAC)) * (1 - LIG2501) * LIG12 ;

LIGTITREXP =  (1 - positif(EXOVOUS)) * (1 - positif(EXOCJT)) * positif(EXOPAC) * (1 - LIG2501) * LIG12 ;

LIGTITREXVC =  positif(EXOVOUS) * positif(EXOCJT) * (1 - positif(EXOPAC)) * (1 - LIG2501) * LIG12 ;

LIGTITREXVP =  positif(EXOVOUS) * (1 - positif(EXOCJT)) * positif(EXOPAC) * (1 - LIG2501) * LIG12 ;

LIGTITREXCP =  (1 - positif(EXOVOUS)) * positif(EXOCJT) * positif(EXOPAC) * (1 - LIG2501) * LIG12 ;

regle 902140:
application :  iliad ;

EXOCET = EXOCETC + EXOCETV ;
LIGEXOCET = positif(EXOCET) * LIG12 ;

LIGEXBA = positif(COD5XA + COD5YA + COD5ZA) * LIG12 ;
LIGMXBIP =  positif(MIBEXV + MIBEXC + MIBEXP) * LIG12 ;
LIGMXBINP =  positif(MIBNPEXV + MIBNPEXC + MIBNPEXP) * LIG12 ;
LIGSXBN =  positif(BNCPROEXV + BNCPROEXC + BNCPROEXP) * LIG12 ;
LIGXSPEN =  positif(XSPENPV + XSPENPC + XSPENPP) * LIG12 ;
LIGXBIP =  positif(XBIPV + XBIPC + XBIPP) * LIG12 ;
LIGXBINP =  positif(XBINPV + XBINPC + XBINPP) * LIG12 ;
LIGXBP =  positif(XBNV + XBNC + XBNP) * LIG12 ;
LIGXBN =  positif(XBNNPV + XBNNPC + XBNNPP) * LIG12 ;

LIGXTSA =  positif(present(TSASSUV) + present(TSASSUC)) * LIG12 ;
LIGXIMPA =  positif(XETRANV + XETRANC) * LIG12 ;
LIGXBA =  positif(XBAV + XBAC + XBAP) * LIG12 ;

LIGBICAP = positif(ABICPDECV + ABICPDECC + ABICPDECP) * CNRLIG12 ;
LIGBNCAP = positif(ABNCPDECV + ABNCPDECC + ABNCPDECP) * CNRLIG12 ;

LIGBAPERP =  positif(BAPERPV + BAPERPC + BAPERPP + BANOCGAV + BANOCGAC + BANOCGAP) * LIG12 ;
LIGBNCCREA =  positif(BNCCREAV + BNCCREAC + BNCCREAP) * LIG12 ;

regle 902150:
application :  iliad ;


LIGPERP = (1 - positif(PERPIMPATRIE+0))
                 * positif(PERPINDV + PERPINDC + PERPINDP
                        + PERPINDCV + PERPINDCC + PERPINDCP)
                 * positif(PERPINDAFFV+PERPINDAFFC+PERPINDAFFP)
                  * (1 - null(PERP_COTV + PERP_COTC + PERP_COTP + 0) * (1 - INDIMPOS))
                  * (1 - positif(PERP_COND1+PERP_COND2))
                  * (1 - LIG8FV)
                  * (1 - LIG2501)
                  * CNRLIG12 
                  +0
                  ;
LIGPERPI = positif(PERPIMPATRIE+0)
                 * positif(PERPINDV + PERPINDC + PERPINDP
                        + PERPINDCV + PERPINDCC + PERPINDCP)
                 * positif(PERPINDAFFV+PERPINDAFFC+PERPINDAFFP)
                  * (1 - null(PERP_COTV + PERP_COTC + PERP_COTP + 0) * (1 - INDIMPOS))
                  * (1 - positif(PERP_COND1+PERP_COND2))
                  * (1 - LIG8FV)
                  * (1 - LIG2501)
                  * CNRLIG12
                  +0
                  ;
LIGPERPM = (1 - positif(PERPIMPATRIE+0))
                 * positif(PERPINDV + PERPINDC + PERPINDP
                        + PERPINDCV + PERPINDCC + PERPINDCP)
                 * positif(PERPINDAFFV+PERPINDAFFC+PERPINDAFFP)
                  * (1 - null(PERP_COTV + PERP_COTC + PERP_COTP + 0) * (1 - INDIMPOS))
                  * positif(PERP_MUT)
                  * positif(PERP_COND1+PERP_COND2)
                  * (1 - LIG8FV)
                  * (1 - LIG2501)
                  * CNRLIG12
                  +0
                  ;
LIGPERPMI = positif(PERPIMPATRIE+0)
                 * positif(PERPINDV + PERPINDC + PERPINDP
                        + PERPINDCV + PERPINDCC + PERPINDCP)
                 * positif(PERPINDAFFV+PERPINDAFFC+PERPINDAFFP)
                  * (1 - null(PERP_COTV + PERP_COTC + PERP_COTP + 0) * (1 - INDIMPOS))
                  * positif(PERP_MUT)
                  * positif(PERP_COND1+PERP_COND2)
                  * (1 - LIG8FV)
                  * (1 - LIG2501)
                  * CNRLIG12
                  +0
                  ;

LIGPERP13 = LIGPERP + LIGPERPM ;

LIGPERP24 = LIGPERPI + LIGPERPMI ;

LIGPERP1234 = LIGPERP + LIGPERPI + LIGPERPM + LIGPERPMI ;

LIGPERPFAM = positif(PERPINDV + PERPINDCV) * positif(PERPINDAFFV)
              * positif(PERPINDC + PERPINDCC)* positif(PERPINDAFFC)
              * positif(PERPINDP + PERPINDCP) * positif(PERPINDAFFP)
              * CNRLIG12
              * positif(LIGPERP + LIGPERPI + LIGPERPM + LIGPERPMI)
              ;

LIGPERPV = positif(PERPINDV + PERPINDCV) * positif(PERPINDAFFV) 
           * (1 - positif(PERPINDC + PERPINDCC) * positif(PERPINDAFFC))
	   * (1 - positif(PERPINDP + PERPINDCP) * positif(PERPINDAFFP))
           * CNRLIG12 * positif(LIGPERP + LIGPERPI + LIGPERPM + LIGPERPMI) ;

LIGPERPC = positif(PERPINDC + PERPINDCC) * positif(PERPINDAFFC) 
           * (1 - positif(PERPINDV + PERPINDCV) * positif(PERPINDAFFV))
	   * (1 - positif(PERPINDP + PERPINDCP) * positif(PERPINDAFFP))
           * CNRLIG12 * positif(LIGPERP + LIGPERPI + LIGPERPM + LIGPERPMI) ;

LIGPERPP = positif(PERPINDP + PERPINDCP) * positif(PERPINDAFFP) 
           * (1 - positif(PERPINDV + PERPINDCV) * positif(PERPINDAFFV))
	   * (1 - positif(PERPINDC + PERPINDCC) * positif(PERPINDAFFC))
           * CNRLIG12 * positif(LIGPERP + LIGPERPI + LIGPERPM + LIGPERPMI) ;

LIGPERPCP = positif(PERPINDP + PERPINDCP) * positif(PERPINDAFFP)
           * positif(PERPINDC + PERPINDCC) * positif(PERPINDAFFC)
           * (1 - positif(PERPINDV + PERPINDCV) * positif(PERPINDAFFV))
           * CNRLIG12
           * positif(LIGPERP + LIGPERPI + LIGPERPM + LIGPERPMI)
           ;

LIGPERPVP = positif(PERPINDP + PERPINDCP) * positif(PERPINDAFFP)
           * positif(PERPINDV + PERPINDCV) * positif(PERPINDAFFV)
           * (1 - positif(PERPINDC + PERPINDCC) * positif(PERPINDAFFC))
           * CNRLIG12
           * positif(LIGPERP + LIGPERPI + LIGPERPM + LIGPERPMI)
           ;

LIGPERPMAR = positif(PERPINDV + PERPINDCV)  * positif(PERPINDAFFV)
             * positif(PERPINDC + PERPINDCC)  * positif(PERPINDAFFC)
             * (1 - positif(PERPINDP + PERPINDCP) * positif(PERPINDAFFP))
             * CNRLIG12
             * positif(LIGPERP + LIGPERPI + LIGPERPM + LIGPERPMI)
             ;

regle 902160:
application :  iliad ;

ZIGTAUX1 = positif(BCSG + V_CSANT) * positif(BRDS + V_RDANT) * positif(BPSOL + V_PSOLANT) * (1 - (V_CNR * (1 - positif(ZIG_RF + max(0 , NPLOCNETSF))))) * LIG2 ;

ZIGTAUX2 = positif(BCSG + V_CSANT) * positif(BRDS + V_RDANT) * (1 - positif(BPSOL + V_PSOLANT)) * (1 - V_CNR) * LIG2 ;

ZIGTAUX3 = positif(BRDS + V_RDANT) * (1 - positif(BCSG + V_CSANT)) * (1 - positif(BPSOL + V_PSOLANT)) * (1 - V_CNR) * LIG2 ;

ZIGTAUX4 = positif(BPSOL + V_PSOLANT) * (1 - positif(BCSG + V_CSANT)) * (1 - positif(BRDS + V_RDANT)) * (1 - V_CNR) * LIG2 ;

ZIGTAUX5 = positif(BRDS + V_RDANT) * positif(BPSOL + V_PSOLANT) * positif(BCSG820) * (1 - positif(BCSG + V_CSANT)) * (1 - V_CNR) * LIG2 ;

regle 902170:
application :  iliad ;

ZIGTITRE = positif(positif(BCSG + V_CSANT + BRDS + V_RDANT + BPSOL + V_PSOLANT) * (1 - (V_CNR * (1 - positif(ZIG_RF + max(0 , NPLOCNETSF))))) 
		   + positif(BCVNSAL + V_CVNANT + BCDIS + V_CDISANT) + positif(CODZRU + CODZRV)) * LIG2 ;

regle 902180:
application :  iliad ;

CS_RVT = RDRV ;
RD_RVT = CS_RVT;
PS_RVT = CS_RVT;
IND_ZIGRVT =  0;

ZIG_RVTO = positif (CS_RVT + RD_RVT + PS_RVT) * null(3 - INDIRPS) * CNRLIG12 ;

regle 902190:
application :  iliad ;

CS_RCM =  RDRCM ;
RD_RCM = CS_RCM ;
PS_RCM = CS_RCM ;

IND_ZIGRCM = positif(present(RCMABD) + present(RCMAV) + present(RCMHAD) + present(RCMHAB)  
                     + present(RCMTNC) + present(RCMAVFT) + present(REGPRIV)) 
	      * positif(V_NOTRAIT - 20) ;

ZIG_RCM = positif(CS_RCM + RD_RCM + PS_RCM + IND_ZIGRCM) * null(3 - INDIRPS) * CNRLIG12 ;

regle 902200:
application :  iliad ;

CS_REVCS = RDNP ;
RD_REVCS = CS_REVCS ;
PS_REVCS = CS_REVCS ;

IND_ZIGPROF = positif(V_NOTRAIT - 20) * positif( present(RCSV)
                     +present(RCSC)
                     +present(RCSP));
ZIG_PROF = positif(CS_REVCS + RD_REVCS + PS_REVCS + IND_ZIGPROF) * null(3 - INDIRPS) * LIG12 ;

regle 902210:
application :  iliad ;


CS_RFG = RDRFPS ;
RD_RFG = CS_RFG ;
PS_RFG = CS_RFG ;

IND_ZIGRFG = positif(V_NOTRAIT - 20) * positif( present(RFORDI)
                     +present(RFDORD)
                     +present(RFDHIS)
                     +present(RFMIC) );

ZIG_RF = positif(CS_RFG + RD_RFG + PS_RFG + IND_ZIGRFG) * null(3 - INDIRPS) * LIG12 ;

regle 902220:
application :  iliad ;


CS_RTF = RDPTP + RDNCP ;
RD_RTF = CS_RTF ;
PS_RTF = CS_RTF ;

IND_ZIGRTF=  positif(V_NOTRAIT - 20) * positif (present (PEA) + present( BPCOPTV ) + present( BPVRCM )) ;

ZIG_RTF = positif(CS_RTF + RD_RTF + PS_RTF + IND_ZIGRTF) * null(3 - INDIRPS) * CNRLIG12 ;

ZIGGAINLEV = positif(CVNSALC) * positif(CVNSALAV) * LIG12 ;

regle 902230:
application :  iliad ;


CS_REVETRANG = 0 ;
RD_REVETRANG = SALECS + SALECSG + ALLECS + INDECS + PENECS 
             + COD8SA + COD8SB + COD8SC + COD8SW + COD8SX + COD8PH;
PS_REVETRANG = 0 ;


ZIG_REVETR = positif(SALECS + SALECSG + ALLECS + INDECS + PENECS 
                     + COD8SA + COD8SB + COD8SC + COD8SW + COD8SX + COD8PH)
                   * CNRLIG12 ;

regle 902240:
application :  iliad ;


CS_RVORIGND =   ESFP;
RD_RVORIGND =   ESFP;
PS_RVORIGND =   ESFP;

IND_ZIGREVORIGIND = present(ESFP) ;

ZIG_RVORIGND = positif (CS_RVORIGND + RD_RVORIGND + PS_RVORIGND
                         + IND_ZIGREVORIGIND)
                   * CNRLIG12 ;

regle 902250:
application :  iliad ;

CS_RE168 = RE168 ;
RD_RE168 = RE168 ;
PS_RE168 = RE168 ;

CS_TAX1649 = TAX1649 ;
RD_TAX1649 = TAX1649 ;
PS_TAX1649 = TAX1649 ;

CS_R1649 = R1649 ;
RD_R1649 = R1649 ;
PS_R1649 = R1649 ;

CS_PREREV = PREREV ;
RD_PREREV = PREREV ;
PS_PREREV = PREREV ;

ZIGRE168 = positif(RE168) * (1 - V_CNR) * LIG2 ;
ZIGTAX1649 = positif(TAX1649) * (1 - V_CNR) * LIG2 ;

ZIGR1649 = positif(R1649) * CNRLIG12 ;
ZIGPREREV = positif(PREREV) * CNRLIG12 ;

regle 902255:
application :  iliad ;


ZIGNONASSU = positif(REVNONASSU) * LIG12 ;

regle 902260:
application :  iliad ;
 
LIGPS = positif(BCSG + BRDS + BPSOL + BCVNSAL + BCDIS 
                + BGLOA + BRSE1 + BRSE2 + BRSE3 + BRSE4 + BRSE5 + BRSE6 + BRSE7 + 0
                + (CODZRU + CODZRV) * LIG1) * LIG2 ;

LIGPSP = 1 - (null(LIGPS) * null(V_ANTCR)) ;

NONLIGPS = positif(positif(1 - LIGPS) + positif(null(V_NOTRAIT - 23) + null(V_NOTRAIT - 33) + null(V_NOTRAIT - 43) + null(V_NOTRAIT - 53) + null(V_NOTRAIT - 63))) ;

INDIRPS =  (1 * (1 - LIGPS) * positif(3 - ANTINDIRPS))
	 + (3 * (1- positif((1 - LIGPS) * positif(3 - ANTINDIRPS)))) ;

regle 902270:
application :  iliad ;


ZIGBASECS = positif(BCSG + V_CSANT + CODZRU + CODZRV + COD8SH + COD8SI) ;
ZIGBASERD = positif(BRDS + V_RDANT + CODZRU + CODZRV + COD8SH + COD8SI) ;
ZIGBASEPS = positif(BPSOL + V_PSOLANT + CODZRU + CODZRV + COD8SH + COD8SI) ;
ZIGBASECVN = positif(BCVNSAL + V_CVNANT) * LIG2 ;
ZIG_BASE = positif(BCSG + BRDS + BPSOL + V_CSANT + V_RDANT + V_PSOLANT + CODZRU + CODZRV + COD8SH + COD8SI) * LIG2 ;
ZIGCDIS = positif(BCDIS + V_CDISANT) * LIG2 ;
ZIGPVTER = positif(PVTERPS) * LIG2 ;
ZIGGLOA = positif(BGLOA) * (1 - V_CNR) * LIG2 ;
ZIGGLOANR = positif(BGLOACNR) * LIG2 ;
ZIGGLOALL = positif(ZIGGLOA + ZIGGLOANR) * LIG2 ;
ZIGRSE1 = positif(BRSE1) * LIG2 ; 
ZIGRSE2 = positif(BRSE2) * LIG2 ;
ZIGRSE3 = positif(BRSE3) * LIG2 ;
ZIGRSE4 = positif(BRSE4) * LIG2 ;
ZIGRSE5 = positif(BRSE5) * LIG2 ;
ZIGRSE6 = positif(BRSE6) * LIG2 ;
ZIGRSE7 = positif(BRSE7) * LIG2 ;
ZIGCSG820 = positif(BCSG820) * LIG2 ;


ZIGRFRET = positif(COD8YK) * LIG2 ;
ZIGRFDEP = positif(COD8XK) * (1 - positif(CODZRA)) * LIG2 ;
ZIGRFZRA = positif(COD8XK) * positif(CODZRA) * LIG2 ;

regle 902290:
application :  iliad ;

ZIGMONTS = positif(BCVNSAL + V_CVNANT) ;
ZIGMONTCS = positif(BCSG + V_CSANT + CODZRU + CODZRV) ;
ZIGMONTRD = positif(BRDS + V_RDANT + CODZRU + CODZRV) ;
ZIGMONTPS = positif(BPSOL + V_PSOLANT + CODZRU + CODZRV) ;
ZIG_MONTANT = positif(BCSG + BRDS + BPSOL + V_CSANT + V_RDANT + V_PSOLANT + CODZRU + CODZRV) * LIG2 ;

regle 902300:
application :  iliad ;


ZIG_INT =  positif (RETCS + RETRD + RETPS + RETPSOL) * LIG2 ;

ZIGCVN = positif(RETCVN) * LIG2 ;

ZIGINT = positif(RETCDIS) * LIG2 ;

ZIGLOA = positif(RETGLOA) * LIG2 ;

ZIGINT1 = positif(RETRSE1) * LIG2 ;
ZIGINT2 = positif(RETRSE2) * LIG2 ;
ZIGINT3 = positif(RETRSE3) * LIG2 ;
ZIGINT4 = positif(RETRSE4) * LIG2 ;
ZIGINT5 = positif(RETRSE5) * LIG2 ;
ZIGINT6 = positif(RETRSE6) * LIG2 ;
ZIGINT7 = positif(RETRSE7) * LIG2 ;

ZIGINT820 = positif(RETCSG820) * LIG2 ;

ZIGCVN22 = positif(RETCVN2224) ;
ZIGINT22 = positif(RETCDIS2224) ;
ZIGLOA22 = positif(RETGLOA2224) ;

ZIGINT122 = positif(RETRSE12224) * LIG2 ;
ZIGINT222 = positif(RETRSE22224) * LIG2 ;
ZIGINT322 = positif(RETRSE32224) * LIG2 ;
ZIGINT422 = positif(RETRSE42224) * LIG2 ;
ZIGINT522 = positif(RETRSE52224) * LIG2 ;
ZIGINT622 = positif(RETRSE62224) * LIG2 ;
ZIGINT722 = positif(RETRSE72224) * LIG2 ;

regle 902310:
application :  iliad ;

ZIG_PEN17281 = ZIG_PENAMONT * positif(NMAJC1 + NMAJR1 + NMAJPSOL1) * LIG2 ;

ZIG_PENATX4 = ZIG_PENAMONT * positif(NMAJC4 + NMAJR4 + NMAJP4) * LIG2 ;
ZIG_PENATAUX = ZIG_PENAMONT * positif(NMAJC1 + NMAJR1 + NMAJPSOL1) * LIG2 ;

ZIGNONR30 = positif(ZIG_PENATX4) * positif(1 - positif(R1649 + PREREV)) * LIG2 ;
ZIGR30 = positif(ZIG_PENATX4) * positif(R1649 + PREREV) * LIG2 ;

ZIGPENACVN = positif(PCVN) * positif(NMAJCVN1) * LIG2 ;
ZIGPENACDIS = positif(PCDIS) * positif(NMAJCDIS1) * LIG2 ;

ZIGPENAGLO1 = positif(PGLOA) * positif(NMAJGLO1) * LIG2 ;

ZIGPENARSE1 = positif(PRSE1) * positif(NMAJRSE11) * LIG2 ;
ZIGPENARSE2 = positif(PRSE2) * positif(NMAJRSE21) * LIG2 ;
ZIGPENARSE3 = positif(PRSE3) * positif(NMAJRSE31) * LIG2 ;
ZIGPENARSE4 = positif(PRSE4) * positif(NMAJRSE41) * LIG2 ;
ZIGPENARSE5 = positif(PRSE5) * positif(NMAJRSE51) * LIG2 ;
ZIGPENARSE6 = positif(PRSE6) * positif(NMAJRSE61) * LIG2 ;
ZIGPENARSE7 = positif(PRSE7) * positif(NMAJRSE71) * LIG2 ;

ZIGPENA8201 = positif(PCSG820) * positif(NMAJC8201) * LIG2 ;

ZIGPENACVN4 = positif(PCVN) * positif(NMAJCVN4) * LIG2 ;
ZIGPENACDIS4 = positif(PCDIS) * positif(NMAJCDIS4) * LIG2 ;

ZIGPENAGLO4 = positif(PGLOA) * positif(NMAJGLO4) * LIG2 ;

ZIGPENARSE14 = positif(PRSE1) * positif(NMAJRSE14) * LIG2 ;
ZIGPENARSE24 = positif(PRSE2) * positif(NMAJRSE24) * LIG2 ;
ZIGPENARSE34 = positif(PRSE3) * positif(NMAJRSE34) * LIG2 ;
ZIGPENARSE44 = positif(PRSE4) * positif(NMAJRSE44) * LIG2 ;
ZIGPENARSE54 = positif(PRSE5) * positif(NMAJRSE54) * LIG2 ;
ZIGPENARSE64 = positif(PRSE6) * positif(NMAJRSE64) * LIG2 ;
ZIGPENARSE74 = positif(PRSE7) * positif(NMAJRSE74) * LIG2 ;

ZIGPENA8204 = positif(PCSG820) * positif(NMAJC8204) * LIG2 ;

regle 902320:
application :  iliad ;

ZIG_PENAMONT = positif(PCSG + PRDS + PPRS + PPSOL) * LIG2 ;

regle 902330:
application :  iliad ;

ZIG_CRDETR = positif(CICSG + CIRDS + CIPRS + CIPSOL) * LIG2 ;

ZIGCIMR = null(4 - V_IND_TRAIT) * positif(CIMRCSGP + CIMRCRDSP + CIMRPSOLP) * LIG2 
          + null(5 - V_IND_TRAIT) * positif(CIMRCSG24TL + CIMRCRDS24TL + CIMRPSOL24TL) * (1 - positif(FLAG_RETARD * null(FLAG_RETARD07) + FLAG_DEFAUT)) * LIG2 ;

regle 902340 :
application :  iliad ;

ZIGCIGLOA = positif(CIGLOA) * (1 - positif(ANNUL2042)) ;

CGLOAPROV = COD8YL * (1-V_CNR) ;
ZIGCOD8YL = positif(CGLOAPROV) * (1 - positif(ANNUL2042)) ;

ZIGCOD8YT = positif(COD8YT) * (1 - positif(ANNUL2042)) ;

ZIGCDISPROV = positif(CDISPROV) * (1 - positif(ANNUL2042)) ;

ZIGREVXA = positif(REVCSXA) * (1 - positif(ANNUL2042)) ;

ZIGREVXB = positif(REVCSXB) * (1 - positif(ANNUL2042)) ;

ZIGREVXC = positif(REVCSXC + COD8XI) * (1 - positif(ANNUL2042)) ;

ZIGREVXD = positif(REVCSXD) * (1 - positif(ANNUL2042)) ;

ZIGREVXE = positif(REVCSXE + COD8XJ) * (1 - positif(ANNUL2042)) ;

ZIGPROVYD = positif(CSPROVYD) * (1 - positif(ANNUL2042)) ;

ZIGPROVYY = positif(COD8YY) * (1 - positif(ANNUL2042)) ;

ZIGPROVYE = positif(CSPROVYE) * (1 - positif(ANNUL2042)) ;

CSPROVRSE2 = CSPROVYF + CSPROVYN ;
ZIGPROVYF = positif(CSPROVRSE2) * (1 - positif(ANNUL2042)) ;

ZIGPROVYG = positif(CSPROVYG) * (1 - positif(ANNUL2042)) ;

CSPROVRSE4 = CSPROVYH + CSPROVYP ;
ZIGPROVYH = positif(CSPROVRSE4) * (1 - positif(ANNUL2042)) ;

ZIGCIRSE6 = positif(REVCSXB + REVCSXC + COD8XI) * (1 - positif(ANNUL2042)) ;

ZIGPROVYQ = positif(COD8YQ) * (1 - positif(ANNUL2042)) ;

ZIGPROVZH = positif(COD8ZH) * (1 - positif(ANNUL2042)) ;

regle 902350 :
application :  iliad ;

ZIG_CTRIANT = positif(V_ANTCR) ;

ZIGCSANT = positif(V_CSANT) * (1 - APPLI_OCEANS) ;

ZIGRDANT = positif(V_RDANT) * (1 - APPLI_OCEANS) ;

ZIGPSANT = positif(V_PSANT) * (1 - APPLI_OCEANS) ;

ZIGPSOLANT = positif(V_PSOLANT) * (1 - APPLI_OCEANS) ;

ZIGCVNANT = positif(V_CVNANT) * (1 - APPLI_OCEANS) ;

ZIGCDISANT = positif(V_CDISANT) * (1 - APPLI_OCEANS) ;

ZIGLOANT = positif(V_GLOANT) * (1 - APPLI_OCEANS) ;

ZIG820ANT = positif(V_CSG820ANT) * (1 - APPLI_OCEANS) ;

ZIGRSE1ANT = positif(V_RSE1ANT) * (1 - APPLI_OCEANS) ;
ZIGRSE2ANT = positif(V_RSE2ANT) * (1 - APPLI_OCEANS) ;
ZIGRSE3ANT = positif(V_RSE3ANT) * (1 - APPLI_OCEANS) ;
ZIGRSE4ANT = positif(V_RSE4ANT) * (1 - APPLI_OCEANS) ;
ZIGRSE5ANT = positif(V_RSE5ANT) * (1 - APPLI_OCEANS) ;
ZIGRSE6ANT = positif(V_RSE6ANT) * (1 - APPLI_OCEANS) ;
ZIGRSE7ANT = positif(V_RSE7ANT) * (1 - APPLI_OCEANS) ;

regle 902360:
application :  iliad ;


ZIG_CTRIPROV = positif(CSGIM + CRDSIM + PRSPROV) * LIG2 ;

regle 902370:
application :  iliad ;


IND_COLC = positif(BCSG) * positif(PCSG + CICSG + CIMRCSG + CSGIM) * LIG2 ;

IND_COLR = positif(BRDS) * positif(PRDS + CIRDS + CIMRCRDS + CRDSIM) * LIG2 ;

INDCOLSOL = positif(BPSOL) * positif(PPSOL + CIPSOL + CIMRPSOL + PROPSOL) * LIG2 ;

INDCOLVN = positif(BCVNSAL) * positif(PCVN + CICVN + COD8YT) * LIG2 ;

INDCOL = positif(IND_COLC + IND_COLR + INDCOLSOL) ;

IND_COLD = positif(BCDIS) * positif(PCDIS + CDISPROV) * LIG2 ;

INDGLOA = positif(BGLOA) * positif(PGLOA + COD8YL + COD8XM) ;

INDCSG820 = positif(BCSG820) * positif(PCSG820 + COD8ZH) ;

INDRSE1 = positif(BRSE1) * positif(PRSE1 + CIRSE1 + CSPROVYD) ;
INDRSE2 = positif(BRSE2) * positif(PRSE2 + CIRSE2 + CSPROVYF + CSPROVYN) ;
INDRSE3 = positif(BRSE3) * positif(PRSE3 + CIRSE3 + CSPROVYG) ;
INDRSE4 = positif(BRSE4) * positif(PRSE4 + CIRSE4 + CSPROVYH + CSPROVYP) ;
INDRSE5 = positif(BRSE5) * positif(PRSE5 + CIRSE5 + CSPROVYE) ;
INDRSE6 = positif(BRSE6) * positif(PRSE6 + CIRSE6 + COD8YQ) ;
INDRSE7 = positif(BRSE7) * positif(PRSE7 + COD8YY) * LIG2 ;

IND_CTXC = positif(CS_DEG) * positif(BCSG) * positif(INDCTX) ;

IND_CTXR = positif(CS_DEG) * positif(BRDS) * positif(INDCTX) ;

IND_CTXP = positif(CS_DEG) * positif(BPSOL) * positif(INDCTX) ;

IND_CTXD = positif(CS_DEG) * positif(BCDIS) * positif(INDCTX) ;

INDTRAIT = null(5 - V_IND_TRAIT) ;

INDT = positif(IND_COLC + IND_COLR + INDCOLSOL + IND_CTXC + IND_CTXR + IND_CTXP) * INDTRAIT ;

INDTD = positif(IND_COLD + IND_CTXD) * INDTRAIT ;

regle 902380:
application :  iliad ;

ZIG_NETAP =  positif(BCSG + BRDS + BPSOL + BCVNSAL + BCDIS + BGLOA + BRSE1 + BRSE2 
                     + BRSE3 + BRSE4 + BRSE5  + BRSE6 + BRSE7 + CODZRU + CODZRV) * LIG2 ;

regle 902390:
application :  iliad ;

ZIG_TOTDEG = positif(CRDEG) * positif(INDCTX) ;

ZIG_TITREP = ZIG_NETAP + ZIG_TOTDEG ;

ZIGANNUL = positif(INDCTX) * positif(ANNUL2042) ;

regle 902400:
application :  iliad ;

ZIG_INF8 = positif (CS_DEG) * positif (SEUIL_8 - CS_DEG) * LIG2 ;

regle 902410:
application :  iliad ;

ZIG_REMPLACE  = positif((1 - positif(20 - V_NOTRAIT)) 
               * positif(ANNEEREV - V_0AX)
               * positif(ANNEEREV - V_0AZ)
               * positif(ANNEEREV - V_0AY) + positif(V_NOTRAIT - 20))
               * LIG2 ;

regle 902420:
application : iliad ;

ZIG_DEGINF61 = positif(V_CSANT+V_RDANT+V_PSANT+0)  
               * positif(CRDEG+0)
               * positif(SEUIL_61-TOTCRA-CSNET-RDNET-PRSNET-CDISNET+0)
               * (1 - null(CSTOT+0))
               * LIG2 ;

regle 902430:
application :  iliad ;


ZIG_CSGDDO = positif(V_IDANT - DCSGD) * positif(IDCSG) * (1 - null(V_IDANT + DCSGD + 0)) * (1 - null(V_IDANT - DCSGD)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIG_CSGDRS = positif(DCSGD - V_IDANT) * positif(IDCSG) * (1 - null(V_IDANT + DCSGD + 0)) * (1 - null(V_IDANT - DCSGD)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIG_CSGDC = positif(ZIG_CSGDDO + ZIG_CSGDRS + null(V_IDANT - DCSGD)) * (1 - null(V_IDANT + DCSGD + 0)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIG_CSGDCOR = positif(ZIG_CSGDDO + ZIG_CSGDRS) * (1 - null(V_IDANT + DCSGD + 0)) * (1 - null(V_IDANT - DCSGD)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGCSGDCOR1 = ZIG_CSGDCOR * null(ANCSDED2 - (ANNEEREV + 1)) ;
ZIGCSGDCOR2 = ZIG_CSGDCOR * null(ANCSDED2 - (ANNEEREV + 2)) ;
ZIGCSGDCOR3 = ZIG_CSGDCOR * null(ANCSDED2 - (ANNEEREV + 3)) ;
ZIGCSGDCOR4 = ZIG_CSGDCOR * null(ANCSDED2 - (ANNEEREV + 4)) ;
ZIGCSGDCOR5 = ZIG_CSGDCOR * null(ANCSDED2 - (ANNEEREV + 5)) ;
ZIGCSGDCOR6 = ZIG_CSGDCOR * null(ANCSDED2 - (ANNEEREV + 6)) ;
ZIGCSGDCOR7 = ZIG_CSGDCOR * null(ANCSDED2 - (ANNEEREV + 7)) ;

regle 902440:
application :  iliad ;

ZIGLODD = positif(V_IDGLOANT - DGLOD) * positif(IDGLO) * (1 - null(V_IDGLOANT + DGLOD + 0)) * (1 - null(V_IDGLOANT - DGLOD)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGLORS = positif(DGLOD - V_IDGLOANT) * positif(IDGLO) * (1 - null(V_IDGLOANT + DGLOD + 0)) * (1 - null(V_IDGLOANT - DGLOD)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGLOCO = positif(ZIGLODD + ZIGLORS + null(V_IDGLOANT - DGLOD)) * (1 - null(V_IDGLOANT + DGLOD + 0)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGLOCOR = positif(ZIGLODD + ZIGLORS) * (1 - null(V_IDGLOANT + DGLOD + 0)) * (1 - null(V_IDGLOANT - DGLOD)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGLOCOR1 = ZIGLOCOR * null(ANCSDED2 - (ANNEEREV + 1)) ;
ZIGLOCOR2 = ZIGLOCOR * null(ANCSDED2 - (ANNEEREV + 2)) ;
ZIGLOCOR3 = ZIGLOCOR * null(ANCSDED2 - (ANNEEREV + 3)) ;
ZIGLOCOR4 = ZIGLOCOR * null(ANCSDED2 - (ANNEEREV + 4)) ;
ZIGLOCOR5 = ZIGLOCOR * null(ANCSDED2 - (ANNEEREV + 5)) ;
ZIGLOCOR6 = ZIGLOCOR * null(ANCSDED2 - (ANNEEREV + 6)) ;
ZIGLOCOR7 = ZIGLOCOR * null(ANCSDED2 - (ANNEEREV + 7)) ;

ZIGRSEDD = positif(V_IDRSEANT - DRSED) * positif(IDRSE) * (1 - null(V_IDRSEANT + DRSED + 0)) 
	    * (1 - null(V_IDRSEANT - DRSED)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGRSERS = positif(DRSED - V_IDRSEANT) * positif(IDRSE) * (1 - null(V_IDRSEANT + DRSED + 0)) 
	    * (1 - null(V_IDRSEANT - DRSED)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGRSECO = positif(ZIGRSEDD + ZIGRSERS + null(V_IDRSEANT - DRSED)) * (1 - null(V_IDRSEANT + DRSED + 0)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGRSECOR = positif(ZIGRSEDD + ZIGRSERS) * (1 - null(V_IDRSEANT + DRSED + 0)) * (1 - null(V_IDRSEANT - DRSED)) * positif(V_NOTRAIT - 20) * LIG2 ;

ZIGRSECOR1 = ZIGRSECOR * null(ANCSDED2 - (ANNEEREV + 1)) ;
ZIGRSECOR2 = ZIGRSECOR * null(ANCSDED2 - (ANNEEREV + 2)) ;
ZIGRSECOR3 = ZIGRSECOR * null(ANCSDED2 - (ANNEEREV + 3)) ;
ZIGRSECOR4 = ZIGRSECOR * null(ANCSDED2 - (ANNEEREV + 4)) ;
ZIGRSECOR5 = ZIGRSECOR * null(ANCSDED2 - (ANNEEREV + 5)) ;
ZIGRSECOR6 = ZIGRSECOR * null(ANCSDED2 - (ANNEEREV + 6)) ;
ZIGRSECOR7 = ZIGRSECOR * null(ANCSDED2 - (ANNEEREV + 7)) ;

regle 902450:
application :  iliad ;

                 
ZIG_PRIM = positif(NAPCR) * LIG2 ;

regle 902460:
application :  iliad ;


CS_BPCOS = RDNCP ;
RD_BPCOS = CS_BPCOS ;
PS_BPCOS = CS_BPCOS ;

ZIG_BPCOS = positif(CS_BPCOS + RD_BPCOS + PS_BPCOS) * CNRLIG12 ;

regle 902470:
application :  iliad ;


ANCSDED2 = (V_ANCSDED * (1 - null(933 - V_ROLCSG)) + (V_ANCSDED + 1) * null(933 - V_ROLCSG)) * positif(V_ROLCSG + 0)
           + V_ANCSDED * (1 - positif(V_ROLCSG + 0)) ;

ZIG_CSGDPRIM = (1 - positif(V_CSANT + V_RDANT + V_PSANT + V_IDANT)) * positif(IDCSG) * LIG2 ;

ZIG_CSGD99 = ZIG_CSGDPRIM * (1 - null(V_IDANT + DCSGD + 0)) * positif(DCSGD) * positif(20 - V_NOTRAIT) * LIG2 ;

ZIGDCSGD1 = ZIG_CSGD99 * null(ANCSDED2 - (ANNEEREV + 1)) * LIG2 ;
ZIGDCSGD2 = ZIG_CSGD99 * null(ANCSDED2 - (ANNEEREV + 2)) * LIG2 ;
ZIGDCSGD3 = ZIG_CSGD99 * null(ANCSDED2 - (ANNEEREV + 3)) * LIG2 ;
ZIGDCSGD4 = ZIG_CSGD99 * null(ANCSDED2 - (ANNEEREV + 4)) * LIG2 ;
ZIGDCSGD5 = ZIG_CSGD99 * null(ANCSDED2 - (ANNEEREV + 5)) * LIG2 ;
ZIGDCSGD6 = ZIG_CSGD99 * null(ANCSDED2 - (ANNEEREV + 6)) * LIG2 ;

ZIGIDGLO = positif(IDGLO) * (1 - null(V_IDGLOANT + DGLOD + 0))  * positif(20 - V_NOTRAIT) * LIG2 ;

ZIGIDGLO1 = ZIGIDGLO * null(ANCSDED2 - (ANNEEREV + 1)) ;
ZIGIDGLO2 = ZIGIDGLO * null(ANCSDED2 - (ANNEEREV + 2)) ;
ZIGIDGLO3 = ZIGIDGLO * null(ANCSDED2 - (ANNEEREV + 3)) ;
ZIGIDGLO4 = ZIGIDGLO * null(ANCSDED2 - (ANNEEREV + 4)) ;
ZIGIDGLO5 = ZIGIDGLO * null(ANCSDED2 - (ANNEEREV + 5)) ;
ZIGIDGLO6 = ZIGIDGLO * null(ANCSDED2 - (ANNEEREV + 6)) ;

ZIGIDRSE = positif(IDRSE) * (1 - null(V_IDRSEANT + DRSED + 0)) * positif(20 - V_NOTRAIT) * LIG2 ;

ZIGDRSED1 = ZIGIDRSE * null(ANCSDED2 - (ANNEEREV + 1)) ;
ZIGDRSED2 = ZIGIDRSE * null(ANCSDED2 - (ANNEEREV + 2)) ;
ZIGDRSED3 = ZIGIDRSE * null(ANCSDED2 - (ANNEEREV + 3)) ;
ZIGDRSED4 = ZIGIDRSE * null(ANCSDED2 - (ANNEEREV + 4)) ;
ZIGDRSED5 = ZIGIDRSE * null(ANCSDED2 - (ANNEEREV + 5)) ;
ZIGDRSED6 = ZIGIDRSE * null(ANCSDED2 - (ANNEEREV + 6)) ;

regle 902480:
application :  iliad ;

ZIGINFO = positif(ZIG_CSGD99 + ZIGIDGLO + ZIGIDRSE + ZIG_CSGDC + ZIGLOCO + ZIGRSECO) * LIG2 ;

regle 902490:
application :  iliad ;


LIGPVSURSI = positif(PVSURSI + CODRWA) * CNRLIG12 ;

LIGIREXITI = positif(IREXITI) * (1 - positif(IREXITS)) * CNRLIG12 ;

LIGIREXITS = positif(IREXITS) * (1 - positif(IREXITI)) * CNRLIG12 ;

LIGIREXIT = positif(PVIMPOS + CODRWB) * positif(PVSURSI + CODRWA) * CNRLIG12 ;

LIGPV3WBI = positif(PVIMPOS + CODRWB) * CNRLIG12 ;

LIG150BTER = positif(COD3TA + COD3TB) * LIG12 ;

LIGSURIMP = positif(SURIMP) * LIG12 ;

LIG_SURSIS = positif( LIGPVSURSI + LIGPV3WBI + LIG150BTER + LIGIREXITI
                      + LIGIREXITS + LIGIREXIT + LIGSURIMP ) * LIG12 ;

LIGSUR = LIG_SURSIS * positif(LIGREPPLU + LIGABDET + LIGABDETP + LIGRCMSOC + LIGCOD3SG
                              + LIGCOD3SL + LIGPV3SB + LIGCOD3WH + LIGRCMIMPAT + LIGABIMPPV + LIGABIMPMV + LIGROBOR
                              + LIGPVIMMO + LIGPVTISOC + LIGMOBNR + LIGDEPCHO + LIGDEPMOB + LIGZRS) ;

LIGI017 = positif(PVSURSI + PVIMPOS + CODRWA + CODRWB) * V_IND_TRAIT ;

regle 902500:
application :  iliad ;

LIG_CORRECT = positif_ou_nul(IAMD2) * INDREV1A8 * LIG12 ;

regle 902510:
application :  iliad ;

LIG_R8ZT = positif(V_8ZT + 0) * LIG12 ;
LIGRZRE = positif(CODZRE + 0) * LIG12 ;
LIGRZRF = positif(CODZRF + 0) * LIG12 ;

regle 902520:
application :  iliad ;

                 
LIGTXMOYPOS = positif(present(RMOND)+positif(VARRMOND)*present(DEFZU)) * (1 - positif(DEFRIMOND)) * LIG12 ;

LIGTXMOYNEG = positif(DMOND) * (1 - positif(DEFRIMOND)) * LIG12 ;

LIGTXPOSYT = positif(VARRMOND) * positif(IPTXMO) * positif(DEFRIMOND) * LIG12 ;

LIGTXNEGYT = (1 - LIGTXPOSYT) * positif(VARDMOND) * positif(IPTXMO) * positif(DEFRIMOND) * LIG12 ;

regle 902530:
application :  iliad ;

                 
LIGAMEETREV = positif(INDREV1A8) * LIG12 ;

regle 902540:
application : iliad  ;

LIGBICTOT = positif(MIBNPRV + MIBNPRC + MIBNPRP + present(MIBNPPVV) + present(MIBNPPVC) 
                    + present(MIBNPPVP) + present(MIBNPDCT) + present(COD5RZ) + present(COD5SZ)) * LIG2 ;
                 
LIGMIBNPPOS = positif_ou_nul(MIBNETNPTOT) * (1 - positif(LIGBICNP + DEFNP)) * LIG2 ;

LIGMIBNPNEG = (1 - LIGMIBNPPOS) * LIG2 ;

LIG_MIBP = (positif(somme(i=V,C,P:MIBVENi) + somme(i=V,C,P:MIBPRESi) + abs(MIB_NETCT) + 0)) * LIG2 ; 

LIGMIBPPOS = positif_ou_nul(MIBNETPTOT) * (1 - LIGBICPRO) * LIG2 ;

LIGMIBPNEG = (1 - LIGMIBPPOS) * LIG2 ;

regle 902550:
application : iliad  ;

LIGSPENP = positif(BNCNPV + BNCNPC + BNCNPP + LIGBNCNPPV + LIGBNCNPMV) * LIG2 ;
                 
LIGSPENPPOS = positif_ou_nul(SPENETNPF) * (1 - positif(LIGNOCEP + LIGDAB)) * LIG2 ;

LIGSPENPNEG = (1 - LIGSPENPPOS) * LIG2 ;

regle 902570:
application : iliad  ;


LIGREPTOUR = positif(INVLOCXN + COD7UY) * LIG12 ;

LIGLOGDOM = positif(DLOGDOM) * LIG12 ;

LIGLOGSOC = positif(DLOGSOC) * LIG12 ;

LIGDOMSOC1 = positif(DDOMSOC1) * LIG12 ;

LIGLOCENT = positif(DLOCENT) * LIG12 ;

LIGCOLENT = positif(DCOLENT) * LIG12 ;

LIGREPHA  = positif(INVLOCXV + COD7UZ) * LIG12 ;

LIGREHAB = positif(DREHAB) * LIG12 ;

LIGRESTKZ = present(COD7KZ) * LIG12 ;

LIGRESTIMO1 = positif(DRESTIMO1) * LIG12 ;

regle 902580:
application : iliad  ;

LIGREDAGRI = positif(DDIFAGRI) * LIG12 ;
LIGFORET = positif(DFORET) * LIG12 ;
LIGCOTFOR = positif(DCOTFOR) * LIG12 ; 

LIGRCIF = positif(REPCIF + REPCIFHSN1 + REPCIFHSN2 + REPCIFHSN3 + REPCIFHSN4) * LIG12 ;
LIGREP7UX = positif(REPCIFHSN4) * LIG12 ;
LIGREP7VM = positif(REPCIFHSN3) * LIG12 ;
LIGREP7VQ = positif(REPCIFHSN2) * LIG12 ;
LIGREP7VS = positif(REPCIFHSN1) * LIG12 ;
LIGREP7UP = positif(REPCIF) * LIG12 ;

LIGRCIFAD = positif(REPCIFAD + REPCIFADHSN1 + REPCIFADHSN2 + REPCIFADHSN3 + REPCIFADHSN4) * LIG12 ;
LIGREP7VP = positif(REPCIFADHSN4) * LIG12 ;
LIGREP7VN = positif(REPCIFADHSN3) * LIG12 ;
LIGREP7VR = positif(REPCIFADHSN2) * LIG12 ;
LIGREP7VL = positif(REPCIFADHSN1) * LIG12 ;
LIGREP7UA = positif(REPCIFAD) * LIG12 ;

LIGRCIFSIN = positif(REPCIFSIN + REPCIFSN1 + REPCIFSN2 + REPCIFSN3 + REPCIFSN4) * LIG12 ;
LIGREP7TJ = positif(REPCIFSN4) * LIG12 ;
LIGREP7TM = positif(REPCIFSN3) * LIG12 ;
LIGREP7TP = positif(REPCIFSN2) * LIG12 ;
LIGREP7TR = positif(REPCIFSN1) * LIG12 ;
LIGREP7UT = positif(REPCIFSIN) * LIG12 ;

LIGRCIFADSN = positif(REPCIFADSIN + REPCIFADSSN1 + REPCIFADSSN2 + REPCIFADSSN3 + REPCIFADSSN4) * LIG12 ;
LIGREP7TK = positif(REPCIFADSSN4) * LIG12 ;
LIGREP7TO = positif(REPCIFADSSN3) * LIG12 ;
LIGREP7TQ = positif(REPCIFADSSN2) * LIG12 ;
LIGREP7TS = positif(REPCIFADSSN1) * LIG12 ;
LIGREP7UB = positif(REPCIFADSIN) * LIG12 ;

LIGNFOREST = positif(REPSIN + REPFORSIN + REPFORSIN2 + REPFORSIN3 + REPNIS) * LIG12 ;
LIGREPSIN = positif(REPSIN) * LIG12 ;
LIGSINFOR = positif(REPFORSIN) * LIG12 ;
LIGSINFOR2 = positif(REPFORSIN2) * LIG12 ;
LIGSINFOR3 = positif(REPFORSIN3) * LIG12 ;
LIGREPNIS = positif(REPNIS) * LIG12 ;

LIGREPREST = positif(REPRESTKZ + REPRESTXY) * CNRLIG12 ;
LIGREPKZ = positif(REPRESTKZ) * CNRLIG12 ;
LIGREPXY = positif(REPRESTXY) * CNRLIG12 ;

LIGFIPC = positif(DFIPC) * LIG12 ;
LIGFIPDOM = positif(DFIPDOM) * LIG12 ;
LIGPRESSE = positif(DPRESSE) * LIG12 ;
LIGCINE = positif(DCINE) * LIG12 ;
LIGRIRENOV = positif(DRIRENOV) * LIG12 ;
LIGREPAR = positif(NUPROPT) * LIG12 ;
LIGREPREPAR = positif(REPNUREPART) * CNRLIG12 ;
LIGREPARN = positif(REPAR) * CNRLIG12 ;
LIGREPAR1 = positif(REPAR1) * CNRLIG12 ;
LIGREPAR2 = positif(REPAR2) * CNRLIG12 ;
LIGREPAR3 = positif(REPAR3) * CNRLIG12 ;
LIGREPAR4 = positif(REPAR4) * CNRLIG12 ;
LIGREPAR5 = positif(REPAR5) * CNRLIG12 ;
LIGREPAR6 = positif(REPAR6) * CNRLIG12 ;
LIGREPAR7 = positif(REPAR7) * CNRLIG12 ;
LIGREPAR8 = positif(REPAR8) * CNRLIG12 ;

LIGRESTIMO = (present(COD7NX) + present(COD7NY)) * LIG12 ;

LIGRCODOU = positif(COD7OU + 0) * CNRLIG12 ;
LIGOU1 = LIGRCODOU * null(RCODOU1 - RCODOU8) ;
LIGOU2 = LIGRCODOU * (1 - null(RCODOU1 - RCODOU8)) ;

LIGRCODOY = positif(COD7OY + 0) * CNRLIG12 ;
LIGOY1 = LIGRCODOY * null(RCODOY1 - RCODOY8) ;
LIGOY2 = LIGRCODOY * (1 - null(RCODOY1 - RCODOY8)) ;

LIGRCODOX = positif(COD7OX + 0) * CNRLIG12 ;
LIGOX1 = LIGRCODOX * null(RCODOX1 - RCODOX8) ;
LIGOX2 = LIGRCODOX * (1 - null(RCODOX1 - RCODOX8)) ;

LIGRCODOW = positif(COD7OW + 0) * CNRLIG12 ;
LIGOW1 = LIGRCODOW * null(RCODOW1 - RCODOW8) ;
LIGOW2 = LIGRCODOW * (1 - null(RCODOW1 - RCODOW8)) ;

LIGRCODOV = positif(COD7OV + 0) * CNRLIG12 ;
LIGOV1 = LIGRCODOV * null(RCODOV1 - RCODOV8) ;
LIGOV2 = LIGRCODOV * (1 - null(RCODOV1 - RCODOV8)) ;

LIGRCODJT = positif(LOCMEUBJT + 0) * positif(RCODJT1 + RCODJT8) * CNRLIG12 ;
LIGJT1 = LIGRCODJT * null(RCODJT1 - RCODJT8) ;
LIGJT2 = LIGRCODJT * (1 - null(RCODJT1 - RCODJT8)) ;

LIGRLOCIDFG = positif(LOCMEUBID) * positif(RLOCIDFG1 + RLOCIDFG8) * CNRLIG12 ;
LIGIDFG1 = LIGRLOCIDFG * null(RLOCIDFG1 - RLOCIDFG8) ;
LIGIDFG2 = LIGRLOCIDFG * (1 - null(RLOCIDFG1 - RLOCIDFG8)) ;

LIGNEUV = positif(LOCRESINEUV + INVNPROF1) * positif(RESINEUV1 + RESINEUV8) * CNRLIG12 ;
LIGNEUV1 = LIGNEUV * null(RESINEUV1 - RESINEUV8) ;
LIGNEUV2 = LIGNEUV * (1 - null(RESINEUV1 - RESINEUV8)) ;

LIGVIEU = positif(RESIVIEU) * positif(RESIVIEU1 + RESIVIEU8) * CNRLIG12 ;
LIGIM1 = LIGVIEU * null(RESIVIEU1 - RESIVIEU8) ;
LIGIM2 = LIGVIEU * (1 - null(RESIVIEU1 - RESIVIEU8)) ;

LIGREPLOC15 = positif(REPMEUPE + REPMEUPJ + REPMEUPO + REPMEUPT + REPMEUPY + REP13MEU) * CNRLIG12 ;
LIGREP7PE = positif(REPMEUPE) * CNRLIG12 ;
LIGREP7PJ = positif(REPMEUPJ) * CNRLIG12 ;
LIGREP7PO = positif(REPMEUPO) * CNRLIG12 ;
LIGREP7PT = positif(REPMEUPT) * CNRLIG12 ;
LIGREP7PY = positif(REPMEUPY) * CNRLIG12 ;
LIGREP13MEU = positif(REP13MEU) * CNRLIG12 ;

LIGREPLOC12 = positif(REPMEUJS + REPMEUPD + REPMEUPI + REPMEUPN + REPMEUPS + REPMEUPX + REP12MEU) * CNRLIG12 ;
LIGREP7JS = positif(REPMEUJS) * CNRLIG12 ;
LIGREP7PD = positif(REPMEUPD) * CNRLIG12 ;
LIGREP7PI = positif(REPMEUPI) * CNRLIG12 ;
LIGREP7PN = positif(REPMEUPN) * CNRLIG12 ;
LIGREP7PS = positif(REPMEUPS) * CNRLIG12 ;
LIGREP7PX = positif(REPMEUPX) * CNRLIG12 ;
LIGREP12MEU = positif(REP12MEU) * CNRLIG12 ;

LIGREPLOC11 = positif(REPMEUIZ + REPMEUJI + REPMEUPC + REPMEUPH + REPMEUPM + REPMEUPR + REPMEUPW + REP11MEU) * CNRLIG12 ;
LIGREP7IZ = positif(REPMEUIZ) * CNRLIG12 ;
LIGREP7JI = positif(REPMEUJI) * CNRLIG12 ;
LIGREP7PC = positif(REPMEUPC) * CNRLIG12 ;
LIGREP7PH = positif(REPMEUPH) * CNRLIG12 ;
LIGREP7PM = positif(REPMEUPM) * CNRLIG12 ;
LIGREP7PR = positif(REPMEUPR) * CNRLIG12 ;
LIGREP7PW = positif(REPMEUPW) * CNRLIG12 ;
LIGREP11MEU = positif(REP11MEU) * CNRLIG12 ;

LIGREPLOC10 = positif(REPMEUIH + REPMEUJC + REPMEUPB + REPMEUPG + REPMEUPL + REPMEUPQ + REPMEUPV + REP10MEU) * CNRLIG12 ;
LIGREP7IH = positif(REPMEUIH) * CNRLIG12 ;
LIGREP7JC = positif(REPMEUJC) * CNRLIG12 ;
LIGREP7PB = positif(REPMEUPB) * CNRLIG12 ;
LIGREP7PG = positif(REPMEUPG) * CNRLIG12 ;
LIGREP7PL = positif(REPMEUPL) * CNRLIG12 ;
LIGREP7PQ = positif(REPMEUPQ) * CNRLIG12 ;
LIGREP7PV = positif(REPMEUPV) * CNRLIG12 ;
LIGREP10MEU = positif(REP10MEU) * CNRLIG12 ;

LIGREPLOC9 = positif(REPMEUIX + REPMEUIY + REPMEUPA + REPMEUPF + REPMEUPK + REPMEUPP + REPMEUPU + REP9MEU) * CNRLIG12 ;
LIGREP7IX = positif(REPMEUIX) * CNRLIG12 ;
LIGREP7IY = positif(REPMEUIY) * CNRLIG12 ;
LIGREP7PA = positif(REPMEUPA) * CNRLIG12 ;
LIGREP7PF = positif(REPMEUPF) * CNRLIG12 ;
LIGREP7PK = positif(REPMEUPK) * CNRLIG12 ;
LIGREP7PP = positif(REPMEUPP) * CNRLIG12 ;
LIGREP7PU = positif(REPMEUPU) * CNRLIG12 ;
LIGREP9MEU = positif(REP9MEU) * CNRLIG12 ;

regle 902590:
application : iliad  ;

LIGILMNP1 = positif(DILMNP1) * LIG12 ;
LIGILMNP2 = positif(DILMNP2) * LIG12 ;
LIGILMNP3 = positif(DILMNP3) * LIG12 ;
LIGILMNP4 = positif(DILMNP4) * LIG12 ;
LIGILMIY = positif(DILMIY) * LIG12 ;
LIGILMPA = positif(DILMPA) * LIG12 ;
LIGILMPB = positif(DILMPB) * LIG12 ;
LIGILMPC = positif(DILMPC) * LIG12 ;
LIGILMPD = positif(DILMPD) * LIG12 ;
LIGILMPE = positif(DILMPE) * LIG12 ;
LIGILMPF = positif(DILMPF) * LIG12 ;
LIGILMPK = positif(DILMPK) * LIG12 ;
LIGILMPG = positif(DILMPG) * LIG12 ;
LIGILMPL = positif(DILMPL) * LIG12 ;
LIGILMPH = positif(DILMPH) * LIG12 ;
LIGILMPM = positif(DILMPM) * LIG12 ;
LIGILMPI = positif(DILMPI) * LIG12 ;
LIGILMPN = positif(DILMPN) * LIG12 ;
LIGILMPJ = positif(DILMPJ) * LIG12 ;
LIGILMPO = positif(DILMPO) * LIG12 ;
LIGINVRED = positif(DINVRED) * LIG12 ;
LIGILMJC = positif(DILMJC) * LIG12 ;
LIGILMJI = positif(DILMJI) * LIG12 ;
LIGILMJS = positif(DILMJS) * LIG12 ;
LIGPROREP = positif(DPROREP) * LIG12 ;
LIGREPNPRO = positif(DREPNPRO) * LIG12 ;
LIGMEUREP = positif(DREPMEU) * LIG12 ;
LIGILMIC = positif(DILMIC) * LIG12 ;
LIGILMIB = positif(DILMIB) * LIG12 ;
LIGILMIA = positif(DILMIA) * LIG12 ;
LIGILMJY = positif(DILMJY) * LIG12 ;
LIGILMJX = positif(DILMJX) * LIG12 ;
LIGILMJW = positif(DILMJW) * LIG12 ;
LIGILMJV = positif(DILMJV) * LIG12 ;
LIGILMOE = positif(DILMOE) * LIG12 ;
LIGILMOD = positif(DILMOD) * LIG12 ;
LIGILMOC = positif(DILMOC) * LIG12 ;
LIGILMOB = positif(DILMOB) * LIG12 ;
LIGILMOA = positif(DILMOA) * LIG12 ;
LIGILMOJ = positif(DILMOJ) * LIG12 ;
LIGILMOI = positif(DILMOI) * LIG12 ;
LIGILMOH = positif(DILMOH) * LIG12 ;
LIGILMOG = positif(DILMOG) * LIG12 ;
LIGILMOF = positif(DILMOF) * LIG12 ;
LIGILMOO = positif(DILMOO) * LIG12 ;
LIGILMON = positif(DILMON) * LIG12 ;
LIGILMOM = positif(DILMOM) * LIG12 ;
LIGILMOL = positif(DILMOL) * LIG12 ;
LIGILMOK = positif(DILMOK) * LIG12 ;
LIGRESIVIEU = positif(DRESIVIEU) * LIG12 ;
LIGRESINEUV = positif(DRESINEUV) * LIG12 ;
LIGLOCIDEFG = positif(DLOCIDEFG) * LIG12 ;
LIGCODJTJU = positif(DCODJTJU) * LIG12 ;
LIGCODOU = positif(DCODOU) * LIG12 ;
LIGCODOV = positif(DCODOV) * LIG12 ;
LIGCODOW = positif(DCODOW) * LIG12 ;

regle 902600:
application : iliad  ;

LIGTAXASSUR = positif(present(CESSASSV) + present(CESSASSC)) * LIG12 ;
LIGTAXASSURV = present(CESSASSV) * LIG12 ;
LIGTAXASSURC = present(CESSASSC) * LIG12 ;

LIGIPCAP = positif(present(PCAPTAXV) + present(PCAPTAXC)) * LIG12 ;
LIGIPCAPV = present(PCAPTAXV) * LIG12 ;
LIGIPCAPC = present(PCAPTAXC) * LIG12 ;

LIGITAXLOY = present(LOYELEV) * LIG12 ;

LIGIHAUT = positif(CHRAVANT) * (1 - positif(TEFFHRC + COD8YJ)) * LIG12 ;

LIGHRTEFF = positif(CHRTEFF) * positif(TEFFHRC + COD8YJ) * LIG12 ;

LIGHR3WT = positif(present(COD3WT)) * LIG12 ;

regle 902610:
application : iliad  ;

LIGCOMP01 = positif(BPRESCOMP01) * CNRLIG12 ;

regle 902620:
application : iliad  ;

LIGDEFBA = positif(DEFBA1 + DEFBA2 + DEFBA3 + DEFBA4 + DEFBA5 + DEFBA6) * LIG12 ;
LIGDEFBA1 = positif(DEFBA1) * LIG12 ;
LIGDEFBA2 = positif(DEFBA2) * LIG12 ;
LIGDEFBA3 = positif(DEFBA3) * LIG12 ;
LIGDEFBA4 = positif(DEFBA4) * LIG12 ;
LIGDEFBA5 = positif(DEFBA5) * LIG12 ;
LIGDEFBA6 = positif(DEFBA6) * LIG12 ;

LIGDFRCM = positif(DFRCMN+DFRCM1+DFRCM2+DFRCM3+DFRCM4+DFRCM5) * CNRLIG12 ;
LIGDFRCMN = positif(DFRCMN) * CNRLIG12 ;
LIGDFRCM1 = positif(DFRCM1) * CNRLIG12 ;
LIGDFRCM2 = positif(DFRCM2) * CNRLIG12 ;
LIGDFRCM3 = positif(DFRCM3) * CNRLIG12 ;
LIGDFRCM4 = positif(DFRCM4) * CNRLIG12 ;
LIGDFRCM5 = positif(DFRCM5) * CNRLIG12 ;

LIG2TUV = positif(COD2TU + COD2TV + COD2TW) * LIG12 ;
LIGR2VQ = positif(COD2VQ + 0) * LIG12 ;
LIGDRCVM = positif(DPVRCM) * LIG12 ;
LIGDRFRP = positif(DRFRP) * LIG12 ;

BAMVV = (COD5XN - BAF1AV) * positif(COD5XN - BAF1AV) ;
BAMVC = (COD5YN - BAF1AC) * positif(COD5YN - BAF1AC) ;
BAMVP = (COD5ZN - BAF1AP) * positif(COD5ZN - BAF1AP) ;
LIGBAMVV = positif(BAMVV) * LIG12 ;
LIGBAMVC = positif(BAMVC) * LIG12 ;
LIGBAMVP = positif(BAMVP) * LIG12 ;

LIGDLMRN = positif(DLMRN6 + DLMRN5 + DLMRN4 + DLMRN3 + DLMRN2 + DLMRN1) * LIG12 ;
LIGDLMRN6 = positif(DLMRN6) * LIG12 ;
LIGDLMRN5 = positif(DLMRN5) * LIG12 ;
LIGDLMRN4 = positif(DLMRN4) * LIG12 ;
LIGDLMRN3 = positif(DLMRN3) * LIG12 ;
LIGDLMRN2 = positif(DLMRN2) * LIG12 ;
LIGDLMRN1 = positif(DLMRN1) * LIG12 ;

LIGBNCDF = positif(BNCDF1 + BNCDF2 + BNCDF3 + BNCDF4 + BNCDF5 + BNCDF6) * LIG12 ;
LIGBNCDF6 = positif(BNCDF6) * LIG12 ;
LIGBNCDF5 = positif(BNCDF5) * LIG12 ;
LIGBNCDF4 = positif(BNCDF4) * LIG12 ;
LIGBNCDF3 = positif(BNCDF3) * LIG12 ;
LIGBNCDF2 = positif(BNCDF2) * LIG12 ;
LIGBNCDF1 = positif(BNCDF1) * LIG12 ;

LIGMBDREPNPV = positif(MIBDREPNPV) * LIG12 ;
LIGMBDREPNPC = positif(MIBDREPNPC) * LIG12 ;
LIGMBDREPNPP = positif(MIBDREPNPP) * LIG12 ;

LIGMIBDREPV = positif(MIBDREPV) * LIG12 ;
LIGMIBDREPC = positif(MIBDREPC) * LIG12 ;
LIGMIBDREPP = positif(MIBDREPP) * LIG12 ;

LIGSPDREPNPV = positif(SPEDREPNPV) * LIG12 ;
LIGSPDREPNPC = positif(SPEDREPNPC) * LIG12 ;
LIGSPDREPNPP = positif(SPEDREPNPP) * LIG12 ;

LIGSPEDREPV = positif(SPEDREPV) * LIG12 ;
LIGSPEDREPC = positif(SPEDREPC) * LIG12 ;
LIGSPEDREPP = positif(SPEDREPP) * LIG12 ;

regle 902630:
application :  iliad ;


LIG_MEMO = positif(LIGPRELIB + LIG_SURSIS + LIGREPPLU + LIGABDET + LIGABDETP 
                     + LIGROBOR + LIGPVIMMO + LIGPVTISOC + LIGMOBNR 
                     + LIGDEPCHO + LIGDEPMOB + LIGZRS + LIGCOD3SG + LIGCOD3SL)
             + positif(LIG3525 + LIGRCMSOC + LIGRCMIMPAT + LIGABIMPPV + LIGABIMPMV + LIGPV3SB) * CNRLIG12 ;

regle 902640:
application :  iliad ;

LIGPRELIB = positif(present(PPLIB) + present(RCMLIB) + present(COD2XX) + present(COD2VM)) * LIG0 * LIG2 ;

LIG3525 = positif(RTNC) * CNRLIG12 ;

LIGREPPLU = positif(REPPLU) * LIG12 ;
LIGPVIMPOS = positif(PVIMPOS) * LIG12 ;

LIGABDET = positif(GAINABDET) * LIG12 ;
ABDEPRET = ABDETPLUS ;
LIGABDETP = positif(ABDEPRET) * LIG12 ;

LIGCOD3SG = positif(COD3SG) * LIG12 ;
LIGCOD3SL = positif(COD3SL) * LIG12 ;
LIGPV3SB = positif(PVBAR3SB) * LIG12 ;
LIGCOD3WH = positif(PVREPORT) * LIG12 ;

LIGRCMSOC = positif(RCMSOC) * CNRLIG12 ;
LIGRCMIMPAT = positif(RCMIMPAT) * CNRLIG12 ;
LIGABIMPPV = positif(ABIMPPV) * CNRLIG12 ;
LIGABIMPMV = positif(ABIMPMV) * CNRLIG12 ;

LIGCVNSAL = positif(CVNSALC) * LIG12 ;
LIGCDIS = positif(GSALV + GSALC) * CNRLIG12 ;
LIGROBOR = positif(RFROBOR) * LIG12 ;
LIGPVIMMO = positif(PVIMMO) * LIG12 ;
LIGPVTISOC = positif(PVTITRESOC) * LIG12 ;
LIGMOBNR = positif(PVMOBNR) * LIG12 ;

DEPMOB = (RDEQPAHA + RDTECH) * positif(V_NOTRAIT - 10) ;

LIGDEPMOB = positif(DIFF7WZ + DIFF7WD) * positif(DEPMOB) * CNRLIG12 ;

DEPCHO = (COD7XD + COD7XE + COD7XF + COD7XG) * positif(V_NOTRAIT - 10) ;
LIGDEPCHO = positif(DEPCHO) * CNRLIG12 ;

LIGZRS = positif(CODZRS) * LIG12 ;

LIGDEFPLOC = positif(DEFLOC1 + DEFLOC2 + DEFLOC3 + DEFLOC4 + DEFLOC5 + DEFLOC6 + DEFLOC7 + DEFLOC8 + DEFLOC9 + DEFLOC10) * LIG12 ;
LIGPLOC1 = positif(DEFLOC1) * LIG12 ;
LIGPLOC2 = positif(DEFLOC2) * LIG12 ;
LIGPLOC3 = positif(DEFLOC3) * LIG12 ;
LIGPLOC4 = positif(DEFLOC4) * LIG12 ;
LIGPLOC5 = positif(DEFLOC5) * LIG12 ;
LIGPLOC6 = positif(DEFLOC6) * LIG12 ;
LIGPLOC7 = positif(DEFLOC7) * LIG12 ;
LIGPLOC8 = positif(DEFLOC8) * LIG12 ;
LIGPLOC9 = positif(DEFLOC9) * LIG12 ;
LIGPLOC10 = positif(DEFLOC10) * LIG12 ;

regle 902650:
application :  iliad ;


LIGDCSGD = positif(DCSGD) * null(5 - V_IND_TRAIT) * INDCTX * LIG12 ;

regle 902660:
application :  iliad ;


LIGREPCORSE = positif(REPCORSE) * LIG12 ;

LIGREPRECH = positif(REPRECH) * LIG12 ;

LIGREPCICE = positif(REPCICE) * LIG12 ;

LIGPME = positif(REPINVPME2 + REPINVPME1) * CNRLIG12 ;

regle 902670:
application :  iliad ;

LIGIBAEX = positif(REVQTOT) * LIG12 
	     * (1 - INDTXMIN) * (1 - INDTXMOY) 
	     * (1 - V_CNR * (1 - LIG1522)) ;

regle 902680:
application :  iliad  ;

LIGANNUL2042 = LIG2 * LIG0 ;

LIG121 = positif(DEFRITS) * LIGANNUL2042 ;
LIG931 = positif(DEFRIRCM)*positif(RCMFR) * LIGANNUL2042 ;
LIG936 = positif(DEFRIRCM)*positif(REPRCM) * LIGANNUL2042 ;
LIG1111 = positif(DFANTIMPUBAR) * LIGANNUL2042 ;
LIG1112 = positif(DFANTIMPU) * positif(DEFRF4BC) * positif(RDRFPS) * LIGANNUL2042 ;
LIGDFANT = positif(DFANTIMPUQUO) * LIGANNUL2042 ;

regle 902690:
application :  iliad ;

LIGTRCT = positif(V_TRCT) ;

regle 902700:
application :  iliad ;

LIGVERSUP = positif(AUTOVERSSUP) ;

regle 902710:
application : iliad  ;


INDRESTIT = positif(IREST + 0) ;

INDIMPOS = positif(null(1 - NATIMP) + null(71 - NATIMP) + 0) ;

regle isf 902740:
application : iliad  ;

LIG_AVISIFI = (positif(LIM_IFIINF)) * present(IFIPAT);


LIGIFI9AA = positif(COD9AA + 0) *(1 - positif(ANNUL2042)) ;
LIGIFI9AB = positif(COD9AB + 0)*(1 - positif(ANNUL2042)) ;
LIGIFI9AC = positif(COD9AC + 0) * (1 - positif(ANNUL2042)) ;
LIGIFI9AD = positif(COD9AD + 0)*(1 - positif(ANNUL2042)) ;
LIGIFI9BA = positif(COD9BA + 0)*(1 - positif(ANNUL2042)) ;
LIGIFI9BB = positif(COD9BB + 0)*(1 - positif(ANNUL2042)) ;
LIGIFI9CA = positif(COD9CA + 0)*(1 - positif(ANNUL2042)) ;
LIGIFIACT = positif(IFIACT)*(1 - positif(ANNUL2042)) ;

LIGIFI9GF = positif(COD9GF + 0)*(1 - positif(ANNUL2042)) ; 
LIGIFI9GH = positif(COD9GH + 0)*(1 - positif(ANNUL2042)) ;
LIGIFIPAS = positif(IFIPAS)*(1 - positif(ANNUL2042)) ;

LIGIFIBASE =  LIGIFI * (1 - positif(ANNUL2042)) ;

LIGIFI = positif(IFIPAT - LIM_IFIINF)*(1 - positif(ANNUL2042)) ;

LIGIFIDIRECT = positif(LIGIFI9AA + LIGIFI9AB + LIGIFI9AC + LIGIFI9AD + LIGIFI9BA + LIGIFI9BB) * LIGIFI*(1 - positif(ANNUL2042)) ;


LIGIFINDIR = LIGIFIDIRECT * LIGIFI9CA * LIGIFI *(1 - positif(ANNUL2042)) ;

LIGIFINDIR1 = LIGIFI9CA * LIGIFI * (1 - LIGIFINDIR) * (1 - LIGIFIDIRECT) *(1 - positif(ANNUL2042)) ;

LIGIFIDEC = positif(IFI1) * positif(DECIFI) * LIGIFI * (1 - positif(ANNUL2042)) ;  


LIGIFIBRUT = (positif(IFI2) * LIGIFI * (1 - null(5 - V_IND_TRAIT))
             * positif(RDONIFI1 + RDONIFI2)
             * positif(COD9NC + COD9NG)
             + positif(IFI2) * null(5 - V_IND_TRAIT)
             * positif(present(COD9NC) + present(COD9NG))) * LIGIFIDEC * (1 - positif(ANNUL2042)) ;
                       

LIGIFIRAN = (positif(COD9NC) * (1 - null(5 - V_IND_TRAIT)) * LIGIFI 
             + present(COD9NC) * positif(DIFIBASE) * null(5 - V_IND_TRAIT)) * (1 - positif(ANNUL2042)) ;

LIGIFICEE = (positif(COD9NG) * (1 - null(5 - V_IND_TRAIT)) * LIGIFI 
             + present(COD9NG) * positif(DIFIBASE) * null(5 - V_IND_TRAIT)) * (1 - positif(ANNUL2042)) ;

LIGIFIDON =(1 - positif(ANNUL2042))* positif(LIGIFIRAN + LIGIFICEE) * LIGIFI ;

LIGIFIRED =(1 - positif(ANNUL2042))* LIGIFIDON * LIGIFI
            * (1 - positif(null((CODE_2042 + CMAJ_ISF)- 8) + null(CMAJ_ISF - 34)+null((CODE_2042 + CMAJ_ISF)- 11)+null(CODE_2042- 3)+null(CODE_2042- 5)+null(CODE_2042- 55)) * (1 - COD9ZA));

LIGIFIREDPEN8 =(1 - positif(ANNUL2042))* LIGIFIDON * LIGIFI
                * positif(null(CODE_2042 + CMAJ_ISF - 8) + null(CMAJ_ISF - 34) + null(CODE_2042 + CMAJ_ISF - 11) + null(CODE_2042 - 3) + null(CODE_2042 - 5) + null(CODE_2042 - 55))
                * (1 - COD9ZA) ; 

LIGIFIAPR =  positif(IFIETR + IFIPLAF)
             * (1 - LIGIFIIMPU)
             * (positif(IFI3) * LIGIFI * (1 - null(5 - V_IND_TRAIT))
                * positif(RDONIFI1 + RDONIFI2)
                * positif(COD9NC + COD9NG)
                + positif(IFI3) * null(5 - V_IND_TRAIT)
                  * positif(present(COD9NC) + present(COD9NG)))
             * (1 - positif(ANNUL2042)) ;


LIGIFIIMPU =(1 - positif(ANNUL2042))* positif(DIFIBASE) * positif(IFIETR + IFIPLAF)
             * LIGIFIDEC * (1 - positif(COD9NC + COD9NG ))
             * LIGIFI * (1 - positif(ANNUL2042)) 
	     * ((1 - null(5 - V_IND_TRAIT))
                + null(5 - V_IND_TRAIT) * (1 - positif(LIGIFIRED + LIGIFIREDPEN8))) ;


LIGIPLAF = positif(PLAFIFI) * (1-null(5-V_IND_TRAIT))
            * LIGIFI * (1 - positif(ANNUL2042))
	    + positif(PLAFIFI) * positif(DIFIBASE) * (1 - positif(ANNUL2042)) * null(5-V_IND_TRAIT) ; 


LIGIFIE = positif(DIFIBASE) * positif(COD9RS + 0) * (1 - positif(ANNUL2042))
          * (1 - null(5 - V_IND_TRAIT)) * LIGIFI
          + positif(DIFIBASE) * present(COD9RS)
            * (1 - positif(ANNUL2042)) * null(5 - V_IND_TRAIT) ;



LIGIFICOR1 = present(IFI4BIS) * positif(DIFIBASE) * positif(PIFI)
             * (1 - positif(SEUIL_12 - IFI4BIS) * (1 - null(IFI4BIS)))
             * LIGIFI * (1 - positif(ANNUL2042))
             * (1 - positif(V_NOTRAIT-20))
             + positif(V_NOTRAIT-20) * LIGIFI * (1 - positif(ANNUL2042)) ;

LIGIFINOPEN = present(IFITOT) * positif(DIFIBASE) * (1 - positif(PIFI))
              * (1 - LIGIFICOR1) * LIGIFI * (1 - positif(ANNUL2042)) ;
				
LIGIFIRET = positif(RETIFI) * LIGIFI * (1 - positif(ANNUL2042))
            * (1 - positif(SEUIL_12 - IFI4BIS) * (1 - null(IFITOT))) ;

LIGIFIRET22 = LIGIFIRET * positif(RETIFIRED)*(1 - positif(ANNUL2042)) ;
 
LIGNMAJIFI1 = positif(NMAJIFI1) * LIGIFI * (1 - positif(ANNUL2042))
              * (1 - positif(SEUIL_12 - IFI4BIS) * (1-null(IFITOT))) ;

LIGIFI9749 =(1 - positif(ANNUL2042))* LIGNMAJIFI1 * (1 - LIGIFIRET) ;

LIGIFI17281 = positif(NMAJIFI1) * LIGIFI * (1 - positif(ANNUL2042))
              * (1 - positif(SEUIL_12 - IFI4BIS) * (1 - null(IFITOT)))
              * (1 - positif(V_FLAGR34 + null(CMAJ_ISF - 34))) ;

LIGIFI17285 = positif(NMAJIFI1) * LIGIFI * (1 - positif(ANNUL2042))
               * (1 - positif( SEUIL_12 - IFI4BIS) * (1 - null(IFITOT)))
               * positif(V_FLAGR34 + null(CMAJ_ISF - 34)) ;

LIGNMAJIFI4 = positif(NMAJIFI4) * LIGIFI * (1 - positif(ANNUL2042)) * (1 - positif(SEUIL_12 - IFI4BIS) * (1 - null(IFITOT))) ;


LIGIFI1729 = positif(NMAJIFI4) * LIGIFI * (1 - positif(ANNUL2042)) * (1 - positif(SEUIL_12 - IFI4BIS) * (1 - null(IFITOT))) ;

LIGIFIANT = positif(V_ANTIFI) * positif(V_NOTRAIT-20);

INDCTX23 = positif(null(V_NOTRAIT - 23) + null(V_NOTRAIT - 33) + null(V_NOTRAIT - 43) + null(V_NOTRAIT - 53) + null(V_NOTRAIT - 63)) ;

LIGIFINET = (positif(PIFI)*positif(DIFIBASE) * (1-null(5-V_IND_TRAIT))
               * (1 - positif( SEUIL_12 - IFI4BIS)*(1-null(IFITOT)))
              * LIGIFI * (1 - positif(ANNUL2042))
            + (null(5-V_IND_TRAIT)) * positif(LIGIFIRET + LIGNMAJIFI1)
              * positif(IFINAP) * (1 - positif( SEUIL_12 - IFINAP)))
           * (1 - positif(INDCTX23)) ;


INDIS26 = positif(null(V_NOTRAIT - 26) + null(V_NOTRAIT - 36) + null(V_NOTRAIT - 46) + null(V_NOTRAIT - 56) + null(V_NOTRAIT - 66)) ;

LIGIFI9269 = (1 - positif(LIGIFIRET + LIGNMAJIFI1)) * (1 - positif( SEUIL_12 - IFINAP)) * INDIS26 ;

LIGIFIRECOU = present(IFIRECOUVR);

LIGIFIANNUL = present(IFIPAT) * positif(ANNUL2042) ;

IND9HI0 = INDCTX23 * null( 5-V_IND_TRAIT ) * present(IFIPAT);

LIGIFIDEG = (1 - LIGIFIDEGR) * IND9HI0 * positif(IFIDEG) ;

LIGIFIDEGR = (null(2- (positif(SEUIL_8 - IFIDEGR) + positif_ou_nul(IFITOT-SEUIL_12)))
              + null(V_ANTIFI))
             * INDCTX23 * null(5-V_IND_TRAIT) * (1 - positif(ANNUL2042)) ;


LIGIFIZERO = null(IFITOT) * positif(20-V_NOTRAIT) * LIGIFI * (1 - positif(ANNUL2042)) ;	 

LIGIFINMR = positif(SEUIL_12 - IFITOT) * (1 - null(IFITOT))
           * (1 - positif(INDCTX23)) * positif(20 - V_NOTRAIT)
           * LIGIFI * (1 - positif(ANNUL2042)) ;


LIGIFINMRIS = positif(SEUIL_12 - IFINAP) * INDIS26 * positif(V_NOTRAIT - 20) * (1 - positif(ANNUL2042)) ; 

LIGIFI0DEG = IND9HI0 * null(IFI4BIS) * (1 - positif(ANNUL2042)) ;

LIGIFINMRDEG = (1 - LIGIFIDEGR) * (1 - null(IFITOT))
               * positif(SEUIL_12 - IFI4BIS) * positif(DIFIBASE)
               * INDCTX23 * null(5-V_IND_TRAIT) * (1 - positif(ANNUL2042)) ;

LIGIFIINF8 = IND9HI0 * LIGIFIDEGR * (1 - positif(ANNUL2042)) ;

LIGIFINEW = present(IFIPAT) * (1 - positif(20-V_NOTRAIT)) * null(5 - V_IND_TRAIT) * (1 - positif(ANNUL2042)) ;

regle 902745:
application :  iliad ;

LIGIFITRCT = present(IFIPAT) * positif(V_TRCT) ;
