#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2019]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2019 
#au titre des revenus perçus en 2018. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************

# ======================================================================
# Chapitre 1 : Net a payer
# ======================================================================

regle 101000:
application : bareme ;


RC1 = positif( NAPI + 1 - SEUIL_12 ) +0 ;


regle 101010:
application : iliad ;

NAPT = NAPTEMPCX - TOTIRPSANT ;
NAPTIR = IRNET + TAXANET + TAXLOYNET + PCAPNET + HAUTREVNET
	    - IRESTITIR ;

regle 101020:
application : iliad ;






NAPCOROLIR = (TOTIRCUM - NONMER -RECUMIR + NONREST) * positif(20 - V_NOTRAIT)

             + ( 0 * null( 2 - null(VARPS) - positif(1 - NATIMP))
                + max(0,NAPT+COMPENSPS-VARPS)* (1 - null( 2 - null(VARPS) - positif(1 - NATIMP)))
               ) * (1 - positif(20 - V_NOTRAIT)) ;

NAPCOROLCS = max(0, NAPCR61 - V_ANTCR) ;

regle 101030:
application : iliad ;

RC1 = si ( NAPINI - V_ANTIR - IRCUM_A + RECUMBIS >= SEUIL_12 )
      alors (1)
      sinon (0)
      finsi ;

regle 101040:
application : iliad ;


IAVIMBIS = IRB + PIR + BRASAR + NRINET ;

IAVIMO = (max(0,max(ID11-ADO1,IMI)-RED) + ITP + COD8EA + REI + PIR + BRASAR + NRINET) * V_CNR ;

regle 101050:
application : bareme , iliad ;


NAPI = ( IRD + PIRD - IRANT ) 
       + TAXASSUR
       + IPCAPTAXT
       + IHAUTREVT + CHRPVIMP
       + TAXLOY
       + BRASAR ;

regle 101060:
application : iliad ;


INTMS = inf( MOISAN / 10000 );
INTAN = (( MOISAN/10000 - INTMS )*10000)  * present(MOISAN) ;
TXINT = (positif(2022-arr(INTAN))*max(0, (INTAN - (ANNEEREV+1) )* 12 + INTMS - 6 ) * TXMOISRETARD2 
         + positif_ou_nul(ANNEEREV-2022)*max(0, (INTAN - (ANNEEREV+1) )* 12 + INTMS - 6 ) * TXMOISRETARD2 
         + (1-positif(2022-arr(INTAN)))*(1-positif_ou_nul(ANNEEREV-2022))
	   * (((2022 - (ANNEEREV+1))*12 - 6) * (TXMOISRETARD * positif(2022 - (ANNEEREV+1)) + TXMOISRETARD2 * null(2022-(ANNEEREV+1)))
	      + ((INTAN - 2022)*12 + INTMS) * TXMOISRETARD2)
          ) 
            * present(MOISAN);
COPETO = si (CMAJ = 7 ou CMAJ = 10 ou CMAJ = 17 ou CMAJ = 18)
         alors (10)
         sinon
              ( si (CMAJ = 8 ou CMAJ = 11)
                alors (40)
                sinon (80)
                finsi )
         finsi;


regle 101070:
application : iliad;

CSTOTSSPENA = max(0,CSGC + RDSC + MPSOL + CVNN + MCSG820 + CDIS + CGLOA + RSE1N + RSE2N + RSE3N + RSE4N
                  + RSE5N + RSE6N+RSE7N);
PTOIR = arr(BTO * COPETO / 100)                
	 + arr(BTO * COPETO /100) * positif(null(CMAJ-10)+null(CMAJ-17))
         + arr((BTOINR) * TXINT / 100) ;

PTOTAXA= arr(max(0,TAXASSUR- min(TAXASSUR+0,max(0,INE-IRB+AVFISCOPTER))+min(0,IRN - IRANT)) * COPETO / 100)
	 + arr(max(0,TAXASSUR- min(TAXASSUR+0,max(0,INE-IRB+AVFISCOPTER))+min(0,IRN - IRANT)) * COPETO /100) * positif(null(CMAJ-10)+null(CMAJ-17))
         + arr(max(0,TAXASSUR- min(TAXASSUR+0,max(0,INE-IRB+AVFISCOPTER))+min(0,IRN - IRANT)-TAXA9YI) * TXINT / 100) ;
PTOTPCAP= arr(max(0,IPCAPTAXT- min(IPCAPTAXT+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR))+min(0,IRN - IRANT+TAXASSUR)) * COPETO / 100)
	 + arr(max(0,IPCAPTAXT- min(IPCAPTAXT+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR))+min(0,IRN - IRANT+TAXASSUR)) * COPETO /100) * positif(null(CMAJ-10)+null(CMAJ-17))
         + arr(max(0,IPCAPTAXT- min(IPCAPTAXT+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR))+min(0,IRN - IRANT+TAXASSUR)-CAP9YI) * TXINT / 100) ;
PTOTLOY = arr(max(0,TAXLOY- min(TAXLOY+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR-IPCAPTAXT))+min(0,IRN - IRANT+TAXASSUR+IPCAPTAXT)) * COPETO / 100)
	 + arr(max(0,TAXLOY- min(TAXLOY+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR-IPCAPTAXT))+min(0,IRN - IRANT+TAXASSUR+IPCAPTAXT)) * COPETO /100) * positif(null(CMAJ-10)+null(CMAJ-17))
         + arr(max(0,TAXLOY- min(TAXLOY+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR-IPCAPTAXT))+min(0,IRN - IRANT+TAXASSUR+IPCAPTAXT)-LOY9YI) * TXINT / 100) ;

PTOTCHR= arr(max(0,IHAUTREVT+CHRPVIMP+min(0,IRN - IRANT+TAXASSUR+IPCAPTAXT+TAXLOY)) * COPETO / 100)
	 + arr(max(0,IHAUTREVT+CHRPVIMP+min(0,IRN - IRANT+TAXASSUR+IPCAPTAXT+TAXLOY)) * COPETO /100) * positif(null(CMAJ-10)+null(CMAJ-17))
         + arr(max(0,IHAUTREVT+CHRPVIMP+min(0,IRN - IRANT+TAXASSUR+IPCAPTAXT+TAXLOY)-CHR9YI) * TXINT / 100) ;

PTOCSG =( arr(max(0,CSGC-CSGIM) * COPETO / 100)                
         + arr(max(0,CSGC-CSGIM-CS9YP) * TXINT / 100) ) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

PTOPSOL =( arr(max(0,MPSOL-PROPSOL) * COPETO / 100)                
         + arr(max(0,MPSOL-PROPSOL - max(0,PS9YP)) * TXINT / 100) ) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

PTORDS =( arr(max(0,RDSC-CRDSIM) * COPETO / 100)                
         + arr(max(0,RDSC-CRDSIM-RD9YP) * TXINT / 100) ) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

PTOCVN = (arr(max(0,CVNN - COD8YT) * COPETO / 100) + arr(max(0,CVNN - COD8YT-CVN9YP) * TXINT / 100))                
         * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

PTOCDIS = (arr(max(0,CDIS-CDISPROV) * COPETO / 100) + arr(max(0,CDISC-CDISPROV-CDIS9YP) * TXINT / 100)) 
          * positif_ou_nul(CSTOTSSPENA - SEUIL_61);
PTOCSG820 = (arr(MCSG820  * COPETO / 100) + arr(max(0,MCSG820-C8209YP) * TXINT / 100)) 
          * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

PTOGLOA = (arr(max(0,GLOBASE) * COPETO / 100) + arr(max(0,GLOBASE-GLO9YP) * TXINT / 100)) 
          * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

PTORSE1 = (arr(max(0,RSE1 -CIRSE1 -CSPROVYD) * COPETO / 100) 
               + arr(max(0,RSE1 -CIRSE1 -CSPROVYD-RSE19YP) * TXINT / 100)
          ) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

PTORSE2 = (arr(max(0,RSE2 -CIRSE2 -CSPROVRSE2) * COPETO / 100) 
               + arr(max(0,RSE2 -CIRSE2 -CSPROVRSE2-RSE29YP) * TXINT / 100)
          ) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

PTORSE3 = (arr(max(0,RSE3 -CIRSE3 -CSPROVYG) * COPETO / 100) 
               + arr(max(0,RSE3 -CIRSE3 -CSPROVYG-RSE39YP) * TXINT / 100)
          ) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

PTORSE4 = (arr(max(0,RSE4 -CIRSE4 -CSPROVRSE4) * COPETO / 100)
               + arr(max(0,RSE4 -CIRSE4 -CSPROVRSE4-RSE49YP) * TXINT / 100)
          ) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

PTORSE5 = (arr(max(0,RSE5 -CIRSE5 -CSPROVYE) * COPETO / 100) 
               + arr(max(0,RSE5 -CIRSE5 -CSPROVYE-RSE59YP) * TXINT / 100)
          ) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

PTORSE6 = (arr(max(0,RSE6BASE) * COPETO / 100) 
               + arr(max(0,RSE6BASE - RSE69YP) * TXINT / 100)
          ) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

PTORSE7 = (arr(max(0,RSE7BASE) * COPETO / 100) 
               + arr(max(0,RSE7BASE - RSE79YP) * TXINT / 100)
          ) * positif_ou_nul(CSTOTSSPENA - SEUIL_61);

regle 101080:
application : iliad ;

BINRIR = max( 0 ,IRN-IRANT) + max(0,TAXASSUR- min(TAXASSUR+0,max(0,INE-IRB+AVFISCOPTER))+min(0,IRN - IRANT))
         + max(0,IPCAPTAXT- min(IPCAPTAXT+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR))+min(0,IRN - IRANT+TAXASSUR))
         + max(0,TAXLOY- min(TAXLOY+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR-IPCAPTAXT))
                           +min(0,IRN - IRANT+TAXASSUR+IPCAPTAXT))
         + max(0,IHAUTREVT+CHRPVIMP+min(0,IRN - IRANT+TAXASSUR+IPCAPTAXT+TAXLOY));


BINRPS = max(0,CSGC-CSGIM)+ max(0,RDSC-CRDSIM) + max(0,MPSOL-PRSPROV) + max(0,CVNN - COD8YT) + max(0,CDIS - CDISPROV)
        + max(0,CGLOA-COD8YL) + max(0,RSE1-CSPROVYD) + max(0,RSE2-CSPROVRSE2) + max(0,RSE3-CSPROVYG)
        + max(0,RSE4-CSPROVRSE4) + max(0,RSE5-CSPROVYE) + max(0,RSE6-COD8YQ) + max(0,MCSG820-COD8ZH) + max(0,RSE7-COD8YY);


VAR9YIIR= arr(ACODELAISINR * BINRIR/(BINRIR+BINRPS));
VAR9YIPS = max(0,ACODELAISINR - VAR9YIIR);
IR9YI  = arr(VAR9YIIR * max( 0 , IRN - IRANT )/BINRIR);
TAXA9YI = positif(IPCAPTAXT + TAXLOY + IHAUTREVT+CHRPVIMP) * arr(VAR9YIIR * max(0,TAXASSUR- min(TAXASSUR+0,max(0,INE-IRB+AVFISCOPTER))+min(0,IRN - IRANT))/BINRIR)
          + (1-positif(IPCAPTAXT + TAXLOY + IHAUTREVT+CHRPVIMP)) * max(0,VAR9YIIR - IR9YI) ;
CAP9YI = positif(TAXLOY + IHAUTREVT+CHRPVIMP) * arr(VAR9YIIR * max(0,IPCAPTAXT- min(IPCAPTAXT+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR))+min(0,IRN - IRANT+TAXASSUR))/BINRIR) 
          + (1-positif(TAXLOY + IHAUTREVT+CHRPVIMP)) * max(0,VAR9YIIR - IR9YI - TAXA9YI);
LOY9YI = positif(IHAUTREVT+CHRPVIMP) * arr(VAR9YIIR * max(0,TAXLOY- min(TAXLOY+0,max(0,INE-IRB+AVFISCOPTER-TAXASSUR-IPCAPTAXT))
                      +min(0,IRN - IRANT+TAXASSUR+IPCAPTAXT))/BINRIR)
        +(1-positif(IHAUTREVT+CHRPVIMP)) *  max(0,VAR9YIIR - IR9YI - TAXA9YI - CAP9YI);
CHR9YI = max(0,VAR9YIIR -IR9YI-TAXA9YI-CAP9YI-LOY9YI);

CS9YP = positif(RDSC+MPSOL+CVNN+CDIS+CGLOA+RSE1+RSE2+RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820) * arr(VAR9YIPS * (CSGC-CSGIM)/BINRPS) 
        + (1-positif(RDSC+MPSOL+ CVNN+CDIS+CGLOA+RSE1+RSE2+RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820)) * VAR9YIPS;

RD9YP = positif(MPSOL + CVNN+CDIS+CGLOA+RSE1+RSE2+RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820) * arr(VAR9YIPS * (RDSC-CRDSIM)/BINRPS) 
        + (1-positif(MPSOL + CVNN+CDIS+CGLOA+RSE1+RSE2+RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820)) * max(0,VAR9YIPS-CS9YP);

PS9YP = positif(CVNN+CDIS+CGLOA+RSE1+RSE2+RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820) * arr(VAR9YIPS * (MPSOL-PRSPROV)/BINRPS)
       + (1-positif(CVNN+CDIS+CGLOA+RSE1+RSE2+RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820)) * max(0,VAR9YIPS-CS9YP-RD9YP);  

CVN9YP = positif(CDIS+CGLOA+RSE1+RSE2+RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820) * arr(VAR9YIPS * (CVNN - COD8YT)/BINRPS)
       +(1-positif(CDIS+CGLOA+RSE1+RSE2+RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820)) * max(0,VAR9YIPS-CS9YP-RD9YP-PS9YP);
CDIS9YP = positif(CGLOA+RSE1+RSE2+RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820) * arr(VAR9YIPS * (CDIS - CDISPROV)/BINRPS)
       +(1-positif(CGLOA+RSE1+RSE2+RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820)) * max(0,VAR9YIPS-CS9YP-RD9YP-PS9YP-CVN9YP);
GLO9YP = positif(RSE1+RSE2+RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820) * arr(VAR9YIPS * (CSGLOA-COD8YL ) /BINRPS)
       +(1-positif(RSE1+RSE2+RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820)) * max(0,VAR9YIPS-CS9YP-RD9YP-PS9YP-CVN9YP-CDIS9YP);
RSE19YP = positif(RSE2+RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820) * arr(VAR9YIPS * (RSE1-CSPROVYD)/BINRPS)
       +(1-positif(RSE2+RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820)) * max(0,VAR9YIPS-CS9YP-RD9YP-PS9YP-CVN9YP-CDIS9YP-GLO9YP);
RSE29YP = positif(RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820) * arr(VAR9YIPS * (RSE2-CSPROVRSE2)/BINRPS)
       +(1-positif(RSE3+RSE4+RSE5+RSE6+RSE7+MCSG820)) * max(0,VAR9YIPS-CS9YP-RD9YP-PS9YP-CVN9YP-CDIS9YP-GLO9YP-RSE19YP);
RSE39YP = positif(RSE4+RSE5+RSE6+RSE7+MCSG820) * arr(VAR9YIPS * (RSE3-CSPROVYG)/BINRPS)
       +(1-positif(RSE4+RSE5+RSE6+RSE7+MCSG820)) * max(0,VAR9YIPS-CS9YP-RD9YP-PS9YP-CVN9YP-CDIS9YP-GLO9YP-RSE19YP-RSE29YP);
RSE49YP = positif(RSE5+RSE6+RSE7+MCSG820) * arr(VAR9YIPS * (RSE4-CSPROVRSE4)/BINRPS)
       +(1-positif(RSE5+RSE6+RSE7+MCSG820)) * max(0,VAR9YIPS-CS9YP-RD9YP-PS9YP-CVN9YP-CDIS9YP-GLO9YP-RSE19YP-RSE29YP-RSE39YP);
RSE59YP = positif(RSE6+RSE7+MCSG820) * arr(VAR9YIPS * (RSE5-CSPROVYE)/BINRPS)
       +(1-positif(RSE6+RSE7+MCSG820)) * max(0,VAR9YIPS-CS9YP-RD9YP-PS9YP-CVN9YP-CDIS9YP-GLO9YP-RSE19YP-RSE29YP-RSE39YP-RSE49YP);
RSE69YP = positif(RSE7+MCSG820) * arr(VAR9YIPS * (RSE6-COD8YQ)/BINRPS)
       +(1-positif(RSE7+MCSG820)) * max(0,VAR9YIPS-CS9YP-RD9YP-PS9YP-CVN9YP-CDIS9YP-GLO9YP-RSE19YP-RSE29YP-RSE39YP-RSE49YP-RSE59YP);
RSE79YP = positif(MCSG820) * arr(VAR9YIPS *  RSE7/BINRPS)
       +(1-positif(MCSG820)) * max(0,VAR9YIPS-CS9YP-RD9YP-PS9YP-CVN9YP-CDIS9YP-GLO9YP-RSE19YP-RSE29YP-RSE39YP-RSE49YP-RSE59YP-RSE69YP);
C8209YP = max(0,VAR9YIPS-CS9YP-RD9YP-PS9YP-CVN9YP-CDIS9YP-GLO9YP-RSE19YP-RSE29YP-RSE39YP-RSE49YP-RSE59YP-RSE69YP-RSE79YP);
regle 101090:
application : bareme , iliad ;

IAMD28EA = IAD11 + IBATMARG + ITP + PVMTS + REI + AUTOVERSSUP + AVFISCOPTER ;

IAN8EA = max(0 , (IAMD28EA - AVFISCOPTER
                  - ((CIRCMAVFT + IRETS + ICREREVET + CIGLO + CICULTUR + CIGPA + CIDONENTR + CICORSE + CIRECH + CICOMPEMPL) * (1 - positif(RE168 + TAX1649)))
                  + min(TAXASSUR+0 , max(0,INE-IRB+AVFISCOPTER))
                  + min(IPCAPTAXTOT+0 , max(0,INE-IRB+AVFISCOPTER - min(TAXASSUR+0,max(0,INE-IRB+AVFISCOPTER))))
                  + min(TAXLOY+0 ,max(0,INE-IRB+AVFISCOPTER - min(IPCAPTAXTOT+0 , max(0,INE-IRB+AVFISCOPTER - min(TAXASSUR+0,max(0,INE-IRB+AVFISCOPTER))))
                                                                                  - min(TAXASSUR+0 , max(0,INE-IRB+AVFISCOPTER))))
                 )) ;

IRN8EA = min(0 , IAN8EA + AVFISCOPTER - IRE) + max(0 , IAN8EA + AVFISCOPTER - IRE) * positif(IAMD1 + 1 - SEUIL_61) ;

BTO = max( 0 , IRN8EA - IRANT )
           * positif( IAMD1 + 1 - SEUIL_61 );
BTOINR = max( 0 , IRN8EA - IR9YI - IRANT ) * positif( IAMD1 + 1 - SEUIL_61 );

regle 101092:
application : bareme ,iliad ;


IRD = IRN * (positif(5 - V_IND_TRAIT)
              +
              (1-positif(5-V_IND_TRAIT)) * (
              positif_ou_nul(IRN+PIR-SEUIL_12) 
             + (1 - positif(IRN + PIR))
             ));

regle 101100:
application : iliad ;



CSGD = NAPCS - V_CSANT ;

RDSD = NAPRD - V_RDANT ;

CVND = NAPCVN - V_CVNANT ;

CGLOAD = NAPGLOA - V_GLOANT ;

CDISD = NAPCDIS - V_CDISANT ;
CSG820D = NAPCSG820 - V_CSG820ANT ;
CRSE1D = NAPRSE1 - V_RSE1ANT ;
CRSE2D = NAPRSE2 - V_RSE2ANT ;
CRSE3D = NAPRSE3 - V_RSE3ANT ;
CRSE4D = NAPRSE4 - V_RSE4ANT ;
CRSE5D = NAPRSE5 - V_RSE5ANT ;
CRSE6D = NAPRSE6 - V_RSE6ANT ;
CRSE7D = NAPRSE7 - V_RSE7ANT ;

regle 101110:
application : iliad;


CSBRUT = max(0,(CSGC + PCSG - CICSG - CSGIM)) ;

RDBRUT = max(0,(RDSC + PRDS - CIRDS - CRDSIM));

PSOLBRUT = max(0,MPSOL + PPSOL - CIPSOL - PRSPROV)  ;


CSNET = max(0 , CSGC + PCSG - CICSG - CIMRCSGP - CSGIM) * null(4 - V_IND_TRAIT)
        + max(0 , CSG + PCSG + CIMRCSGP * positif(FLAG_RETARD * null(FLAG_RETARD07) + FLAG_DEFAUT)
                                     + (CIMRCSGP - CIMRCSG24TLDEF) * (1 - positif(FLAG_RETARD * null(FLAG_RETARD07) + FLAG_DEFAUT)) - CSGIM) * null(5 - V_IND_TRAIT) ;

RDNET = max(0,RDSC + PRDS - CIRDS - CIMRCRDSP - CRDSIM)*null(4-V_IND_TRAIT)
        + max(0,RDSN + PRDS + CIMRCRDSP * positif(FLAG_RETARD * null(FLAG_RETARD07)+FLAG_DEFAUT)
                                    + (CIMRCRDSP - CIMRCRDS24TLDEF) * (1 - positif(FLAG_RETARD * null(FLAG_RETARD07) + FLAG_DEFAUT)) - CRDSIM) *null(5 - V_IND_TRAIT) ;

regle 101114:
application : iliad;

PSOLNET = max(0,MPSOL + PPSOL - CIPSOL - CIMRPSOLP - PRSPROV)  *null(4-V_IND_TRAIT)
          + max(0,PSOL + PPSOL + CIMRPSOLP * positif(FLAG_RETARD * null(FLAG_RETARD07)+FLAG_DEFAUT)
                                        + (CIMRPSOLP - CIMRPSOL24TLDEF) * (1 - positif(FLAG_RETARD * null(FLAG_RETARD07) + FLAG_DEFAUT)) - PRSPROV) * null(5 - V_IND_TRAIT) ;

CVNNET  =  max(0,(CVNSALC + PCVN - CICVN - COD8YT));

CDISNET = max(0,(CDISC + PCDIS - CDISPROV))  ;
CSG820NET = max(0,(MCSG820 - COD8ZH  + PCSG820))  ;

CGLOANET = max(0,(CGLOA - CIGLOA - COD8YL + PGLOA))  ;

regle 101120:
application : iliad ;

RSE1NET = max(0,(RSE1 + PRSE1 - CIRSE1 - CSPROVYD))  ;

RSE2NET = max(0, RSE8TV - CIRSE8TV - CSPROVYF) 
        + max(0, RSE8SA - CIRSE8SA - CSPROVYN) + PRSE2 ;

RSE3NET = max(0,(RSE3 + PRSE3 - CIRSE3 - CSPROVYG))  ;

RSE4NET = max(0,(RSE4 + PRSE4 - CIRSE4 - CSPROVRSE4))  ;

RSE5NET = max(0,(RSE5 + PRSE5 - CIRSE5 - CSPROVYE))  ;

RSE6NET = max(0,(RSE6 - COD8YQ - CIRSE6 + PRSE6))  ;
RSE7NET = max(0,(RSE7 - COD8YY + PRSE7))  ;
CSG92NET= RSE7NET;
RSENETTOT = RSE1NET + RSE2NET + RSE3NET + RSE4NET + RSE5NET + RSE7NET ;

regle 101130:
application : iliad;

TOTCRBRUT = max(0,CSGC+PCSG-CICSG-CSGIM + RDSC+PRDS-CIRDS-CRDSIM + MPSOL + PPSOL -CIPSOL-PRSPROV
                       + CVNSALC+PCVN-CICVN-COD8YT + CDISC+PCDIS-CDISPROV + CGLOA+PGLOA-COD8YL + MCSG820-COD8ZH 
                       + RSE1+PRSE1-CIRSE1-CSPROVYD + RSE2+PRSE2-CIRSE2-CSPROVRSE2 
                       + RSE3+PRSE3-CIRSE3-CSPROVYG + RSE4+PRSE4-CIRSE4-CSPROVRSE4
                       + RSE5+PRSE5-CIRSE5-CSPROVYE + RSE6+PRSE6-CIRSE6+ RSE7+PRSE7-COD8YY);
TOTCRNET = CSNET + RDNET + PSOLNET + CVNNET + CDISNET + CSG820NET
          + CGLOANET + RSE1NET + RSE2NET + RSE3NET + RSE4NET 
          + RSE5NET + RSE6NET + RSE7NET ;

regle 101140:
application : iliad ;


IARD = IAR - IAR_A ;

regle 101150:
application : iliad ;


PIRD = PIR * (positif(5 - V_IND_TRAIT)
              +
              (1-positif(5-V_IND_TRAIT)) * (
              positif_ou_nul(IRN+PIR-SEUIL_12) 
              + 
              (1-positif(IRN+PIR)) 
             ))
    - 
              PIR_A * ( positif_ou_nul(PIR_A-SEUIL_12) 
               + 
              (1-positif(PIR_A))  
              );
PCSGD = PCSG* CSREC - PCSG_A * CSRECA ;
PRDSD = PRDS * CSREC - PRDS_A * CSRECA;
PTOTD = PIRD ;

regle 101160:
application : iliad;

BPSOL = arr(( RDRFPS + max(0,NPLOCNETSF)) * V_CNR
        + max( 0, RDRV + RDRCM + RDRFPS + COD8XK + COD8YK + RDNP + RDNCP + RDPTP + ESFP + R1649 + PREREV+ PVTERPS
              ) * (1-V_CNR) 
      ) * (1 - positif(present(RE168) + present(TAX1649)))
      + (RE168 + TAX1649) * (1-V_CNR) ;


regle 101170:
application : iliad;


MPSOL = arr(max(0,(BPSOL-PVTERPS)) * TXPSOL/100 + (PVTERPS *TX068/100))* (1 - positif(ANNUL2042));

regle 101190:
application : iliad;


CSGC = arr( BCSG * T_CSG / 100) * (1 - positif(ANNUL2042)) ;

regle 101200:
application : iliad ;

RSE1 = arr(BRSE1 * TXTQ/100) * (1 - positif(ANNUL2042)) ;

BRSE8TV = ALLECS * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;
BRSE8SA = COD8SA * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;

RSE2 = (arr(BRSE8TV * TXTV/100) + arr(BRSE8SA * TXTV/100)) * (1 - positif(ANNUL2042)) ;

RSE3 = arr(BRSE3 * TXTW/100) * (1 - positif(ANNUL2042)) ;

RSE4 = arr(BRSE4 * TXTX/100) * (1 - positif(ANNUL2042)) ;
BRSE8TX = PENECS * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;
BRSE8SB = COD8SB * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;

RSE5 = arr(BRSE5 * TXTR/100) * (1 - positif(ANNUL2042)) ;

RSE6 = arr(BRSE6 * TXCASA/100) * (1 - positif(ANNUL2042)) ;
RSE7 = arr(BRSE7 * TX092/100) * (1 - positif(ANNUL2042)) ;
MCSG92=RSE7;

RSETOT = RSE1 + RSE2 + RSE3 + RSE4 + RSE5 + RSE7 ;

regle 101210:
application : iliad;

CSG = max(0 , CSGC - CICSG - CIMRCSGP) ;
RDSN = max(0 , RDSC - CIRDS - CIMRCRDSP) ;
PSOL = max(0,MPSOL - CIPSOL - CIMRPSOLP) ;
CVNN = max (0,CVNSALC - CICVN) ;
CSGLOA = CGLOA - CIGLOA;
CICPQ = CSGLOA;

RSE1N = max(0,RSE1 - CIRSE1) ;
RSE2N = max(0,RSE2 - CIRSE2) ;
RSE3N = max(0,RSE3 - CIRSE3) ;
RSE4N = max(0,RSE4 - CIRSE4) ;
RSE5N = max(0,RSE5 - CIRSE5) ;
RSE6N = max(0,RSE6 - CIRSE6) ;
RSE7N = RSE7 ;

regle 101220:
application : iliad;

RDRF = max(0 , RFCF + RFMIC - MICFR - RFDANT) * (1 - positif(ART1731BIS))
      + max(0 , RFCF + RFMIC - MICFR + DEFRF4BC)  * positif(ART1731BIS);

RDRFPS = max(0 , RFCFPS + RFMIC - MICFR - RFDANT )* (1 - positif(ART1731BIS))
       + max(0 , RFCFPS + RFMIC - MICFR - max(0,RFDANT  - DEFRFNONI) + DEFRF4BC) * positif(ART1731BIS);

RDRFAPS = max(0 , RFCFAPS + RFMIC + COD4BK - MICFR - RFDANT ) * (1 - positif(ART1731BIS))
       + max(0 , RFCFAPS + RFMIC + COD4BK - MICFR +  DEFRF4BD+ DEFRF4BC) * positif(ART1731BIS);
RDRCM1 =  max(0,TRCMABD + DRTNC + RCMAV + PROVIE + RCMNAB + RTCAR + REGPRIV + RESTUC
       +  COD2VV + COD2WW + COD2YY + COD2ZZ + COD2VN + COD2VO + COD2VP
		+ RCMIMPAT
		+ COD2TT
                - RCMSOC
                  -  positif(RCMRDS)
		       * min(RCMRDS ,  
			 RCMABD + REVACT
                       + RCMAV + PROVIE
                       + RCMHAD  + DISQUO
                       + RCMHAB + INTERE
		       + RCMTNC + REVPEA
                       + COD2TT + COD2VV
		       + COD2WW + COD2YY
		       + COD2ZZ  
		       + COD2VO + COD2VP + RCMIMPAT))
		       ;

RDRCM1NEG = min(0,RDRCM1);
 # Limite 2CG (voir spec) 
RDRCM1NEGPLAF  = abs(RDRCM1);
RDRCM1BIS = (1-positif(RDRCM1)) * RDRCM1NEGPLAF * (-1)
           + positif_ou_nul(RDRCM1) * RDRCM1;
 # RCM = RMC1 + 2FA (voir spec) 
RDRCM = RDRCM1BIS;
RDRCM1APS = RCMABD + RCMTNC + RCMAV + RCMHAD + RCMHAB + REGPRIV
          +  COD2VV + COD2WW + COD2YY + COD2ZZ + COD2VN + COD2VO + COD2VP  
		+ RCMIMPAT
		+ COD2TT
		- RCMSOC
                  -  positif(RCMRDS)
		       * min(RCMRDS ,  
			 RCMABD 
                       + RCMAV 
                       + RCMHAD 
                       + RCMHAB
		       + RCMTNC
                       + RCMIMPAT
		       + COD2TT
                       + COD2VV
                       + COD2WW + COD2YY
                       + COD2ZZ
                       + COD2VO + COD2VP + RCMIMPAT)
		       ;
RDRCM1NEGAPS = min(0,RDRCM1APS);
RDRCM1NEGPLAFAPS  = abs(RDRCM1APS);
RDRCM1BISAPS = (1-positif(RDRCM1APS)) * RDRCM1NEGPLAFAPS * (-1)
           + positif_ou_nul(RDRCM1APS) * RDRCM1APS;
RDRCMAPS = RDRCM1BISAPS ;
RDRV =   arr((RVB1 + COD1AR + RENTAX+CODRAR) * TXRVT1 / 100)
       + arr((RVB2 + COD1BR + RENTAX5+CODRBR) * TXRVT2 / 100)
       + arr((RVB3 + COD1CR + RENTAX6+CODRCR) * TXRVT3 / 100)
       + arr((RVB4 + COD1DR + RENTAX7+CODRDR) * TXRVT4 / 100);



RDNP = ( RCSV + RCSC + RCSP + max(0,NPLOCNETSF)) * (1-V_CNR) 
        + max(0,NPLOCNETSF) * V_CNR ;

        

PVTAUXPS = BPV18V + BPCOPTV + BPV40V + BPCOSAV + BPCOSAC +  BPVSJ + BPVSK +  PEA + GAINPEA + COD3PI ;

RDNCP = PVBARPS + PVTAUXPS + PVTZPS ;

RDPTP = max(0,BAF1AV-COD5XN) + max(0,BAF1AC-COD5YN) + max(0,BAF1AP-COD5ZN)
       + BA1AV + BA1AC + BA1AP
       + max(0,MIB1AV - MIBDEV) + max(0,MIB1AC - MIBDEC) + max(0,MIB1AP - MIBDEP)
       + BI1AV + BI1AC + BI1AP
       + max(0,MIBNP1AV - MIBNPDEV) + max(0,MIBNP1AC - MIBNPDEC) + max(0,MIBNP1AP - MIBNPDEP)
       + BI2AV + BI2AC + BI2AP
       + max(0,BNCPRO1AV - BNCPRODEV) + max(0,BNCPRO1AC - BNCPRODEC) + max(0,BNCPRO1AP - BNCPRODEP)
       + BN1AV + BN1AC + BN1AP
       + max(0,BNCNP1AV - BNCNPDEV) + max(0,BNCNP1AC - BNCNPDEC) + max(0,BNCNP1AP - BNCNPDEP)
       + PVINVE + PVINCE + PVINPE
       + PVSOCV + PVSOCC
       ;


RDPTPA = max(0,BAF1AV-COD5XN)+ BA1AV + max(0,MIB1AV - MIBDEV) + BI1AV + max(0,MIBNP1AV - MIBNPDEV) + BI2AV + max(0,BNCPRO1AV - BNCPRODEV) + BN1AV + max(0,BNCNP1AV - BNCNPDEV) + PVINVE + PVSOCV ; 


RDPTPB = max(0,BAF1AC-COD5YN) +BA1AC + max(0,MIB1AC - MIBDEC) + BI1AC + max(0,MIBNP1AC - MIBNPDEC) + BI2AC + max(0,BNCPRO1AC - BNCPRODEC) + BN1AC + max(0,BNCNP1AC - BNCNPDEC) + PVINCE + PVSOCC ; 

RGLOA = GLDGRATV + GLDGRATC;


regle 101225:
application : iliad ;

REVNONASSU = (1 - positif(COD8SH + COD8SI)) * (CODZRU + CODZRV)
             + positif(COD8SH) * positif(V_0AC + V_0AD + V_0AV) * (RDRV + RDRCM + RDRFPS + COD8XK + COD8YK + RDNP + RDNCP + RDPTP) * (1-V_CNR)
             + positif(COD8SH) * positif(V_0AC + V_0AD + V_0AV) * (RDRFPS + max(0,NPLOCNETSF) + CODZRV) * (V_CNR)

             + positif(COD8SH) * positif(COD8SI) * BOOL_0AM * (RDRV + RDRCM + RDRFPS + COD8XK + COD8YK + RDNP + RDNCP + RDPTP) * (1-V_CNR)
             + positif(COD8SH) * positif(COD8SI) * BOOL_0AM * (RDRFPS + max(0,NPLOCNETSF) + CODZRV) * (V_CNR)

             + positif(COD8SH) * (1 - positif(COD8SI)) * BOOL_0AM * (min(COD8RV,RDRV) + min(COD8RC,RDRCM) + min(COD8RF,RDRFPS + COD8XK + COD8YK) + RDPTPA + min(COD8RM,RDNCP)+ min((RCSV+max(0,NPLOCNETSV)),RDNP) + (CODZRU + CODZRV))* (1-V_CNR)
             + positif(COD8SH) * (1 - positif(COD8SI)) * BOOL_0AM * (min(COD8RF,RDRFPS) + NPLOCNETSV + CODZRV) * (V_CNR)

             + positif(COD8SI) * (1 - positif(COD8SH)) * BOOL_0AM * (min(COD8RV,RDRV) + min(COD8RC,RDRCM) + min(COD8RF,RDRFPS + COD8XK + COD8YK) +RDPTPB +min(COD8RM,RDNCP)+ min((RCSC+max(0,NPLOCNETSC)),RDNP) + (CODZRU + CODZRV))* (1-V_CNR)
             + positif(COD8SI) * (1 - positif(COD8SH)) * BOOL_0AM * (min(COD8RF,RDRFPS) + NPLOCNETSC + CODZRV) * (V_CNR)
           ;

regle 101226:
application : iliad;


BCSG = arr( max(0,RDRFPS +  max(0,NPLOCNETSF)-REVNONASSU) * V_CNR
       + max( 0, RDRV + RDRCM + RDRFPS + COD8XK + COD8YK + RDNP + RDNCP + RDPTP + ESFP + R1649 + PREREV
      -REVNONASSU) * (1-V_CNR)
        ) * (1 - positif(present(RE168) + present(TAX1649)))
       + (RE168 + TAX1649) * (1-V_CNR);
       


BCSGNORURV = (1-positif(COD8SH))*(
             arr(RDRFPS + max(0,NPLOCNETSF) -REVNONASSU * V_CNR
                 + ( RDRV + RDRCM + RDRFPS + COD8XK + COD8YK + RDNP + RDNCP + RDPTP + ESFP + R1649 + PREREV           - REVNONASSU ) * (1-V_CNR)
                ) * (1 - positif(present(RE168) + present(TAX1649)))
             + (RE168 + TAX1649) * (1-V_CNR) 
	     )
	     + positif(COD8SH)*0 ;

BRSE1 = SALECS * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;

BRSE2 = (ALLECS + COD8SA) * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;

BRSE3 = (INDECS + COD8SW) * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;

BRSE4 = (PENECS + COD8SB + COD8SX) * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;

BRSE5 = (SALECSG + COD8SC)  * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;

BRSE6 = (ALLECS + COD8SA + COD8SC) * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;
BRSE7 = COD8PH * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;
BCSG92=BRSE7;

BRSETOT = BRSE1 + BRSE2 + BRSE3 + BRSE4 + BRSE5 + BRSE7 ;

regle 101230:
application : iliad;


RETRSETOT = RETRSE1 + RETRSE2 + RETRSE3 + RETRSE4 + RETRSE5 + RETRSE7 ;
RSEPROVTOT = CSPROVYD + CSPROVYE + CSPROVYF + CSPROVYN + CSPROVYG + CSPROVYH + CSPROVYP ;
NMAJRSE1TOT = NMAJRSE11 + NMAJRSE21 + NMAJRSE31 + NMAJRSE41 + NMAJRSE51 + NMAJRSE71 ;
NMAJRSE4TOT = NMAJRSE14 + NMAJRSE24 + NMAJRSE34 + NMAJRSE44 + NMAJRSE54 + NMAJRSE74 ;

regle 101240:
application : iliad;


BDCSG = (min (BCSG , max (0, RDRV + positif(COD2OP)* (max(0,RDRCM1)+ PVBARPS) + RDRFPS + RDNP +(1-positif(COD2OP))*COD3XN - IPPNCS - REVNONASSU + (1-positif(COD2OP))*(min(COD8RC,RDRCM1)+min(COD8RM,RDNCP)))) 
                    * (1 - positif (ABDETPLUS + COD3SL + CODRVA + CODRSL + COD1TZ + COD1UZ + COD1VZ + COD1WZ))

       + min ( BCSG  , BDCSG3VAVB)     
                     * positif( ABDETPLUS + COD3SL + CODRVA + CODRSL + COD1TZ + COD1UZ + COD1VZ + COD1WZ)  
         ) * (1 - positif(present(RE168)+present(TAX1649))) * (1-V_CNR);


BDCSGNORUYT = ( min ( BCSGNORURV , max( 0, RDRFPS+RDRV +RDNP+ positif(COD2OP)* (max(0,RDRCM1) + PVBARPS) - IPPNCS))
                   * (1- positif(ABDETPLUS + COD1TZ + COD1UZ + COD1VZ+COD3UA + COD3SL))

        + min ( BCSGNORURV  , BDCSG3VAVB)
                   * positif(ABDETPLUS + COD1TZ + COD1UZ + COD1VZ+COD3UA + COD3SL)

        ) * (1 - positif(present(RE168)+present(TAX1649))) * (1-V_CNR) ;


regle 101250:
application : iliad;

DGLOD = positif(CSREC+V_GLOANT) * max(0,arr((BGLOA-COD8XM-(COD8YL/0.075)) * TX068/100)) * (1 - positif(present(RE168)+present(TAX1649)))
	  * positif(NAPCR61);
IDGLO = si (V_IND_TRAIT = 4) 
        alors ((max(0,arr((BGLOA -COD8XM-(COD8YL/0.075))* TX068/ 100))) * positif(CSREC))

        sinon  
              (abs(DGLOD - V_IDGLOANT))
        finsi ;

CSGDED3UA =  positif (COD2OP)*((( COD3UA + CODRUA + 0) * TX068/100)* ((COD3UA + CODRUA - COD3SL - CODRSL - CODRVA - ABDETPLUS) /(COD3UA + CODRUA + 0))) ; 
CSGDEDAUTRE = positif (COD2OP)*(arr((PVBARPS - (COD3UA + CODRUA)+ 0
                                                    ) * TX068/100
                                                  )) ; 

CSGDED = positif(COD2OP)* max(0 , CSGDED3UA + CSGDED3UB + CSGDED3UO + CSGDED3UP + CSGDED3UY + CSGDEDAUTRE) ;
CSGDED1TZ =  (PVTZPS * TX068/100) * (COD1TZ / (COD1TZ + COD1UZ + COD1VZ + COD1WZ));

PVTZBIS = arr((CSGDED1TZ/TX068)*100)+0; 
PVBAR3VAVB = positif (COD2OP) * positif(CSGDED)* arr( CSGDED * 100/TX068) ;

BDCSG3VAVB = max(0, RDRV + positif(COD2OP)* max(0,RDRCM1) + RDRFPS + RDNP + PVBAR3VAVB + PVTZBIS + (1-positif(COD2OP))* COD3XN - IPPNCS - REVNONASSU+(1-positif(COD2OP))* min(COD8RC,RDRCM1)+min(COD8RM,RDNCP) ) * (1-V_CNR) * (1 - positif(present(RE168)+present(TAX1649)));

regle 101260:
application : iliad ;


BDRSE1 = max(0,SALECS - min( SALECS,REVCSXA ) - arr(CSPROVYD/(TX075/100))
            ) * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;

BDRSE2 = max(0,ALLECS - min( ALLECS,REVCSXC ) - arr(CSPROVYF/(TX066/100))
            ) * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;

BDRSE3 = max(0,INDECS + COD8SW - min( INDECS + COD8SW,REVCSXD ) - arr(CSPROVYG/(TXTW/100))
            ) * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;

BDRSE4 = max(0,PENECS + COD8SX - min( PENECS + COD8SX,REVCSXE ) - arr(CSPROVYH/(TXTX/100))
            ) * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;

BDRSE5 = max(0,SALECSG + COD8SC - min( SALECSG + COD8SC,REVCSXB ) - arr(CSPROVYE/(TX075/100))
            ) * (1 - positif(present(RE168) + present(TAX1649))) * (1-V_CNR) ;

DRSED = (arr(BDRSE1 * TXTQDED/100) + arr(BDRSE2 * TXTVDED/100)
	   + arr(BDRSE3 * TXTWDED/100) + arr(BDRSE4 * TXTXDED/100) + arr(BDRSE5 * TXTRDED/100)
        ) * positif(CSREC+V_IDRSEANT) * positif(NAPCR61) ;

IDRSE = si (V_IND_TRAIT = 4)
	alors (positif(CSREC)*(arr(BDRSE1 * TXTQDED/100) + arr(BDRSE2 * TXTVDED/100) 
                               + arr(BDRSE3 * TXTWDED/100) + arr(BDRSE4 * TXTXDED/100) 
                               + arr(BDRSE5 * TXTRDED/100)
                              )
              )
	sinon
	      (abs(DRSED - V_IDRSEANT))
	finsi ;

regle 101270:
application : iliad ;


DCSGD = positif(CSRECB+V_IDANT) * max( 0, arr((BDCSG * TX068 / 100)+ (COD3WN * TX051/100)) - DCSGIM)  * positif(NAPCRB61);


IDCSG = si (V_IND_TRAIT = 4) 
        alors ( max( 0, arr(BDCSG * T_IDCSG / 100)+ (COD3WN * TX051/100)-DCSGIM) * positif(CSRECB))
        sinon ( abs(DCSGD - V_IDANT ))
        finsi ;

regle 101280:
application : iliad;

BRDS = 
       arr(  max( 0, RDRFPS + max(0,NPLOCNETSF) - REVNONASSU ) * V_CNR
     + max( 0, RDRV + RDRCM + RDRFPS + COD8XK + COD8YK + RDNP + RDNCP + RDPTP + PVTERPS 
        + SALECS + SALECSG + ALLECS + INDECS + PENECS
        + COD8SA + COD8SB + COD8SC + COD8SW + COD8SX + COD8PH
        + ESFP + R1649 + PREREV + RGLOA 
        - REVNONASSU) * (1-V_CNR) 
        ) * (1 - positif(present(RE168) + present(TAX1649)))
        + (RE168 + TAX1649) * (1-V_CNR) ;
	                  
	                    
                       
	

regle 101290:
application : iliad;


RDSC = arr( BRDS * T_RDS / 100 ) * (1 - positif(ANNUL2042)) ;

regle 101310:                                                             
application : iliad;                               

                                                                          
CSRTF = (RDPTP + PVINVE+PVINCE+PVINPE 
         + somme(i=V,C,P:BN1Ai + BI1Ai                          
         + BI2Ai + BA1Ai )); 
RDRTF = CSRTF ;                                                          
PSRTF = CSRTF ;                                                          

regle 101320:
application : iliad;

BASSURV3 = max(0,CESSASSV - LIM_ASSUR3);
BASSURV2 = max(0,CESSASSV - BASSURV3 - LIM_ASSUR2);
BASSURV1 = max(0,CESSASSV - BASSURV3 - BASSURV2 - LIM_ASSUR1);
BASSURC3 = max(0,CESSASSC - LIM_ASSUR3);
BASSURC2 = max(0,(CESSASSC -BASSURC3) - LIM_ASSUR2);
BASSURC1 = max(0,(CESSASSC - BASSURC3 -BASSURC2) - LIM_ASSUR1);
BASSURV = CESSASSV;
BASSURC = CESSASSC;

TAXASSURV = arr(BASSURV1 * TX_ASSUR1/100 + BASSURV2 * TX_ASSUR2/100 + BASSURV3 * TX_ASSUR3/100) * (1 - positif(RE168 + TAX1649));
TAXASSURC = arr(BASSURC1 * TX_ASSUR1/100 + BASSURC2 * TX_ASSUR2/100 + BASSURC3 * TX_ASSUR3/100) * (1 - positif(RE168 + TAX1649));
TAXASSUR = TAXASSURV + TAXASSURC ;

regle 101330:
application : iliad;

BCVNSAL = (1-positif(COD8SH +COD8SI))* (CVNSALAV + GLDGRATV + GLDGRATC) * (1-positif(present(TAX1649)+present(RE168)))
        +  positif(COD8SH)* positif(V_0AC + V_0AD + V_0AV) * (0)
	+ positif(COD8SH)* (1-positif(COD8SI)) * positif(V_0AM + V_0AO) * (CVNSALAV + GLDGRATC)
	+ positif(COD8SI)* (1-positif(COD8SH)) * positif(V_0AM + V_0AO) * (CVNSALAV + GLDGRATV) ; 
	  
CVNSALC = arr( BCVNSAL * TX10 / 100 ) * (1 - positif(ANNUL2042));

B3SVN  = CVNSALAV * (1 - positif(present(TAX1649) + present(RE168)));

BGLOA = (GLDGRATV+GLDGRATC) * (1-V_CNR) * (1-positif(present(TAX1649)+present(RE168)));
CGLOA = arr( BGLOA * T_CSG / 100 ) * (1 - positif(ANNUL2042));

BGLOACNR = (GLDGRATV+GLDGRATC) * V_CNR * (1-positif(present(TAX1649)+present(RE168)));

regle 101345:
application : iliad;

BCDIS = (GSALV + GSALC) * (1 - V_CNR)* (1-positif(present(TAX1649)+present(RE168))) ;

CDISC = arr(BCDIS * TCDIS / 100) * (1 - positif(ANNUL2042)) ;

CDIS = CDISC ;
regle 101350:
application : iliad;

BCSG820 = PVTERPS * (1 - V_CNR)* (1-positif(present(TAX1649)+present(RE168))) ;

MCSG820 = arr(BCSG820 * TX082 / 100) * (1 - positif(ANNUL2042)) ;


