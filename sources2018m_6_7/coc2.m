#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2019]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2019 
#au titre des revenus perçus en 2018. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************

# =================================================================================
# Chapitre coc2 : Anomalies
# =================================================================================

verif 10001:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 COD1AX > ( TSHALLOV + COD1AA + COD1GB + COD1GF + ALLOV + COD1AG + 0 ) 
 et
 (V_GESTPAS+0) < 1

alors erreur A10001;
verif 10002:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 COD1BX > ( TSHALLOC + COD1BA + COD1HB + COD1HF + ALLOC + COD1BG + 0 )
 et
 (V_GESTPAS+0) < 1


alors erreur A10002; 
verif 10003:
application : iliad  ;

si 
 (V_MODUL+0) < 1
 et
 COD1CX > ( TSHALLO1 + COD1CA + COD1IB + COD1IF + ALLO1 + COD1CG + 0 )
 et
 (V_GESTPAS+0) < 1


alors erreur A10003; 
verif 100004:
application : iliad  ;

si 
 (V_MODUL+0) < 1
 et
 COD1DX > ( TSHALLO2 + COD1DA + COD1JB + COD1JF + ALLO2 + COD1DG + 0 )
 et
(V_GESTPAS+0) < 1


alors erreur A10004 ; 
verif 100005:
application : iliad  ;

si 
 (V_MODUL+0) < 1
 et
 COD1EX > ( TSHALLO3 + COD1EA + COD1KF + ALLO3 + COD1EG + 0 )
 et
 (V_GESTPAS+0) < 1


alors erreur A10005 ; 
verif 10006:
application : iliad  ;

si 
 (V_MODUL+0) < 1
 et
 COD1FX > ( TSHALLO4 + COD1FA + COD1LF + ALLO4 + COD1FG + 0 )
 et
 (V_GESTPAS+0) < 1


alors erreur A10006 ;
verif 101001:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 COD1AD > ( PRBRV + PENINV + PALIV + COD1AM + 0)
 et
(V_GESTPAS+0) < 1


alors erreur A10101 ;
verif 101002:
application : iliad  ;

si 
 (V_MODUL+0) < 1
 et
 COD1BD > ( PRBRC + PENINC + PALIC + COD1BM + 0)
 et
 (V_GESTPAS+0) < 1

alors erreur A10102 ;  
verif 101003:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 COD1CD > ( PRBR1 + PENIN1 + PALI1 + COD1CM + 0 ) 
 et
(V_GESTPAS+0) < 1

alors erreur A10103 ; 
verif 101004:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 COD1DD > ( PRBR2 + PENIN2 + PALI2 + COD1DM +0 ) 
 et
(V_GESTPAS+0) < 1

alors erreur A10104 ; 
verif 101005:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 COD1ED > ( PRBR3 + PENIN3 + PALI3 + COD1EM +0 ) 
 et
(V_GESTPAS+0) < 1

alors erreur A10105 ;  
verif 101006:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 COD1FD > ( PRBR4 + PENIN4 + PALI4 + COD1FM +0 ) 
 et
(V_GESTPAS+0) < 1

alors erreur A10106 ; 
verif 102001:
application : iliad  ;

si 
 (V_MODUL+0) < 1
 et
 COD1AU > (RVB1+0)
 et
(V_GESTPAS+0) < 1

alors erreur A10201 ; 
verif 102002:
application : iliad  ;

si 
 (V_MODUL+0) < 1
 et
 COD1BU > (RVB2+0)
 et
(V_GESTPAS+0) < 1

alors erreur A10202 ; 
verif 102003:
application : iliad  ;

si 
 (V_MODUL+0) < 1
 et
 COD1CU > (RVB3+0)
 et
(V_GESTPAS+0) < 1

alors erreur A10203 ; 
verif 102004:
application : iliad  ;

si 
 (V_MODUL+0) < 1
 et
 COD1DU > (RVB4+0)
 et
(V_GESTPAS+0) < 1

alors erreur A10204 ; 
verif 103001:
application : iliad  ;

si
 (V_MODUL+0) < 1
  et
  (TSN1AJ + TSN1GB - TSN1AX+0) > 0
  et
 (COD1AN + COD1GN + COD1KN + COD1LN + COD1MN + COD1PN) > (TSN1AJ + TSN1GB - TSN1AX+0)
 et
 (V_GESTPAS+0) < 1

alors erreur A10301 ;
verif 103002:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 (TSN1BJ + TSN1HB - TSN1BX +0) > 0
 et
 (COD1BN + COD1HN + COD1QN + COD1RN + COD1SN + COD1TN) > (TSN1BJ + TSN1HB - TSN1BX +0) 
 et
(V_GESTPAS+0) < 1

alors erreur A10302 ;
verif 103003:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 (TSN1CJ + TSN1IB - TSN1CX +0) > 0
 et
 (COD1CN + COD1IN ) > (TSN1CJ + TSN1IB - TSN1CX +0)
 et
(V_GESTPAS+0) < 1

alors erreur A10303 ;
verif 103004:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 (TSN1DJ + TSN1JB - TSN1DX+0) > 0
 et
 (COD1FN + COD1JN) > (TSN1DJ + TSN1JB - TSN1DX+0) 
 et
(V_GESTPAS+0) < 1

alors erreur A10304 ;
verif 104001:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1GB) = 1
 et
 present(COD1AN) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10401 ;
verif 104002:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1AN) = 1
 et
 positif(COD1AV + 0) = 0
 et 
 positif(COD1AY + COD1UA + COD1UB + CODCAA +0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10402 ;
verif 104003:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1GN) = 1
 et
 positif(COD1GV + 0) = 0
 et
 positif(COD1GY + COD1UC + COD1UD + CODCBA +0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10403 ;
verif 104004:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1KN) = 1
 et
 positif(COD1KV + 0) = 0
 et 
 positif(COD1KY + COD1UE + COD1UF + CODCCA +0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10404 ;
verif 104005:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1LN) = 1
 et
 positif(COD1LV + 0) = 0
 et 
 positif(COD1LY + COD1UG + COD1UH + CODCDA +0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10405 ;
verif 104006:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1MN) = 1
 et
 positif(COD1MV + 0) = 0
 et
 positif(COD1MY + COD1UI + COD1UJ + CODCEA +0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10406 ;
verif 104007:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1PN) = 1
 et
 positif(COD1PV + 0) = 0
 et
 positif(COD1PY + COD1UK + COD1UL + CODCFA +0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10407 ;
verif 104008:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1AV) = 1
 et
 positif(COD1AN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10408 ;
verif 104009:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1GV) = 1
 et
 positif(COD1GN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10409 ;
verif 104010:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1KV) = 1
 et
 positif(COD1KN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10410 ;
verif 104011:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1LV) = 1
 et
 positif(COD1LN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10411 ;
verif 104012:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1MV) = 1
 et
 positif(COD1MN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10412 ;
verif 104013:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1PV) = 1
 et
 positif(COD1PN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10413 ;
verif 104014:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1AY + COD1UA + COD1UB + CODCAA) = 1
 et
 positif(COD1AN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10414 ;
verif 104015:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1GY + COD1UC + COD1UD + CODCBA) = 1
  et
 positif(COD1GN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10415 ;
verif 104016:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1KY + COD1UE + COD1UF + CODCCA) = 1
 et
 positif(COD1KN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10416 ;
verif 104017:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1LY + COD1UG + COD1UH + CODCDA) = 1
 et
 positif(COD1LN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10417 ;
verif 104018:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1MY + COD1UI + COD1UJ + CODCEA) = 1
 et
 positif(COD1MN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10418 ;
verif 104019:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1PY + COD1UK + COD1UL + CODCFA) = 1
 et
 positif(COD1PN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10419 ;
verif 105001:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1HB) = 1
  et
 positif(COD1BN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10501 ;
verif 105002:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1BN) = 1
 et
 positif(COD1BV + 0) = 0
 et
 positif(COD1BY + COD1VA + COD1VB + CODCAB + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10502 ;
verif 105003:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1HN) = 1
 et
 positif(COD1HV + 0) = 0
 et
 positif(COD1HY + COD1VC + COD1VD + CODCBB + CODCBB +0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10503 ;
verif 105004:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1QN) = 1
 et
 positif(COD1QV + 0) = 0
 et
 positif(COD1QY + COD1VE + COD1VF + CODCCB + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10504 ;
verif 105005:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1RN) = 1
 et
 positif(COD1RV + 0) = 0
 et
 positif(COD1RY + COD1VG + COD1VH + CODCDB + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10505 ;
verif 105006:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1SN) = 1
 et
 positif(COD1SV + 0) = 0
 et
 positif(COD1SY + COD1VI + COD1VJ + CODCEB + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10506 ;
verif 105007:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1TN) = 1
 et
 positif(COD1TV + 0) = 0
 et
 positif(COD1TY + COD1VK + COD1VL + CODCFB + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10507 ;
verif 105008:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1BV) = 1
 et
 positif(COD1BN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10508 ;
verif 105009:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1HV) = 1
 et
 positif(COD1HN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10509 ;
verif 105010:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1QV) = 1
 et
 positif(COD1QN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10510 ;
verif 105011:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1RV) = 1
 et
 positif(COD1RN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10511 ;
verif 105012:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1SV) = 1
 et
 positif(COD1SN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10512 ;
verif 105013:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif(COD1TV) = 1
 et
 positif(COD1TN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10513 ;
verif 105014:
application : iliad  ;

si
 (V_MODUL+0) < 1
 et
 positif (COD1BY + COD1VA + COD1VB + CODCAB) = 1
 et
 positif(COD1BN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10514 ;
verif 105015:
application : iliad  ;

si
 (V_MODUL+0) < 1
  et
 positif (COD1HY + COD1VC + COD1VD + CODCBB) = 1
 et
 positif(COD1HN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10515 ;
verif 105016:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif (COD1QY + COD1VE + COD1VF + CODCCB) = 1
 et
 positif(COD1QN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10516 ;
verif 105017:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif (COD1RY + COD1VG + COD1VH + CODCDB) = 1
 et
 positif(COD1RN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10517 ;
verif 105018:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif (COD1SY + COD1VI + COD1VJ + CODCEB) = 1
 et
 positif(COD1SN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10518 ;
verif 105019:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif (COD1TY + COD1VK + COD1VL + CODCFB) = 1
 et
 positif(COD1TN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10519 ;
verif 106001:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif (COD1IB) = 1
 et
 positif(COD1CN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10601 ;
verif 106002:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif(COD1CN) = 1
 et
 positif (COD1CV + 0) = 0
 et
 positif(COD1CY + COD1WA + COD1WB + CODCAC +0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10602 ;
verif 106003:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif(COD1IN) = 1
 et
 positif (COD1IV + 0) = 0
 et
 positif(COD1IY + COD1WC + COD1WD + CODCBC +0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10603 ;
verif 106004:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif (COD1CV) = 1
 et
 positif(COD1CN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10604 ;
verif 106005:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif (COD1IV) = 1
   et
 positif(COD1IN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10605 ;
verif 106006:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif (COD1CY + COD1WA + COD1WB + CODCAC) = 1
    et
 positif(COD1CN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10606 ;
verif 106007:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif (COD1IY + COD1WC + COD1WD + CODCBC) = 1
 et
 positif(COD1IN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10607 ;
verif 107001:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif (COD1JB) = 1
 et
 positif(COD1FN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10701 ;
verif 107002:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif(COD1FN) = 1
 et
 positif (COD1FV + 0) = 0
 et
 positif(COD1FY + COD1XA + COD1XB + CODCAD +  0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10702 ;
verif 107003:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif(COD1JN) = 1
 et
 positif (COD1JV + 0) = 0
 et
 positif(COD1JY + COD1XC + COD1XD + CODCBD + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10703 ;
verif 107004:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif (COD1FV) = 1
 et
 positif(COD1FN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10704 ;
verif 107005:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif (COD1JV) = 1
 et
 positif(COD1JN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10705 ;
verif 107006:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif (COD1FY + COD1XA + COD1XB + CODCAD) = 1
 et
 positif(COD1FN + 0) = 0
 et
(V_GESTPAS+0) < 1

alors erreur A10706 ;
verif 107007:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 positif (COD1JY + COD1XC + COD1XD + CODCBD) = 1
 et
 positif(COD1JN + 0) = 0
 et
 (V_GESTPAS+0) < 1

alors erreur A10707 ;
verif 108001:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(COD5AC + 0) = 1
  et
  positif(COD5AX + 0) = 1
  et
  (V_GESTPAS+0) < 1

alors erreur A10801 ;
verif 108002:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(COD5HN + 0) = 1
  et
  positif(COD5HO + 0) = 1
  et
  (V_GESTPAS+0) < 1

alors erreur A10802 ;
verif 108003:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(COD5KG + 0) = 1
  et
  positif(COD5KS + 0) = 1
  et
  (V_GESTPAS+0) < 1

alors erreur A10803 ;
verif 108004:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(COD5EW + 0) = 1
  et
  positif(COD5EX + 0) = 1
  et
  (V_GESTPAS+0) < 1

alors erreur A10804 ;
verif 108006:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BACIMRV) = 1
  et
  positif(COD5AX + 0) = 1
  et
  positif(present(COD5TD) + present(COD5TK) + present(COD5TL) + CODBAA + 0) * (present(COD5WD) + present(COD5UD) + 0) < 2
  et
  (V_GESTPAS+0) < 1

alors erreur A10806 ;
verif 108008:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BICIMRV) = 1
  et
  positif(COD5HO + 0) = 1
  et
  positif(present(COD5TJ) + present(COD5UV) + present(COD5UW) + CODBIA + 0) * (present(COD5WJ) + present(COD5UJ) + 0) < 2
  et
  (V_GESTPAS+0) < 1

alors erreur A10808 ;
verif 108010:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BNCIMRV) = 1
  et
  positif(COD5KS + 0) = 1
  et
  positif(present(COD5QR) + present(COD5QS) + present(COD5QT) + CODBNA + 0) * (present(COD5WT) + present(COD5TT) + 0) < 2
  et
  (V_GESTPAS+0) < 1

alors erreur A10810 ;
verif 109001:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(COD5BC + 0) = 1
  et
  positif(COD5BX + 0) = 1
  et
  (V_GESTPAS+0) < 1

alors erreur A10901 ;
verif 109002:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(COD5IN + 0) = 1
  et
  positif(COD5IO + 0) = 1
  et
  (V_GESTPAS+0) < 1

alors erreur A10902 ;
verif 109003:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(COD5LG + 0) = 1
  et
  positif(COD5LS + 0) = 1
  et
  (V_GESTPAS+0) < 1

alors erreur A10903 ;
verif 109004:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(COD5FW + 0) = 1
  et
  positif(COD5FX + 0) = 1
  et
  (V_GESTPAS+0) < 1

alors erreur A10904 ;
verif 109006:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BACIMRC) = 1
  et
  positif(COD5BX + 0) = 1
  et
  positif(present(COD5TG) + present(COD5TM) + present(COD5TN) + CODBAB + 0) * (present(COD5WG) + present(COD5UG) + 0) < 2
  et
  (V_GESTPAS+0) < 1

alors erreur A10906 ;
verif 109008:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BICIMRC) = 1
  et
  positif(COD5IO + 0) = 1
  et
  positif(present(COD5TV) + present(COD5SQ) + present(COD5SR) + CODBIB + 0) * (present(COD5WV) + present(COD5UK) + 0) < 2
  et
  (V_GESTPAS+0) < 1

alors erreur A10908 ;
verif 109010:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BNCIMRC) = 1
  et
  positif(COD5LS + 0) = 1
  et
  positif(present(COD5QU) + present(COD5QV) + present(COD5QW) + CODBNB + 0) * (present(COD5WU) + present(COD5TU) + 0) < 2
  et
  (V_GESTPAS+0) < 1

alors erreur A10910 ;
verif 110001:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(COD5CC + 0) = 1
  et
  positif(COD5CX + 0) = 1
  et
  (V_GESTPAS+0) < 1

alors erreur A11001 ;
verif 110002:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(COD5JN + 0) = 1
  et
  positif(COD5JO + 0) = 1
  et
  (V_GESTPAS+0) < 1

alors erreur A11002 ;
verif 110003:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(COD5MG + 0) = 1
  et
  positif(COD5MS + 0) = 1
  et
  (V_GESTPAS+0) < 1

alors erreur A11003 ;
verif 110004:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(COD5GW + 0) = 1
  et
  positif(COD5GX + 0) = 1
  et
  (V_GESTPAS+0) < 1

alors erreur A11004 ;
verif 110006:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BACIMRP) = 1
  et
  positif(COD5CX + 0) = 1
  et
  positif(present(COD5TO) + present(COD5UM) + present(COD5UN) + CODBAC + 0) * (present(COD5WO) + present(COD5UO) + 0) < 2
  et
  (V_GESTPAS+0) < 1

alors erreur A11006 ;
verif 110008:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BICIMRP) = 1
  et
  positif(COD5JO + 0) = 1
  et
  positif(present(COD5SM) + present(COD5SS) + present(COD5ST) + CODBIC + 0) * (present(COD5WP) + present(COD5UL) + 0) < 2
  et
  (V_GESTPAS+0) < 1

alors erreur A11008 ;
verif 110010:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BNCIMRP) = 1
  et
  positif(COD5MS + 0) = 1
  et
  positif(present(COD5NV) + present(COD5OV) + present(COD5PV) + CODBNC + 0) * (present(COD5WW) + present(COD5TW) + 0) < 2
  et
  (V_GESTPAS+0) < 1

alors erreur A11010 ;
verif 111001:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(LOCNPCIPS1) = 1
  et 
  positif(COD5DH + CODLMA + 0) = 0
  et
  positif(present(COD5QX) + present(COD5RX) + present(COD5SU) + 0) = 0
  et
  (V_GESTPAS+0) < 1

alors erreur A11101 ;
verif 111002:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(LOCNPCIPS1) = 1
  et 
  positif(COD5DH + 0) = 1
  et
  positif(present(COD5QX) + present(COD5RX) + present(COD5SU) + CODLMA + 0) = 1
  et
  (V_GESTPAS+0) < 1

alors erreur A11102 ;
verif 111003:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(LOCNPCIPS2) = 1
  et
  positif(COD5EH + CODLMB + 0) = 0
  et 
  positif(present(COD5QY) + present(COD5RY) + present(COD5SY) + 0) = 0
  et
  (V_GESTPAS+0) < 1

alors erreur A11103 ;
verif 111004:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(LOCNPCIPS2) = 1
  et 
  positif(COD5EH + 0) = 1
  et 
  positif(present(COD5QY) + present(COD5RY) + present(COD5SY) + CODLMB + 0) = 1
  et
  (V_GESTPAS+0) < 1

alors erreur A11104 ;
verif 111005:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(LOCNPCIPS3) = 1
  et 
  positif(COD5FH + CODLMC + 0) = 0
  et
  positif(present(COD5QZ) + present(COD5RV) + present(COD5UX) + 0) = 0
  et
  (V_GESTPAS+0) < 1

alors erreur A11105 ;
verif 111006:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(LOCNPCIPS3) = 1
  et
  positif(COD5FH + 0) = 1
  et
  positif(present(COD5QZ) + present(COD5RV) + present(COD5UX) + CODLMC + 0) = 1
  et
  (V_GESTPAS+0) < 1

alors erreur A11106 ;
verif 111008:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(RCSV + 0) = 1
  et
  positif(COD5EX + 0) = 1
  et
  positif(present(COD5TX) + present(COD5TQ) + present(COD5UQ) + CODPSA + 0) * (present(COD5WX) + present(COD5KA) + 0) < 2
  et
  (V_GESTPAS+0) < 1

alors erreur A11108 ;
verif 111010:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(RCSC + 0) = 1
  et
  positif(COD5FX + 0) = 1
  et
  positif(present(COD5TY) + present(COD5YV) + present(COD5ZV) + CODPSB + 0) * (present(COD5WY) + present(COD5LA) + 0) < 2
  et
  (V_GESTPAS+0) < 1

alors erreur A11110 ;
verif 111012:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(RCSP + 0) = 1
  et
  positif(COD5GX + 0) = 1
  et
  positif(present(COD5TZ) + present(COD5XG) + present(COD5YG) + CODPSC + 0) * (present(COD5MA) + present(COD5WZ) + 0) < 2
  et
  (V_GESTPAS+0) < 1

alors erreur A11112 ;
verif 11201:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BACIMRV) = 1
  et
  positif(COD5AX + COD5AC + present(COD5TD) + present(COD5TK) + present(COD5TL) + CODBAA + 0) = 0
  et
  (V_GESTPAS+0) < 1

alors erreur A11201 ;
verif 11202:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BACIMRC) = 1
  et
  positif(COD5BX + COD5BC + present(COD5TG) + present(COD5TM) + present(COD5TN) + CODBAB + 0) = 0
  et
  (V_GESTPAS+0) < 1

alors erreur A11202 ;
verif 11203:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BACIMRP) = 1
  et
  positif(COD5CX + COD5CC + present(COD5TO) + present(COD5UM) + present(COD5UN) + CODBAC + 0) = 0
  et
  (V_GESTPAS+0) < 1

alors erreur A11203 ;
verif 11204:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BICIMRV) = 1
  et
  positif(COD5HN + COD5HO + present(COD5TJ) + present(COD5UV) + present(COD5UW) + CODBIA + 0) = 0
  et
  (V_GESTPAS+0) < 1

alors erreur A11204 ;
verif 11205:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BICIMRC) = 1
  et
  positif(COD5IN + COD5IO + present(COD5TV) + present(COD5SQ) + present(COD5SR) + CODBIB + 0) = 0
  et
  (V_GESTPAS+0) < 1

alors erreur A11205 ;
verif 11206:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BICIMRP) = 1
  et
  positif(COD5JN + COD5JO + present(COD5SM) + present(COD5SS) + present(COD5ST) + CODBIC + 0) = 0
  et
  (V_GESTPAS+0) < 1

alors erreur A11206 ;
verif 11207:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BNCIMRV) = 1
  et
  positif(COD5KG + COD5KS + present(COD5QS) + present(COD5QR) + present(COD5QT) + CODBNA + 0) = 0
  et
  (V_GESTPAS+0) < 1

alors erreur A11207 ;
verif 11208:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BNCIMRC) = 1
  et
  positif(COD5LG + COD5LS + present(COD5QU) + present(COD5QV) + present(COD5QW) + CODBNB + 0) = 0
  et
  (V_GESTPAS+0) < 1

alors erreur A11208 ;
verif 11209:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(BNCIMRP) = 1
  et
  positif(COD5MG + COD5MS + present(COD5NV) + present(COD5OV) + present(COD5PV) + CODBNC + 0) = 0
  et
  (V_GESTPAS+0) < 1

alors erreur A11209 ;
verif 11210:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(RCSV + 0) = 1
  et
  positif(COD5EW + COD5EX + present(COD5TX) + present(COD5TQ) + present(COD5UQ) + CODPSA + 0) = 0
  et
  (V_GESTPAS+0) < 1

alors erreur A11210 ;
verif 11211:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(RCSC + 0) = 1
  et
  positif(COD5FW + COD5FX + present(COD5TY) + present(COD5YV) + present(COD5ZV) + CODPSB + 0) = 0
  et
  (V_GESTPAS+0) < 1

alors erreur A11211 ;
verif 11212:
application : iliad ;

si
  (V_MODUL+0) < 1
  et
  positif(RCSP + 0) = 1
  et
  positif(COD5GW + COD5GX + present(COD5TZ) + present(COD5XG) + present(COD5YG) + CODPSC + 0) = 0
  et
  (V_GESTPAS+0) < 1

alors erreur A11212 ;
verif 11391:
application : iliad  ;


si
   ((V_IND_TRAIT = 4 )
     et
    (
     CARPENBAV < 2 ou CARPENBAV > 45
     ou
     CARPENBAC < 2 ou CARPENBAC > 45
     ou
     CARPENBAP1 < 2 ou CARPENBAP1 > 45
     ou
     CARPENBAP2 < 2 ou CARPENBAP2 > 45
     ou
     CARPENBAP3 < 2 ou CARPENBAP3 > 45
     ou
     CARPENBAP4 < 2 ou CARPENBAP4 > 45
     ou
     PENSALNBV < 2 ou PENSALNBV > 45
     ou
     PENSALNBC < 2 ou PENSALNBC > 45
     ou
     PENSALNBP1 < 2 ou PENSALNBP1 > 45
     ou
     PENSALNBP2 < 2 ou PENSALNBP2 > 45
     ou
     PENSALNBP3 < 2 ou PENSALNBP3 > 45
     ou
     PENSALNBP4 < 2 ou PENSALNBP4 > 45
     ou
     RENTAXNB < 2 ou RENTAXNB > 45
     ou
     RENTAXNB5 < 2 ou RENTAXNB5 > 45
     ou
     RENTAXNB6 < 2 ou RENTAXNB6 > 45
     ou
     RENTAXNB7 < 2 ou RENTAXNB7 > 45
     ou
     CODNAZ < 2 ou CODNAZ > 45
     ou
     CODNBZ < 2 ou CODNBZ > 45
     ou
     CODNCZ < 2 ou CODNCZ > 45
     ou
     CODNDZ < 2 ou CODNDZ > 45
     ou
     CODNEZ < 2 ou CODNEZ > 45
     ou
     CODNFZ < 2 ou CODNFZ > 45
     ou
     CODNAL < 2 ou CODNAL > 45
     ou
     CODNAM < 2 ou CODNAM > 45
     ou
     CODNBL < 2 ou CODNBL > 45
     ou
     CODNBM < 2 ou CODNBM > 45
     ou
     CODNCL < 2 ou CODNCL > 45
     ou
     CODNCM < 2 ou CODNCM > 45
     ou
     CODNDL < 2 ou CODNDL > 45
     ou
     CODNDM < 2 ou CODNDM > 45
     ou
     CODNEL < 2 ou CODNEL > 45
     ou
     CODNEM < 2 ou CODNEM > 45
     ou
     CODNFL < 2 ou CODNFL > 45
     ou
     CODNFM < 2 ou CODNFM > 45
     ou
     CODNAR < 2 ou CODNAR > 45
     ou
     CODNBR < 2 ou CODNBR > 45
     ou
     CODNCR < 2 ou CODNCR > 45
     ou
     CODNDR < 2 ou CODNDR > 45
    )
   )
   ou
   ((V_IND_TRAIT = 5 )
     et
    (
     CARPENBAV = 1 ou CARPENBAV > 45
     ou
     CARPENBAC = 1 ou CARPENBAC > 45
     ou
     CARPENBAP1 = 1 ou CARPENBAP1 > 45
     ou
     CARPENBAP2 = 1 ou CARPENBAP2 > 45
     ou
     CARPENBAP3 = 1 ou CARPENBAP3 > 45
     ou
     CARPENBAP4 = 1 ou CARPENBAP4 > 45
     ou
     PENSALNBV = 1 ou PENSALNBV > 45
     ou
     PENSALNBC = 1 ou PENSALNBC > 45
     ou
     PENSALNBP1 = 1 ou PENSALNBP1 > 45
     ou
     PENSALNBP2 = 1 ou PENSALNBP2 > 45
     ou
     PENSALNBP3 = 1 ou PENSALNBP3 > 45
     ou
     PENSALNBP4 = 1 ou PENSALNBP4 > 45
     ou
     RENTAXNB = 1 ou RENTAXNB > 45
     ou
     RENTAXNB5 = 1 ou RENTAXNB5 > 45
     ou
     RENTAXNB6 = 1 ou RENTAXNB6 > 45
     ou
     RENTAXNB7 = 1 ou RENTAXNB7 > 45
     ou
     CODNAZ = 1 ou CODNAZ > 45
     ou
     CODNBZ = 1 ou CODNBZ > 45
     ou
     CODNCZ = 1 ou CODNCZ > 45
     ou
     CODNDZ = 1 ou CODNDZ > 45
     ou
     CODNEZ = 1 ou CODNEZ > 45
     ou
     CODNFZ = 1 ou CODNFZ > 45
     ou
     CODNAL = 1 ou CODNAL > 45
     ou
     CODNAM = 1 ou CODNAM > 45
     ou
     CODNBL = 1 ou CODNBL > 45
     ou
     CODNBM = 1 ou CODNBM > 45
     ou
     CODNCL = 1 ou CODNCL > 45
     ou
     CODNCM = 1 ou CODNCM > 45
     ou
     CODNDL = 1 ou CODNDL > 45
     ou
     CODNDM = 1 ou CODNDM > 45
     ou
     CODNEL = 1 ou CODNEL > 45
     ou
     CODNEM = 1 ou CODNEM > 45
     ou
     CODNFL = 1 ou CODNFL > 45
     ou
     CODNFM = 1 ou CODNFM > 45
     ou
     CODNAR = 1 ou CODNAR > 45
     ou
     CODNBR = 1 ou CODNBR > 45
     ou
     CODNCR = 1 ou CODNCR > 45
     ou
     CODNDR = 1 ou CODNDR > 45

    )
   )
alors erreur A13901 ;
verif 11392:
application : iliad  ;


si
  (V_IND_TRAIT = 4
    et
    (
     (positif(CARPEV) + present(CARPENBAV) = 1)
     ou
     (positif(CARPEC) + present(CARPENBAC) = 1)
     ou
     (positif(CARPEP1) + present(CARPENBAP1) = 1)
     ou
     (positif(CARPEP2) + present(CARPENBAP2) = 1)
     ou
     (positif(CARPEP3) + present(CARPENBAP3) = 1)
     ou
     (positif(CARPEP4) + present(CARPENBAP4) = 1)
     ou
     (positif(PENSALV) + present(PENSALNBV) = 1)
     ou
     (positif(PENSALC) + present(PENSALNBC) = 1)
     ou
     (positif(PENSALP1) + present(PENSALNBP1) = 1)
     ou
     (positif(PENSALP2) + present(PENSALNBP2) = 1)
     ou
     (positif(PENSALP3) + present(PENSALNBP3) = 1)
     ou
     (positif(PENSALP4) + present(PENSALNBP4) = 1)
     ou
     (positif(RENTAX) + present(RENTAXNB) = 1)
     ou
     (positif(RENTAX5) + present(RENTAXNB5) = 1)
     ou
     (positif(RENTAX6) + present(RENTAXNB6) = 1)
     ou
     (positif(RENTAX7) + present(RENTAXNB7) = 1)
     ou
     (positif(CODRAZ) + present(CODNAZ) = 1)
     ou
     (positif(CODRBZ) + present(CODNBZ) = 1)
     ou
     (positif(CODRCZ) + present(CODNCZ) = 1)
     ou
     (positif(CODRDZ) + present(CODNDZ) = 1)
     ou
     (positif(CODREZ) + present(CODNEZ) = 1)
     ou
     (positif(CODRFZ) + present(CODNFZ) = 1)
     ou
     (positif(CODRAL) + present(CODNAL) = 1)
     ou
     (positif(CODRAM) + present(CODNAM) = 1)
     ou
     (positif(CODRBL) + present(CODNBL) = 1)
     ou
     (positif(CODRBM) + present(CODNBM) = 1)
     ou
     (positif(CODRCL) + present(CODNCL) = 1)
     ou
     (positif(CODRCM) + present(CODNCM) = 1)
     ou
     (positif(CODRDL) + present(CODNDL) = 1)
     ou
     (positif(CODRDM) + present(CODNDM) = 1)
     ou
     (positif(CODREL) + present(CODNEL) = 1)
     ou
     (positif(CODREM) + present(CODNEM) = 1)
     ou
     (positif(CODRFL) + present(CODNFL) = 1)
     ou
     (positif(CODRFM) + present(CODNFM) = 1)
     ou
     (positif(CODRAR) + present(CODNAR) = 1)
     ou
     (positif(CODRBR) + present(CODNBR) = 1)
     ou
     (positif(CODRCR) + present(CODNCR) = 1)
     ou
     (positif(CODRDR) + present(CODNDR) = 1)
    )
  )
  ou
  (V_IND_TRAIT = 5
    et
    (
     (positif(CARPEV) + positif(CARPENBAV) = 1)
     ou
     (positif(CARPEC) + positif(CARPENBAC) = 1)
     ou
     (positif(CARPEP1) + positif(CARPENBAP1) = 1)
     ou
     (positif(CARPEP2) + positif(CARPENBAP2) = 1)
     ou
     (positif(CARPEP3) + positif(CARPENBAP3) = 1)
     ou
     (positif(CARPEP4) + positif(CARPENBAP4) = 1)
     ou
     (positif(PENSALV) + positif(PENSALNBV) = 1)
     ou
     (positif(PENSALC) + positif(PENSALNBC) = 1)
     ou
     (positif(PENSALP1) + positif(PENSALNBP1) = 1)
     ou
     (positif(PENSALP2) + positif(PENSALNBP2) = 1)
     ou
     (positif(PENSALP3) + positif(PENSALNBP3) = 1)
     ou
     (positif(PENSALP4) + positif(PENSALNBP4) = 1)
     ou
     (positif(RENTAX) + positif(RENTAXNB) = 1)
     ou
     (positif(RENTAX5) + positif(RENTAXNB5) = 1)
     ou
     (positif(RENTAX6) + positif(RENTAXNB6) = 1)
     ou
     (positif(RENTAX7) + positif(RENTAXNB7) = 1)
     ou
     (positif(CODRAZ) + positif(CODNAZ) = 1)
     ou
     (positif(CODRBZ) + positif(CODNBZ) = 1)
     ou
     (positif(CODRCZ) + positif(CODNCZ) = 1)
     ou
     (positif(CODRDZ) + positif(CODNDZ) = 1)
     ou
     (positif(CODREZ) + positif(CODNEZ) = 1)
     ou
    (positif(CODRFZ) + positif(CODNFZ) = 1)
    ou
    (positif(CODRAL) + positif(CODNAL) = 1)
    ou
    (positif(CODRAM) + positif(CODNAM) = 1)
    ou
    (positif(CODRBL) + positif(CODNBL) = 1)
    ou
    (positif(CODRBM) + positif(CODNBM) = 1)
    ou
   (positif(CODRCL) + positif(CODNCL) = 1)
    ou
   (positif(CODRCM) + positif(CODNCM) = 1)
    ou
   (positif(CODRDL) + positif(CODNDL) = 1)
   ou
   (positif(CODRDM) + positif(CODNDM) = 1)
   ou
   (positif(CODREL) + positif(CODNEL) = 1)
   ou
  (positif(CODREM) + positif(CODNEM) = 1)
   ou
   (positif(CODRFL) + positif(CODNFL) = 1)
   ou
  (positif(CODRFM) + positif(CODNFM) = 1)
   ou
   (positif(CODRAR) + positif(CODNAR) = 1)
   ou
   (positif(CODRBR) + positif(CODNBR) = 1)
   ou
   (positif(CODRCR) + positif(CODNCR) = 1)
   ou
   (positif(CODRDR) +positif(CODNDR) = 1)
    )
  )
alors erreur A13902 ;
verif 11401:
application : iliad  ;

si
   ((V_IND_TRAIT = 4 )
     et
    (
     CARTSNBAV < 2 ou CARTSNBAV > 45
     ou
     CARTSNBAC < 2 ou CARTSNBAC > 45
     ou
     CARTSNBAP1 < 2 ou CARTSNBAP1 > 45
     ou
     CARTSNBAP2 < 2 ou CARTSNBAP2 > 45
     ou
     CARTSNBAP3 < 2 ou CARTSNBAP3 > 45
     ou
     CARTSNBAP4 < 2 ou CARTSNBAP4 > 45
     ou
     REMPLANBV < 2 ou REMPLANBV > 45
     ou
     REMPLANBC < 2 ou REMPLANBC > 45
     ou
     REMPLANBP1 < 2 ou REMPLANBP1 > 45
     ou
     REMPLANBP2 < 2 ou REMPLANBP2 > 45
     ou
     REMPLANBP3 < 2 ou REMPLANBP3 > 45
     ou
     REMPLANBP4 < 2 ou REMPLANBP4 > 45
     ou
     CODNAF < 2 ou CODNAF > 45
     ou
     CODNAG < 2 ou CODNAG > 45
     ou
     CODNBF < 2 ou CODNBF > 45
     ou
     CODNBG < 2 ou CODNBG > 45
     ou
     CODNCF < 2 ou CODNCF > 45
     ou
     CODNCG < 2 ou CODNCG > 45
     ou
     CODNDF < 2 ou CODNDF > 45
     ou
     CODNDG < 2 ou CODNDG > 45
     ou
     CODNEF < 2 ou CODNEF > 45
     ou
     CODNGG < 2 ou CODNGG > 45
     ou
     CODNFF < 2 ou CODNFF > 45
     ou
     CODNFG < 2 ou CODNFG > 45
    )
   )
   ou
   ((V_IND_TRAIT = 5 )
     et
    (
     CARTSNBAV = 1 ou CARTSNBAV > 45
     ou
     CARTSNBAC = 1 ou CARTSNBAC > 45
     ou
     CARTSNBAP1 = 1 ou CARTSNBAP1 > 45
     ou
     CARTSNBAP2 = 1 ou CARTSNBAP2 > 45
     ou
     CARTSNBAP3 = 1 ou CARTSNBAP3 > 45
     ou
     CARTSNBAP4 = 1 ou CARTSNBAP4 > 45
     ou
     REMPLANBV = 1 ou REMPLANBV > 45
     ou
     REMPLANBC = 1 ou REMPLANBC > 45
     ou
     REMPLANBP1 = 1 ou REMPLANBP1 > 45
     ou
     REMPLANBP2 = 1 ou REMPLANBP2 > 45
     ou
     REMPLANBP3 = 1 ou REMPLANBP3 > 45
     ou
     REMPLANBP4 = 1 ou REMPLANBP4 > 45
     ou
     CODNAF = 1 ou CODNAF > 45
     ou
     CODNAG = 1 ou CODNAG > 45
     ou
     CODNBF = 1 ou CODNBF > 45
     ou
     CODNBG = 1 ou CODNBG > 45
     ou
     CODNCF = 1 ou CODNCF > 45
     ou
     CODNCG = 1 ou CODNCG > 45
     ou
     CODNDF = 1 ou CODNDF > 45
     ou
     CODNDG = 1 ou CODNDG > 45
     ou
     CODNEF = 1 ou CODNEF > 45
     ou
     CODNGG = 1 ou CODNGG > 45
     ou
     CODNFF = 1 ou CODNFF > 45
     ou
     CODNFG = 1 ou CODNFG > 45
    )
   )
alors erreur A14001 ;
verif 11402:
application : iliad  ;


si
  (V_IND_TRAIT = 4
    et
    (
     (positif(CARTSV) + present(CARTSNBAV) = 1)
     ou
     (positif(CARTSC) + present(CARTSNBAC) = 1)
     ou
     (positif(CARTSP1) + present(CARTSNBAP1) = 1)
     ou
     (positif(CARTSP2) + present(CARTSNBAP2) = 1)
     ou
     (positif(CARTSP3) + present(CARTSNBAP3) = 1)
     ou
     (positif(CARTSP4) + present(CARTSNBAP4) = 1)
     ou
     (positif(REMPLAV) + present(REMPLANBV) = 1)
     ou
     (positif(REMPLAC) + present(REMPLANBC) = 1)
     ou
     (positif(REMPLAP1) + present(REMPLANBP1) = 1)
     ou
     (positif(REMPLAP2) + present(REMPLANBP2) = 1)
     ou
     (positif(REMPLAP3) + present(REMPLANBP3) = 1)
     ou
     (positif(REMPLAP4) + present(REMPLANBP4) = 1)
     ou
     (positif(CODRAF) + present(CODNAF) = 1)
     ou
     (positif(CODRAG) + present(CODNAG) = 1)
     ou
     (positif(CODRBF) + present(CODNBF) = 1)
     ou
     (positif(CODRBG) + present(CODNBG) = 1)
     ou
     (positif(CODRCF) + present(CODNCF) = 1)
     ou
     (positif(CODRCG) + present(CODNCG) = 1)
     ou
     (positif(CODRDF) + present(CODNDF) = 1)
     ou
     (positif(CODRDG) + present(CODNDG) = 1)
     ou
     (positif(CODREF) + present(CODNEF) = 1)
     ou
     (positif(CODRGG) + present(CODNGG) = 1)
     ou
     (positif(CODRFF) + present(CODNFF) = 1)
     ou
     (positif(CODRFG) + present(CODNFG) = 1)
    )
  )
  ou
  (V_IND_TRAIT = 5
    et
    (
     (positif(CARTSV) + positif(CARTSNBAV) = 1)
     ou
     (positif(CARTSC) + positif(CARTSNBAC) = 1)
     ou
     (positif(CARTSP1) + positif(CARTSNBAP1) = 1)
     ou
     (positif(CARTSP2) + positif(CARTSNBAP2) = 1)
     ou
     (positif(CARTSP3) + positif(CARTSNBAP3) = 1)
     ou
     (positif(CARTSP4) + positif(CARTSNBAP4) = 1)
     ou
     (positif(REMPLAV) + positif(REMPLANBV) = 1)
     ou
     (positif(REMPLAC) + positif(REMPLANBC) = 1)
     ou
     (positif(REMPLAP1) + positif(REMPLANBP1) = 1)
     ou
     (positif(REMPLAP2) + positif(REMPLANBP2) = 1)
     ou
     (positif(REMPLAP3) + positif(REMPLANBP3) = 1)
     ou
     (positif(REMPLAP4) + positif(REMPLANBP4) = 1)
     ou
     (positif(CODRAF) + positif(CODNAF) = 1)
     ou
     (positif(CODRAG) + positif(CODNAG) = 1)
     ou
     (positif(CODRBF) + positif(CODNBF) = 1)
     ou
     (positif(CODRBG) + positif(CODNBG) = 1)
    ou
     (positif(CODRCF) + positif(CODNCF) = 1)
    ou
     (positif(CODRCG) + positif(CODNCG) = 1)
    ou
    (positif(CODRDF) + positif(CODNDF) = 1)
    ou
    (positif(CODRDG) + positif(CODNDG) = 1)
    ou
   (positif(CODREF) + positif(CODNEF) = 1)
    ou
   (positif(CODRGG) + positif(CODNGG) = 1)
   ou
   (positif(CODRFF) + positif(CODNFF) = 1)
   ou
  (positif(CODRFG) + positif(CODNFG) = 1)
    )
  )
alors erreur A14002 ;
verif 11411:
application : iliad  ;


si
   V_IND_TRAIT > 0
   et
   (COTFV + 0 > 25
    ou
    COTFC + 0 > 25
    ou
    COTF1 + 0 > 25
    ou
    COTF2 + 0 > 25
    ou
    COTF3 + 0 > 25
    ou
    COTF4 + 0 > 25)

alors erreur A14101 ;
verif 11412:
application : iliad  ;


si
   (V_IND_TRAIT = 4
    et
    (
     (positif(PEBFV) + present(COTFV) = 1)
     ou
     (positif(PEBFC) + present(COTFC) = 1)
     ou
     (positif(PEBF1) + present(COTF1) = 1)
     ou
     (positif(PEBF2) + present(COTF2) = 1)
     ou
     (positif(PEBF3) + present(COTF3) = 1)
     ou
     (positif(PEBF4) + present(COTF4) = 1)
     ou
     (positif(COTFV) + present(PEBFV) = 1)
     ou
     (positif(COTFC) + present(PEBFC) = 1)
     ou
     (positif(COTF1) + present(PEBF1) = 1)
     ou
     (positif(COTF2) + present(PEBF2) = 1)
     ou
     (positif(COTF3) + present(PEBF3) = 1)
     ou
     (positif(COTF4) + present(PEBF4) = 1)
    )
   )
   ou
   (V_IND_TRAIT = 5
    et
    (
     (positif(PEBFV) + positif(COTFV) = 1)
     ou
     (positif(PEBFC) + positif(COTFC) = 1)
     ou
     (positif(PEBF1) + positif(COTF1) = 1)
     ou
     (positif(PEBF2) + positif(COTF2) = 1)
     ou
     (positif(PEBF3) + positif(COTF3) = 1)
     ou
     (positif(PEBF4) + positif(COTF4) = 1)
     ou
     (positif(COTFV) + positif(PEBFV) = 1)
     ou
     (positif(COTFC) + positif(PEBFC) = 1)
     ou
     (positif(COTF1) + positif(PEBF1) = 1)
     ou
     (positif(COTF2) + positif(PEBF2) = 1)
     ou
     (positif(COTF3) + positif(PEBF3) = 1)
     ou
     (positif(COTF4) + positif(PEBF4) = 1)
    )
   )

alors erreur A14102 ;
verif 1143:
application : iliad  ;


si
    (
 ( FRNV + COD1AE > 0 et (present(TSHALLOV) + present(ALLOV) + present(SALEXTV) + present(COD1AF)+ present(COD1AG) + present(COD1GB) + present (COD1AA) + present (COD1GF)) = 0 )
     ou
 ( FRNC + COD1BE > 0 et (present(TSHALLOC) + present(ALLOC) + present(SALEXTC) + present(COD1BF)+ present(COD1BG) + present(COD1HB) + present (COD1BA)+ present (COD1HF)) = 0 )
     ou
 ( FRN1 + COD1CE > 0 et (present(TSHALLO1) + present(ALLO1) + present(SALEXT1) + present(COD1CF)+ present(COD1CG) + present(COD1IB) + present (COD1CA)+ present (COD1IF)) = 0 )
     ou
 ( FRN2 + COD1DE > 0 et (present(TSHALLO2) + present(ALLO2) + present(SALEXT2) + present(COD1DF)+ present(COD1DG) + present(COD1JB) + present (COD1DA) + present (COD1JF)) = 0 )
     ou
 ( FRN3 + COD1EE > 0 et (present(TSHALLO3) + present(ALLO3) + present(SALEXT3) + present(COD1EF)+ present(COD1EG) + present (COD1EA) + present (COD1KF)) = 0 )
     ou
 ( FRN4 + COD1FE > 0 et (present(TSHALLO4) + present(ALLO4) + present(SALEXT4) + present(COD1FF)+ present(COD1FG) + present (COD1FA) + present (COD1LF)) = 0 )
    )
alors erreur A143 ;
verif 11441:
application : iliad  ;


si
   COD1NX + 0 < GSALV + 0
   et
   GSALV + 0 > 0

alors erreur A14401 ;
verif 11442:
application : iliad  ;


si
   COD1OX + 0 < GSALC + 0
   et
   GSALC + 0 > 0

alors erreur A14402 ;
verif 12231:
application : iliad  ;

si
  ((V_IND_TRAIT = 4 )
   et
   (
    REVACTNB < 2 ou REVACTNB > 20
    ou
    REVPEANB < 2 ou REVPEANB > 20
    ou
    PROVIENB < 2 ou PROVIENB > 20
    ou
    DISQUONB < 2 ou DISQUONB > 20
    ou
    RESTUCNB < 2 ou RESTUCNB > 20
    ou
    INTERENB < 2 ou INTERENB > 20
   )
  )
  ou
  ((V_IND_TRAIT = 5 )
   et
   (
    REVACTNB = 1 ou REVACTNB > 20
    ou
    REVPEANB = 1 ou REVPEANB > 20
    ou
    PROVIENB = 1 ou PROVIENB > 20
    ou
    DISQUONB = 1 ou DISQUONB > 20
    ou
    RESTUCNB = 1 ou RESTUCNB > 20
    ou
    INTERENB = 1 ou INTERENB > 20
   )
  )
alors erreur A22301 ;
verif 12232:
application : iliad  ;

si
   (V_IND_TRAIT = 4
    et
    (
     positif(REVACT) + present(REVACTNB) = 1
     ou
     positif(REVPEA) + present(REVPEANB) = 1
     ou
     positif(PROVIE) + present(PROVIENB) = 1
     ou
     positif(DISQUO) + present(DISQUONB) = 1
     ou
     positif(RESTUC) + present(RESTUCNB) = 1
     ou
     positif(INTERE) + present(INTERENB) = 1
    )
   )
   ou
   (V_IND_TRAIT = 5
    et
    (
     positif(REVACT) + positif(REVACTNB) = 1
     ou
     positif(REVPEA) + positif(REVPEANB) = 1
     ou
     positif(PROVIE) + positif(PROVIENB) = 1
     ou
     positif(DISQUO) + positif(DISQUONB) = 1
     ou
     positif(RESTUC) + positif(RESTUCNB) = 1
     ou
     positif(INTERE) + positif(INTERENB) = 1
    )
   )
alors erreur A22302 ;
verif 1227:
application : iliad  ;

si
   positif(COD2TT)> 0 
   et (positif(COD2TU)+ positif (COD2TV) + positif(COD2TW))>0
   
alors erreur A227 ;
verif 1228:
application : iliad  ;

si
 positif(COD2UU) > 0
 et
(((COD2VV + COD2WW) < COD2UU)
ou
  (present(COD2VV) +present( COD2WW) + 0) = 0)

alors erreur A228 ;  
verif 1229:
application : iliad  ;

si
 present (COD2OP) = 0
 et
 positif(REVACT + PROVIE + REVPEA + DISQUO + INTERE + RESTUC ) = 1

alors erreur A229 ;
