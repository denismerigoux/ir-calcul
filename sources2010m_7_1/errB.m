#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2017]
#
#Ce logiciel a été initialement développé par la Direction Générale des 
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2011 
#au titre des revenus perçus en 2010. La présente version a permis la 
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt 
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************
# Millesime 10 Application bareme
A000:anomalie :"A":"000":"00":"SAISIE D UN MONTANT NEGATIF";
A063:anomalie :"A":"063":"00":"nombre de parts incompatible avec sit.  de famille";
A064:anomalie :"A":"064":"00":"nombre de parts incompatible avec sit.  de famille et pers.  a charge";
A06501:anomalie :"A":"065":"01":"LE NOMBRE DE PARTS DOIT ETRE UN MULTIPLE DE 0,25";
A06502:anomalie :"A":"065":"02":"LE NOMBRE DE PARTS DOIT ETRE COMPRIS ENTRE 1 ET 99,75";
A066:anomalie :"A":"066":"00":"nombre de parts incompatible avec sit.  de famille";
A423:anomalie :"A":"423":"00":"INCOHERENCE ENTRE SAISIE LIGNE 4 BY ET LIGNE 4 BD (SANS 4 BA, 4 BB, 4 BC)";
A424:anomalie :"A":"424":"00":"LIGNE 4 BY SAISIE AVEC CODE QUOTIENT (RBA , SBA) : TRAITEMENT PAR 1551 MI";
AS03:anomalie :"A":"S03":"00":"SAISIE DES VARIABLES CONTEXTE ERRONEE";
