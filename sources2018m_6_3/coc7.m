#*************************************************************************************************************************
#
#Copyright or © or Copr.[DGFIP][2019]
#
#Ce logiciel a été initialement développé par la Direction Générale des
#Finances Publiques pour permettre le calcul de l'impôt sur le revenu 2019
#au titre des revenus perçus en 2018. La présente version a permis la
#génération du moteur de calcul des chaînes de taxation des rôles d'impôt
#sur le revenu de ce millésime.
#
#Ce logiciel est régi par la licence CeCILL 2.1 soumise au droit français
#et respectant les principes de diffusion des logiciels libres. Vous pouvez
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de
#la licence CeCILL 2.1 telle que diffusée par le CEA, le CNRS et l'INRIA  sur
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris
#connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
#
#**************************************************************************************************************************

# =================================================================================
# Chapitre coc7 : Anomalies
# =================================================================================

verif 19611:
application : iliad  ;


si
        (V_MODUL+0) < 1
	  et
	((
	CMAJ != 7 et ((APPLI_BATCH = 1 et APPLI_COLBERT = 0)
                    ou APPLI_OCEANS = 1
                    ou (APPLI_ILIAD = 1 et
                          non ( V_CALCULIR+0=1
                               ou (V_NOTRAIT+0) dans (16,23,26,33,36,43,46,53,56)
                              )
                        )
	             )
	)
	ou
	(
	CMAJ2 != 7 et CMAJ2 != 0 et ((APPLI_BATCH = 1 et APPLI_COLBERT = 0)
                                   ou APPLI_OCEANS = 1
                                   ou (APPLI_ILIAD = 1 et
                                       non ( V_CALCULIR+0=1
                                            ou (V_NOTRAIT+0) dans (16,23,26,33,36,43,46,53,56)
                              )
                        )
	             )
        ))

alors erreur A96101 ;
verif 19612:
application :  iliad ;


si
       (V_MODUL+0) < 1
         et
       ((
       CMAJ non dans ( 7..8 ) et CMAJ non dans (10..11) et CMAJ non dans (17..18)
     et (  (APPLI_ILIAD = 1 et
                   ( V_CALCULIR + 0 = 1 ou (V_NOTRAIT + 0) dans (16,23,26,33,36,43,46,53,56))
           )
           ou APPLI_COLBERT = 1)
       )
       ou
       (
       CMAJ2 non dans ( 7..8 ) et CMAJ2 non dans (10..11) et CMAJ2 non dans (17..18) et CMAJ2 != 0
     et (  (APPLI_ILIAD = 1 et
                   ( V_CALCULIR + 0 = 1 ou (V_NOTRAIT + 0) dans (16,23,26,33,36,43,46,53,56))
           )
           ou APPLI_COLBERT = 1)
       ))

alors erreur A96102 ;
verif isf 19613:
application :  iliad ;
si
   (V_MODUL+0) < 1
     et
   ((CMAJ_ISF non dans ( 7,8,10,11,17,18,34 )  et V_IND_TRAIT+0 = 4 )
   ou
   (CMAJ_ISF non dans ( 0,7,8,10,11,17,18,34 )  et V_IND_TRAIT+0 = 5 ))

alors erreur A96103 ;
verif 19621:
application : iliad  ;


si
   (V_MODUL+0) < 1
     et
   (( present(CMAJ)=1 et present(MOISAN)=0 )
   ou
   ( present(CMAJ2)=1 et present(MOISAN2)=0 ))

alors erreur A96201 ;
verif 19622:
application : iliad  ;


si
   (V_MODUL+0) < 1
     et
   (( present(CMAJ)=0 et present(MOISAN)=1)
   ou
   ( present(CMAJ2)=0 et present(MOISAN2)=1))

alors erreur A96202 ;
verif isf 19623:
application : iliad  ;
si
   (V_MODUL+0) < 1
     et
          ( present(CMAJ_ISF)=1 et present(MOISAN_ISF)=0 )

alors erreur A96203 ;
verif isf 19624:
application : iliad  ;
si
   (V_MODUL+0) < 1
     et
   ( present(CMAJ_ISF)=0 et present(MOISAN_ISF)=1)

alors erreur A96204 ;
verif 19631:
application : iliad  ;


si
      (V_MODUL+0) < 1
        et
        (V_IND_TRAIT > 0 )
       et
        (
        inf(MOISAN/10000) non dans (01..12)
        ou
        inf(MOISAN2/10000) non dans (00..12)
        )
alors erreur A96301 ;
verif 19632:
application : iliad ;


si
 (V_MODUL+0) < 1
  et
   (((APPLI_COLBERT=0) et (APPLI_ILIAD=1) et
	V_IND_TRAIT !=5)
et(
   (
 arr( (MOISAN/10000 - inf(MOISAN/10000))*10000 ) != ANNEEREV +1
et
 arr( (MOISAN/10000 - inf(MOISAN/10000))*10000 ) != ANNEEREV +2
et
 arr( (MOISAN/10000 - inf(MOISAN/10000))*10000 ) != ANNEEREV +3
et
 arr( (MOISAN/10000 - inf(MOISAN/10000))*10000 ) != ANNEEREV +4
   )
   ou
   (
 arr( (MOISAN2/10000 - inf(MOISAN2/10000))*10000 ) != ANNEEREV +1
et
 arr( (MOISAN2/10000 - inf(MOISAN2/10000))*10000 ) != ANNEEREV +2
et
 arr( (MOISAN2/10000 - inf(MOISAN2/10000))*10000 ) != ANNEEREV +3
et
 arr( (MOISAN2/10000 - inf(MOISAN2/10000))*10000 ) != ANNEEREV +4
et
 arr( (MOISAN2/10000 - inf(MOISAN2/10000))*10000 ) != 0
   )))
alors erreur A96302 ;
verif 196321:
application : iliad ;


si
   (V_MODUL+0) < 1
     et
   (APPLI_COLBERT = 1)
   et
   (
    arr( (MOISAN/10000 - inf(MOISAN/10000))*10000 ) != ANNEEREV +1
    et
    arr( (MOISAN/10000 - inf(MOISAN/10000))*10000 ) != ANNEEREV +2
    et
    arr( (MOISAN/10000 - inf(MOISAN/10000))*10000 ) != ANNEEREV +3
   )

alors erreur A96302 ;
verif isf 19633:
application : iliad  ;
si
         (V_MODUL+0) < 1
	   et
        (
                (V_IND_TRAIT+0 = 4 et inf(MOISAN_ISF/10000) non dans (01..12) )
                ou
                (V_IND_TRAIT+0 = 5 et inf(MOISAN_ISF/10000) non dans (01..12) et MOISAN_ISF != 0 )
        )
alors erreur A96303 ;
verif isf 19634:
application : iliad ;
si
(V_MODUL+0) < 1
  et
(APPLI_OCEANS = 0) et (
   (
   	V_IND_TRAIT !=5 et(
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV +1
et
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV +2
et
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV +3
et
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV +4
   ))
   ou
   (
   	V_IND_TRAIT = 5 et(
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV +1
et
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV +2
et
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV +3
et
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV +4
et
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV +5
et
 arr( (MOISAN_ISF/10000 - inf(MOISAN_ISF/10000))*10000 ) != V_ANREV +6
 ))

   )

alors erreur A96304 ;
verif 19641:
application : iliad  ;


si
        (V_MODUL+0) < 1
	  et
	(
       ((inf( DATDEPETR/1000000 ) non dans (01..31)) et V_IND_TRAIT+0 = 4)
ou
       ((inf( DATDEPETR/1000000 ) non dans (01..31)) et V_IND_TRAIT = 5 et DATDEPETR != 0)
	)

alors erreur A96401;
verif 19642:
application : iliad  ;


si
        (V_MODUL+0) < 1
	  et
	(
       ((inf( (DATDEPETR/1000000 - inf(DATDEPETR/1000000))*100 ) non dans (01..12)) et (V_IND_TRAIT+0= 4))
ou
       ((inf( (DATDEPETR/1000000 - inf(DATDEPETR/1000000))*100 ) non dans (01..12))
		et V_IND_TRAIT = 5 et DATDEPETR != 0)
	)

alors erreur A96402;
verif 19643:
application : iliad  ;


si
(V_MODUL+0) < 1
  et
(
 ((arr( (DATDEPETR/10000 - inf(DATDEPETR/10000))*10000 ) != ANNEEREV ) et V_IND_TRAIT+0 = 4)
ou
 ((arr( (DATDEPETR/10000 - inf(DATDEPETR/10000))*10000 ) != ANNEEREV )
		et V_IND_TRAIT = 5 et DATDEPETR != 0)
   )

alors erreur A96403;
verif 19651:
application : iliad  ;


si
        (V_MODUL+0) < 1
	  et
	(
       ((inf( DATRETETR/1000000) non dans (01..31)) et V_IND_TRAIT+0 = 4)
	ou
       ((inf( DATRETETR/1000000) non dans (01..31)) et V_IND_TRAIT = 5 et DATRETETR != 0)
	)

alors erreur A96501;
verif 19652:
application : iliad  ;


si
        (V_MODUL+0) < 1
	  et
	(
       ((inf( (DATRETETR/1000000 - inf(DATRETETR/1000000))*100 ) non dans (01..12)) et V_IND_TRAIT+0 = 4)
	ou
       ((inf( (DATRETETR/1000000 - inf(DATRETETR/1000000))*100 ) non dans (01..12))
	et V_IND_TRAIT = 5 et DATRETETR != 0)
	)

alors erreur A96502 ;
verif 19653:
application : iliad  ;


si
(V_MODUL+0) < 1
  et
(
 ((arr( (DATRETETR/10000 - inf(DATRETETR/10000))*10000 ) != ANNEEREV ) et V_IND_TRAIT+0 = 4)
ou
 ((arr( (DATRETETR/10000 - inf(DATRETETR/10000))*10000 ) != ANNEEREV )
            et V_IND_TRAIT = 5 et DATRETETR != 0)
   )

alors erreur A96503 ;
verif 1966:
application : iliad  ;


si
   (V_MODUL+0) < 1
     et
   DATDEPETR > 0
   et
   DATRETETR > 0

alors erreur A966 ;
verif isf 1967:
application : iliad  ;
 si
   (V_MODUL+0) < 1
     et
   positif(V_ZDC) > 0
   et
   positif(V_0AZ + 0) = 1
   et
   positif(IFIPAT + 0) = 1

alors erreur A967 ;
verif isf 1979:
application : iliad  ;

si
 (V_MODUL+0) < 1
   et
 present(COD9PR)+ present (COD9PX) =1

alors erreur A979;
verif isf 19801:
application : iliad  ;
 si
   (V_MODUL+0) < 1
     et
   V_NOTRAIT + 0 < 14
   et
   V_IND_TRAIT + 0 = 4
   et
   (IFIPAT+0) <= LIM_IFIINF
  et
 positif(present(COD9AA)+present(COD9AB)+present(COD9AC)+present(COD9AD)+present(COD9BA)+present(COD9BB)+present(COD9CA)+present(COD9GF)+present(COD9GH))>0
  et
 present(COD9GN) = 0

alors erreur A980 ;
verif isf 19803:
application : iliad  ;
 si
   ( V_NOTRAIT + 0 = 14 ou V_NOTRAIT + 0 = 16 )
    et
    IFIPAT <= LIM_IFIINF
    et
 positif(present(COD9AA)+present(COD9AB)+present(COD9AC)+present(COD9AD)+present(COD9BA)+present(COD9BB)+present(COD9CA)+present(COD9GF)+present(COD9GH))>0

    alors erreur A98003 ;
verif isf 19804:
application : iliad  ;
 si
     V_NOTRAIT + 0 > 20
    et
    IFIPAT <= LIM_IFIINF
    et
    IFIPAT != 0

   alors erreur A98004 ;
verif isf 1982:
application : iliad ;

si
 (V_MODUL+0) < 1
   et
 (V_REGCO = 2 ou V_REGCO = 3)
 et
  VAR9GN = 1
  et
 INDREV1A8 = 1

alors erreur A982 ;
verif isf 1983:
application :  iliad ;
 si
 (V_MODUL+0) < 1
   et
 (APPLI_OCEANS=0) et
      (
                  (V_IND_TRAIT + 0 = 4)
                  et
                  (
                  positif(COD9GL + 0 ) = 1
                  et
                        (positif(V_0AM + V_0AO + 0 ) = 1
                         ou
                                (positif(V_0AC + V_0AD + V_0AV + 0 )=1
                                 et
                                 positif(V_0AB + 0)= 1
                                )
                        )
                  )
        )

alors erreur A983 ;
verif isf 1984:
application :  iliad ;
 si
 (V_MODUL+0) < 1
   et
      (
                  ((V_IND_TRAIT + 0 = 4) ou (V_IND_TRAIT + 0 = 5))
                  et
                  (
                  positif(COD9GM + 0 ) = 1
                  et
                        (positif(V_0AM + V_0AO + 0 ) = 1
                         ou
                                (positif(V_0AC + V_0AD + V_0AV + 0 )=1
                                 et
                                 positif(V_0AB + 0)= 0
                                )
                        )
                   )
        )

alors erreur A984 ;
verif isf 1985:
application :  iliad ;
 si
 (V_MODUL+0) < 1
   et
      positif(COD9GY + 0) = 1
      et
       IFIPAT>LIM_IFIINF

alors erreur A985 ;
verif isf 1986:
application :  iliad ;

si
(V_MODUL+0) < 1
  et
    ((APPLI_ILIAD) = 1  ou (APPLI_BATCH) = 1)
    et
    V_NOTRAIT + 0 < 14
    et
    (V_REGCO = 1 ou V_REGCO = 5 ou V_REGCO = 6)
    et
   (positif(COD9GN + 0) = 1)

alors erreur A986 ;
verif isf 19871:
application :  iliad ;
si
   APPLI_OCEANS = 0
   et
   V_NOTRAIT + 0 >= 14
   et
   V_ETCVL + 0 = 1
   et
   COD9GL + COD9GM + 0 = 0

alors erreur A98701 ;
verif isf 19872:
application :  iliad ;
si
   APPLI_OCEANS = 0
   et
   V_NOTRAIT + 0 >= 14
   et
   present(V_ETCVL) = 1
   et
   V_ETCVL + 0 = 0
   et
   COD9GL + COD9GM + 0 > 0

alors erreur A98702 ;
verif isf 19873:
application :  iliad ;

si
   APPLI_OCEANS = 0
   et
   V_NOTRAIT + 0 >= 14
   et
   COD9GL + 0 > 0
  et
   COD9GM + 0 > 0


alors erreur A98703 ;
verif isf 1988:
application : iliad ;

si
  (V_MODUL+0) < 1
    et
  APPLI_BATCH = 1
  et
  ((V_REGCO = 2 ou V_REGCO = 3))
  et
  VAR9GN = 1
  et
  INDREV1A8 = 0

alors erreur A988;
verif 1991:
application : iliad ;


si
   positif(FLAGDERNIE+0) = 1
   et
   positif(null(V_NOTRAIT - 23) + null(V_NOTRAIT - 33) + null(V_NOTRAIT - 43) + null(V_NOTRAIT - 53) + null(V_NOTRAIT - 63)) = 1
   et
   NAPCR61 > V_ANTCR

alors erreur A991 ;
verif 19921:
application : iliad ;


si
                      ((DEFRI = 1)  et (PREM8_11 =0) et (VARR10=1))
alors erreur A992 ;
verif 19922:
application : iliad ;

si
                      ((DEFRI = 1)  et (PREM8_11 =0) et (ANO1731=1))
alors erreur A992 ;
verif 19941:
application : iliad ;

si
   PREM8_11 = 1
   et
   positif(COD9ZA + 0) = 1

alors erreur A99401 ;
verif 19942:
application : iliad ;

si
   PREM8_11 = 0
   et
   positif(COD9ZA + 0) = 1
   et
   PENA994 = 0

alors erreur A99402 ;
verif isf 1995:
application : iliad ;

si
(V_MODUL+0) < 1
  et
IFIPAT=0
et
positif((COD9NC)+(COD9NG)+(COD9PR)+(COD9PX)+(COD9RS)+(COD9GL)+(COD9GM)) > 0

alors erreur A995;
verif isf 1997:
application : iliad ;

si

present(COD9GN) = 1
et
IFIPAT <= LIM_IFIINF

alors erreur A997 ;
verif 101:
application :  iliad ;

si
  (V_MODUL+0) < 1
    et
   APPLI_COLBERT + APPLI_ILIAD = 1
   et
   V_ZDC + 0 = 0
   et
   V_0AC = 1
   et
   V_0AZ + 0 > 0

alors erreur AS0101 ;
verif 102:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   APPLI_COLBERT + APPLI_ILIAD = 1
   et
   V_ZDC + 0 = 0
   et
   BOOL_0AM = 1
   et
   V_0AX + 0 > 0
   et
   V_0AB + 0 > 0

alors erreur AS0102 ;
verif 103:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   APPLI_COLBERT + APPLI_ILIAD = 1
   et
   V_ZDC + 0 = 0
   et
   V_0AC + V_0AD + V_0AV + 0 = 1
   et
   V_0AX + 0 > 0
   et
   positif(V_0AB + 0) = 0

alors erreur AS0103 ;
verif 104:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   APPLI_COLBERT + APPLI_ILIAD = 1
   et
   V_ZDC + 0 = 0
   et
   BOOL_0AM = 1
   et
   V_0AY + 0 > 0

alors erreur AS0104 ;
verif 105:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   APPLI_COLBERT + APPLI_ILIAD = 1
   et
   V_ZDC + 0 = 0
   et
   V_0AM = 1
   et
   V_0AY + 0 > 0
   et
   V_0AZ + 0 > 0

alors erreur AS0105 ;
verif 106:
application :  iliad ;

si
   (V_MODUL+0) < 1
     et
   APPLI_COLBERT + APPLI_ILIAD = 1
   et
   V_ZDC + 0 = 0
   et
   V_0AD = 1
   et
   V_0AZ + 0 > 0

alors erreur AS0106 ;
verif 107:
application : iliad  ;


si
   (V_MODUL+0) < 1
    et
   (APPLI_OCEANS+APPLI_COLBERT = 0) et
   (( pour un i dans 0, 1, 2, 3, 4, 5, 6, 7: V_0Fi + 0 > ANNEEREV  )
   ou
   ( pour un j dans G, J, N, H, I, P et un i dans 0, 1, 2, 3: V_0ji + 0 > ANNEEREV  ))
 ou (APPLI_COLBERT+APPLI_OCEANS=1) et
   (( pour un i dans 0, 1, 2, 3, 4, 5, 6, 7: V_0Fi + 0 > ANNEEREV  )
   ou
   ( pour un j dans 0, 1, 2, 3: V_0Hj + 0 > ANNEEREV  ))

alors erreur AS02;
verif 108:
application : iliad ;

si
   (V_MODUL+0) < 1
     et
   APPLI_COLBERT = 1
   et
   positif(V_IND_TRAIT + 0) = 1
   et
   V_NOTRAIT + 0 < 14
   et
   present(V_ANTIR) = 0
   et
   positif(V_0DA + 0) = 0

alors erreur AS11 ;
