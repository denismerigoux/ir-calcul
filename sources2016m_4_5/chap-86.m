#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2017]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des 
#Finances Publiques pour permettre le calcul de l'imp�t sur le revenu 2017 
#au titre des revenus per�us en 2016. La pr�sente version a permis la 
#g�n�ration du moteur de calcul des cha�nes de taxation des r�les d'imp�t 
#sur le revenu de ce mill�sime.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
regle 861000:
application : iliad , batch ;


BNNAV = BNCREV + COD5XJ - BNCDEV ;

SP5QC = (1 - positif(BNNAV)) * BNNAV
        + positif_ou_nul(BNNAV) * (BNCREV - arr(BNCDEV*BNCREV/(BNCREV + COD5XJ))) ;

PASBNNAV = SP5QC - COD5XP + COD5XH ;

BNNAC = BNCREC + COD5YJ - BNCDEC ;

SP5RC = (1 - positif(BNNAC)) * BNNAC
	+ positif_ou_nul(BNNAC) * (BNCREC - arr(BNCDEC*BNCREC/(BNCREC + COD5YJ))) ;

PASBNNAC = SP5RC - COD5YP + COD5YH ;

BNNAP = BNCREP + COD5ZJ - BNCDEP ;




BNNSV = positif(BNHREV + COD5XK - BNHDEV) * arr((BNHREV + COD5XK - BNHDEV)*MAJREV)
      + (1-positif(BNHREV + COD5XK -BNHDEV)) * (BNHREV + COD5XK - BNHDEV) ;

RP5QI = (1-positif(BNHREV + COD5XK - BNHDEV)) 
		* (BNHREV + COD5XK - BNHDEV)
         + positif(BNHREV + COD5XK - BNHDEV) 
		* arr(BNHREV - (BNHDEV * BNHREV/(BNHREV + COD5XK))) ;

RPMAJ5QI = arr(RP5QI * MAJREV) * positif(RP5QI)
           + RP5QI * (1-positif(RP5QI )) ;

PASBNNSV =  arr(( RP5QI - COD5XQ + COD5XL )* MAJREV )  * positif(RP5QI - COD5XQ + COD5XL)
            + ( RP5QI - COD5XQ + COD5XL ) * (1- positif(RP5QI - COD5XQ + COD5XL)) ;



BNNSC = positif(BNHREC + COD5YK - BNHDEC) * arr((BNHREC + COD5YK - BNHDEC)* MAJREV)
      + (1-positif(BNHREC + COD5YK -BNHDEC)) * (BNHREC + COD5YK-BNHDEC) ;

RP5RI = (1-positif(BNHREC + COD5YK -BNHDEC))
                * (BNHREC + COD5YK-BNHDEC)
         + positif(BNHREC + COD5YK -BNHDEC)
                * arr(BNHREC - (BNHDEC * BNHREC/(BNHREC + COD5YK))) ;


RPMAJ5RI = arr(RP5RI * MAJREV) * positif(RP5RI)
           + RP5RI * (1-positif( RP5RI )) ;


PASBNNSC = arr(( RP5RI - COD5YQ + COD5YL ) * MAJREV )  * positif(RP5RI - COD5YQ + COD5YL)
            + ( RP5RI - COD5YQ + COD5YL ) * (1- positif(RP5RI - COD5YQ + COD5YL)) ;


BNNSP = positif(BNHREP + COD5ZK - BNHDEP) * arr((BNHREP + COD5ZK - BNHDEP)*MAJREV)
      + (1-positif(BNHREP + COD5ZK -BNHDEP)) * (BNHREP + COD5ZK-BNHDEP) ;

regle 861010:
application : iliad , batch ;




BNNAAV = BNCAABV + COD5XS - BNCAADV ;

SNP5JG = (1 - positif(BNNAAV)) * BNNAAV
         + positif_ou_nul(BNNAAV) * (BNCAABV - arr(BNCAADV*BNCAABV/(BNCAABV + COD5XS))) ;


PASBNNAAV = SNP5JG - COD5XY + COD5VM ;


BNNAAC = BNCAABC + COD5YS - BNCAADC ;

SNP5RF = (1 - positif(BNNAAC)) * BNNAAC
         + positif_ou_nul(BNNAAC) * (BNCAABC - arr(BNCAADC*BNCAABC/(BNCAABC + COD5YS))) ;

PASBNNAAC = SNP5RF - COD5YY + COD5WM ;


BNNAAP = BNCAABP + COD5ZS - BNCAADP ;



NOCEPV = BNCAABV + COD5XS - BNCAADV + ANOCEP + COD5XX - DNOCEP ;

NOCEPC = BNCAABC + COD5YS - BNCAADC + ANOVEP + COD5YX - DNOCEPC ; 

NOCEPP = BNCAABP + COD5ZS - BNCAADP + ANOPEP + COD5ZX - DNOCEPP ; 

NOCEPIMPV = positif(ANOCEP + COD5XX - DNOCEP ) * arr((ANOCEP + COD5XX - DNOCEP)*MAJREV)
            + (1 - positif(ANOCEP + COD5XX - DNOCEP )) * (ANOCEP + COD5XX - DNOCEP ) 
	    + BNNAAV ;

RNPDEC1BNC =  positif(ANOCEP + COD5XX - DNOCEP ) * arr((ANOCEP + COD5XX - DNOCEP)*MAJREV)
            + (1 - positif(ANOCEP + COD5XX - DNOCEP )) * (ANOCEP + COD5XX - DNOCEP );

RNP5SN = (ANOCEP + COD5XX - DNOCEP ) 
		* (1 - positif_ou_nul(ANOCEP + COD5XX - DNOCEP))
            + arr (ANOCEP - (DNOCEP * ANOCEP/ (ANOCEP + COD5XX))) 
		* positif(ANOCEP + COD5XX - DNOCEP) ;

RNPMAJ5SN = arr( RNP5SN * MAJREV) * positif( RNP5SN)
            + RNP5SN * (1-positif(RNP5SN)) ;

PASNOCEPIMPV = arr(( RNP5SN - (COD5XZ - COD5VN)) * MAJREV ) * positif(RNP5SN - (COD5XZ - COD5VN))
               + (RNP5SN - (COD5XZ - COD5VN)) * ( 1 - positif(RNP5SN - (COD5XZ - COD5VN))) ;


NOCEPIMPC = positif(ANOVEP + COD5YX - DNOCEPC ) *arr((ANOVEP + COD5YX - DNOCEPC)*MAJREV) 
            + (1 - positif(ANOVEP + COD5YX - DNOCEPC )) * (ANOVEP + COD5YX - DNOCEPC)
	   + BNNAAC ;

RNPDEC2BNC =  positif(ANOVEP + COD5YX - DNOCEPC ) *arr((ANOVEP + COD5YX - DNOCEPC)*MAJREV)
            + (1 - positif(ANOVEP + COD5YX - DNOCEPC )) * (ANOVEP + COD5YX - DNOCEPC) ;

RNP5NS = (ANOVEP + COD5YX - DNOCEPC)
		* (1 - positif_ou_nul(ANOVEP + COD5YX - DNOCEPC))
            +  arr( ANOVEP -  (DNOCEPC  * ANOVEP / ( ANOVEP + COD5YX))) 
		* positif(ANOVEP + COD5YX - DNOCEPC) ;
										
RNPMAJ5NS = arr( RNP5NS * MAJREV) * positif( RNP5NS)
            + RNP5NS * (1-positif(RNP5NS)) ;

PASNOCEPIMPC = arr(( RNP5NS - (COD5YZ - COD5WN)) * MAJREV ) * positif(RNP5NS - (COD5YZ - COD5WN)) 
               + (RNP5NS - (COD5YZ - COD5WN)) * (1-positif(RNP5NS - (COD5YZ - COD5WN))) ;


NOCEPIMPP = positif(ANOPEP + COD5ZX - DNOCEPP) *arr((ANOPEP + COD5ZX - DNOCEPP)*MAJREV)
            + (1 - positif(ANOPEP + COD5ZX - DNOCEPP)) * (ANOPEP + COD5ZX - DNOCEPP) 
	    + BNNAAP;

RNPPACBNC = positif(ANOPEP + COD5ZX - DNOCEPP) *arr((ANOPEP + COD5ZX - DNOCEPP)*MAJREV)
            + (1 - positif(ANOPEP + COD5ZX - DNOCEPP)) * (ANOPEP + COD5ZX - DNOCEPP) ; 

NOCEPIMP = NOCEPIMPV+NOCEPIMPC+NOCEPIMPP;

TOTDABNCNP = (DABNCNP6 + DABNCNP5 + DABNCNP4 + DABNCNP3 + DABNCNP2 + DABNCNP1);

regle 861020:
application : iliad , batch ;


BNN = somme(i=V,C,P:BNRi) + SPENETPF + BNCIF ;

regle 861030:
application : iliad , batch ;


BNNV =  BNRV + SPENETV ;
BNNC =  BNRC + SPENETC ;
BNNP =  BNRP + SPENETP ;

regle 861040:
application : iliad , batch ;

BNRV = BNNSV + BNNAV;
BNRC = BNNSC + BNNAC;
BNRP = BNNSP + BNNAP;
BNRPROV = (somme(i=V,C,P: (positif(BNHREi - BNHDEi) * arr((BNHREi-BNHDEi)*MAJREV)
                       + (1-positif_ou_nul(BNHREi-BNHDEi)) *(BNHREi-BNHDEi))
                              + (BNCREi - BNCDEi)));
BNRTOT = BNRV + BNRC + BNRP ;

RNONPV =  NOCEPIMPV ;
RNONPC =  NOCEPIMPC ;
RNONPP =  NOCEPIMPP ;

regle 861050:
application : iliad , batch ;


BN1 = somme(i=V,C,P:BN1i) ;

regle 861060:
application : iliad , batch ;


BN1V = BN1AV + PVINVE + INVENTV ;
BN1C = BN1AC + PVINCE + INVENTC ;
BN1P = BN1AP + PVINPE + INVENTP ;

regle 861070:                                                                    
application : iliad , batch ;                          
                                                                               
SPETOTV = BNCPROV + BNCNPV ;
SPETOTC = BNCPROC + BNCNPC ;
SPETOTP = BNCPROP + BNCNPP ;

regle 861080:
application : iliad , batch ;                          
                                                                   

SPEBASABV =SPETOTV ;
SPEBASABC =SPETOTC ;
SPEBASABP =SPETOTP ;
                                                                               
SPEABV = arr((max(MIN_SPEBNC,(SPEBASABV * SPETXAB/100))) * 
                       positif_ou_nul(SPETOTV - MIN_SPEBNC)) +
          arr((min(MIN_SPEBNC,SPEBASABV )) * 
                       positif(MIN_SPEBNC - SPETOTV)) ; 
SPEABC = arr((max(MIN_SPEBNC,(SPEBASABC * SPETXAB/100))) * 
                       positif_ou_nul(SPETOTC - MIN_SPEBNC)) +
          arr((min(MIN_SPEBNC,SPEBASABC )) * 
                       positif(MIN_SPEBNC - SPETOTC)) ; 
SPEABP = arr((max(MIN_SPEBNC,(SPEBASABP * SPETXAB/100))) * 
                       positif_ou_nul(SPETOTP - MIN_SPEBNC)) +
          arr((min(MIN_SPEBNC,SPEBASABP )) * 
                       positif(MIN_SPEBNC - SPETOTP)) ; 

regle 861090:
application : iliad , batch ;                          
                                                                               
                                                                               
SPEABPV = arr((SPEABV * BNCPROV)/SPETOTV) ; 
SPEABPC = arr((SPEABC * BNCPROC)/SPETOTC) ; 
SPEABPP = arr((SPEABP * BNCPROP)/SPETOTP) ; 
                                                                               
SPEABNPV = SPEABV - SPEABPV ; 
SPEABNPC = SPEABC - SPEABPC ; 
SPEABNPP = SPEABP - SPEABPP ; 

regle 861100:
application : iliad , batch ;                          
                                                                        
SPENETPV = max (0,(BNCPROV - SPEABPV)) ; 
SPENETPC = max (0,(BNCPROC - SPEABPC)) ; 
SPENETPP = max (0,(BNCPROP - SPEABPP)) ; 
                                                                               
SPENETNPV = max (0,(BNCNPV - SPEABNPV)) ;
SPENETNPC = max (0,(BNCNPC - SPEABNPC)) ;
SPENETNPP = max (0,(BNCNPP - SPEABNPP)) ;

SPENETV = SPENETPV + SPENETNPV ;
SPENETC = SPENETPC + SPENETNPC ;
SPENETP = SPENETPP + SPENETNPP ;
                                                                               
SPENET = somme(i=V,C,P:(SPENETi)) ;

regle 861110:
application : iliad , batch ;                          
SPENETCT = BNCPROPVV + BNCPROPVC + BNCPROPVP - BNCPMVCTV - BNCPMVCTC - BNCPMVCTP ;
                                                                               
SPENETNPCT = BNCNPPVV + BNCNPPVC + BNCNPPVP - BNCNPDCT - COD5LD - COD5MD  ;

regle 861111:
application : iliad , batch ;                          



BNCR2TOTALV = RNONPV + SPENETNPV + BNCNPPVV - BNCNPDCT ;

BNCR2TOTALC = RNONPC + SPENETNPC + BNCNPPVC - COD5LD ;

BNCR2TOTALP = RNONPP  + SPENETNPP + BNCNPPVP - COD5MD ;

BNCR2TOTALF = somme(i=V,C,P: BNCR2TOTALi) ; 

regle 861120:
application : iliad , batch ; 

SPENETPF = somme(i=V,C,P:SPENETPi) + SPENETCT ;

SPENETNPF = somme(i=V,C,P:SPENETNPi) + SPENETNPCT ;                                    
BNCNPTOT = SPENETPF + SPENETNPF ;

regle 861130:
application : iliad , batch ;                          
SPEPVPV = BNCPRO1AV - BNCPRODEV;
SPEPVPC = BNCPRO1AC - BNCPRODEC;
SPEPVPP = BNCPRO1AP - BNCPRODEP;

SPEPVNPV = BNCNP1AV - BNCNPDEV;
SPEPVNPC = BNCNP1AC - BNCNPDEC;
SPEPVNPP = BNCNP1AP - BNCNPDEP;
                                                                               
SPEPV = somme(i=V,C,P:max(0,SPEPVPi + SPEPVNPi)) ;

regle 861135:
application : iliad , batch ;                          


PVBNCFOYER = BN1 +SPEPV ;

regle 861140:
application : iliad , batch ;                          

DCTSPE = positif_ou_nul(BNRTOT+SPENETPF) * BNCPMVCTV
        + ( 1 - positif_ou_nul(BNRTOT+SPENETPF)) * (BNCPMVCTV-abs(BNRTOT+SPENETPF))
        + ( 1 - positif_ou_nul(BNRTOT+SPENETPF)) * null(BNCPMVCTV-abs(BNRTOT+SPENETPF))* BNCPMVCTV
	;
DCTSPENP = positif_ou_nul(NOCEPIMP+SPENETNPF) * BNCNPDCT
        + ( 1 - positif_ou_nul(NOCEPIMP+SPENETNPF)) * (BNCNPDCT-abs(NOCEPIMP+SPENETNPF))
        + ( 1 - positif_ou_nul(NOCEPIMP+SPENETNPF)) * null(BNCNPDCT-abs(NOCEPIMP+SPENETNPF)) * BNCNPDCT
	;
regle 861150:
application : iliad , batch ;

BNCDF1 = ((1-positif_ou_nul(NOCEPIMP+SPENETNPF)) * abs(NOCEPIMP+SPENETNPF)
                + positif_ou_nul(NOCEPIMP+SPENETNPF)
                * positif_ou_nul(DABNCNP5+DABNCNP4+DABNCNP3+DABNCNP2+DABNCNP1-NOCEPIMP-SPENETNPF)
                * (DABNCNP5+DABNCNP4+DABNCNP3+DABNCNP2+DABNCNP1-NOCEPIMP-SPENETNPF)
                * null(BNCDF6P+BNCDF5P+BNCDF4P+BNCDF3P+BNCDF2P)) * null(4-V_IND_TRAIT)
          + null(5-V_IND_TRAIT) * (
               positif(DEFBNCNPF) * max(0,DEFBNCNPF-DIDABNCNP)
              + (1-positif(DEFBNCNPF)) *  max(0,-(NOCEPIMPV+NOCEPIMPC+NOCEPIMPP+SPENETNPF)));

regle 861160:
application : iliad , batch ;                          
                                                                               
BNCDF2 = ((1-positif_ou_nul(NOCEPIMP+SPENETNPF)) * (DABNCNP1)
                + positif_ou_nul(NOCEPIMP+SPENETNPF)
                * min(max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5-DABNCNP4-DABNCNP3-DABNCNP2,0)-DABNCNP1,DABNCNP1)*(-1)
                * positif_ou_nul(DABNCNP1-max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5-DABNCNP4-DABNCNP3-DABNCNP2,0)))* null(4-V_IND_TRAIT)
          + null(5-V_IND_TRAIT) * (
               positif(DEFBNCNPF) * min(DABNCNP1,DEFBNCNPF+DABNCNP-DIDABNCNP-BNCDF1)
              + (1-positif(DEFBNCNPF)) *  min(DABNCNP1,DABNCNP-DIDABNCNP));

regle 861170:
application : iliad , batch ;                          

BNCDF3 = ((1 - positif_ou_nul(NOCEPIMP+SPENETNPF)) * (DABNCNP2)
                 + positif_ou_nul(NOCEPIMP+SPENETNPF)
                 * min(max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5-DABNCNP4-DABNCNP3,0)-DABNCNP2,DABNCNP2)*(-1)
                 * positif_ou_nul(DABNCNP2-max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5-DABNCNP4-DABNCNP3,0)))* null(4-V_IND_TRAIT)
          + null(5-V_IND_TRAIT) * (
               positif(DEFBNCNPF) * min(DABNCNP2,DEFBNCNPF+DABNCNP-DIDABNCNP-BNCDF1-BNCDF2)
              + (1-positif(DEFBNCNPF)) *  min(DABNCNP2,DABNCNP-DIDABNCNP-BNCDF2));
regle 861180:
application : iliad , batch ;                          
                                                                               
BNCDF4 = ((1 - positif_ou_nul(NOCEPIMP+SPENETNPF)) * (DABNCNP3)
                 + positif_ou_nul(NOCEPIMP+SPENETNPF)
                 * min(max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5-DABNCNP4,0)-DABNCNP3,DABNCNP3)*(-1)
                 * positif_ou_nul(DABNCNP3-max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5-DABNCNP4,0)))* null(4-V_IND_TRAIT)
          + null(5-V_IND_TRAIT) * (
               positif(DEFBNCNPF) * min(DABNCNP3,DEFBNCNPF+DABNCNP-DIDABNCNP-BNCDF1-BNCDF2-BNCDF3)
              + (1-positif(DEFBNCNPF)) *  min(DABNCNP3,DABNCNP-DIDABNCNP-BNCDF2-BNCDF3));
regle 861190:
application : iliad , batch ;                          

BNCDF5 = ((1 - positif_ou_nul(NOCEPIMP+SPENETNPF)) * (DABNCNP4)
                 + positif_ou_nul(NOCEPIMP+SPENETNPF)
                 * min(max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5,0)-DABNCNP4,DABNCNP4)*(-1)
                 * positif_ou_nul(DABNCNP4-max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5,0)))* null(4-V_IND_TRAIT)
          + null(5-V_IND_TRAIT) * (
               positif(DEFBNCNPF) * min(DABNCNP4,DEFBNCNPF+DABNCNP-DIDABNCNP-BNCDF1-BNCDF2-BNCDF3-BNCDF4)
              + (1-positif(DEFBNCNPF)) *  min(DABNCNP4,DABNCNP-DIDABNCNP-BNCDF2-BNCDF3-BNCDF4));
regle 861200:
application : iliad , batch ;                          

BNCDF6 = ((1 - positif_ou_nul(NOCEPIMP+SPENETNPF)) * (DABNCNP5)
                 + positif_ou_nul(NOCEPIMP+SPENETNPF)
                 * min(max(NOCEPIMP+SPENETNPF-DABNCNP6,0)-DABNCNP5,DABNCNP5)*(-1)
                 * positif_ou_nul(DABNCNP5-max(NOCEPIMP+SPENETNPF-DABNCNP6,0)))* null(4-V_IND_TRAIT)
          + null(5-V_IND_TRAIT) * (
               positif(DEFBNCNPF) * min(DABNCNP5,DEFBNCNPF+DABNCNP-DIDABNCNP-BNCDF1-BNCDF2-BNCDF3-BNCDF4-BNCDF5)
              + (1-positif(DEFBNCNPF)) *  min(DABNCNP5,DABNCNP-DIDABNCNP-BNCDF2-BNCDF3-BNCDF4-BNCDF5));
regle 86917:
application : iliad , batch  ;
BNCDF2P = ((1-positif_ou_nul(NOCEPIMP+SPENETNPF)) * (DABNCNP1)
                + positif_ou_nul(NOCEPIMP+SPENETNPF)
                * min(max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5-DABNCNP4-DABNCNP3-DABNCNP2,0)-DABNCNP1,DABNCNP1)*(-1)
                * positif_ou_nul(DABNCNP1-max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5-DABNCNP4-DABNCNP3-DABNCNP2,0)));
BNCDF3P = ((1 - positif_ou_nul(NOCEPIMP+SPENETNPF)) * (DABNCNP2)
                 + positif_ou_nul(NOCEPIMP+SPENETNPF)
                 * min(max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5-DABNCNP4-DABNCNP3,0)-DABNCNP2,DABNCNP2)*(-1)
                 * positif_ou_nul(DABNCNP2-max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5-DABNCNP4-DABNCNP3,0)));
BNCDF4P = ((1 - positif_ou_nul(NOCEPIMP+SPENETNPF)) * (DABNCNP3)
                 + positif_ou_nul(NOCEPIMP+SPENETNPF)
                 * min(max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5-DABNCNP4,0)-DABNCNP3,DABNCNP3)*(-1)
                 * positif_ou_nul(DABNCNP3-max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5-DABNCNP4,0)));
BNCDF5P = ((1 - positif_ou_nul(NOCEPIMP+SPENETNPF)) * (DABNCNP4)
                 + positif_ou_nul(NOCEPIMP+SPENETNPF)
                 * min(max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5,0)-DABNCNP4,DABNCNP4)*(-1)
                 * positif_ou_nul(DABNCNP4-max(NOCEPIMP+SPENETNPF-DABNCNP6-DABNCNP5,0)));
BNCDF6P = ((1 - positif_ou_nul(NOCEPIMP+SPENETNPF)) * (DABNCNP5)
                 + positif_ou_nul(NOCEPIMP+SPENETNPF)
                 * min(max(NOCEPIMP+SPENETNPF-DABNCNP6,0)-DABNCNP5,DABNCNP5)*(-1)
                 * positif_ou_nul(DABNCNP5-max(NOCEPIMP+SPENETNPF-DABNCNP6,0)));
regle 861210:
application : iliad , batch ;

DABNCNP = DABNCNP1 + DABNCNP2 + DABNCNP3 + DABNCNP4 + DABNCNP5 + DABNCNP6 ;


DABNCNPF = (DABNCNP * positif(BNCR2TOTALF - DABNCNP)
            + BNCR2TOTALF *( 1-positif(BNCR2TOTALF - DABNCNP))
           ) * positif(BNCR2TOTALF) ;


DABNCNPV = arr(DABNCNPF * BNCR2TOTALV /(max(0, BNCR2TOTALV) + max(0, BNCR2TOTALC) + max(0, BNCR2TOTALP))
              ) * positif(BNCR2TOTALV) ;

DABNCNPC = ( arr(DABNCNPF * BNCR2TOTALC /(max(0, BNCR2TOTALV) + max(0, BNCR2TOTALC) + max(0, BNCR2TOTALP))
                ) * positif(BNCR2TOTALP)
	      + ( DABNCNPF - DABNCNPV
                ) * (1-positif(BNCR2TOTALP)) 
	   ) * positif(BNCR2TOTALC) ;

DABNCNPP = max(0,  DABNCNPF - DABNCNPV - DABNCNPC) * positif(BNCR2TOTALP); 

RNPFV = BNCR2TOTALV - DABNCNPV ;
RNPFC = BNCR2TOTALC - DABNCNPC ;
RNPFP = BNCR2TOTALP - DABNCNPP ;



DEFBNCNP = (SPENETNPV+SPENETNPC+SPENETNPP+BNCNPPVV+BNCNPPVC+BNCNPPVP+BNCAABV+ANOCEP*MAJREV+BNCAABC+ANOVEP*MAJREV+BNCAABP+ANOPEP*MAJREV
                               +COD5XS + arr(COD5XX*MAJREV)+COD5YS+arr(COD5YX*MAJREV)+COD5ZS+arr(COD5ZX*MAJREV));
regle 861220:
application : iliad , batch ;

DEFBNCNPF = DABNCNPF - (DABNCNPV + DABNCNPC + DABNCNPP);

regle 861250:
application : iliad , batch ;


PASBNCPV = arr((SPENETPV + PASBNNAV + PASBNNSV) * 12 / min(12 , COD5XI + 12 * null(COD5XI + 0))) ;

PASBNCPC = arr((SPENETPC + PASBNNAC + PASBNNSC) * 12 / min(12 , COD5YI + 12 * null(COD5YI + 0))) ;

PASBNCNPV = arr( max(0, ( SPENETNPV + PASBNNAAV + PASNOCEPIMPV ) * (12 / min(12 , COD5XR + 12 * null(COD5XR + 0)))
		          - (DABNCNPV * (SNP5JG + RNPMAJ5SN + SPENETNPV + BNCNPPVV
			                    - BNCNPDCT )
	             	    ) / BNCR2TOTALV
 	            )	
                ) ;


PASBNCNPC= arr( max(0 , (SPENETNPC + PASBNNAAC + PASNOCEPIMPC ) * (12 / min(12 , COD5YR + 12 * null(COD5YR + 0))) 
                          - ( DABNCNPC * (SNP5RF + RNPMAJ5NS + SPENETNPC + BNCNPPVC 
			                     - COD5LD )
			    ) / BNCR2TOTALC
	            ) 
              ) ;

PASBNCV =  PASBNCPV + PASBNCNPV ; 

PASBNCC =  PASBNCPC + PASBNCNPC ;

PASBNCTOT =  PASBNCV + PASBNCC ;



PASCODESBNC1 = si (( BNCPROV + BNCREV + BNCDEV + BNHREV + BNHDEV
                    + BNCNPV + BNCAABV + BNCAADV + ANOCEP + DNOCEP
		    + DABNCNP6 + DABNCNP5 + DABNCNP4 + DABNCNP3 + DABNCNP2 + DABNCNP1) > 0)
		    	alors (1)
           		sinon (BIDONUNDEF)
            	finsi ;


PASCODESBNC2 = si (( BNCPROC + BNCREC + BNCDEC + BNHREC + BNHDEC
                     + BNCNPC + BNCAABC + BNCAADC + ANOVEP + DNOCEPC
		     + DABNCNP6 + DABNCNP5 + DABNCNP4 + DABNCNP3 + DABNCNP2 + DABNCNP1) > 0)
		    	alors (1)
           		sinon (BIDONUNDEF)
            	finsi ;


MICROBNCV = SPENETPV + BNCPROPVV - BNCPMVCTV ;
MICROBNCC = SPENETPC + BNCPROPVC - BNCPMVCTC ; 
MICROBNCP = SPENETPP + BNCPROPVP - BNCPMVCTP ;

