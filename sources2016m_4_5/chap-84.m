#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2017]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des 
#Finances Publiques pour permettre le calcul de l'imp�t sur le revenu 2017 
#au titre des revenus per�us en 2016. La pr�sente version a permis la 
#g�n�ration du moteur de calcul des cha�nes de taxation des r�les d'imp�t 
#sur le revenu de ce mill�sime.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
regle 841000:
application : iliad , batch ;


BAMICRSV = (COD5XF + COD5XG + COD5XB);
BAMICRSC = (COD5YF + COD5YG + COD5YB);
BAMICRSP = (COD5ZF + COD5ZG + COD5ZB);

BAMICABAV = max(min(COD5XF,MIN_MBIC),COD5XF * TX87/100) + max(min(COD5XG,MIN_MBIC),COD5XG * TX87/100) + max(min(COD5XB,MIN_MBIC),COD5XB * TX87/100);
BAMICABAC = max(min(COD5YF,MIN_MBIC),COD5YF * TX87/100) + max(min(COD5YG,MIN_MBIC),COD5YG * TX87/100) + max(min(COD5YB,MIN_MBIC),COD5YB * TX87/100);
BAMICABAP = max(min(COD5ZF,MIN_MBIC),COD5ZF * TX87/100) + max(min(COD5ZG,MIN_MBIC),COD5ZG * TX87/100) + max(min(COD5ZB,MIN_MBIC),COD5ZB * TX87/100);

BAMICV = max(0,si (present(COD5XC)=1)
	alors(
		si (COD5XC <= 2014)
		alors(
			arr((COD5XD + COD5XE + (BAMICRSV - BAMICABAV) )/3)
		)	
		sinon(
			si (COD5XC = 2015)
			alors (arr((COD5XE + (BAMICRSV - BAMICABAV) )/2))
			sinon(
				si(COD5XC = 2016)
				alors(arr( BAMICRSV - BAMICABAV ))
				finsi	
			)
			finsi	
		)
		finsi)
	sinon (0)
	finsi);

BAMICC =max(0, si (present(COD5YC)=1)
	alors(
		si (COD5YC <= 2014)
		alors(
			 arr((COD5YD + COD5YE + (BAMICRSC - BAMICABAC) )/3)
		)	
		sinon(
			si (COD5YC = 2015)
			alors (arr((COD5YE + (BAMICRSC - BAMICABAC) )/2))
			sinon(
				si(COD5YC = 2016)
				alors(arr( BAMICRSC - BAMICABAC ))	
				finsi
			)
			finsi	
		)
		finsi)
	sinon (0)
	finsi);


BAMICP = max(0,si ( present(COD5ZC)=1)
	alors(
		si (COD5ZC <= 2014)
		alors(
			 arr((COD5ZD + COD5ZE + (BAMICRSP - BAMICABAP) )/3)
		)
		sinon(
			si( COD5ZC = 2015 )
			alors( arr((COD5ZE + (BAMICRSP - BAMICABAP) )/2) )
			sinon (
				si( COD5ZC = 2016 )
				alors(  arr(BAMICRSP - BAMICABAP) )
				finsi
			)
			finsi
		)
		finsi)

	sinon(0)
	finsi);	

BAABAV = COD5XB - BAMICV;
BAABAC = COD5YB - BAMICC;
BAABAP = COD5ZB - BAMICP;

regle 841010:
application : iliad , batch ;

BARSV = BAHREV + 4BAHREV + COD5AL - BAHDEV;
BARSREVV = BAHREV +4BAHREV + COD5AL;
BARSC = BAHREC + 4BAHREC + COD5BL- BAHDEC;
BARSREVC = BAHREC +4BAHREC + COD5BL;
BARSP = BAHREP + 4BAHREP + COD5CL- BAHDEP;
BARSREVP = BAHREP +4BAHREP+ COD5CL ;
BARAV = BACREV + 4BACREV + COD5AK - BACDEV;
BARREVAV = BACREV + 4BACREV + COD5AK;
BARAC = BACREC + 4BACREC + COD5BK - BACDEC;
BARREVAC = BACREC + 4BACREC + COD5BK;
BARAP = BACREP + 4BACREP + COD5CK - BACDEP;
BARREVAP = BACREP + 4BACREP + COD5CK;

RBAV = BARAV + BARSV ;
RBAC = BARAC + BARSC ;
RBAP = BARAP + BARSP ;

regle 841020:
application : iliad , batch ;


DEFBACREV = positif(BARREVAV) * (present(BACDEV) * arr(BACDEV * BACREV / BARREVAV));
DEFBACREC = positif(BARREVAC) * (present(BACDEC) * arr(BACDEC * BACREC / BARREVAC));
DEFBACREP = positif(BARREVAP) * (present(BACDEP) * arr(BACDEP * BACREP / BARREVAP));

4DEFBACREV =  positif(BARREVAV) * present(4BACREV) * ( present(COD5AK) * arr(BACDEV * 4BACREV / BARREVAV) + (1-present(COD5AK)) * (BACDEV - DEFBACREV));
4DEFBACREC =  positif(BARREVAC) * present(4BACREC) * ( present(COD5BK) * arr(BACDEC * 4BACREC / BARREVAC) + (1-present(COD5BK)) * (BACDEC - DEFBACREC));
4DEFBACREP =  positif(BARREVAP) * present(4BACREP) * ( present(COD5CK) * arr(BACDEP * 4BACREP / BARREVAP) + (1-present(COD5CK)) * (BACDEP - DEFBACREP));

EDEFBACREV =  positif(BARREVAV) * present(COD5AK) * (BACDEV - DEFBACREV - 4DEFBACREV);
				   
EDEFBACREC =  positif(BARREVAC) * present(COD5BK) * (BACDEC - DEFBACREC - 4DEFBACREC);

EDEFBACREP =  positif(BARREVAP) * present(COD5CK) * (BACDEP - DEFBACREP - 4DEFBACREP);
				  

regle 841030:
application : iliad , batch ;

BANV = ((BACREV - DEFBACREV) +(COD5AK - EDEFBACREV)) * positif_ou_nul(BARAV)
	+  BARAV * (1-positif_ou_nul(BARAV)) ;

BANC = ((BACREC - DEFBACREC) +(COD5BK - EDEFBACREC)) * positif_ou_nul(BARAC)
	+ BARAC * (1-positif_ou_nul(BARAC)) ;

BANP = ( (BACREP - DEFBACREP) + (COD5CK - EDEFBACREP) ) * positif_ou_nul(BARAP)
	+ BARAP * (1-positif_ou_nul(BARAP)) ;

BAEV = (4BACREV - 4DEFBACREV) * positif_ou_nul(BARAV) + 0 ;

BAEC =  (4BACREC - 4DEFBACREC) * positif_ou_nul(BARAC) + 0 ;

BAEP = (4BACREP - 4DEFBACREP) * positif_ou_nul(BARAP) + 0 ;

regle 841040:
application : iliad , batch ;

DEFBAHREV = positif(BARSREVV) * (present(BAHREV) * arr(BAHDEV * BAHREV / BARSREVV));
DEFBAHREC = positif(BARSREVC) * (present(BAHREC) * arr(BAHDEC * BAHREC / BARSREVC));
DEFBAHREP = positif(BARSREVP) * (present(BAHREP) * arr(BAHDEP * BAHREP / BARSREVP));

4DEFBAHREV = positif( BARSREVV) * present(4BAHREV) * ( present(COD5AL) * arr(BAHDEV * 4BAHREV / BARSREVV) + (1-present(COD5AL)) * ( BAHDEV - DEFBAHREV));
4DEFBAHREC = positif( BARSREVC) * present(4BAHREC) * ( present(COD5BL) * arr(BAHDEC * 4BAHREC / BARSREVC) + (1-present(COD5BL)) * ( BAHDEC - DEFBAHREC));
4DEFBAHREP = positif( BARSREVP) * present(4BAHREP) * ( present(COD5CL) * arr(BAHDEP * 4BAHREP / BARSREVP) + (1-present(COD5CL)) * ( BAHDEP - DEFBAHREP));


EDEFBAHREV = positif(BARSREVV) * present(COD5AL) * (BAHDEV - DEFBAHREV - 4DEFBAHREV);

EDEFBAHREC = positif(BARSREVC) * present(COD5BL) * (BAHDEC - DEFBAHREC - 4DEFBAHREC);

EDEFBAHREP = positif(BARSREVP) * present(COD5CL) * (BAHDEP - DEFBAHREP - 4DEFBAHREP);


regle 841050:
application : iliad , batch ;

BAMV = arr(((BAHREV - DEFBAHREV)+(COD5AL - EDEFBAHREV)) * MAJREV) * positif_ou_nul(BARSV) 
	+ BARSV * (1-positif(BARSV)) ;
R2MAJ5HI = arr((BAHREV - DEFBAHREV)*MAJREV)*positif_ou_nul(BARSV) + BARSV * (1-positif_ou_nul(BARSV));

BAMC = arr(((BAHREC - DEFBAHREC)+(COD5BL - EDEFBAHREC)) * MAJREV) * positif_ou_nul(BARSC)
	+ BARSC * (1-positif(BARSC)) ;
R2MAJ5II = arr((BAHREC - DEFBAHREC)*MAJREV)*positif_ou_nul(BARSC) + BARSC * (1-positif_ou_nul(BARSC));	

BAMP = arr(((BAHREP - DEFBAHREP)+(COD5CL - EDEFBAHREP)) * MAJREV)  * positif_ou_nul(BARSP)
	+ BARSP * (1-positif(BARSP)) ;
R2MAJ5JI = arr((BAHREP - DEFBAHREP)*MAJREV)*positif_ou_nul(BARSP) + BARSP * (1-positif_ou_nul(BARSP));

BAEMV = (arr((4BAHREV - 4DEFBAHREV) * MAJREV)) * positif_ou_nul(BARSV) + 0;

BAEMC = (arr((4BAHREC - 4DEFBAHREC) * MAJREV)) * positif_ou_nul(BARSC) + 0;

BAEMP = (arr((4BAHREP - 4DEFBAHREP) * MAJREV)) * positif_ou_nul(BARSP) + 0;


IBAMICV = BAMICV +  BAFPVV - COD5XO;
IBAMICC = BAMICC + BAFPVC - COD5YO;
IBAMICP = BAMICP + BAFPVP - COD5ZO;

IBAMICF = IBAMICV + IBAMICC + IBAMICP ;

regle 841070:
application : iliad , batch ;


BAHDEF = BAFORESTV + BAFPVV + BACREV + BAHREV * MAJREV + BAFORESTC + BAFPVC + BACREC + BAHREC * MAJREV
       + BAFORESTP + BAFPVP + BACREP + BAHREP * MAJREV + 4BACREV + 4BAHREV * MAJREV + 4BACREC + 4BAHREC * MAJREV + 4BACREP + 4BAHREP * MAJREV+0;

BAHQNODEFV = (BANV + BAMV + IBAMICV + BAFORESTV);
BAHQNODEFC = (BANC + BAMC + IBAMICC + BAFORESTC);
BAHQNODEFP = (BANP + BAMP + IBAMICP + BAFORESTP);

BAHQNODEFF = BAHQNODEFV + BAHQNODEFC + BAHQNODEFP;

BAHQAVISV = BANV + BAMV ;
BAHQAVISC = BANC + BAMC ;
BAHQAVISP = BANP + BAMP ;

regle 841080:
application : iliad , batch ;



BAQNODEFV = BAEV + BAEMV ; 
BAQNODEFC = BAEC + BAEMC ;
BAQNODEFP = BAEP + BAEMP ;

BAQNODEFF = BAQNODEFV + BAQNODEFC + BAQNODEFP;

BAQAVISV = max(0,BAEV + BAEMV + ( BAHQNODEFV * (1-positif(BAHQNODEFV)))) ;
BAQAVISC = max(0,BAEC + BAEMC + ( BAHQNODEFC * (1-positif(BAHQNODEFC)))) ;
BAQAVISP = max(0,BAEP + BAEMP + ( BAHQNODEFP * (1-positif(BAHQNODEFP)))) ;

regle 841090:
application : iliad , batch ;


DEFANTBAF = somme (i=1..6:DAGRIi);

BAPLUSFOYER = max(0,BAHQNODEFV) + max(0,BAHQNODEFC) + max(0,BAHQNODEFP) + max(0,BAQNODEFV) + max(0,BAQNODEFC) + max(0,BAQNODEFP); 

DEFANTIMP = positif(BAHQNODEFF + BAQNODEFF)*min(BAHQNODEFF + BAQNODEFF,DEFANTBAF);

DEFANTBAV = max(0,arr(DEFANTIMP * ( (positif(BAHQNODEFV) * BAHQNODEFV) + BAQNODEFV) / BAPLUSFOYER));
DEFANTBAC = max(0,arr(DEFANTIMP * ( (positif(BAHQNODEFC) * BAHQNODEFC) + BAQNODEFC) / BAPLUSFOYER));
DEFANTBAP = max(0,arr(DEFANTIMP * ( (positif(BAHQNODEFP) * BAHQNODEFP) + BAQNODEFP) / BAPLUSFOYER));

BAHQV = positif(BAHQNODEFV) * max(0,BAHQNODEFV - DEFANTBAV) + (1-positif(BAHQNODEFV)) * BAHQNODEFV;
BAHQC = positif(BAHQNODEFC) * max(0,BAHQNODEFC - DEFANTBAC) + (1-positif(BAHQNODEFC)) * BAHQNODEFC;
BAHQP = positif(BAHQNODEFP) * max(0,BAHQNODEFP - DEFANTBAP) + (1-positif(BAHQNODEFP)) * BAHQNODEFP;

DEFANTBAQV = abs(min(0,max(0,BAHQNODEFV) - DEFANTBAV));
DEFANTBAQC = abs(min(0,max(0,BAHQNODEFC) - DEFANTBAC));
DEFANTBAQP = abs(min(0,max(0,BAHQNODEFP) - DEFANTBAP));

BAQV = max(0,BAQNODEFV - DEFANTBAQV);
BAQC = max(0,BAQNODEFC - DEFANTBAQC);
BAQP = max(0,BAQNODEFP - DEFANTBAQP);

regle 841110:
application : iliad , batch ;

BA1V = BA1AV + BAF1AV -COD5XN;
BA1C = BA1AC + BAF1AC - COD5YN;
BA1P = BA1AP + BAF1AP - COD5ZN;


regle 841120:
application : iliad , batch ;

BAHQT = BAHQV + BAHQC + BAHQP ;
BAQT = BAQV + BAQC + BAQP;

regle 841180:
application : iliad , batch ;


BA1 = BA1V + BA1C + BA1P ; 

regle 841190:
application : iliad , batch ;

BANOR =  BAHQV + BAHQC + BAHQP ;

regle 841200:
application : iliad , batch ;


BAGF1A = BANV + BAMV + BANC + BAMC + BANP + BAMP  
         + (max(0,(4BACREV - 4DEFBACREV))*positif_ou_nul(BARAV)+arr(max(0,(4BAHREV - 4DEFBAHREV))*MAJREV) * positif_ou_nul(BARSV))
         + (max(0,(4BACREC - 4DEFBACREC))*positif_ou_nul(BARAC)+arr(max(0,(4BAHREC - 4DEFBAHREC))*MAJREV) * positif_ou_nul(BARSC))
         + (max(0,(4BACREP - 4DEFBACREP))*positif_ou_nul(BARAP)+arr(max(0,(4BAHREP - 4DEFBAHREP))*MAJREV) * positif_ou_nul(BARSP)) ;

DEFBA1 = ((1-positif(BAHQNODEFF+BAQNODEFF)) * (abs(BAHQNODEFF+BAQNODEFF)) - abs(DEFIBA)
                 + positif(BAHQNODEFF+BAQNODEFF) *
                 positif_ou_nul(DAGRI5+DAGRI4+DAGRI3+DAGRI2+DAGRI1-BAHQNODEFF-BAQNODEFF-BAEV - BAEMV-BAEC - BAEMC-BAEP - BAEMP)
                 * (DAGRI5+DAGRI4+DAGRI3+DAGRI2+DAGRI1-BAHQNODEFF-BAQNODEFF-BAEV - BAEMV-BAEC - BAEMC-BAEP - BAEMP)
                  * null(DEFBA2P+DEFBA3P+DEFBA4P+DEFBA5P+DEFBA6P)) * null(4 - V_IND_TRAIT)
                 +  (positif(SHBA-SEUIL_IMPDEFBA) * positif(DEFBANIF) * max(0,DEFBANIF - DBAIP) * positif(DEFBANIF+0)
                 + positif(SHBA-SEUIL_IMPDEFBA) * max(0,-(BAHQV+BAHQC+BAHQP+4BAQV+4BAQC+4BAQP))* (1-positif(DEFBANIF+0))) * null(5 - V_IND_TRAIT);

regle 860:
application : iliad , batch  ;
DEFBA2 = ((1-positif(BAHQNODEFF+BAQNODEFF)) * (DAGRI1)
                 + positif(BAHQNODEFF+BAQNODEFF) *
                 abs(min(max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5-DAGRI4-DAGRI3-DAGRI2,0)-DAGRI1,DAGRI1))
                 * positif_ou_nul(DAGRI1-max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5-DAGRI4-DAGRI3-DAGRI2,0))) * null(4-V_IND_TRAIT)
                  + (positif(DEFBANIF) * min(DAGRI1,DEFBANIF+DAGRI-DBAIP-DEFBA1)
                    + null(DEFBANIF) * min(DAGRI1,DAGRI-DBAIP))  * null(5-V_IND_TRAIT);
regle 862:
application : iliad , batch  ;
DEFBA3 = ((1-positif(BAHQNODEFF+BAQNODEFF)) * DAGRI2
                 + positif(BAHQNODEFF+BAQNODEFF) *
                 abs(min(max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5-DAGRI4-DAGRI3,0)-DAGRI2,DAGRI2))
                 * positif_ou_nul(DAGRI2-max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5-DAGRI4-DAGRI3,0))) * null(4-V_IND_TRAIT)
                  + (null(DEFBANIF) * min(DAGRI2,DAGRI-DBAIP-DEFBA2)
                    + positif(DEFBANIF) * min(DAGRI2,DEFBANIF+DAGRI-DBAIP-DEFBA1- DEFBA2))  * null(5-V_IND_TRAIT);
regle 864:
application : iliad , batch  ;
DEFBA4 = ((1-positif(BAHQNODEFF+BAQNODEFF)) * (DAGRI3)
                 + positif(BAHQNODEFF+BAQNODEFF) *
                 abs(min(max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5-DAGRI4,0)-DAGRI3,DAGRI3))
                 * positif_ou_nul(DAGRI3-max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5-DAGRI4,0))) * null(4-V_IND_TRAIT)
                  + (null(DEFBANIF) * min(DAGRI3,DAGRI-DBAIP-DEFBA2-DEFBA3)
                    + positif(DEFBANIF) * min(DAGRI3,DEFBANIF+DAGRI-DBAIP-DEFBA1- DEFBA2-DEFBA3))  * null(5-V_IND_TRAIT);
regle 866:
application : iliad , batch  ;
DEFBA5 = ((1-positif(BAHQNODEFF+BAQNODEFF)) * (DAGRI4)
                 + positif(BAHQNODEFF+BAQNODEFF) *
                 abs(min(max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5,0)-DAGRI4,DAGRI4))
                 * positif_ou_nul(DAGRI4-max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5,0))) * null(4-V_IND_TRAIT)
                  + (null(DEFBANIF) * min(DAGRI4,DAGRI-DBAIP-DEFBA2-DEFBA3-DEFBA4)
                    + positif(DEFBANIF) * min(DAGRI4,DEFBANIF+DAGRI-DBAIP-DEFBA1- DEFBA2-DEFBA3-DEFBA4))  * null(5-V_IND_TRAIT);
regle 868:
application : iliad , batch  ;
DEFBA6 = ((1-positif(BAHQNODEFF+BAQNODEFF)) * (DAGRI5)
                 + positif(BAHQNODEFF+BAQNODEFF) *
                 abs(min(max(BAHQNODEFF+BAQNODEFF-DAGRI6,0)-DAGRI5,DAGRI5))
                 * positif_ou_nul(DAGRI5-max(BAHQNODEFF+BAQNODEFF-DAGRI6,0))) * null(4-V_IND_TRAIT)
                  + (null(DEFBANIF) * min(DAGRI5,DAGRI-DBAIP-DEFBA2-DEFBA3-DEFBA4-DEFBA5)
                    + positif(DEFBANIF) * min(DAGRI5,DEFBANIF+DAGRI-DBAIP-DEFBA1- DEFBA2-DEFBA3-DEFBA4-DEFBA5))  * null(5-V_IND_TRAIT);
regle 870:
application : iliad , batch  ;
DEFBA2P = ((1-positif(BAHQNODEFF+BAQNODEFF)) * (DAGRI1)
                 + positif(BAHQNODEFF+BAQNODEFF) *
                 abs(min(max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5-DAGRI4-DAGRI3-DAGRI2,0)-DAGRI1,DAGRI1))
                 * positif_ou_nul(DAGRI1-max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5-DAGRI4-DAGRI3-DAGRI2,0)));
regle 87202:
application : iliad , batch  ;
DEFBA3P = ((1-positif(BAHQNODEFF+BAQNODEFF)) * DAGRI2
                 + positif(BAHQNODEFF+BAQNODEFF) *
                 abs(min(max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5-DAGRI4-DAGRI3,0)-DAGRI2,DAGRI2))
                 * positif_ou_nul(DAGRI2-max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5-DAGRI4-DAGRI3,0)));
regle 874:
application : iliad , batch  ;
DEFBA4P = ((1-positif(BAHQNODEFF+BAQNODEFF)) * (DAGRI3)
                 + positif(BAHQNODEFF+BAQNODEFF) *
                 abs(min(max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5-DAGRI4,0)-DAGRI3,DAGRI3))
                 * positif_ou_nul(DAGRI3-max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5-DAGRI4,0)));
regle 87602:
application : iliad , batch  ;
DEFBA5P = ((1-positif(BAHQNODEFF+BAQNODEFF)) * (DAGRI4)
                 + positif(BAHQNODEFF+BAQNODEFF) *
                 abs(min(max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5,0)-DAGRI4,DAGRI4))
                 * positif_ou_nul(DAGRI4-max(BAHQNODEFF+BAQNODEFF-DAGRI6-DAGRI5,0)));
regle 878:
application : iliad , batch  ;
DEFBA6P = ((1-positif(BAHQNODEFF+BAQNODEFF)) * (DAGRI5)
                 + positif(BAHQNODEFF+BAQNODEFF) *
                 abs(min(max(BAHQNODEFF+BAQNODEFF-DAGRI6,0)-DAGRI5,DAGRI5))
                 * positif_ou_nul(DAGRI5-max(BAHQNODEFF+BAQNODEFF-DAGRI6,0)));
regle 841210:
application : iliad , batch ;


DEFIBAANT = positif_ou_nul(BAQT+BAHQTOT-DAGRI1 - DAGRI2 - DAGRI3 - DAGRI4 - DAGRI5 - DAGRI6 )
            * (DAGRI1 - DAGRI2 - DAGRI3 - DAGRI4 - DAGRI5 - DAGRI6)
            + positif_ou_nul(DAGRI1 + DAGRI2 + DAGRI3 + DAGRI4 + DAGRI5 + DAGRI6 -BAQT-BAHQTOT)
            * (BAQT+BAHQTOT);

regle 841230:
application : iliad , batch ;

BAQTOTAVIS = max(0,((1-positif(BAHQNODEFF))* BAHQNODEFF) + BAQV + BAQC + BAQP);
regle 841240:
application : iliad , batch ;


SOMDEFBANI = max(0,BAFORESTV+BAFPVV+BACREV+BAHREV*MAJREV+BAFORESTC+BAFPVC+BACREC+BAHREC*MAJREV+BAFORESTP+BAFPVP+BACREP+BAHREP*MAJREV
                 + 4BACREV + 4BAHREV * MAJREV + 4BACREC + 4BAHREC * MAJREV + 4BACREP + 4BAHREP * MAJREV - BAHQPROV) ;

regle 841250:
application : iliad , batch ;

BAHQPROV = BANV + BAMV + BANC + BAMC + BANP + BAMP 
        +(max(0,(4BACREV - 4DEFBACREV))*positif_ou_nul(BARAV)+arr(max(0,(4BAHREV - 4DEFBAHREV))*MAJREV) * positif_ou_nul(BARSV))
        +(max(0,(4BACREC - 4DEFBACREC))*positif_ou_nul(BARAC)+arr(max(0,(4BAHREC - 4DEFBAHREC))*MAJREV) * positif_ou_nul(BARSC))
        +(max(0,(4BACREP - 4DEFBACREP))*positif_ou_nul(BARAP)+arr(max(0,(4BAHREP - 4DEFBAHREP))*MAJREV) * positif_ou_nul(BARSP)) ;

regle 841260:
application : iliad , batch ;

DEFBANI = max(0,BAFORESTV+BAFPVV+BACREV+arr(BAHREV*MAJREV)+BAFORESTC+BAFPVC+BACREC+arr(BAHREC*MAJREV)+BAFORESTP+BAFPVP+BACREP+arr(BAHREP*MAJREV)
                 +4BACREV + arr(4BAHREV * MAJREV) + 4BACREC + arr(4BAHREC * MAJREV) + 4BACREP + arr(4BAHREP * MAJREV) 
		 + COD5AK+arr(COD5AL*MAJREV)+ COD5BK+arr(COD5BL*MAJREV)+ COD5CK+arr(COD5CL*MAJREV)
                 + min(0,BAHQV+BAHQC+BAHQP+4BAQV+4BAQC+4BAQP) * (1-positif(SHBA-SEUIL_IMPDEFBA))) ;

DEFBANIH470 = max(0,BAFORESTV+BAFPVV+BACREV+arr(BAHREV*MAJREV)+BAFORESTC+BAFPVC+BACREC+arr(BAHREC*MAJREV)+BAFORESTP+BAFPVP+BACREP+arr(BAHREP*MAJREV)
                 +4BACREV + arr(4BAHREV * MAJREV) + 4BACREC + arr(4BAHREC * MAJREV) + 4BACREP + arr(4BAHREP * MAJREV));

DEFBANI470 =  max(0,-BAHQV-BAHQC-BAHQP-4BAQV-4BAQC-4BAQP) * (1-positif(SHBA-SEUIL_IMPDEFBA)) ;
DEFBANI470BIS =  max(0,-BAHQV-BAHQC-BAHQP-4BAQV-4BAQC-4BAQP) * positif(SHBA-SEUIL_IMPDEFBA);
regle 841270:
application : iliad , batch ;


DEFBANIF = (1-PREM8_11) * positif(SOMMEBAND_2) * positif(DEFBA_P+DEFBAP2 +DEFBA1731)
                      * max(0,min(min(DEFBA1731+DEFBA71731+ DEFBANI4701731 * positif(SHBA-SEUIL_IMPDEFBA),
                                            max(DEFBA_P+DEFBA7_P+DEFBANI470_P * positif(SHBA-SEUIL_IMPDEFBA),
                                                DEFBAP2+DEFBA7P2+DEFBANI470P2 * positif(SHBA-SEUIL_IMPDEFBA)))
                                                ,DBAIP+SOMDEFBANI
                                        -max(DEFBANIH4701731 + DEFBANI4701731 * (1-positif(SHBA-SEUIL_IMPDEFBA))
                                                   ,max(DEFBANIH470_P + DEFBANI470_P * (1-positif(SHBA-SEUIL_IMPDEFBA))
                                                       ,DEFBANIH470P2 + DEFBANI470P2 * (1-positif(SHBA-SEUIL_IMPDEFBA))))
                                                       - max(0,DEFBANI-DEFBANIH470P3 + DEFBANI470P3 * (1-positif(SHBA-SEUIL_IMPDEFBA)))))
         + PREM8_11 * positif(DEFBANI) * (DBAIP + SOMDEFBANI * positif(SHBA-SEUIL_IMPDEFBA));

regle 841280:
application : iliad , batch ;


PRORATABA = (4BACREV + 4BACREC +4BACREP +arr(4BAHREV*MAJREV) +arr(4BAHREC*MAJREV) +arr(4BAHREP*MAJREV)-max(0,4BAQV+4BAQC+4BAQP+min(0,BAHQV+BAHQC+BAHQP)))/SOMDEFBANI+0;
regle 841290:
application : iliad , batch ;


DEFNIBAQ = (DEFNIBAQ1+max(0,arr((DEFBANIF - DBAIP) * PRORATABA))) * positif(DEFBANIF-DBAIP)
         + DEFNIBAQ1 * (1-positif(DEFBANIF-DBAIP));
regle 8509355:
application : iliad , batch  ;
DEFNIBAQ1 = max(0,min(DEFBANIF,min(4BAQV+4BAQC+4BAQP,DBAIP-max(0,BAHQV+BAHQC+BAHQP)))) * positif(DBAIP - max(0,BAHQV+BAHQC+BAHQP));
regle 8509365:
application : iliad , batch  ;
DAGRIIMP = min(max(0,BAHQNODEFF + BAQNODEFF),DEFANTBAF);
regle 8509375:
application : iliad , batch  ;
DBAIP = DAGRIIMP;
regle 841300:
application : iliad , batch ;


BATMARGV = COD5XT + arr(COD5XV * MAJREV) ;
BATMARGC = COD5XU + arr(COD5XW * MAJREV) ;
BATMARGTOT =  BATMARGV + BATMARGC ;

regle 841310:
application : iliad , batch ;


IBATMARG = arr(BATMARGTOT * TXMARJBA/100) ;

PASBACV = (positif(BARAV) * (BACREV - DEFBACREV) + (1-positif(BARAV)) * BARAV) - COD5AQ + COD5AY;
PASBACC = (positif(BARAC) * (BACREC - DEFBACREC) + (1-positif(BARAC)) * BARAC) - COD5BQ + COD5BY;

R25HIV = (positif(BARSV) * (BAHREV - DEFBAHREV) + (1-positif(BARSV)) * (BARSV)) - COD5AR + COD5AZ;
R25HIC = (positif(BARSC) * (BAHREC - DEFBAHREC) + (1-positif(BARSC)) * (BARSC)) - COD5BR + COD5BZ;
PASBAHV = positif(R25HIV) * arr(R25HIV * MAJREV) + (1-positif(R25HIV)) * R25HIV;
PASBAHC = positif(R25HIC) * arr(R25HIC * MAJREV) + (1-positif(R25HIC)) * R25HIC;

PASBAACPTV = supzero(arr(max(0,
			(	((BAMICV + BAFORESTV + PASBACV + PASBAHV)* (12/(min(12,COD5AD) + 12 * null(COD5AD + 0)))) 
			- (DEFANTBAV * ((IBAMICV + BAFORESTV + BACREV - DEFBACREV + 4BACREV - 4DEFBACREV + R2MAJ5HI + BAEMV)/(BAHQNODEFV + BAEV + BAEMV))) )
			)
		       	* (1 - positif(COD5AF)))) ;

PASBAACPTC = supzero(arr(max(0,
			(	((BAMICC + BAFORESTC + PASBACC + PASBAHC)* (12/( min(12,COD5BD) + 12 * null(COD5BD + 0)))) 
			- (DEFANTBAC * ((IBAMICC + BAFORESTC + BACREC - DEFBACREC + 4BACREC - 4DEFBACREC + R2MAJ5II + BAEMC)/(BAHQNODEFC + BAEC + BAEMC))) )
			)
		       	* (1 - positif(COD5AI)))) ;

