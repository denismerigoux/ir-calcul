#*************************************************************************************************************************
#
#Copyright or � or Copr.[DGFIP][2017]
#
#Ce logiciel a �t� initialement d�velopp� par la Direction G�n�rale des 
#Finances Publiques pour permettre le calcul de l'imp�t sur le revenu 2017 
#au titre des revenus per�us en 2016. La pr�sente version a permis la 
#g�n�ration du moteur de calcul des cha�nes de taxation des r�les d'imp�t 
#sur le revenu de ce mill�sime.
#
#Ce logiciel est r�gi par la licence CeCILL 2.1 soumise au droit fran�ais 
#et respectant les principes de diffusion des logiciels libres. Vous pouvez 
#utiliser, modifier et/ou redistribuer ce programme sous les conditions de 
#la licence CeCILL 2.1 telle que diffus�e par le CEA, le CNRS et l'INRIA  sur 
#le site "http://www.cecill.info".
#
#Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
#connaissance de la licence CeCILL 2.1 et que vous en avez accept� les termes.
#
#**************************************************************************************************************************
regle taux 201600:
application : batch , iliad ;

INDPASV = positif( TSHALLOV + COD1GA + ALLOV + FRNV + CARTSV + REMPLAV
                   + PRBRV + PENINV + CARPEV + CODRAZ + PEBFV
                   + COD1AG + PALIV + PENSALV + COD1AM
                   + COD5XB + BAFORESTV + BACREV + BACDEV
                   + BAHREV + BAHDEV + 4BACREV + 4BAHREV
                   + MIBVENV + MIBPRESV + BICNOV + BICDNV + BIHNOV + BIHDNV + MIBMEUV 
                   + MIBGITEV + LOCGITV + LOCNPCGAV + LOCGITCV + LOCDEFNPCGAV + LOCNPV 
                   + LOCGITHCV + LOCDEFNPV + MIBNPVENV + MIBNPPRESV + BICREV + BICDEV 
	           + BICHREV + BICHDEV
                   + BNCPROV + BNCREV + BNCDEV + BNHREV + BNHDEV
                   + BNCNPV + BNCAABV + BNCAADV + ANOCEP + DNOCEP + 0) ; 

INDPASC = positif( TSHALLOC + COD1HA + ALLOC + FRNC + CARTSC + REMPLAC
                   + PRBRC + PENINC + CARPEC + CODRBZ + PEBFC
                   + COD1BG + PALIC + PENSALC + COD1BM
                   + COD5YB + BAFORESTC + BACREC + BACDEC 
                   + BAHREC + BAHDEC + 4BACREC + 4BAHREC
                   + MIBVENC + MIBPRESC + BICNOC + BICDNC + BIHNOC + BIHDNC + MIBMEUC 
                   + MIBGITEC + LOCGITC + LOCNPCGAC + LOCGITCC + LOCDEFNPCGAC + LOCNPC 
	           + LOCGITHCC + LOCDEFNPC + MIBNPVENC + MIBNPPRESC + BICREC + BICDEC 
	           + BICHREC + BICHDEC
                   + BNCPROC + BNCREC + BNCDEC + BNHREC + BNHDEC
                   + BNCNPC + BNCAABC + BNCAADC + ANOVEP + DNOCEPC + 0) ;

INDPASF = positif( RVB1 + RVB2 + RVB3 + RVB4 + RENTAX + RENTAX5 + RENTAX6 + RENTAX7
                   + DAGRI6 + DAGRI5 + DAGRI4 + DAGRI3 + DAGRI2 + DAGRI1
                   + LNPRODEF10 + LNPRODEF9 + LNPRODEF8 + LNPRODEF7 + LNPRODEF6
                   + LNPRODEF5 + LNPRODEF4 + LNPRODEF3 + LNPRODEF2 + LNPRODEF1
	           + DEFBIC6 + DEFBIC5 + DEFBIC4 + DEFBIC3 + DEFBIC2 + DEFBIC1
                   + DABNCNP6 + DABNCNP5 + DABNCNP4 + DABNCNP3 + DABNCNP2 + DABNCNP1
                   + RFORDI + RFDORD + RFDHIS + RFDANT + RFMIC + FONCI + REAMOR + 0) ;

INDPAS = 2 * positif(ANNUL2042 + (null(INDPASV) * null(INDPASC) * null(INDPASF) * positif(IDRS4) * null(ELURASV + 0) * null(ELURASC + 0))
                     + (ELURASV * null(INDPASV) * null(INDPASF)) + (ELURASV * ELURASC * null(INDPASV) * null(INDPASC) * null(INDPASF)))
	 + 3 * positif(ELURASV * null(INDPASV) * null(INDPASF) * INDPASC) * (1 - positif(ANNUL2042))
	 + 4 * positif(ELURASC * null(INDPASC) * null(INDPASF) * INDPASV) * (1 - positif(ANNUL2042))
	 + (1 - positif(ELURASC * null(INDPASC) * null(INDPASF) * INDPASV))
	   * (1 - positif(ELURASV * null(INDPASV) * null(INDPASF) * INDPASC))
	   * (1 - positif(ANNUL2042 + (null(INDPASV) * null(INDPASC) * null(INDPASF) * positif(IDRS4) * null(ELURASV + 0) * null(ELURASC + 0))
	                  + (ELURASV * null(INDPASV) * null(INDPASF)) + (ELURASV * ELURASC * null(INDPASV) * null(INDPASC) * null(INDPASF)))) ;

IRTOTAL = (IDRS3 + ((ABADO + ABAGU) * positif(CODDAJ + CODEAJ + CODDBJ + CODEBJ + PRODOM + PROGUY)) - IDEC - RMENAGE) 
          * ((1 - V_CNR) * positif_ou_nul(IDRS3 + ((ABADO + ABAGU) * positif(CODDAJ + CODEAJ + CODDBJ + CODEBJ + PRODOM + PROGUY)) - IDEC - RMENAGE) 
             + V_CNR * positif(IDRS3 + ((ABADO + ABAGU) * positif(CODDAJ + CODEAJ + CODDBJ + CODEBJ + PRODOM + PROGUY)) - IDEC - RMENAGE)) ;

regle 201620:
application : batch , iliad ;

TSRASF = PASTSNTV + PASPRNV + PASTSNTC + PASPRNC ;

BAPASV = max(0 , arr(IBAMICV + BAFORESTV + (positif(BARAV) * (BACREV - DEFBACREV) + (1 - positif(BARAV)) * BARAV) + R2MAJ5HI
                     - arr(DEFANTBAV * ((IBAMICV + BAFORESTV + (positif(BARAV) * (BACREV - DEFBACREV) + (1-positif(BARAV)) * BARAV) + 4BACREV - 4DEFBACREV 
		                         + R2MAJ5HI + (4BAHREV - 4DEFBAHREV) * MAJREV) / (BAHQNODEFV + BAEV + BAEMV)))) 
                 + BAQV + BATMARGV) * (1 - positif(COD5AF)) ;

BAPASC = max(0 , arr(IBAMICC + BAFORESTC + (positif(BARAC) * (BACREC - DEFBACREC) + (1 - positif(BARAC)) * BARAC) + R2MAJ5II
                     - arr(DEFANTBAC * ((IBAMICC + BAFORESTC + (positif(BARAC) * (BACREC - DEFBACREC) + (1-positif(BARAC)) * BARAC) + 4BACREC - 4DEFBACREC 
		                         + R2MAJ5II + (4BAHREC - 4DEFBAHREC) * MAJREV) / (BAHQNODEFC + BAEC + BAEMC)))) 
                 + BAQC + BATMARGC) * (1 - positif(COD5AI)) ;

BICPASV = max(0 , (MIB_NETVV + MIB_NETPV + MIBPVV - BICPMVCTV + SP5KC + RPMAJ5KI) * (1 - positif(COD5BF))
                  + max(0 , MIB_NETNPVV + MIB_NETNPPV + MIBNPPVV - MIBNPDCT + arr(SNP5NC + RNPMAJ5NI - (BICNPDEFANTV * (MIB_NETNPVV + MIB_NETNPPV + MIBNPPVV - MIBNPDCT + SNP5NC + RNPMAJ5NI) / BICR2TOTALV))) * (1 - positif(COD5AN))
                  + max(0 , MLOCNETV + SNPLOCPASV + RNPLOCPASV - arr(DEFANTLOCV * (SNPLOCPASV + RNPLOCPASV + MLOCNETV) / RNPILOCV)) * (1 - positif(COD5CF))) ;

BICPASC = max(0 , (MIB_NETVC + MIB_NETPC + MIBPVC - BICPMVCTC + SP5LC + RPMAJ5LI) * (1 - positif(COD5BI))
                  + max(0 , MIB_NETNPVC + MIB_NETNPPC + MIBNPPVC - COD5RZ + arr(SNP5OC + RNPMAJ5OI - (BICNPDEFANTC * (MIB_NETNPVC + MIB_NETNPPC + MIBNPPVC - COD5RZ + SNP5OC + RNPMAJ5OI) / BICR2TOTALC))) * (1 - positif(COD5BN))
                  + max(0 , MLOCNETC + SNPLOCPASC + RNPLOCPASC - arr(DEFANTLOCC * (SNPLOCPASC + RNPLOCPASC + MLOCNETC) / RNPILOCC)) * (1 - positif(COD5CI))) ;

BNCPASV = max(0 , (MICROBNCV + SP5QC + RPMAJ5QI) * (1 - positif(COD5AO))
                  + max(0 , SPENETNPV + BNCNPPVV - BNCNPDCT + arr(SNP5JG + RNPMAJ5SN - arr(DABNCNPV * (SPENETNPV + BNCNPPVV - BNCNPDCT + SNP5JG + RNPMAJ5SN) / BNCR2TOTALV))) * (1 - positif(COD5AP))) ; 

BNCPASC = max(0 , (MICROBNCC + SP5RC + RPMAJ5RI) * (1 - positif(COD5BO))
                  + max(0 , SPENETNPC + BNCNPPVC - COD5LD + arr(SNP5RF + RNPMAJ5NS - arr(DABNCNPC * (SPENETNPC + BNCNPPVC - COD5LD + SNP5RF + RNPMAJ5NS) / BNCR2TOTALC))) * (1 - positif(COD5BP))) ;

REVACOMP = PASTSN1AG + PASTSN1BG + PASPRNAOM + PASPRNBOM 
           + RV1 + RV2 + RV3 + RV4 + T2RV
           + BAPASV + BAPASC
	   + BICPASV + BICPASC
	   + BNCPASV + BNCPASC
	   + ((PASRF + 2REVF + 3REVF + PASRFASS) * (1 - positif(COD4BN))) ;

BICPAS1 = MIB_NETVV + MIB_NETPV + (MIBPVV - BICPMVCTV) + BIPTAV + BIHTAV + max(0 , PASBICNPV) + max(0 , PASRNPLOCFV) ;

BICPAS2 = MIB_NETVC + MIB_NETPC + (MIBPVC - BICPMVCTC) + BIPTAC + BIHTAC + max(0 , PASBICNPC) + max(0 , PASRNPLOCFC) ;

BICPASP = MIB_NETVP + MIB_NETPP + (MIBPVP - BICPMVCTP) + BIPTAP + BIHTAP + max(0 , PASBICNPP) + max(0 , PASRNPLOCFP) ;

REVTOT =   max(0 , TSPRV + 0) + TSNN2VAFF + PENSTOTV + max(0 , BAHQV + BAQV) + max(0 , BICPAS1 + 0) + max(0 , MICROBNCV + BNRV + max(0 , RNPFV + 0))
         + max(0 , TSPRC + 0) + TSNN2CAFF + PENSTOTC + max(0 , BAHQC + BAQC) + max(0 , BICPAS2 + 0) + max(0 , MICROBNCC + BNRC + max(0 , RNPFC + 0))
	 + max(0 , TSPRP + 0) + TSNN2PAFF + PENSTOTP + max(0 , BAHQP + BAQP) + max(0 , BICPASP + 0) + max(0 , MICROBNCP + BNRP + max(0 , RNPFP + 0))
	 + PRNFAS + PRNFBS + PRNFCS + PRNFDS + PRNFES + PRNFFS
	 + GLN3 + BATMARGTOT + COD1TZ + RVTOT + T2RV + RRCM + REVRCM + PVBAR3VG + CODRVG + PVBAR3VE + max(0 , RRFI + 0) + REVRF + DESV ;

regle taux 201640:
application : batch , iliad ;
 
PASDENF = TSHALLOV + COD1GA + ALLOV + CARTSV + REMPLAV + CODDAJ + CODEAJ
          + TSHALLOC + COD1HA + ALLOC + CARTSC + REMPLAC + CODDBJ + CODEBJ
          + PRBRV + PENINV + CARPEV + CODRAZ + PEBFV
          + PRBRC + PENINC + CARPEC + CODRBZ + PEBFC
	  + REVACOMP ;

CIPAS = (COD8VM + COD8WM) * (1 - positif(COD8PA + 0)) + min(COD8VM + COD8WM , (COD8VM + COD8WM) * COD8PA / max(1 , COD8VM + COD8WM + COD8UM)) * positif(COD8PA + 0) ;

PASNUMF = max(0 , arr((IRTOTAL * (TSRASF + REVACOMP) / REVTOT) - CIPAS)) * (1 - null(REVTOT)) ;

INDTAZ = positif(null(IINETIR) + positif(IREST) + (positif(IINETIR) * null(1 - IND61IR)) + (positif(IINETIR) * null(2 - IND61IR) * null(IINET)))
         * null((1 - ((null(V_BTNATIMP) * null(1 - V_BTANC)) + null(11 - V_BTNATIMP) + null(21 - V_BTNATIMP))) * (1 - positif(COD8OT)))
         * positif_ou_nul((25000 * NBPT) - REVKIRE) ;

RASTXFOYER = arr(((PASNUMF / PASDENF) * 100 * (1 - null(PASDENF))) * 10) / 10 * (1 - null(2 - INDPAS)) * (1 - INDTAZ) ;

regle taux 201660:
application : batch , iliad ;

RASTSPR1 = (TSHALLOV + COD1GA + ALLOV + CARTSV + REMPLAV + CODDAJ + CODEAJ + PRBRV + PENINV + CARPEV + CODRAZ + PEBFV) 
           * (1 - null(2 - INDPAS) - null(3 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASTSPR2 = (TSHALLOC + COD1HA + ALLOC + CARTSC + REMPLAC + CODDBJ + CODEBJ + PRBRC + PENINC + CARPEC + CODRBZ + PEBFC)
           * BOOL_0AM * (1 - null(2 - INDPAS) - null(3 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASTSPE1 = (PASTSN1AG + max(0 , PRN1AM + PRN1AO)) * (1 - null(2 - INDPAS) - null(3 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASTSPE2 = (PASTSN1BG + max(0 , PRN1BM + PRN1BO)) * BOOL_0AM * (1 - null(2 - INDPAS) - null(4 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASRVTO = (RV1 + RV2 + RV3 + RV4) * (1 - null(2 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASRVTO1 = arr(RASRVTO / (2 - null(BOOL_0AM))) * (1 - null(2 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASRVTO2 = (RASRVTO - RASRVTO1) * BOOL_0AM * (1 - null(2 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASRF = (PASRF + PASRFASS) * (1 - positif(COD4BN)) * (1 - null(2 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASRF1 = arr(RASRF / (2 - null(BOOL_0AM))) * (1 - null(2 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASRF2 = (RASRF - RASRF1) * BOOL_0AM * (1 - null(2 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASBA1 = max(0 , arr((BAMICV + BAFORESTV + PASBACV + PASBAHV) * 12 / min(12 , COD5AD + 12 * null(COD5AD + 0)))
                 - arr(DEFANTBAV * ((IBAMICV + BAFORESTV + (positif(BARAV) * (BACREV - DEFBACREV) + (1-positif(BARAV)) * BARAV) + 4BACREV - 4DEFBACREV 
		                     + (BAHREV - DEFBAHREV + 4BAHREV - 4DEFBAHREV) * MAJREV) / (BAHQNODEFV + BAEV + BAEMV))))
         * (1 - positif(COD5AF)) * (1 - null(2 - INDPAS) - null(3 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASBA2 = max(0 , arr((BAMICC + BAFORESTC + PASBACC + PASBAHC) * 12 / min(12 , COD5BD + 12 * null(COD5BD + 0)))
                 - arr(DEFANTBAC * ((IBAMICC + BAFORESTC + (positif(BARAC) * (BACREC - DEFBACREC) + (1-positif(BARAC)) * BARAC) + 4BACREC - 4DEFBACREC 
		                     + (BAHREC - DEFBAHREC + 4BAHREC - 4DEFBAHREC)*MAJREV)/(BAHQNODEFC + BAEC + BAEMC))))
         * (1 - positif(COD5AI)) * BOOL_0AM * (1 - null(2 - INDPAS) - null(4 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;
          
RASBIC1 = BICASSV * (1 - null(2 - INDPAS) - null(3 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASBIC2 = BICASSC * BOOL_0AM * (1 - null(2 - INDPAS) - null(4 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASBNC1 = max(0 , arr((SPENETPV + PASBNNAV + PASBNNSV) * 12 / min(12 , COD5XI + 12 * null(COD5XI + 0))) * (1 - positif(COD5AO))
                  + max(0 , arr((SPENETNPV + PASBNNAAV + PASNOCEPIMPV) * 12 / min(12 , COD5XR + 12 * null(COD5XR + 0))) 
		            - arr(DABNCNPV * (SPENETNPV + BNCNPPVV - BNCNPDCT + SNP5JG + RNPMAJ5SN) / BNCR2TOTALV)) * (1 - positif(COD5AP)))
	   * (1 - null(2 - INDPAS) - null(3 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASBNC2 = max(0 , arr((SPENETPC + PASBNNAC + PASBNNSC) * 12 / min(12 , COD5YI + 12 * null(COD5YI + 0))) * (1 - positif(COD5BO))
                  + max(0 , arr((SPENETNPC + PASBNNAAC + PASNOCEPIMPC) * 12 / min(12 , COD5YR + 12 * null(COD5YR + 0))) 
		            - arr(DABNCNPC * (SPENETNPC + BNCNPPVC - COD5LD + SNP5RF + RNPMAJ5NS) / BNCR2TOTALC)) * (1 - positif(COD5BP))) 
           * BOOL_0AM * (1 - null(2 - INDPAS) - null(4 - INDPAS)) * (1 - INDTAZ) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

regle 201680:
application : batch , iliad ;

REVDEC1 = PASTSNTV + PASPRNV + PASTSN1AG + PASPRNAOM + BAPASV + BICPASV + BNCPASV ;

REVDEC2 = PASTSNTC + PASPRNC + PASTSN1BG + PASPRNBOM + BAPASC + BICPASC + BNCPASC ;

TXPASMIN = positif(REVDEC2 - REVDEC1) + 2 * positif(REVDEC1 - REVDEC2) + 4 * null(REVDEC1 - REVDEC2) ;

SHBAPAS = (TSPRV + PENSTOTV + MICROBNCV + BNRV + max(0 , RNPFV) + BICPAS1) * null(1 - TXPASMIN)
          + (TSPRC + PENSTOTC + MICROBNCC + BNRC + max(0 , RNPFC) + BICPAS2) * null(2 - TXPASMIN)
          + arr(COD1TZ/2) + arr(RVTOT/2) + arr(RRCM/2) + arr(PVBAR3VG/2) + arr(RRFI/2) + arr(DESV/2) ;

REVORDI = (TSPRV + MICROBNCV + BNRV + max(0 , RNPFV) + BAHQV * (positif(BAHQV) + (1 - positif(BAHQV)) * positif(SEUIL_IMPDEFBA - SHBAPAS)) + BICPAS1) * null(1 - TXPASMIN)
          + (TSPRC + MICROBNCC + BNRC + max(0 , RNPFC) + BAHQC * (positif(BAHQC) + (1 - positif(BAHQC)) * positif(SEUIL_IMPDEFBA - SHBAPAS)) + BICPAS2) * null(2 - TXPASMIN)
          + arr(COD1TZ/2) + arr(RVTOT/2) + arr(RRCM/2) + arr(PVBAR3VG/2) + arr(RRFI/2) + arr(DESV/2) ;

REVQUOT = (TSNN2VAFF + PENSTOTV + PENFV + GLN3V + BAQV) * null(1 - TXPASMIN)
	  + (TSNN2CAFF + PENSTOTC + PENFC + GLN3C + BAQC) * null(2 - TXPASMIN)
	  + arr(T2RV/2) + arr(REVRCM/2) + arr(CODRVG/2) + arr(REVRF/2) ;

REVMARG = (BATMARGV * null(1 - TXPASMIN)) + (BATMARGC * null(2 - TXPASMIN)) ;

CHARGES = arr(DFANT/2) + arr(RDCSG/2) + arr(CHTOT/2) + arr(ABVIE/2) + arr(ABMAR/2) ;

REVINDIV = max(REVORDI - CHARGES , 0) ;

NBPTPAS = arr(NBPT*2) / 4 ;

regle 201700:
application : batch , iliad ;

IRINDPASV = max(positif(arr(REVINDIV/NBPTPAS) - LIM_BAR2) * positif_ou_nul(LIM_BAR3 - arr(REVINDIV/NBPTPAS)) * arr((REVINDIV * TX_BAR2/100) - (NBPTPAS * PLABAR2))
                + positif(arr(REVINDIV/NBPTPAS) - LIM_BAR3) * positif_ou_nul(LIM_BAR4 - arr(REVINDIV/NBPTPAS)) * arr((REVINDIV * TX_BAR3/100) - (NBPTPAS * PLABAR3))
	        + positif(arr(REVINDIV/NBPTPAS) - LIM_BAR4) * positif_ou_nul(LIM_BAR5 - arr(REVINDIV/NBPTPAS)) * arr((REVINDIV * TX_BAR4/100) - (NBPTPAS * PLABAR4))
	        + positif(arr(REVINDIV/NBPTPAS) - LIM_BAR5) * arr((REVINDIV * TX_BAR5/100) - (NBPTPAS * PLABAR5)) + 0
             ,  positif(REVINDIV - LIM_BAR2) * positif_ou_nul(LIM_BAR3 - REVINDIV) * arr((REVINDIV * TX_BAR2/100) - PLABAR2 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
                + positif(REVINDIV - LIM_BAR3) * positif_ou_nul(LIM_BAR4 - REVINDIV) * arr((REVINDIV * TX_BAR3/100) - PLABAR3 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
	        + positif(REVINDIV - LIM_BAR4) * positif_ou_nul(LIM_BAR5 - REVINDIV) * arr((REVINDIV * TX_BAR4/100) - PLABAR4 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
	        + positif(REVINDIV - LIM_BAR5) * arr((REVINDIV * TX_BAR5/100) - PLABAR5 - (2 * PLAF_DEMIPART * (NBPTPAS - 1))) + 0) * null(1 - TXPASMIN) ;

IRINDPASC = max(positif(arr(REVINDIV/NBPTPAS) - LIM_BAR2) * positif_ou_nul(LIM_BAR3 - arr(REVINDIV/NBPTPAS)) * arr((REVINDIV * TX_BAR2/100) - (NBPTPAS * PLABAR2))
                + positif(arr(REVINDIV/NBPTPAS) - LIM_BAR3) * positif_ou_nul(LIM_BAR4 - arr(REVINDIV/NBPTPAS)) * arr((REVINDIV * TX_BAR3/100) - (NBPTPAS * PLABAR3))
	        + positif(arr(REVINDIV/NBPTPAS) - LIM_BAR4) * positif_ou_nul(LIM_BAR5 - arr(REVINDIV/NBPTPAS)) * arr((REVINDIV * TX_BAR4/100) - (NBPTPAS * PLABAR4))
	        + positif(arr(REVINDIV/NBPTPAS) - LIM_BAR5) * arr((REVINDIV * TX_BAR5/100) - (NBPTPAS * PLABAR5)) + 0
             ,  positif(REVINDIV - LIM_BAR2) * positif_ou_nul(LIM_BAR3 - REVINDIV) * arr((REVINDIV * TX_BAR2/100) - PLABAR2 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
                + positif(REVINDIV - LIM_BAR3) * positif_ou_nul(LIM_BAR4 - REVINDIV) * arr((REVINDIV * TX_BAR3/100) - PLABAR3 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
	        + positif(REVINDIV - LIM_BAR4) * positif_ou_nul(LIM_BAR5 - REVINDIV) * arr((REVINDIV * TX_BAR4/100) - PLABAR4 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
	        + positif(REVINDIV - LIM_BAR5) * arr((REVINDIV * TX_BAR5/100) - PLABAR5 - (2 * PLAF_DEMIPART * (NBPTPAS - 1))) + 0) * null(2 - TXPASMIN) ;

IRINDPAS = IRINDPASV + IRINDPASC ;

IRINDPASQV = max(positif(arr((REVINDIV + RASTONEQUO1V)/NBPTPAS) - LIM_BAR2) * positif_ou_nul(LIM_BAR3 - arr((REVINDIV + RASTONEQUO1V)/NBPTPAS)) * arr(((REVINDIV + RASTONEQUO1V) * TX_BAR2/100) - (NBPTPAS * PLABAR2))
                 + positif(arr((REVINDIV + RASTONEQUO1V)/NBPTPAS) - LIM_BAR3) * positif_ou_nul(LIM_BAR4 - arr((REVINDIV + RASTONEQUO1V)/NBPTPAS)) * arr(((REVINDIV + RASTONEQUO1V) * TX_BAR3/100) - (NBPTPAS * PLABAR3))
                 + positif(arr((REVINDIV + RASTONEQUO1V)/NBPTPAS) - LIM_BAR4) * positif_ou_nul(LIM_BAR5 - arr((REVINDIV + RASTONEQUO1V)/NBPTPAS)) * arr(((REVINDIV + RASTONEQUO1V) * TX_BAR4/100) - (NBPTPAS * PLABAR4))
                 + positif(arr((REVINDIV + RASTONEQUO1V)/NBPTPAS) - LIM_BAR5) * arr(((REVINDIV + RASTONEQUO1V) * TX_BAR5/100) - (NBPTPAS * PLABAR5)) + 0
              ,  positif((REVINDIV + RASTONEQUO1V) - LIM_BAR2) * positif_ou_nul(LIM_BAR3 - (REVINDIV + RASTONEQUO1V)) * arr(((REVINDIV + RASTONEQUO1V) * TX_BAR2/100) - PLABAR2 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
                 + positif((REVINDIV + RASTONEQUO1V) - LIM_BAR3) * positif_ou_nul(LIM_BAR4 - (REVINDIV + RASTONEQUO1V)) * arr(((REVINDIV + RASTONEQUO1V) * TX_BAR3/100) - PLABAR3 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
                 + positif((REVINDIV + RASTONEQUO1V) - LIM_BAR4) * positif_ou_nul(LIM_BAR5 - (REVINDIV + RASTONEQUO1V)) * arr(((REVINDIV + RASTONEQUO1V) * TX_BAR4/100) - PLABAR4 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
                 + positif((REVINDIV + RASTONEQUO1V) - LIM_BAR5) * arr(((REVINDIV + RASTONEQUO1V) * TX_BAR5/100) - PLABAR5 - (2 * PLAF_DEMIPART * (NBPTPAS - 1))) + 0) * null(1 - TXPASMIN) ;

IRINDPASQC = max(positif(arr((REVINDIV + RASTONEQUO1C)/NBPTPAS) - LIM_BAR2) * positif_ou_nul(LIM_BAR3 - arr((REVINDIV + RASTONEQUO1C)/NBPTPAS)) * arr(((REVINDIV + RASTONEQUO1C) * TX_BAR2/100) - (NBPTPAS * PLABAR2))
                 + positif(arr((REVINDIV + RASTONEQUO1C)/NBPTPAS) - LIM_BAR3) * positif_ou_nul(LIM_BAR4 - arr((REVINDIV + RASTONEQUO1C)/NBPTPAS)) * arr(((REVINDIV + RASTONEQUO1C) * TX_BAR3/100) - (NBPTPAS * PLABAR3))
                 + positif(arr((REVINDIV + RASTONEQUO1C)/NBPTPAS) - LIM_BAR4) * positif_ou_nul(LIM_BAR5 - arr((REVINDIV + RASTONEQUO1C)/NBPTPAS)) * arr(((REVINDIV + RASTONEQUO1C) * TX_BAR4/100) - (NBPTPAS * PLABAR4))
                 + positif(arr((REVINDIV + RASTONEQUO1C)/NBPTPAS) - LIM_BAR5) * arr(((REVINDIV + RASTONEQUO1C) * TX_BAR5/100) - (NBPTPAS * PLABAR5)) + 0
              ,  positif((REVINDIV + RASTONEQUO1C) - LIM_BAR2) * positif_ou_nul(LIM_BAR3 - (REVINDIV + RASTONEQUO1C)) * arr(((REVINDIV + RASTONEQUO1C) * TX_BAR2/100) - PLABAR2 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
                 + positif((REVINDIV + RASTONEQUO1C) - LIM_BAR3) * positif_ou_nul(LIM_BAR4 - (REVINDIV + RASTONEQUO1C)) * arr(((REVINDIV + RASTONEQUO1C) * TX_BAR3/100) - PLABAR3 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
                 + positif((REVINDIV + RASTONEQUO1C) - LIM_BAR4) * positif_ou_nul(LIM_BAR5 - (REVINDIV + RASTONEQUO1C)) * arr(((REVINDIV + RASTONEQUO1C) * TX_BAR4/100) - PLABAR4 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
                 + positif((REVINDIV + RASTONEQUO1C) - LIM_BAR5) * arr(((REVINDIV + RASTONEQUO1C) * TX_BAR5/100) - PLABAR5 - (2 * PLAF_DEMIPART * (NBPTPAS - 1))) + 0) * null(2 - TXPASMIN) ;

IRINDPASQ = IRINDPASQV + IRINDPASQC ;

regle taux 201720:
application : batch , iliad ;

RASMARJ = positif((positif(arr(REVINDIV/NBPTPAS) - LIM_BAR2) * positif_ou_nul(LIM_BAR3 - arr(REVINDIV/NBPTPAS)) * arr((REVINDIV * TX_BAR2/100) - (NBPTPAS * PLABAR2))
                + positif(arr(REVINDIV/NBPTPAS) - LIM_BAR3) * positif_ou_nul(LIM_BAR4 - arr(REVINDIV/NBPTPAS)) * arr((REVINDIV * TX_BAR3/100) - (NBPTPAS * PLABAR3))
	        + positif(arr(REVINDIV/NBPTPAS) - LIM_BAR4) * positif_ou_nul(LIM_BAR5 - arr(REVINDIV/NBPTPAS)) * arr((REVINDIV * TX_BAR4/100) - (NBPTPAS * PLABAR4))
	        + positif(arr(REVINDIV/NBPTPAS) - LIM_BAR5) * arr((REVINDIV * TX_BAR5/100) - (NBPTPAS * PLABAR5)))
             - (positif(REVINDIV - LIM_BAR2) * positif_ou_nul(LIM_BAR3 - REVINDIV) * arr((REVINDIV * TX_BAR2/100) - PLABAR2 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
                + positif(REVINDIV - LIM_BAR3) * positif_ou_nul(LIM_BAR4 - REVINDIV) * arr((REVINDIV * TX_BAR3/100) - PLABAR3 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
	        + positif(REVINDIV - LIM_BAR4) * positif_ou_nul(LIM_BAR5 - REVINDIV) * arr((REVINDIV * TX_BAR4/100) - PLABAR4 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
	        + positif(REVINDIV - LIM_BAR5) * arr((REVINDIV * TX_BAR5/100) - PLABAR5 - (2 * PLAF_DEMIPART * (NBPTPAS - 1))))) 
   + (1 - positif((positif(arr(REVINDIV/NBPTPAS) - LIM_BAR2) * positif_ou_nul(LIM_BAR3 - arr(REVINDIV/NBPTPAS)) * arr((REVINDIV * TX_BAR2/100) - (NBPTPAS * PLABAR2))
                + positif(arr(REVINDIV/NBPTPAS) - LIM_BAR3) * positif_ou_nul(LIM_BAR4 - arr(REVINDIV/NBPTPAS)) * arr((REVINDIV * TX_BAR3/100) - (NBPTPAS * PLABAR3))
	        + positif(arr(REVINDIV/NBPTPAS) - LIM_BAR4) * positif_ou_nul(LIM_BAR5 - arr(REVINDIV/NBPTPAS)) * arr((REVINDIV * TX_BAR4/100) - (NBPTPAS * PLABAR4))
	        + positif(arr(REVINDIV/NBPTPAS) - LIM_BAR5) * arr((REVINDIV * TX_BAR5/100) - (NBPTPAS * PLABAR5)))
             - (positif(REVINDIV - LIM_BAR2) * positif_ou_nul(LIM_BAR3 - REVINDIV) * arr((REVINDIV * TX_BAR2/100) - PLABAR2 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
                + positif(REVINDIV - LIM_BAR3) * positif_ou_nul(LIM_BAR4 - REVINDIV) * arr((REVINDIV * TX_BAR3/100) - PLABAR3 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
	        + positif(REVINDIV - LIM_BAR4) * positif_ou_nul(LIM_BAR5 - REVINDIV) * arr((REVINDIV * TX_BAR4/100) - PLABAR4 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))
	        + positif(REVINDIV - LIM_BAR5) * arr((REVINDIV * TX_BAR5/100) - PLABAR5 - (2 * PLAF_DEMIPART * (NBPTPAS - 1)))))) * 2
		;

TXMARJPAS = max(positif(REVINDIV/(NBPTPAS * null(1 - RASMARJ) + null(2 - RASMARJ)) -  LIM_BAR1) * TX_BAR1 ,
                max(positif(REVINDIV/(NBPTPAS * null(1 - RASMARJ) + null(2 - RASMARJ)) - LIM_BAR2) * TX_BAR2 ,
                    max(positif(REVINDIV/(NBPTPAS * null(1 - RASMARJ) + null(2 - RASMARJ)) - LIM_BAR3) * TX_BAR3 ,
                        max(positif(REVINDIV/(NBPTPAS * null(1 - RASMARJ) + null(2 - RASMARJ)) - LIM_BAR4) * TX_BAR4 ,
                            max(positif(REVINDIV/(NBPTPAS * null(1 - RASMARJ) + null(2 - RASMARJ)) - LIM_BAR5) * TX_BAR5 , 0))))) * positif(IRINDPAS + RASIPQ1001) ;

MARGPAS = arr(REVMARG * TXMARJPAS/100) ;

IRINDIV = max( 0 , IRINDPAS + RASIPQ1001V * null(1 - TXPASMIN) + RASIPQ1001C * null(2 - TXPASMIN) + MARGPAS - max(0 , CHARGES - REVORDI)) ;

PASDENI1 = (TSHALLOV + COD1GA + ALLOV + CARTSV + REMPLAV + CODDAJ + CODEAJ + PRBRV + PENINV + CARPEV + CODRAZ + PEBFV
            + PASTSN1AG + PASPRNAOM + BAPASV + BICPASV + BNCPASV) * null(1 - TXPASMIN)
           + (TSHALLOC + COD1HA + ALLOC + CARTSC + REMPLAC + CODDBJ + CODEBJ + PRBRC + PENINC + CARPEC + CODRBZ + PEBFC
              + PASTSN1BG + PASPRNBOM + BAPASC + BICPASC + BNCPASC) * null(2 - TXPASMIN)
	   + arr(((PASRF + 2REVF + 3REVF + PASRFASS) * (1 - positif(COD4BN)) + RASRVTO + T2RV)/2) ;

REVORDI1 = (max(0 , TSPRV) + max(0 , MICROBNCV) + max(0 , BNRV) + max(0 , RNPFV) + max(0 , BAHQV) + max(0 , BICPAS1)) * null(1 - TXPASMIN)
          + (max(0 , TSPRC) + max(0 , MICROBNCC) + max(0 , BNRC) + max(0 , RNPFC) + max(0 , BAHQC) + max(0 , BICPAS2)) * null(2 - TXPASMIN)
          + arr(COD1TZ/2) + arr(RVTOT/2) + arr(RRCM/2) + arr(PVBAR3VG/2) + arr(max(0 , RRFI)/2) + arr(DESV/2) ;

ABADOPAS = min(arr(IRINDIV * (TX_RABDOM / 100) * (V_EAD + 0)) , PLAF_RABDOM) ;

ABAGUPAS = min(arr(IRINDIV * (TX_RABGUY / 100) * (V_EAG + 0)) , PLAF_RABGUY) ;

DECPAS = min(max(arr(SEUIL_DECOTE1 - ((IRINDIV - (ABADOPAS + ABAGUPAS)) * 3/4)) , 0) , (IRINDIV - (ABADOPAS + ABAGUPAS))) ;

INDPASMIN = positif(((REVINDIV + REVQUOT) * TX_MIN_MET / 100) - IRINDIV + 0) * null(2 - V_REGCO) ;

PASNUMI1 = max(0 , arr(((max(0 , (IRINDIV - (ABADOPAS + ABAGUPAS) - DECPAS - arr(RMENAGE/2)) 
                                * ((1 - V_CNR) * positif_ou_nul(IRINDIV - (ABADOPAS + ABAGUPAS) - DECPAS  - arr(RMENAGE/2))
                                    + V_CNR * positif(IRINDIV - (ABADOPAS + ABAGUPAS) - DECPAS  - arr(RMENAGE/2)))) * (1 - INDPASMIN)
                         + (REVINDIV + REVQUOT) * TX_MIN_MET / 100 * INDPASMIN) 
                        * (REVDEC1 * null(1 - TXPASMIN) + REVDEC2 * null(2 - TXPASMIN) + arr(((PASRF + 2REVF + 3REVF + PASRFASS) * (1 - positif(COD4BN)) + RASRVTO + T2RV)/2)) / (REVORDI1 + REVQUOT + REVMARG)) 
                        - ((COD8VM * (1 - positif(COD8PA + 0)) + min(COD8VM , COD8VM * COD8PA / max(1 , COD8VM + COD8WM + COD8UM)) * positif(COD8PA + 0)) * null(1 - TXPASMIN) 
			   + (COD8WM * (1 - positif(COD8PA + 0)) + min(COD8WM , COD8WM * COD8PA / max(1 , COD8VM + COD8WM + COD8UM)) * positif(COD8PA + 0)) * null(2 - TXPASMIN)))) ;

TXINDIV1 = arr(((PASNUMI1 / PASDENI1) * 100 * (1 - null(PASDENI1))) * 10) / 10 ;

PASNUMI2 = max(0 , PASNUMF - arr(((TSHALLOV + COD1GA + ALLOV + CARTSV + REMPLAV + CODDAJ + CODEAJ + PRBRV + PENINV + CARPEV + CODRAZ + PEBFV + RASTSPE1 + RASBA1 + RASBIC1 + RASBNC1) * null(1 - TXPASMIN)
                                  + (TSHALLOC + COD1HA + ALLOC + CARTSC + REMPLAC + CODDBJ + CODEBJ + PRBRC + PENINC + CARPEC + CODRBZ + PEBFC + RASTSPE2 + RASBA2 + RASBIC2 + RASBNC2) * null(2 - TXPASMIN)) * TXINDIV1/100) 
                           - arr((RASRF + RASRVTO) * RASTXFOYER/100)) ;

PASDENI2 = (TSHALLOV + COD1GA + ALLOV + CARTSV + REMPLAV + CODDAJ + CODEAJ + PRBRV + PENINV + CARPEV + CODRAZ + PEBFV
            + PASTSN1AG + PASPRNAOM + BAPASV + BICPASV + BNCPASV) * null(2 - TXPASMIN)
           + (TSHALLOC + COD1HA + ALLOC + CARTSC + REMPLAC + CODDBJ + CODEBJ + PRBRC + PENINC + CARPEC + CODRBZ + PEBFC
              + PASTSN1BG + PASPRNBOM + BAPASC + BICPASC + BNCPASC) * null(1 - TXPASMIN) ;

TXINDIV2 = arr(((PASNUMI2 / PASDENI2) * 100 * (1 - null(PASDENI2))) * 10) / 10 ; 

RASTXDEC1 = (TXINDIV1 * null(1 - TXPASMIN) + TXINDIV2 * null(2 - TXPASMIN) + RASTXFOYER * null(4 - TXPASMIN))
             * positif(RASTXFOYER) * BOOL_0AM * (1 - null(2 - INDPAS) - null(3 - INDPAS)) * (1 - positif(RMOND + DMOND)) 
	    + RASTXFOYER * BOOL_0AM * positif(RMOND + DMOND) ;

RASTXDEC2 = (TXINDIV1 * null(2 - TXPASMIN) + TXINDIV2 * null(1 - TXPASMIN) + RASTXFOYER * null(4 - TXPASMIN))
             * positif(RASTXFOYER) * BOOL_0AM * (1 - null(2 - INDPAS) - null(4 - INDPAS)) * (1 - positif(RMOND + DMOND)) 
	    + RASTXFOYER * BOOL_0AM * positif(RMOND + DMOND) ;

regle taux 201740:
application : batch , iliad ;


RASRVTOA = arr(RASRVTO * RASTXFOYER / 100 / 12) ;

RASRFA = arr(RASRF * RASTXFOYER / 100 / 12) ;


RASPSRF = (PASRF + PASRFASS) * (1 - positif(COD4BN)) * (1 - null(2 - INDPAS)) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASPSRF1 = arr(RASPSRF / (2 - null(BOOL_0AM))) * (1 - null(2 - INDPAS)) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASPSRF2 = (RASPSRF - RASPSRF1) * BOOL_0AM * (1 - null(2 - INDPAS)) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ; 

RASPSRFA = arr(RASPSRF * 15.5 / 100 / 12) * (1 - null(2 - INDPAS)) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASPSRVTO = (RV1 + RV2 + RV3 + RV4) * (1 - null(2 - INDPAS)) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASPSRVTO1 = arr(RASPSRVTO / (2 - null(BOOL_0AM))) * (1 - null(2 - INDPAS)) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASPSRVTO2 = (RASPSRVTO - RASPSRVTO1) * BOOL_0AM * (1 - null(2 - INDPAS)) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASPSRVTOA = arr(RASPSRVTO * 15.5 / 100 / 12) * (1 - null(2 - INDPAS)) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASPSBIC1 = LOCNPASSPSV * (1 - positif(COD5CF)) * (1 - null(2 - INDPAS)) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASPSBIC2 = LOCNPASSPSC * (1 - positif(COD5CI)) * BOOL_0AM * (1 - null(2 - INDPAS)) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASPS5HY = RCSV * (1 - positif(COD5AF + COD5BF + COD5AN + COD5CF + COD5AO + COD5AP)) * (1 - null(2 - INDPAS)) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

RASPS5IY = RCSC * (1 - positif(COD5AI + COD5BI + COD5BN + COD5CI + COD5BO + COD5BP)) * (1 - null(2 - INDPAS)) * (1 - null(V_CNR2) * positif(DATDEPETR + 0)) ;

regle taux 201760:
application : batch , iliad ;

RASTSPRFM1 = arr(RASTSPR1 * RASTXFOYER / 100 / 12) ;

RASTSPRFM2 = arr(RASTSPR2 * RASTXFOYER / 100 / 12) ;

RASTSPRFT1 = arr(RASTSPR1 * RASTXFOYER / 100 / 4) ;

RASTSPRFT2 = arr(RASTSPR2 * RASTXFOYER / 100 / 4) ;

RASTSPRIM1 = arr(RASTSPR1 * RASTXDEC1 / 100 / 12) ;

RASTSPRIM2 = arr(RASTSPR2 * RASTXDEC2 / 100 / 12) ;

RASTSPRIT1 = arr(RASTSPR1 * RASTXDEC1 / 100 / 4) ;

RASTSPRIT2 = arr(RASTSPR2 * RASTXDEC2 / 100 / 4) ;

RASBICFM1 = arr(RASBIC1 * RASTXFOYER / 100 / 12) ;

RASBICFM2 = arr(RASBIC2 * RASTXFOYER / 100 / 12) ;

RASBICFT1 = arr(RASBIC1 * RASTXFOYER / 100 / 4) ;

RASBICFT2 = arr(RASBIC2 * RASTXFOYER / 100 / 4) ;

RASBICIM1 = arr(RASBIC1 * RASTXDEC1 / 100 / 12) ;

RASBICIM2 = arr(RASBIC2 * RASTXDEC2 / 100 / 12) ;

RASBICIT1 = arr(RASBIC1 * RASTXDEC1 / 100 / 4) ;

RASBICIT2 = arr(RASBIC2 * RASTXDEC2 / 100 / 4) ;

RASBNCFM1 = arr(RASBNC1 * RASTXFOYER / 100 / 12) ;

RASBNCFM2 = arr(RASBNC2 * RASTXFOYER / 100 / 12) ;

RASBNCFT1 = arr(RASBNC1 * RASTXFOYER / 100 / 4) ;

RASBNCFT2 = arr(RASBNC2 * RASTXFOYER / 100 / 4) ;

RASBNCIM1 = arr(RASBNC1 * RASTXDEC1 / 100 / 12) ;

RASBNCIM2 = arr(RASBNC2 * RASTXDEC2 / 100 / 12) ;

RASBNCIT1 = arr(RASBNC1 * RASTXDEC1 / 100 / 4) ;

RASBNCIT2 = arr(RASBNC2 * RASTXDEC2 / 100 / 4) ;

RASBAFM1 = arr(RASBA1 * RASTXFOYER / 100 / 12) ;

RASBAFM2 = arr(RASBA2 * RASTXFOYER / 100 / 12) ;

RASBAFT1 = arr(RASBA1 * RASTXFOYER / 100 / 4) ;

RASBAFT2 = arr(RASBA2 * RASTXFOYER / 100 / 4) ;

RASBAIM1 = arr(RASBA1 * RASTXDEC1 / 100 / 12) ;

RASBAIM2 = arr(RASBA2 * RASTXDEC2 / 100 / 12) ;

RASBAIT1 = arr(RASBA1 * RASTXDEC1 / 100 / 4) ;

RASBAIT2 = arr(RASBA2 * RASTXDEC2 / 100 / 4) ;

RASRVTOFM1 = arr(arr(RASRVTO * RASTXFOYER / 100 / 12) / (2 - null(BOOL_0AM))) ;

RASRVTOFM2 = (arr(RASRVTO * RASTXFOYER / 100 / 12) - RASRVTOFM1) * BOOL_0AM ;

RASRVTOFT1 = arr(arr(RASRVTO * RASTXFOYER / 100 / 4) / (2 - null(BOOL_0AM))) ;

RASRVTOFT2 = (arr(RASRVTO * RASTXFOYER / 100 / 4) - RASRVTOFT1) * BOOL_0AM ;

RASRFFM1 = arr(arr(RASRF * RASTXFOYER / 100 / 12) / (2 - null(BOOL_0AM))) ;

RASRFFM2 = (arr(RASRF * RASTXFOYER / 100 / 12) - RASRFFM1) * BOOL_0AM ;

RASRFFT1 = arr(arr(RASRF * RASTXFOYER / 100 / 4) / (2 - null(BOOL_0AM))) ;

RASRFFT2 = (arr(RASRF * RASTXFOYER / 100 / 4) - RASRFFT1) * BOOL_0AM ;

RASTSPEFM1 = arr(RASTSPE1 * RASTXFOYER / 100 / 12) ;

RASTSPEFM2 = arr(RASTSPE2 * RASTXFOYER / 100 / 12) * BOOL_0AM ;

RASTSPEFT1 = arr(RASTSPE1 * RASTXFOYER / 100 / 4) ;

RASTSPEFT2 = arr(RASTSPE2 * RASTXFOYER / 100 / 4) * BOOL_0AM ;

RASTSPEIM1 = arr(RASTSPE1 * RASTXDEC1 / 100 / 12) * (1 - INDTAZ) * (1 - null(2 - INDPAS)) ;

RASTSPEIM2 = arr(RASTSPE2 * RASTXDEC2 / 100 / 12) * BOOL_0AM * (1 - INDTAZ) * (1 - null(2 - INDPAS)) ;

RASTSPEIT1 = arr(RASTSPE1 * RASTXDEC1 / 100 / 4) * (1 - INDTAZ) * (1 - null(2 - INDPAS)) ;

RASTSPEIT2 = arr(RASTSPE2 * RASTXDEC2 / 100 / 4) * BOOL_0AM * (1 - INDTAZ) * (1 - null(2 - INDPAS)) ;

regle taux 201780:
application : batch , iliad ;

RASPSRFM1 = arr(RASPSRFA / (2 - null(BOOL_0AM))) * (1 - null(2 - INDPAS)) ;

RASPSRFM2 = (RASPSRFA - RASPSRFM1) * BOOL_0AM * (1 - null(2 - INDPAS)) ;

RASPSRFT1 = arr(arr(RASPSRF * 15.5 / 100 / 4) / (2 - null(BOOL_0AM))) * (1 - null(2 - INDPAS)) ;

RASPSRFT2 = (arr(RASPSRF * 15.5 / 100 / 4) - RASPSRFT1) * BOOL_0AM * (1 - null(2 - INDPAS)) ;

RASPSRVTOM1 = arr(RASPSRVTOA / (2 - null(BOOL_0AM))) * (1 - null(2 - INDPAS)) ;

RASPSRVTOM2 = (RASPSRVTOA - RASPSRVTOM1) * BOOL_0AM * (1 - null(2 - INDPAS)) ;

RASPSRVTOT1 = arr(arr(RASPSRVTO * 15.5 / 100 / 4) / (2 - null(BOOL_0AM))) * (1 - null(2 - INDPAS)) ;

RASPSRVTOT2 = (arr(RASPSRVTO * 15.5 / 100 / 4) - RASPSRVTOT1) * BOOL_0AM * (1 - null(2 - INDPAS)) ;

RASPSBICM1 = arr(RASPSBIC1 * 15.5 / 100 / 12) * (1 - null(2 - INDPAS)) ;

RASPSBICM2 = arr(RASPSBIC2 * 15.5 / 100 / 12) * BOOL_0AM * (1 - null(2 - INDPAS)) ;

RASPSBICT1 = arr(RASPSBIC1 * 15.5 / 100 / 4) * (1 - null(2 - INDPAS)) ;

RASPSBICT2 = arr(RASPSBIC2 * 15.5 / 100 / 4) * BOOL_0AM * (1 - null(2 - INDPAS)) ;

RASPS5HYM = arr(RASPS5HY * 15.5 / 100 / 12) * (1 - null(2 - INDPAS)) ;

RASPS5HYT = arr(RASPS5HY * 15.5 / 100 / 4) * (1 - null(2 - INDPAS)) ;

RASPS5IYM = arr(RASPS5IY * 15.5 / 100 / 12) * BOOL_0AM * (1 - null(2 - INDPAS)) ;

RASPS5IYT = arr(RASPS5IY * 15.5 / 100 / 4) * BOOL_0AM * (1 - null(2 - INDPAS)) ;

regle taux 201800:
application : batch , iliad ;

RASTOTF = RASRFA + RASRVTOA + RASPSRFA + RASPSRVTOA ;

RASTOT1 = RASTSPEFM1 + RASBAFM1 + RASBICFM1 + RASBNCFM1 + RASPSBICM1 + RASPS5HYM ;

RASTOT2 = RASTSPEFM2 + RASBAFM2 + RASBICFM2 + RASBNCFM2 + RASPSBICM2 + RASPS5IYM ;

RASTOT = RASTOTF + RASTOT1 + RASTOT2 ;

INDACPAS = positif_ou_nul(RASBAFM1 + RASBICFM1 + RASBNCFM1 + RASTSPEFM1 + RASBAFM2 + RASBICFM2 + RASBNCFM2 + RASTSPEFM2 + RASRFA + RASRVTOA 
                          + RASPSBICM1 + RASPS5HYM + RASPSBICM2 + RASPS5IYM + RASPSRFA + RASPSRVTOA - 5) ;

regle taux 201820:
application : batch , iliad ;


LIGPASEND = (1 - positif(V_NOPAS)) * positif(14 - V_NOTRAIT) * (1 - BOOL_0AM) * null(V_ZDC - 1) ;

LIGPAS = (1 - positif(V_NOPAS)) * positif(14 - V_NOTRAIT) * (1 - LIGPASEND) * (1 - positif(V_0AZ * BOOL_0AM)) ;

LIGPASIND = LIGPAS * BOOL_0AM ;

LIGPASIND1 = LIGPASIND * (null(11 - V_ROLEIR) + null(16 - V_ROLEIR)) ;

LIGPASIND2 = LIGPASIND * null(26 - V_ROLEIR) ;

LIGPASIND3 = LIGPASIND * null(36 - V_ROLEIR) ;

LIGPASZ = (1 - positif(V_NOPAS)) * positif(14 - V_NOTRAIT) * positif(V_0AZ * BOOL_0AM) ;

LIGPASPART = 1 - null(INDPAS - 1) ;

LIGRASTSPE = positif(RASTSPE1 + RASTSPE2) ;

LIGRASRVTO = positif(RASRVTO) ;

LIGRASRVTOS = positif(RASPSRVTO) ;

LIGRASRF = positif(RASRF) ;

LIGRASRFS = positif(RASPSRF) ;

LIGRASBA = positif(RASBA1 + RASBA2) ;

LIGRASBIC = positif(RASBIC1 + RASBIC2) ;

LIGRASBNC = positif(RASBNC1 + RASBNC2) ;

LIGPSBIC = positif(RASPSBIC1 + RASPSBIC2) ;

LIG5HY = positif(RASPS5HY) ;

LIG5IY = positif(RASPS5IY) ;

LIGIRRAS = positif(LIGRASTSPE + LIGRASRVTO + LIGRASRF + LIGRASBA + LIGRASBIC + LIGRASBNC) * (1 - null(RASTXFOYER)) * (1 - LIGPASEND) * (1 - positif(V_0AZ * BOOL_0AM)) ;

LIGPSRAS = positif(LIGRASRFS + LIGRASRVTOS + LIGPSBIC + LIG5HY + LIG5IY) * (1 - LIGPASEND) * (1 - positif(V_0AZ * BOOL_0AM)) ; 

LIGRASTOTF = positif(LIGRASRVTO + LIGRASRF + LIGRASRFS + LIGRASRVTOS) ;

LIGRASTOT1 = positif(RASTSPE1 + RASBA1 + RASBIC1 + RASBNC1 + RASPSBIC1 + RASPS5HY) ;

LIGRASTOT2 = positif(RASTSPE2 + RASBA2 + RASBIC2 + RASBNC2 + RASPSBIC2 + RASPS5IY) ;

LIGRAS = positif(LIGIRRAS + LIGPSRAS) * LIGPAS * (1 - positif(V_NOPAS)) ;

LIGRASSUP = positif_ou_nul(RASTOT - 5) * LIGRAS ;

LIGRASINF = positif(5 - RASTOT) * LIGRAS ;

LIG8VAB = positif(COD8VA + COD8VB) * LIGPAS * (1 - positif(V_NOPAS)) ;

INDNCTAZ = INDPAS + (5 * INDTAZ) ;


LIGRASIND = BOOL_0AM * (1 - positif(RMOND + DMOND)) ;

LIGPAS1 = (1 - positif(V_NOPAS)) * positif(V_NOTRAIT - 10) ;

LIGRAS1 = positif(LIGIRRAS + LIGPSRAS) * LIGPAS1 * (1 - positif(V_NOPAS)) * (1 - null(RASTXFOYER)) ;

LIGPASZ1 = LIGPAS1 * positif(V_0AZ) ;

LIGPAS2 = (1 - positif(V_NOPAS)) * positif(null(V_NOTRAIT - 14) + null(V_NOTRAIT - 16)) ;

LIGPAS3 = (1 - positif(V_NOPAS)) * positif(null(V_NOTRAIT - 23) + null(V_NOTRAIT - 33) + null(V_NOTRAIT - 43) + null(V_NOTRAIT - 53) + null(V_NOTRAIT - 63)) ;

LIGPAS6 = (1 - positif(V_NOPAS)) * positif(null(V_NOTRAIT - 26) + null(V_NOTRAIT - 36) + null(V_NOTRAIT - 46) + null(V_NOTRAIT - 56) + null(V_NOTRAIT - 66)) ;

regle taux 201840:
application : batch , iliad ;

RASCISP = max(0 , arr(AAIDE * TX50/100) + CIADCRE + CIGARD - (IDRS4 * INDTAZ)) ;

INDSAP = positif(RASCISP + 0) ;

