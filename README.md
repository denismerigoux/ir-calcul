# README du projet

## Code source des impôts sur les revenus

### Contenu
Les fichiers contenus dans l'archive sont l'ensemble des fichiers de paramétrage utilisés par les services informatiques de la Direction Générale des Finances Publiques pour réaliser la taxation des foyers fiscaux (IR, ISF, CSG).

Ces fichiers sont développés sous licence CeCILL 2.1 soumise au droit français et respectant les principes de diffusion des logiciels libres.
Ces fichiers sont publiés avec le statut "publié" du guide Etalab d'ouverture des codes sources publics (cf https://github.com/etalab/ouverture-des-codes-sources-publics).
Les contributions extérieures ne seront donc pas traitées.

Ces versions sont respectivement :

    * la 6.7  pour l'imposition des revenus 2018 (4e émission et batch de réactivité fiscale)	
    * la 6.3  pour l'imposition des revenus 2018 (3 premières émissions de l'impôt sur le revenu)
    * la 6.10 pour l'imposition des revenus 2017
    * la 4.5  pour l'imposition des revenus 2016
    * la 4.6  pour l'imposition des revenus 2015
    * la 2.15 pour l'imposition des revenus 2014
    * la 3.8  pour l'imposition des revenus 2013
    * la 3.13 pour l'imposition des revenus 2012
    * la 9.6  pour l'imposition des revenus 2011
    * la 7.1  pour l'imposition des revenus 2010

Les fichiers contenus dans le répertoire src sont notamment :

    tgvH.m : Tableau général des variables qui assure la correspondance entre 
    *  les codes issus de la 2042 et les variables internes au calcul
    * les variables de calcul 
    * les variables restituées par la calculette IR
    errH.m : Fichier décrivant les différentes anomalies
    coiX.m, cocX.m, horizoc.m, horizoi.m : Fichiers de gestion des anomalies de la calculette
    chap-X.m, res-ser1.m, res-ser2.m : Fichiers comportant les différentes règles de calcul pour un ensemble fonctionnel cohérent.

### Quelques éléments de contexte

Le code de la calculette est écrit dans un langage dédié au sein de la DGFiP, le langage M. Ce langage correspond aux besoins spécifiques de la DGFiP en matière de calcul des impôts mais il ne dispose pas de l’écosystème qui entoure les langages généralistes couramment utilisés (compilateur, interpréteur, éditeur, débuggeur…). Pour permettre de faciliter l’utilisation du code source, l’équipe Etalab a développé un parseur (analyseur syntaxique) capable de transformer le code original en données JSON, utilisables par n’importe quel langage de programmation (voir https://github.com/etalab/calculette-impots-m-language-parser).

### Noms d'application
Cette calculette intervient dans le système d'information en interaction avec d'autres applications, parfois mentionnées dans les programmes, dont les traitements batch de taxation ("batch") et Iliad, l'application d'assiette des services des impôts des particuliers.

### Variables
Les variables sont de plusieurs types :

        Les variables saisies correspondent aux cases de la déclaration des revenus (par exemple 1AJ).
        Les variables calculées ont une formule qui renvoie la valeur de la variable.
        Les variables calculées de base sont des variables qui peuvent être affectées avant le début du calcul.

Le fichier tgvH.m définit des variables via plusieurs champs qui varient selon le type.

Exemples :

    10MINS1 : calculee : "deductions hors droits d'auteur plafonnees" ;
    10MINS1TOUT : calculee base : "10 pourcent TS dernier evt pour calcul de DEFRI" ;
    4BACREP : saisie revenu classe = 2 priorite = 10 categorie_TL = 20 cotsoc = 5 ind_abat = 0 acompte = 1 
    avfisc = 0 rapcat = 8 sanction = 2 nat_code = 0 alias CJC : "BA exceptionnels sous CGA - Quotient 4 - PAC" ;

### Formules

Quelques exemples de fichiers contenant des formules :

        chap-1.m contient les règles de calcul du montant net à payer
        chap-2.m contient les règles de calcul du montant net à payer
        chap-51.m contient les règles de calcul des droits simples résultant du taux progressif
        chap-55.m contient les règles de calcul des droits simples résultant du taux progressif
        chap-6.m contient les règles de calcul du nombre de parts
        chap-isf.m contient les règles de calcul de l'ISF
        chap-perp.m contient les règles de calcul des déductions pour verserment sur un Plan d'Epargne Retraite Populaire 
        inr intérêts de retard (correctif) cinr cumul (correctif) majo majorations (correctif) cmajo cumul majorations (correctif)
